;; Per-directory local variables for GNU Emacs 23 and later.

((nil
  .
  ((compile-command . "ninja -k0 test")
   (eglot-ignored-server-capabilities . (:documentOnTypeFormattingProvider))))
 (auto-mode-alist
  .
  (("\\.ninja.spec\\'" . makefile-mode)))
 (c-mode
  .
  ((eval . (font-lock-add-keywords 'c-mode '(("\\<\\(ast_children_foreach\\|ast_children_foreach_slice\\|ast_child_list_foreach\\|ast_words_foreach\\|ast_words_foreach_slice\\|ast_word_list_foreach\\|ast_word_children_foreach\\|ast_word_children_foreach_slice\\|ast_lookup_option_groups_foreach\\|ast_lookup_option_descriptions_foreach\\|ast_lookup_options_foreach\\|ast_lookup_uses_foreach\\|ast_lookup_flavors_foreach\\|ast_lookup_licenses_foreach\\|ast_lookup_subpackages_foreach\\|ast_lookup_shebang_langs_foreach\\|ast_lookup_post_plist_targets_foreach\\|ast_lookup_cabal_executables_foreach\\)\\>" . font-lock-keyword-face))))
   (eval . (font-lock-add-keywords 'c-mode '(("\\<\\(libias_panic\\|\\|libias_panic_if\\|libias_panic_unless\\|libias_array_foreach\\|libias_array_foreach_slice\\|libias_framemap_foreach\\|libias_framemap_foreach_slice\\|libias_list_foreach\\|libias_list_foreach_slice\\|libias_map_foreach\\|libias_map_foreach_slice\\|libias_queue_foreach\\|libias_queue_foreach_slice\\|libias_scope_mempool\\|libias_set_foreach\\|libias_set_foreach_slice\\|libias_stack_foreach\\|libias_stack_foreach_slice\\|libias_file_line_foreach\\|libias_file_line_foreach_slice\\|libias_directory_foreach\\|libias_directory_foreach_slice\\|libias_scope_array\\|libias_scope_map\\|libias_scope_set\\|libias_scope_stack\\|libias_scope_framemap\\|libias_scope_queue\\|libias_scope_list\\|libias_declare_variable_with_cleanup\\|libias_scope_buffer\\|libias_scope_on_exit\\|libias_scope_free\\|libias_cleanup\\|libias_free\\|libias_alloc\\|libias_alloc_buffer\\|libias_realloc_array\\|libias_auto\\)\\>" . font-lock-keyword-face))))
   (c-file-style . "BSD")
   (c-basic-offset . 4)
	;; New comments should use // and they should be auto-filled after 72 columns
	;; Clangd will show them as documentation for symbols etc.

   (fill-column . 72)
   (comment-auto-fill-only-comments . 1)
   (eval . (auto-fill-mode))
   (comment-start . "//")
   (comment-end . "")))
 (scheme-mode
  .
  ((indent-tabs-mode . nil)
   (eval . (put 'modify-inputs 'scheme-indent-function 1))
   (eval . (put 'modify-phases 'scheme-indent-function 1))
   (eval . (put 'replace 'scheme-indent-function 1))
   (eval . (put 'add-before 'scheme-indent-function 2))
   (eval . (put 'add-after 'scheme-indent-function 2))
   (eval . (put 'package 'scheme-indent-function 0))
   (eval . (put 'package/inherit 'scheme-indent-function 1))
   (eval . (put 'origin 'scheme-indent-function 0))
   (eval . (put 'build-system 'scheme-indent-function 0))
   (eval . (put 'bag 'scheme-indent-function 0))
   (eval . (put 'gexp->derivation 'scheme-indent-function 1))
   (eval . (put 'graft 'scheme-indent-function 0))

   ;; This notably allows '(' in Paredit to not insert a space when the
   ;; preceding symbol is one of these.
   (eval . (modify-syntax-entry ?~ "'"))
   (eval . (modify-syntax-entry ?$ "'"))
   (eval . (modify-syntax-entry ?+ "'")))))
