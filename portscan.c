// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/diff.h>
#include <libias/io.h>
#include <libias/iterator.h>
#include <libias/hashmap.h>
#include <libias/hashset.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>
#include <libias/trait/hash.h>
#include <libias/workqueue.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/loader.h"
#include "ast/query.h"
#include "ast/word.h"
#include "capsicum_helpers.h"
#include "diagnostic.h"
#include "lint/bsd_port.h"
#include "lint/clones.h"
#include "lint/commented_portrevision.h"
#include "lint/unknown_targets.h"
#include "lint/unknown_variables.h"
#include "main.h"
#include "portscan/log.h"
#include "portscan/status.h"
#include "regexp.h"
#include "rules.h"

enum ScanFlags {
	SCAN_NOTHING                     = 0,
	SCAN_CATEGORIES                  = 1 << 0,
	SCAN_CLONES                      = 1 << 1,
	SCAN_OPTION_DEFAULT_DESCRIPTIONS = 1 << 2,
	SCAN_OPTIONS                     = 1 << 3,
	SCAN_UNKNOWN_TARGETS             = 1 << 4,
	SCAN_UNKNOWN_VARIABLES           = 1 << 5,
	SCAN_VARIABLE_VALUES             = 1 << 6,
	SCAN_PARTIAL                     = 1 << 7,
	SCAN_COMMENTS                    = 1 << 8,
	SCAN_STRICT_VARIABLES            = 1 << 9,
};

struct CategoryReaderState {
	// Input
	const char *category;
	struct LibiasDirectory *portsdir;
	uint32_t flags; // see enum ScanFlags
	struct Rules *rules;

	// Output
	struct LibiasMempool *pool;
	struct LibiasArray *error_origins;
	struct LibiasArray *error_msgs;
	struct LibiasArray *nonexistent;
	struct LibiasArray *origins;
	struct LibiasArray *unhooked;
	struct LibiasArray *unsorted;
};

struct PortReaderState {
	// Input
	struct LibiasDirectory *portsdir;
	const char *origin;
	struct Regexp *keyquery;
	struct Regexp *query;
	ssize_t editdist;
	uint32_t flags; // see enum ScanFlags
	struct LibiasHashmap *default_option_descriptions;
	struct Rules *rules;

	// Output
	struct LibiasMempool *pool;
	const char *path;
	struct LibiasHashset *comments;
	struct LibiasHashset *errors;
	struct LibiasHashset *unknown_variables;
	struct LibiasHashset *unknown_targets;
	struct LibiasHashset *clones;
	struct LibiasHashset *option_default_descriptions;
	struct LibiasHashset *option_groups;
	struct LibiasHashset *options;
	struct LibiasHashset *variable_values;
};

struct GetDefaultOptionDescriptionsState {
	struct LibiasMempool *pool;
	struct LibiasHashmap *descriptions;
};

struct ScanVariableValuesData {
	struct LibiasMempool *pool;
	struct LibiasHashset *variable_values;
	struct Regexp *name_query;
	struct Regexp *query;
};

// Prototypes
static void add_error(struct LibiasHashset *errors, struct LibiasMempool *pool, const char *msg);
static int char_cmp(const void *ap, const void *bp, void *userdata);
static void collect_variable_values(struct ASTVisit *visit, struct AST *node);
static ssize_t edit_distance(const char *a, const char *b);
static struct LibiasHashmap *get_default_option_descriptions(struct LibiasMempool *extpool, struct AST *root);
static void get_default_option_descriptions_visit_variable(struct ASTVisit *visit, struct AST *node);
static struct LibiasArray *lookup_origins(struct LibiasMempool *extpool, struct LibiasWorkqueue *workqueue, struct LibiasDirectory *portsdir, uint32_t flags, struct Rules *rules, struct PortscanLog *log);
static void lookup_origins_worker(int tid, void *userdata);
static void lookup_subdirs(struct LibiasDirectory *portsdir, const char *category, const char *path, uint32_t flags, struct Rules *rules, struct LibiasMempool *extpool, struct LibiasArray *subdirs, struct LibiasArray *nonexistent, struct LibiasArray *unhooked, struct LibiasArray *unsorted, struct LibiasArray *error_origins, struct LibiasArray *error_msgs);
static void scan_port_worker(int tid, void *userdata);
static void scan_ports(struct LibiasWorkqueue *workqueue, struct LibiasDirectory *portsdir, struct LibiasArray *origins, uint32_t flags, struct Rules *rules, struct Regexp *keyquery, struct Regexp *query, ssize_t editdist, struct PortscanLog *retval);

enum PortfmtExitStatus do_scan(struct Portfmt *this);

void
add_error(
	struct LibiasHashset *errors,
	struct LibiasMempool *pool,
	const char *msg)
{
	if (!libias_hashset_contains_p(errors, msg)) {
		libias_hashset_add(errors, libias_mempool_take(pool, libias_str_dup(msg)));
	}
}

void
lookup_subdirs(
	struct LibiasDirectory *portsdir,
	const char *category,
	const char *path,
	uint32_t flags,
	struct Rules *rules,
	struct LibiasMempool *extpool,
	struct LibiasArray *subdirs,
	struct LibiasArray *nonexistent,
	struct LibiasArray *unhooked,
	struct LibiasArray *unsorted,
	struct LibiasArray *error_origins,
	struct LibiasArray *error_msgs)
{
	libias_scope_mempool(pool);

	auto category_makefile = libias_file_openat(portsdir, path, O_RDONLY, 0);
	libias_scope_free(category_makefile);
	if (!category_makefile) {
		char *origin = libias_str_dup(path);
		libias_mempool_take(extpool, origin);
		libias_array_append(error_origins, origin);
		char *error = libias_str_printf("file_openat: %s", strerror(errno));
		libias_mempool_take(extpool, error);
		libias_array_append(error_msgs, error);
		return;
	}
#if HAVE_CAPSICUM
	else if (caph_limit_stream(libias_file_fd(in), CAPH_READ) < 0) {
		libias_panic("caph_limit_stream: %s: %s", path, strerror(errno));
	}
#endif

	auto diagnostic = diagnostic_new();
	libias_scope_free(diagnostic);
	auto ast = ast_from_file(category_makefile, diagnostic, path);
	libias_scope_free(ast);
	if (!ast) {
		char *origin = libias_str_dup(path);
		libias_mempool_take(extpool, origin);
		libias_array_append(error_origins, origin);
		libias_array_append(error_msgs, "read or parse error");
		return;
	}

	auto nodes = ast_lookup_variables(
		ast,
		"SUBDIR",
		AST_LOOKUP_VARIABLE_DEFAULT);
	if (!nodes) {
		return;
	}
	libias_scope_free(nodes);
	libias_scope_array(tmp);
	libias_array_foreach(nodes, struct AST *, node) {
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			libias_array_append(tmp, libias_mempool_take(pool, ast_word_flatten(word)));
		}
	}

	if (unhooked && (flags & SCAN_CATEGORIES)) {
		struct LibiasDirectory *dir = libias_directory_openat(portsdir, category);
		libias_scope_free(dir);
		if (dir == NULL) {
			char *origin = libias_str_dup(category);
			libias_mempool_take(extpool, origin);
			libias_array_append(error_origins, origin);
			char *msg = libias_str_printf("directory_openat: %s", strerror(errno));
			libias_mempool_take(extpool, msg);
			libias_array_append(error_msgs, msg);
		} else {
#if HAVE_CAPSICUM
			if (caph_limit_stream(libias_directory_fd(dir), CAPH_READ | CAPH_READDIR) < 0) {
				libias_panic("caph_limit_stream: %s: %s", path, strerror(errno));
			}
#endif
			libias_directory_foreach(dir, dp) {
				if (dp->d_name[0] == '.') {
					continue;
				}
				char *path = libias_str_printf("%s/%s", category, dp->d_name);
				libias_mempool_take(pool, path);
				struct LibiasFile *f = libias_file_openat(portsdir, path, O_DIRECTORY, 0);
				if (!f) {
					continue;
				}
				libias_cleanup(&f);
				if (libias_array_find(tmp, dp->d_name, libias_str_compare) == -1) {
					char *origin = libias_str_dup(path);
					libias_mempool_take(extpool, origin);
					libias_array_append(unhooked, origin);
				}
			}
		}
	}

	libias_array_foreach(tmp, char *, port) {
		char *origin;
		if (flags != SCAN_NOTHING) {
			origin = libias_str_printf("%s/%s", category, port);
			libias_mempool_take(pool, origin);
		} else {
			origin = port;
		}
		if (flags & SCAN_CATEGORIES) {
			if (nonexistent) {
				struct LibiasFile *f = libias_file_openat(portsdir, origin, O_DIRECTORY, 0);
				if (f) {
					libias_cleanup(&f);
				} else {
					char *origin_copy = libias_str_dup(origin);
					libias_mempool_take(extpool, origin_copy);
					libias_array_append(nonexistent, origin_copy);
				}
			}
		}
		char *origin_copy = libias_str_dup(origin);
		libias_mempool_take(extpool, origin_copy);
		libias_array_append(subdirs, origin_copy);
	}

	if (unsorted && (flags & SCAN_CATEGORIES)) {
		libias_scope_buffer(original_buf);
		ast_render(ast, original_buf);

		ast_format(ast, rules, NULL, NULL);
		libias_scope_buffer(formatted_buf);
		ast_render(ast, formatted_buf);

		bool category_makefile_changed = 0 != strcmp(
			libias_buffer_data(original_buf),
			libias_buffer_data(formatted_buf));
		if (category_makefile_changed) {
			char *category_copy = libias_str_dup(category);
			libias_mempool_take(extpool, category_copy);
			libias_array_append(unsorted, category_copy);
		}
	}
}

int
char_cmp(const void *ap, const void *bp, void *userdata)
{
	char a = *(char *)ap;
	char b = *(char *)bp;
	if (a < b) {
		return -1;
	} else if (a > b) {
		return 1;
	} else {
		return 0;
	}
}

ssize_t
edit_distance(const char *a, const char *b)
{
	if (!a || !b) {
		return -1;
	}

	ssize_t editdist = -1;
	struct LibiasCompareTrait cmp = { char_cmp, NULL };
	struct LibiasDiff *d = libias_diff(
		&cmp,
		sizeof(char),
		a, strlen(a),
		b, strlen(b));
	if (d) {
		editdist = d->editdist;
		libias_cleanup(&d);
	}

	return editdist;
}

void
collect_variable_values(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ScanVariableValuesData *this = ast_visit_context(visit);

	char *varname = ast_word_flatten(ast_variable_name(node));
	libias_scope_free(varname);
	if (!this->name_query || regexp_exec(this->name_query, varname) == 0) {
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			char *value = ast_word_flatten(word);
			libias_scope_free(value);
			if (!this->query || regexp_exec(this->query, value) == 0) {
				char *buf = libias_str_printf("%-30s\t%s", varname, value);
				if (libias_hashset_contains_p(this->variable_values, buf)) {
					libias_free(buf);
				} else {
					libias_mempool_take(this->pool, buf);
					libias_hashset_add(this->variable_values, buf);
				}
			}
		}
	}
}

void
scan_port_worker(int tid, void *userdata)
{
	struct PortReaderState *this = userdata;
	this->pool = libias_mempool_new();
	{
		char *origin = libias_str_dup(this->origin);
		libias_mempool_take(this->pool, origin);
		this->origin = origin;
	}
	portscan_status_print(this->origin);
	{
		char *path = libias_str_printf("%s/Makefile", this->origin);
		libias_mempool_take(this->pool, path);
		this->path = path;
	}

	this->clones = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->comments = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->errors = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->option_default_descriptions = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->option_groups = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->options = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->unknown_variables = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->unknown_targets = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);
	this->variable_values = libias_mempool_hashset(this->pool, libias_str_compare, libias_hash_str);

	auto diagnostic = diagnostic_new();
	libias_scope_free(diagnostic);

	struct LibiasFile *in = libias_file_openat(this->portsdir, this->path, O_RDONLY, 0);
	if (in == NULL) {
		char *msg = libias_str_printf("file_openat: %s", strerror(errno));
		add_error(this->errors, this->pool, msg);
		libias_free(msg);
		portscan_status_inc();
		return;
	}
#if HAVE_CAPSICUM
	else if (caph_limit_stream(libias_file_fd(in), CAPH_READ) < 0) {
		libias_panic("caph_limit_stream: %s: %s", this->path, strerror(errno));
	}
#endif
	auto root = ast_from_file(in, diagnostic, this->path);
	libias_cleanup(&in);
	if (!root) {
		auto msg = libias_str_printf("could not read or parse %s", this->path);
		add_error(this->errors, this->pool, msg);
		libias_cleanup(&msg);
		portscan_status_inc();
		return;
	}
	libias_scope_free(root);
	if (!ast_load_includes(root, this->portsdir, this->path, diagnostic)) {
		add_error(this->errors, this->pool, "cannot open includes");
		portscan_status_inc();
		return;
	}

	if (this->flags & SCAN_PARTIAL) {
		if (!lint_bsd_port_p(root)) {
			add_error(this->errors, this->pool, "not a FreeBSD Ports Makefile");
			portscan_status_inc();
			return;
		}
	}

	if (this->flags & SCAN_UNKNOWN_VARIABLES) {
		lint_unknown_variables_foreach(root, this->rules, var) {
			auto name = lint_unknown_variable_name(var);
			if (libias_hashset_contains_p(this->unknown_variables, name)) {
				continue;
			} else if (!this->query || regexp_exec(this->query, name) == 0) {
				auto name_copy = libias_str_dup(name);
				libias_mempool_take(this->pool, name_copy);
				libias_hashset_add(this->unknown_variables, name_copy);
			}
		}
	}

	if (this->flags & SCAN_UNKNOWN_TARGETS) {
		lint_unknown_targets_foreach(root, this->rules, name) {
			if (libias_hashset_contains_p(this->unknown_targets, name)) {
				continue;
			} else if (!this->query || regexp_exec(this->query, name) == 0) {
				auto name_copy = libias_str_dup(name);
				libias_mempool_take(this->pool, name_copy);
				libias_hashset_add(this->unknown_targets, name_copy);
			}
		}
	}

	if (this->flags & SCAN_CLONES) {
		// XXX: Limit by query?
		lint_clones_foreach(root, clone) {
			if (!libias_hashset_contains_p(this->clones, clone)) {
				char *tmp = libias_str_dup(clone);
				libias_mempool_take(this->pool, tmp);
				libias_hashset_add(this->clones, tmp);
			}
		}
	}

	if (this->flags & SCAN_OPTION_DEFAULT_DESCRIPTIONS) {
		ast_lookup_option_descriptions_foreach(root, var, desc) {
			char *default_desc = libias_hashmap_get(this->default_option_descriptions, var);
			if (!default_desc) {
				continue;
			}
			if (!libias_hashset_contains_p(this->option_default_descriptions, var)) {
				ssize_t editdist = edit_distance(default_desc, desc);
				if (strcasecmp(default_desc, desc) == 0 || (editdist > 0 && editdist <= this->editdist)) {
					char *desc = libias_str_dup(var);
					libias_mempool_take(this->pool, desc);
					libias_hashset_add(this->option_default_descriptions, desc);
				}
			}
		}
	}

	if (this->flags & SCAN_OPTIONS) {
		ast_lookup_option_groups_foreach(root, group) {
			if (!libias_hashset_contains_p(this->option_groups, group) &&
			    (this->query == NULL || regexp_exec(this->query, group) == 0)) {
				char *optgroup = libias_str_dup(group);
				libias_mempool_take(this->pool, optgroup);
				libias_hashset_add(this->option_groups, optgroup);
			}
		}
		ast_lookup_options_foreach(root, option) {
			if (!libias_hashset_contains_p(this->options, option) &&
			    (this->query == NULL || regexp_exec(this->query, option) == 0)) {
				char *opt = libias_str_dup(option);
				libias_mempool_take(this->pool, opt);
				libias_hashset_add(this->options, opt);
			}
		}
	}

	if (this->flags & SCAN_VARIABLE_VALUES) {
		struct ASTVisitTrait visit_trait;
		ast_visit_trait_init(&visit_trait);
		visit_trait.visit_variable = collect_variable_values;

		struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
		struct ScanVariableValuesData data = {
			.pool = this->pool,
			.variable_values = this->variable_values,
			.name_query = this->keyquery,
			.query = this->query,
		};
		ast_visit_set_context(visit, &data);
		ast_visit_run(visit);
		libias_cleanup(&visit);
	}

	if (this->flags & SCAN_COMMENTS) {
		lint_commented_portrevision_foreach(root, comment) {
			char *msg = libias_str_printf("commented revision or epoch: %s", comment);
			if (libias_hashset_contains_p(this->comments, msg)) {
				libias_free(msg);
			} else {
				libias_mempool_take(this->pool, msg);
				libias_hashset_add(this->comments, msg);
			}
		}
	}

	portscan_status_inc();
}

void
lookup_origins_worker(int tid, void *userdata)
{
	struct CategoryReaderState *this = userdata;
	struct LibiasMempool *pool = libias_mempool_new();
	this->pool = pool;
	this->error_origins = libias_mempool_array(pool);
	this->error_msgs = libias_mempool_array(pool);
	this->nonexistent = libias_mempool_array(pool);
	this->unhooked = libias_mempool_array(pool);
	this->unsorted = libias_mempool_array(pool);
	this->origins = libias_mempool_array(pool);

	portscan_status_print(this->category);
	char *path = libias_str_printf("%s/Makefile", this->category);
	libias_mempool_take(pool, path);
	lookup_subdirs(
		this->portsdir,
		this->category,
		path,
		this->flags,
		this->rules,
		this->pool,
		this->origins,
		this->nonexistent,
		this->unhooked,
		this->unsorted,
		this->error_origins,
		this->error_msgs);
	portscan_status_inc();
}

struct LibiasArray *
lookup_origins(
	struct LibiasMempool *extpool,
	struct LibiasWorkqueue *workqueue,
	struct LibiasDirectory *portsdir,
	uint32_t flags,
	struct Rules *rules,
	struct PortscanLog *log)
{
	libias_scope_mempool(pool);
	struct LibiasArray *retval = libias_mempool_array(extpool);

	struct LibiasArray *categories = libias_mempool_array(pool);
	struct LibiasArray *error_origins = libias_mempool_array(pool);
	struct LibiasArray *error_msgs = libias_mempool_array(pool);
	lookup_subdirs(
		portsdir,
		"",
		"Makefile",
		SCAN_NOTHING,
		rules,
		pool,
		categories,
		NULL,
		NULL,
		NULL,
		error_origins,
		error_msgs);

	libias_array_foreach(error_origins, char *, origin) {
		char *msg = libias_array_get(error_msgs, origin_index);
		portscan_log_add_entry(log, PORTSCAN_LOG_ENTRY_ERROR, origin, msg);
	}

	portscan_status_reset(PORTSCAN_STATUS_CATEGORIES, libias_array_len(categories));
	struct LibiasArray *results = libias_mempool_array(pool);
	libias_array_foreach(categories, char *, category) {
		struct CategoryReaderState *this = libias_mempool_alloc(
			pool,
			struct CategoryReaderState);
		this->category = category;
		this->portsdir = portsdir;
		this->flags = flags;
		this->rules = rules;
		libias_workqueue_push(workqueue, lookup_origins_worker, this);
		libias_array_append(results, this);
	}
	libias_workqueue_wait(workqueue);
	libias_array_foreach(results, struct CategoryReaderState *, result) {
		libias_array_foreach(result->error_origins, char *, origin) {
			char *msg = libias_array_get(result->error_msgs, origin_index);
			portscan_log_add_entry(log, PORTSCAN_LOG_ENTRY_ERROR, origin, msg);
		}
		libias_array_foreach(result->nonexistent, char *, origin) {
			portscan_log_add_entry(log, PORTSCAN_LOG_ENTRY_CATEGORY_NONEXISTENT_PORT, origin, "entry without existing directory");
		}
		libias_array_foreach(result->unhooked, char *, origin) {
			portscan_log_add_entry(log, PORTSCAN_LOG_ENTRY_CATEGORY_UNHOOKED_PORT, origin, "unhooked port");
		}
		libias_array_foreach(result->unsorted, char *, origin) {
			portscan_log_add_entry(log, PORTSCAN_LOG_ENTRY_CATEGORY_UNSORTED, origin, "unsorted category or other formatting issues");
		}
		libias_array_foreach(result->origins, char *, origin) {
			libias_mempool_move(result->pool, origin, extpool);
			libias_array_append(retval, origin);
		}
		libias_cleanup(&result->pool);
	}

	// Get consistent output even when category Makefiles are
	// not sorted correctly
	libias_array_sort(retval, libias_str_compare);

	return retval;
}

void
get_default_option_descriptions_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct GetDefaultOptionDescriptionsState *this = ast_visit_context(visit);

	char *varname = libias_mempool_take(
		this->pool,
		ast_word_flatten(ast_variable_name(node)));
	if (
		libias_str_suffix_p(varname, "_DESC") &&
		!libias_hashmap_contains_p(this->descriptions, varname)
		)
	{
		char *value = ast_words_no_meta_flatten(node, " ");
		libias_mempool_take(this->pool, value);
		libias_hashmap_add(this->descriptions, varname, value);
	}
}

struct LibiasHashmap *
get_default_option_descriptions(
	struct LibiasMempool *extpool,
	struct AST *root)
{
	struct GetDefaultOptionDescriptionsState this = {
		.pool = extpool,
		.descriptions = libias_mempool_hashmap(extpool, libias_str_compare, libias_hash_str),
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = get_default_option_descriptions_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	return this.descriptions;
}

void
scan_ports(
	struct LibiasWorkqueue *workqueue,
	struct LibiasDirectory *portsdir,
	struct LibiasArray *origins,
	uint32_t flags,
	struct Rules *rules,
	struct Regexp *keyquery,
	struct Regexp *query,
	ssize_t editdist,
	struct PortscanLog *retval)
{
	libias_scope_mempool(pool);

	if (!(flags & (SCAN_CLONES |
		       SCAN_COMMENTS |
		       SCAN_OPTION_DEFAULT_DESCRIPTIONS |
		       SCAN_OPTIONS |
		       SCAN_UNKNOWN_TARGETS |
		       SCAN_UNKNOWN_VARIABLES |
		       SCAN_VARIABLE_VALUES))) {
		return;
	}

	struct LibiasFile *in = libias_file_openat(portsdir, "Mk/bsd.options.desc.mk", O_RDONLY, 0);
	if (in == NULL) {
		char *msg = libias_str_printf("file_openat: %s", strerror(errno));
		portscan_log_add_entry(
			retval,
			PORTSCAN_LOG_ENTRY_ERROR,
			"Mk/bsd.options.desc.mk",
			msg);
		libias_free(msg);
		return;
	}
#if HAVE_CAPSICUM
	else if (caph_limit_stream(libias_file_fd(in), CAPH_READ) < 0) {
		libias_panic("caph_limit_stream: Mk/bsd.options.desc.mk: %s", strerror(errno));
	}
#endif

	auto diagnostic = diagnostic_new();
	libias_scope_free(diagnostic);
	auto root = ast_from_file(in, diagnostic, "Mk/bsd.options.desc.mk");
	libias_cleanup(&in);
	if (!root) {
		portscan_log_add_entry(retval, PORTSCAN_LOG_ENTRY_ERROR, "Mk/bsd.options.desc.mk", "read or parse error");
		return;
	}
	libias_scope_free(root);

	struct LibiasHashmap *default_option_descriptions = get_default_option_descriptions(
		pool,
		root);
	if (libias_hashmap_len(default_option_descriptions) == 0) {
		portscan_log_add_entry(
			retval,
			PORTSCAN_LOG_ENTRY_ERROR,
			"Mk/bsd.options.desc.mk",
			"no default option descriptions found");
	}

	struct LibiasArray *results = libias_mempool_array(pool);
	libias_array_foreach(origins, const char *, origin) {
		struct PortReaderState *this = libias_mempool_alloc(
			pool,
			struct PortReaderState);
		this->portsdir = portsdir;
		this->origin = origin;
		this->keyquery = keyquery;
		this->query = query;
		this->editdist = editdist;
		this->flags = flags;
		this->default_option_descriptions = default_option_descriptions;
		this->rules = rules; // XXX: Is Rules threadsafe?
		libias_workqueue_push(workqueue, scan_port_worker, this);
		libias_array_append(results, this);
	}
	libias_workqueue_wait(workqueue);
	libias_array_foreach(results, struct PortReaderState *, this) {
		portscan_status_print(NULL);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_ERROR, this->origin, this->errors);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_UNKNOWN_VAR, this->origin, this->unknown_variables);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_UNKNOWN_TARGET, this->origin, this->unknown_targets);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_DUPLICATE_VAR, this->origin, this->clones);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_OPTION_DEFAULT_DESCRIPTION, this->origin, this->option_default_descriptions);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_OPTION_GROUP, this->origin, this->option_groups);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_OPTION, this->origin, this->options);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_VARIABLE_VALUE, this->origin, this->variable_values);
		portscan_log_add_entries(retval, PORTSCAN_LOG_ENTRY_COMMENT, this->origin, this->comments);
		libias_cleanup(&this->pool);
	}
}

enum PortfmtExitStatus
do_scan(struct Portfmt *this)
{
	libias_scope_mempool(pool);

	uint32_t flags = SCAN_NOTHING;
	if (this->scan.flags.categories) {
		flags |= SCAN_CATEGORIES;
	}
	if (this->scan.flags.clones) {
		flags |= SCAN_CLONES;
	}
	if (this->scan.flags.comments) {
		flags |= SCAN_COMMENTS;
	}
	if (this->scan.flags.option_default_descriptions) {
		flags |= SCAN_OPTION_DEFAULT_DESCRIPTIONS;
	}
	if (this->scan.flags.options) {
		flags |= SCAN_OPTIONS;
	}
	if (this->scan.flags.unknown_targets) {
		flags |= SCAN_UNKNOWN_TARGETS;
	}
	if (this->scan.flags.unknown_variables) {
		flags |= SCAN_UNKNOWN_VARIABLES;
	}
	if (this->scan.flags.variable_values) {
		flags |= SCAN_VARIABLE_VALUES;
	}

	if (flags == SCAN_NOTHING) {
		flags = SCAN_CATEGORIES | SCAN_CLONES | SCAN_COMMENTS |
			SCAN_OPTION_DEFAULT_DESCRIPTIONS | SCAN_UNKNOWN_TARGETS |
			SCAN_UNKNOWN_VARIABLES;
	}
	if (this->scan.flags.strict_variables) {
		flags |= SCAN_STRICT_VARIABLES;
	}

	if (isatty(STDERR_FILENO)) {
		this->scan.flags.progress++;
	}

#if HAVE_CAPSICUM
	if (caph_limit_stdio() < 0) {
		libias_panic("caph_limit_stdio: %s", strerror(errno));
	}

	closefrom(STDERR_FILENO + 1);
	close(STDIN_FILENO);
#endif

	struct LibiasDirectory *portsdir = libias_directory_open(this->scan.portsdir);
	libias_scope_free(portsdir);
	if (!portsdir) {
		libias_panic("open: %s: %s", this->scan.portsdir, strerror(errno));
	}

	struct PortscanLogDir *logdir = NULL;
	struct LibiasFile *out = libias_file_openfd(STDOUT_FILENO);
	if (this->scan.logdir != NULL) {
		logdir = portscan_log_dir_open(this->scan.logdir, portsdir);
		if (logdir == NULL) {
			libias_panic("portscan_log_dir_open: %s: %s", this->scan.logdir, strerror(errno));
		}
		libias_cleanup(&out);
	}
	libias_scope_free(out);
	libias_scope_on_exit(portscan_log_dir_close, logdir);

#if HAVE_CAPSICUM
	if (caph_limit_stream(libias_directory_fd(portsdir), CAPH_LOOKUP | CAPH_READ | CAPH_READDIR) < 0) {
		libias_panic("caph_limit_stream: %s", strerror(errno));
	}

	if (caph_enter() < 0) {
		libias_panic("caph_enter: %s", strerror(errno));
	}
#endif

	portscan_status_init(this->scan.progressinterval);

	uint32_t rules_behavior = RULES_DEFAULT;
	if (!(flags & SCAN_STRICT_VARIABLES)) {
		rules_behavior |= RULES_CHECK_VARIABLE_REFERENCES;
	}
	auto rules = rules_new(rules_behavior);
	libias_scope_free(rules);
	struct LibiasWorkqueue *workqueue = libias_mempool_workqueue(pool, 0);
	struct PortscanLog *result = portscan_log_new();
	libias_scope_free(result);
	struct LibiasArray *origins = this->scan.origins;
	if (!origins) {
		origins = lookup_origins(
			pool,
			workqueue,
			portsdir,
			flags,
			rules,
			result);
	} else {
		flags |= SCAN_PARTIAL;
	}

	portscan_status_reset(PORTSCAN_STATUS_PORTS, libias_array_len(origins));
	scan_ports(
		workqueue,
		portsdir,
		origins,
		flags,
		rules,
		this->scan.keyquery,
		this->scan.query,
		this->scan.option_default_description_editdist,
		result);
	if (portscan_log_len(result) > 0) {
		if (logdir != NULL) {
			struct PortscanLog *prev_result = portscan_log_read_all(logdir, PORTSCAN_LOG_LATEST);
			libias_scope_free(prev_result);
			if (portscan_log_compare(prev_result, result)) {
				if (this->scan.flags.progress) {
					portscan_status_reset(PORTSCAN_STATUS_FINISHED, 0);
					portscan_status_print(NULL);
				}
				fprintf(stderr, "no changes compared to previous result\n");
				return PORTFMT_EXIT_SCAN_NO_CHANGES;
			}
			if (this->scan.flags.progress) {
				portscan_status_reset(PORTSCAN_STATUS_FINISHED, 0);
				portscan_status_print(NULL);
			}
			if (!portscan_log_serialize_to_dir(result, logdir)) {
				libias_panic("portscan_log_serialize_to_dir: %s", strerror(errno));
			}
		} else {
			if (this->scan.flags.progress) {
				portscan_status_reset(PORTSCAN_STATUS_FINISHED, 0);
				portscan_status_print(NULL);
			}
			if (!portscan_log_serialize_to_file(result, out)) {
				libias_panic("portscan_log_serialize: %s", strerror(errno));
			}
		}
	} else if (this->scan.flags.progress) {
		portscan_status_reset(PORTSCAN_STATUS_FINISHED, 0);
		portscan_status_print(NULL);
	}

	return PORTFMT_EXIT_OK;
}
