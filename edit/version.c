// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/merge.h"
#include "ast/query.h"
#include "diagnostic.h"
#include "edit/version.h"

// Prototypes
static ssize_t extract_git_describe_prefix(const char *ver);
static ssize_t extract_git_describe_suffix(const char *ver);
static bool is_git_describe_version(struct LibiasMempool *pool, const char *ver, char **distversion, char **prefix, char **suffix);

ssize_t
extract_git_describe_suffix(const char *ver)
{
	if (strlen(ver) == 0) {
		return -1;
	}

	bool gflag = false;
	for (size_t i = strlen(ver) - 1; i != 0; i--) {
		switch (ver[i]) {
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
			break;
		case 'g':
			gflag = true;
			break;
		case '-':
			if (gflag) {
				return i;
			} else {
				return -1;
			}
		default:
			if (!isdigit((unsigned char)ver[i])) {
				return -1;
			}
		}
	}

	return -1;
}

ssize_t
extract_git_describe_prefix(const char *ver)
{
	if (*ver == 0 || isdigit((unsigned char)*ver)) {
		return -1;
	}

	for (size_t i = 0; i < strlen(ver); i++) {
		if (i > 0 && isdigit((unsigned char)ver[i])) {
			return i - 1;
		}
	}

	return -1;
}

bool
is_git_describe_version(
	struct LibiasMempool *pool,
	const char *ver,
	char **distversion,
	char **prefix,
	char **suffix)
{
	ssize_t suffix_index;
	if ((suffix_index = extract_git_describe_suffix(ver)) == -1) {
		if (distversion != NULL) {
			*distversion = NULL;
		}
		if (prefix != NULL) {
			*prefix = NULL;
		}
		if (suffix != NULL) {
			*suffix = NULL;
		}
		return false;
	}

	ssize_t prefix_index;
	if ((prefix_index = extract_git_describe_prefix(ver)) != -1) {
		if (prefix != NULL) {
			*prefix = libias_str_ndup(ver, prefix_index + 1);
			libias_mempool_take(pool, *prefix);
		}

	} else {
		if (prefix != NULL) {
			*prefix = NULL;
		}
	}

	if (suffix != NULL) {
		*suffix = libias_str_dup(ver + suffix_index);
		libias_mempool_take(pool, *suffix);
	}

	if (distversion != NULL) {
		*distversion = libias_str_slice(ver, prefix_index + 1, suffix_index);
		libias_mempool_take(pool, *distversion);
	}

	return true;
}

bool
edit_set_version(
	struct AST *root,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings,
	struct Diagnostic *external_diagnostic,
	const char *newversion)
{
	libias_scope_mempool(pool);

	auto diagnostic = diagnostic_new();
	libias_scope_free(diagnostic);

	const char *ver = "DISTVERSION";
	if (ast_lookup_variable(root, "PORTVERSION", AST_LOOKUP_VARIABLE_FIRST)) {
		ver = "PORTVERSION";
	}

	uint32_t rev = 0;
	bool rev_opt = false;
	struct AST *node = ast_lookup_variable(root, ver, AST_LOOKUP_VARIABLE_FIRST);
	if (node) {
		char *version = ast_words_no_meta_flatten(node, " ");
		libias_scope_free(version);
		struct AST *rev_var;
		if (strcmp(version, newversion) != 0 &&
		    (rev_var = ast_lookup_variable(root, "PORTREVISION", AST_LOOKUP_VARIABLE_FIRST))) {
			char *revision = ast_words_no_meta_flatten(rev_var, " ");
			libias_scope_free(revision);
			rev_opt = ast_variable_modifier(rev_var) == AST_VARIABLE_MODIFIER_OPTIONAL;
			const char *errstr = NULL;
			rev = strtonum(revision, 0, INT_MAX, &errstr);
			if (errstr != NULL) {
				diagnostic_ast_error(
					diagnostic,
					node,
					NULL, NULL, // TODO word range for better error message?
					"PORTREVISION does not appear to be an integer: %s",
					errstr);
				goto done;
			}
		}
	}

	bool remove_distversionprefix = false;
	bool remove_distversionsuffix = false;
	char *distversion;
	char *prefix;
	char *suffix;
	if (!is_git_describe_version(pool, newversion, &distversion, &prefix, &suffix)) {
		auto node = ast_lookup_variable(
			root,
			"DISTVERSIONSUFFIX",
			AST_LOOKUP_VARIABLE_FIRST);
		if (node) {
			suffix = ast_words_no_meta_flatten(node, " ");
			libias_mempool_take(pool, suffix);
			if (libias_str_suffix_p(newversion, suffix)) {
				char *tmp = libias_str_ndup(
					newversion,
					strlen(newversion) - strlen(suffix));
				libias_mempool_take(pool, tmp);
				newversion = tmp;
			} else {
				remove_distversionsuffix = true;
			}
			suffix = NULL;
		}
		node = ast_lookup_variable(
			root,
			"DISTVERSIONPREFIX",
			AST_LOOKUP_VARIABLE_FIRST);
		if (node) {
			prefix  = ast_words_no_meta_flatten(node, " ");
			libias_mempool_take(pool, prefix);
			if (libias_str_prefix_p(newversion, prefix)) {
				newversion += strlen(prefix);
			}
			prefix = NULL;
		}
	} else if (prefix == NULL) {
		remove_distversionprefix = true;
		ver = "DISTVERSION";
	} else {
		ver = "DISTVERSION";
	}

	auto script = libias_buffer_new();
	if (suffix) {
		libias_buffer_puts(script, "DISTVERSIONSUFFIX=");
		libias_buffer_puts(script, suffix);
		libias_buffer_puts(script, "\n");
	} else if (remove_distversionsuffix) {
		libias_buffer_puts(script, "DISTVERSIONSUFFIX!=\n");
	}

	if (prefix) {
		libias_buffer_puts(script, "DISTVERSIONPREFIX=");
		libias_buffer_puts(script, prefix);
		libias_buffer_puts(script, "\n");
	} else if (remove_distversionprefix) {
		libias_buffer_puts(script, "DISTVERSIONPREFIX!=\n");
	}

	if (strcmp(ver, "DISTVERSION") == 0) {
		libias_buffer_puts(script, "PORTVERSION!=\n");
	}

	if (distversion) {
		newversion = distversion;
	}

	if (rev > 0) {
		if (rev_opt) {
			// Reset PORTREVISION?= to 0
			libias_buffer_puts(script, ver);
			libias_buffer_puts(script, "=");
			libias_buffer_puts(script, newversion);
			libias_buffer_puts(script, "\nPORTREVISION=0\n");
		} else {
			// Remove PORTREVISION
			libias_buffer_puts(script, ver);
			libias_buffer_puts(script, "=");
			libias_buffer_puts(script, newversion);
			libias_buffer_puts(script, "\nPORTREVISION!=\n");
		}
	} else {
		libias_buffer_puts(script, ver);
		libias_buffer_puts(script, "=");
		libias_buffer_puts(script, newversion);
		libias_buffer_puts(script, "\n");
	}

	auto script_ast = ast_from_buffer(script, diagnostic, "<edit-version-merge-script>");
	if (script_ast) {
		ast_merge(
			root,
			script_ast,
			diagnostic,
			rules,
			format_settings,
			AST_MERGE_SHELL_IS_DELETE);
		libias_cleanup(&script_ast);
	}
	libias_cleanup(&script);

done:
	if (diagnostic_error_p(diagnostic)) {
		if (external_diagnostic) {
			diagnostic_move_reports(external_diagnostic, diagnostic);
		}
		return false;
	} else {
		return true;
	}
}
