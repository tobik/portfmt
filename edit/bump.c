// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/iterator.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/merge.h"
#include "ast/query.h"
#include "ast/word.h"
#include "diagnostic.h"
#include "edit/bump.h"

struct ShouldDeleteVariableState {
	const char *variable;
	bool delete_variable;
};

// Prototypes
static bool edit_bump(struct AST *root, struct Rules *rules, struct ASTFormatSettings *format_settings, struct Diagnostic *external_diagnostic, const char *variable);
static struct LibiasBuffer *get_merge_script(struct AST *root, struct Diagnostic *diagnostic, const char *variable);
static void should_delete_variable_visit_variable(struct ASTVisit *visit, struct AST *node);

void
should_delete_variable_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ShouldDeleteVariableState *this = ast_visit_context(visit);

	if (ast_word_strcmp(ast_variable_name(node), this->variable) == 0) {
		struct AST *previous = ast_prev_sibling(node);
		if (previous && ast_comment_p(previous)) {
			this->delete_variable = true;
			ast_words_foreach(previous, line) {
				this->delete_variable = this->delete_variable
					&& ast_word_whitespace_p(line);
				if (!this->delete_variable) {
					break;
				}
			}
		}
		ast_visit_stop(visit);
	}
}

struct LibiasBuffer *
get_merge_script(
	struct AST *root,
	struct Diagnostic *diagnostic,
	const char *variable)
{
	auto script = libias_buffer_new();

	struct AST *node;
	if (strcmp(variable, "PORTEPOCH") == 0) {
		if ((node = ast_lookup_variable(root, "PORTREVISION", AST_LOOKUP_VARIABLE_FIRST)) &&
		    ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_OPTIONAL) {
			libias_buffer_puts(script, "PORTREVISION=0\n");
		} else {
			libias_buffer_puts(script, "PORTREVISION!=\n");
		}
	}

	if ((node = ast_lookup_variable(root, variable, AST_LOOKUP_VARIABLE_FIRST))) {
		char *current_revision = ast_words_no_meta_flatten(node, " ");
		libias_scope_free(current_revision);
		const char *errstr = NULL;
		int32_t rev = strtonum(current_revision, 0, INT_MAX, &errstr);
		if (errstr == NULL) {
			rev++;
		} else {
			diagnostic_ast_error(
				diagnostic,
				node,
				NULL, NULL, // TODO word range for better error?
				"%s does not appear to be an integer: %s",
				variable, errstr);
			return NULL;
		}
		if (ast_lookup_variable(root, "MASTERDIR", AST_LOOKUP_VARIABLE_FIRST) == NULL) {
			// In slave ports we do not delete the variable first since
			// they have a non-uniform structure and edit_merge will probably
			// insert it into a non-optimal position.

			// If the variable appears after a non-empty comment
			// block we do not delete it either since the comment
			// is probably about the variable and it is natural
			// to have the comment above the variable.
			struct ASTVisitTrait visit_trait;
			ast_visit_trait_init(&visit_trait);
			visit_trait.visit_variable = should_delete_variable_visit_variable;
			struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
			struct ShouldDeleteVariableState this = { variable, true };
			ast_visit_set_context(visit, &this);
			ast_visit_run(visit);
			libias_cleanup(&visit);

			// Otherwise we can safely remove it.
			if (this.delete_variable) {
				libias_buffer_puts(script, variable);
				libias_buffer_puts(script, "!=\n");
			}
		}
		char *name = ast_word_flatten(ast_variable_name(node));
		libias_buffer_puts(script, name);
		libias_buffer_puts(script, ASTVariableModifier_human(ast_variable_modifier(node)));
		libias_free(name);
		struct ASTWord *node_comment = ast_node_comment(node);
		if (node_comment) {
			const char *comment = ast_word_value(node_comment);
			libias_buffer_puts(script, rev);
			libias_buffer_puts(script, " #");
			libias_buffer_puts(script, comment);
			libias_buffer_puts(script, "\n");
		} else {
			libias_buffer_puts(script, rev);
			libias_buffer_puts(script, "\n");
		}
	} else {
		libias_buffer_puts(script, variable);
		libias_buffer_puts(script, "=1\n");
	}

	return script;
}

bool
edit_bump(
	struct AST *root,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings,
	struct Diagnostic *external_diagnostic,
	const char *variable)
{
	auto diagnostic = diagnostic_new();
	libias_scope_free(diagnostic);
	auto script = get_merge_script(
		root,
		diagnostic,
		variable);
	if (script) {
		auto script_ast = ast_from_buffer(script, diagnostic, "<edit-bump-script>");
		if (script_ast) {
			ast_merge(
				root,
				script_ast,
				diagnostic,
				rules,
				format_settings,
				AST_MERGE_SHELL_IS_DELETE);
			libias_cleanup(&script_ast);
		}
		libias_cleanup(&script);
	}

	if (diagnostic_error_p(diagnostic)) {
		if (external_diagnostic) {
			diagnostic_move_reports(external_diagnostic, diagnostic);
		}
		return false;
	} else {
		return true;
	}
}

bool
edit_bump_epoch(
	struct AST *root,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings,
	struct Diagnostic *diagnostic)
{
	return edit_bump(
		root,
		rules,
		format_settings,
		diagnostic,
		"PORTEPOCH");
}

bool
edit_bump_revision(
	struct AST *root,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings,
	struct Diagnostic *diagnostic)
{
	return edit_bump(
		root,
		rules,
		format_settings,
		diagnostic,
		"PORTREVISION");
}
