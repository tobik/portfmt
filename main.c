// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <fcntl.h>
#include <limits.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/color.h>
#include <libias/diff.h>
#include <libias/diffutil.h>
#include <libias/io.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/path.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/format/normalize_append_modifier.h"
#include "ast/merge.h"
#include "ast/word.h"
#include "buildinfo.h"
#include "diagnostic.h"
#include "edit/bump.h"
#include "edit/version.h"
#include "lint/bsd_port.h"
#include "lint/order.h"
#include "main.h"
#include "mainutils.h"
#include "regexp.h"
#include "rules.h"

int portscan_main(int, char *[]);

struct GetVariableValuesData {
	struct LibiasFile *fp_out;
	struct Regexp *query;
	bool found;
};

struct PortfmtArgs {
	size_t argc;
	char **argv;
	size_t i;
};

// Prototypes
static int PortfmtExitStatus_code(enum PortfmtExitStatus status);
static struct PortfmtCommand *find_subcommand(const char *name);
static void get_variable_visit_variable(struct ASTVisit *visit, struct AST *node);
static void portfmt_cleanup(struct Portfmt **this_ptr);
static struct Portfmt *portfmt_new(void);
static struct AST *read_file(struct Portfmt *this, uint32_t open_flags);
static void render_ast(struct Portfmt *this, struct AST *ast);
static enum PortfmtExitStatus render_ast_diff(struct Portfmt *this, struct AST *original, struct AST *edited);
static enum PortfmtExitStatus run_subcommand(struct Portfmt *this, struct PortfmtCommand *cmd);

const char *
args_peek(struct PortfmtArgs *this)
{
	if (this->i < this->argc) {
		return this->argv[this->i];
	} else {
		return NULL;
	}
}

const char *
args_pop(struct PortfmtArgs *this)
{
	if (this->i < this->argc) {
		return this->argv[this->i++];
	} else {
		return NULL;
	}
}

void
args_unwind(struct PortfmtArgs *this)
{
	if (this->i == 0) {
		return;
	} else {
		this->i--;
	}
}

enum PortfmtExitStatus
read_diff_context(
	struct Portfmt *this,
	const char *arg)
{
	const char *errstr = NULL;
	this->diff_context = strtonum(arg, 0, INT_MAX, &errstr);
	if (errstr) {
		// errx(1, "-D%s is %s", arg, errstr);
		// TODO: diagnostic
		return PORTFMT_EXIT_ERROR;
	} else {
		return PORTFMT_EXIT_OK;
	}
}

enum PortfmtExitStatus
read_format_variable_wrapcol(
	struct Portfmt *this,
	const char *arg)
{
	const char *errstr = NULL;
	this->format->variable_wrapcol = strtonum(arg, -1, INT_MAX, &errstr);
	this->format->if_wrapcol = this->format->variable_wrapcol;
	this->format->for_wrapcol = this->format->variable_wrapcol;
	if (errstr) {
		// errx(1, "-w%s is %s", arg, errstr);
		// TODO: diagnostic
		return PORTFMT_EXIT_ERROR;
	} else {
		return PORTFMT_EXIT_OK;
	}
}

enum PortfmtExitStatus
read_get_variable_regexp(
	struct Portfmt *this,
	const char *arg)
{
	auto regexp = regexp_new_from_str(arg, REG_EXTENDED);
	if (regexp) {
		this->get_variable.regexp = regexp;
		return PORTFMT_EXIT_OK;
	} else {
		// TODO diagnostic
		fprintf(stderr, "invalid regexp: %s\n", arg);
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
read_help_topic(
	struct Portfmt *this,
	const char *arg)
{
	this->help.cmd = find_subcommand(arg);
	if (this->help.cmd) {
		return PORTFMT_EXIT_OK;
	} else {
		// TODO diagnostic
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
read_makefile_arg(
	struct Portfmt *this,
	const char *arg)
{
	this->input_makefile_arg = arg;
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
read_scan_logdir_path(
	struct Portfmt *this,
	const char *arg)
{
	this->scan.logdir = arg;
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
read_scan_progress_interval(
	struct Portfmt *this,
	const char *arg)
{
	const char *error;
	this->scan.progressinterval = strtonum(arg, 0, 100000000, &error);
	if (error) {
		fprintf(stderr, "--progress=%s is %s (must be >=1)", arg, error);
		// TODO diagnostic
		return PORTFMT_EXIT_ERROR;
	} else {
		return PORTFMT_EXIT_OK;
	}
}

enum PortfmtExitStatus
read_scan_option_default_descriptions_editdist(
	struct Portfmt *this,
	const char *arg)
{
	const char *error;
	this->scan.option_default_description_editdist = strtonum(
		arg,
		0, INT_MAX,
		&error);
	if (error) {
		fprintf(
			stderr,
			"--option-default-descriptions=%s is %s (must be >=0)\n",
			arg,
			error);
		return PORTFMT_EXIT_ERROR;
	} else {
		return PORTFMT_EXIT_OK;
	}
}

enum PortfmtExitStatus
read_scan_origin_arg(
	struct Portfmt *this,
	const char *arg)
{
	if (!this->scan.origins) {
		this->scan.origins = libias_array_new();
	}

	libias_array_append(this->scan.origins, arg);

	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
read_scan_portsdir(
	struct Portfmt *this,
	const char *arg)
{
	this->scan.portsdir = arg;
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
read_scan_variable_values(
	struct Portfmt *this,
	const char *arg)
{
	libias_cleanup(&this->scan.keyquery);
	this->scan.keyquery = regexp_new_from_str(arg, REG_EXTENDED);
	if (this->scan.keyquery) {
		return PORTFMT_EXIT_OK;
	} else {
		fprintf(stderr, "invalid regexp: %s\n", arg);
		// TODO diagnostic
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
read_scan_query(
	struct Portfmt *this,
	const char *arg)
{
	libias_cleanup(&this->scan.query);
	this->scan.query = regexp_new_from_str(arg, REG_EXTENDED);
	if (this->scan.query) {
		return PORTFMT_EXIT_OK;
	} else {
		fprintf(stderr, "invalid regexp: %s\n", arg);
		// TODO diagnostic
		return PORTFMT_EXIT_ERROR;
	}
}

void
render_ast(
	struct Portfmt *this,
	struct AST *ast)
{
	libias_scope_buffer(buf);
	ast_render(ast, buf);
	libias_file_puts(this->fp_out, buf);
}

enum PortfmtExitStatus
render_ast_diff(
	struct Portfmt *this,
	struct AST *original,
	struct AST *edited)
{
	libias_scope_mempool(pool);

	libias_scope_buffer(original_buf);
	ast_render(original, original_buf);

	libias_scope_buffer(edited_buf);
	ast_render(edited, edited_buf);

	auto original_lines = libias_str_split(
		pool,
		libias_buffer_data(original_buf),
		"\n");
	auto edited_lines = libias_str_split(
		pool,
		libias_buffer_data(edited_buf),
		"\n");

	auto diff = libias_array_diff(
		original_lines,
		edited_lines,
		libias_str_compare);
	libias_scope_free(diff);

	if (diff == NULL) {
		// TODO diagnostic
		// parser_set_error(parser, PARSER_ERROR_UNSPECIFIED, "could not create diff");
		return PORTFMT_EXIT_ERROR;
	}

	if (diff->editdist > 0) {
		const char *filename = libias_path_data(this->filename);
		if (filename == NULL) {
			filename = "Makefile";
		}
		const char *color_add = LIBIAS_ANSI_COLOR_GREEN;
		const char *color_delete = LIBIAS_ANSI_COLOR_RED;
		const char *color_reset = LIBIAS_ANSI_COLOR_RESET;
		if (this->flags.no_color) {
			color_add = "";
			color_delete = "";
			color_reset = "";
		}
		libias_file_puts(this->fp_out, color_delete);
		libias_file_puts(this->fp_out, "--- ");
		libias_file_puts(this->fp_out, filename);
		libias_file_puts(this->fp_out, "\n");
		libias_file_puts(this->fp_out, color_add);
		libias_file_puts(this->fp_out, "+++ ");
		libias_file_puts(this->fp_out, filename);
		libias_file_puts(this->fp_out, color_reset);
		libias_file_puts(this->fp_out, "\n");

		libias_scope_buffer(patch);
		libias_diff_to_patch(
			diff,
			patch,
			this->diff_context,
			!this->flags.no_color,
			NULL,
			NULL);
		libias_file_puts(this->fp_out, patch);

		return PORTFMT_EXIT_DIFFERENCES_FOUND;
	}

	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
do_bump_epoch(
	struct Portfmt *this,
	struct AST *ast)
{
	if (edit_bump_epoch(ast, this->rules, this->format, this->diagnostic)) {
		return PORTFMT_EXIT_OK;
	} else {
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
do_bump_revision(
	struct Portfmt *this,
	struct AST *ast)
{
	if (edit_bump_revision(ast, this->rules, this->format, this->diagnostic)) {
		return PORTFMT_EXIT_OK;
	} else {
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
do_get_variable(
	struct Portfmt *this,
	struct AST *ast)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = get_variable_visit_variable;

	struct ASTVisit *visit = ast_visit_new(ast, &visit_trait);
	struct GetVariableValuesData data = {
		.fp_out = this->fp_out,
		.query = this->get_variable.regexp,
		.found = false,
	};
	ast_visit_set_context(visit, &data);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	if (data.found) {
		return PORTFMT_EXIT_OK;
	} else {
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
run_subcommand(
	struct Portfmt *this,
	struct PortfmtCommand *cmd)
{
	// -D or -d with -i doesn't make any sense
	if (this->flags.dump_ast || this->flags.show_diff) {
		if (this->flags.edit_in_place) {
			// TODO: diagnostic
			return PORTFMT_EXIT_ERROR;
		}
	}

	// Set up formatting as requested
	if (this->flags.format_target_commands) {
		this->format->flags |= AST_FORMAT_TARGET_COMMANDS;
	}
	if (this->flags.keep_variables_unsorted) {
		this->format->flags |= AST_FORMAT_UNSORTED_VARIABLES;
	}
	if (this->flags.always_sort_variables) {
		this->format->flags |= AST_FORMAT_ALWAYS_SORT_VARIABLES;
	}

	// Load ruleset, cleanup is in portfmt_cleanup()
	uint32_t rules_flags = RULES_DEFAULT;
	if (this->flags.fuzzy_variable_matching) {
		rules_flags |= RULES_ALLOW_FUZZY_MATCHING;
	}
	if (this->lint.flags.check_variable_references) {
		rules_flags |= RULES_CHECK_VARIABLE_REFERENCES;
	}
	this->rules = rules_new(rules_flags);

	enum PortfmtExitStatus status = PORTFMT_EXIT_ERROR;

	if (cmd->main_callback) {
		status = cmd->main_callback(this);
		if (status != PORTFMT_EXIT_OK) {
			return status;
		}
	}

	if (!cmd->filter_callback && !cmd->query_callback) {
		return status;
	}

	uint32_t open_flags = MAINUTILS_OPEN_FILE_DEFAULT;
	if (this->flags.keep_stdin_open) {
		open_flags |= MAINUTILS_OPEN_FILE_KEEP_STDIN;
	}
	if (this->flags.edit_in_place) {
		open_flags |= MAINUTILS_OPEN_FILE_INPLACE;
	}
	auto ast = read_file(this, open_flags);
	if (!ast) {
		// TODO: diagnostic
		return PORTFMT_EXIT_ERROR;
	}
	libias_scope_free(ast);

	if (cmd->query_callback) {
		status = cmd->query_callback(this, ast);
		if (status != PORTFMT_EXIT_OK) {
			return status;
		}
	}

	if (cmd->filter_callback) {
		struct AST *original_ast = NULL;
		if (this->flags.show_diff) {
			original_ast = ast_clone(ast);
		}
		libias_scope_free(original_ast);

		status = cmd->filter_callback(this, ast);
		if (status != PORTFMT_EXIT_OK) {
			return status;
		}

		if (this->flags.edit_in_place) {
			if (libias_file_set_position(this->fp_out, 0) < 0) {
				diagnostic_io_error(
					this->diagnostic,
					libias_path_data(this->filename),
					"failed to set file position");
				return PORTFMT_EXIT_ERROR;
			}
			if (libias_file_truncate(this->fp_out) < 0) {
				diagnostic_io_error(
					this->diagnostic,
					libias_path_data(this->filename),
					"failed to truncate file");
				return PORTFMT_EXIT_ERROR;
			}
		}

		if (this->flags.dump_ast) {
			libias_scope_buffer(buf);
			ast_sexp(ast, buf, !this->flags.no_color);
			libias_file_puts(this->fp_out, buf);
			return PORTFMT_EXIT_OK;
		} else if (this->flags.show_diff) {
			auto status = render_ast_diff(
				this,
				original_ast,
				ast);
			return status;
		} else {
			render_ast(this, ast);
			return PORTFMT_EXIT_OK;
		}
	}

	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
do_ast(
	struct Portfmt *this,
	struct AST *ast)
{
	libias_scope_buffer(buf);
	ast_sexp(ast, buf, !this->flags.no_color);
	libias_file_puts(this->fp_out, buf);
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
do_cat(
	struct Portfmt *this,
	struct AST *ast)
{
	// Nothing to do :-)
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
do_format(
	struct Portfmt *this,
	struct AST *ast)
{
	ast_format(ast, this->rules, this->format, this->diagnostic);
	return PORTFMT_EXIT_OK;
}

void
get_variable_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct GetVariableValuesData *this = ast_visit_context(visit);
	char *varname = ast_word_flatten(ast_variable_name(node));
	libias_scope_free(varname);
	if (!this->query || regexp_exec(this->query, varname) == 0) {
		this->found = true;
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			char *value = ast_word_flatten(word);
			libias_scope_free(value);
			libias_file_puts(this->fp_out, value);
			libias_file_puts(this->fp_out, "\n");
		}
	}
}

enum PortfmtExitStatus
do_lint(
	struct Portfmt *this,
	struct AST *ast)
{
	if (!lint_bsd_port_p(ast)) {
		// TODO diagnostic
		fprintf(stderr, "not a FreeBSD Ports Makefile\n");
		return PORTFMT_EXIT_ERROR;
	}

	auto buf =  lint_order(
		ast,
		this->rules,
		!this->flags.no_color);
	if (buf) {
		libias_file_puts(this->fp_out, buf);
		libias_cleanup(&buf);
		return PORTFMT_EXIT_OK;
	} else {
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
read_merge_expression(
	struct Portfmt *this,
	const char *expr)
{
	if (!this->merge.script_buffer) {
		this->merge.script_buffer = libias_buffer_new();
	}

	libias_buffer_puts(this->merge.script_buffer, expr);
	libias_buffer_puts(this->merge.script_buffer, "\n");

	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
do_merge(
	struct Portfmt *this,
	struct AST *ast)
{
	if (!this->merge.script_buffer) {
		auto f = libias_file_openfd(STDIN_FILENO);
		this->merge.script_buffer = libias_file_slurp(f);
		libias_cleanup(&f);
		if (!this->merge.script_buffer) {
			// TODO: Diagnostic
			return PORTFMT_EXIT_ERROR;
		}
	}

	auto merge_ast = ast_from_buffer(
		this->merge.script_buffer,
		this->diagnostic,
		"<merge-script>");
	if (merge_ast) {
		ast_merge(
			ast,
			merge_ast,
			this->diagnostic,
			this->rules,
			this->format,
			AST_MERGE_SHELL_IS_DELETE);
		// Always format category makefiles fully since they don't
		// follow the normal formattings rules, i.e., the edits applied
		// the wrong formatting before this point and we "correct" that
		// here as a hack.
		if (ast_format_category_makefile_p(ast, this->diagnostic)) {
			ast_format(ast, this->rules, this->format, this->diagnostic);
		}
		libias_cleanup(&merge_ast);
		return PORTFMT_EXIT_OK;
	} else {
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
do_sanitize_append(
	struct Portfmt *this,
	struct AST *ast)
{
	ast_format_normalize_append_modifier(
		ast,
		this->rules,
		this->format,
		this->diagnostic);
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
read_set_version_arg(
	struct Portfmt *this,
	const char *arg)
{
	this->set_version.version = arg;
	return PORTFMT_EXIT_OK;
}

enum PortfmtExitStatus
do_set_version(
	struct Portfmt *this,
	struct AST *ast)
{
	bool ok = edit_set_version(
		ast,
		this->rules,
		this->format,
		this->diagnostic,
		this->set_version.version);
	if (ok) {
		return PORTFMT_EXIT_OK;
	} else {
		return PORTFMT_EXIT_ERROR;
	}
}

enum PortfmtExitStatus
do_help(struct Portfmt *this)
{
	auto cmd = this->help.cmd;
	if (cmd) {
		// TODO implement help
		fprintf(stderr, "Help for subcommands is unimplemented\n");
		return PORTFMT_EXIT_ERROR;
	} else {
		fprintf(stderr, "usage: portfmt <command> [<args>]\n\n");
		fprintf(stderr, "Supported commands:\n");
		for (size_t i = 0; i < subcommands_len; i++) {
			if (subcommands[i].help) {
				fprintf(
					stderr,
					"  %-20s%s\n",
					subcommands[i].name,
					subcommands[i].help);
			}
		}
		return PORTFMT_EXIT_OK;
	}
}

struct AST *
read_file(
	struct Portfmt *this,
	uint32_t open_flags)
{
	struct LibiasFile *fp_in = NULL;
	if (!open_file(
			open_flags,
			this->input_makefile_arg,
			&fp_in,
			&this->fp_out,
			&this->filename))
	{
		// TODO diagnostic
		return NULL;
	}
	if (!libias_file_can_use_colors(this->fp_out)) {
		this->flags.no_color++;
	}

	enter_sandbox();

	const char *filename = this->input_makefile_arg;
	if (!filename) {
		filename = "/dev/stdin";
	}
	auto ast = ast_from_file(fp_in, this->diagnostic, filename);
	if (!(open_flags & MAINUTILS_OPEN_FILE_INPLACE)) {
		libias_cleanup(&fp_in);
	}
	return ast;
}

struct Portfmt *
portfmt_new(void)
{
	auto this = libias_alloc(struct Portfmt);
	this->diff_context = 3;

	this->format = libias_alloc(struct ASTFormatSettings);
	ast_format_init_settings(this->format);

	this->diagnostic = diagnostic_new();

	this->scan.option_default_description_editdist = 3;
	this->scan.progressinterval = 1;

	this->scan.portsdir = getenv("PORTSDIR");
	if (!this->scan.portsdir) {
		this->scan.portsdir = "/usr/ports";
	}

	return this;
}

void
portfmt_cleanup(struct Portfmt **this_ptr)
{
	auto this = *this_ptr;
	if (this) {
		libias_cleanup(&this->diagnostic);
		libias_cleanup(&this->filename);
		libias_free(this->format);
		libias_cleanup(&this->fp_out);
		libias_cleanup(&this->get_variable.regexp);
		libias_cleanup(&this->merge.script_buffer);
		libias_cleanup(&this->rules);
		libias_cleanup(&this->scan.origins);
		libias_cleanup(&this->scan.query);
		libias_cleanup(&this->scan.keyquery);
		libias_free(this);
		*this_ptr = NULL;
	}
}

struct PortfmtCommand *
find_subcommand(const char *name)
{
	for (size_t i = 0; i < subcommands_len; i++) {
		if (strcmp(name, subcommands[i].name) == 0) {
			return &subcommands[i];
		}
	}

	return NULL;
}

int
PortfmtExitStatus_code(enum PortfmtExitStatus status)
{
	switch (status) {
	case PORTFMT_EXIT_OK:
		return 0;
	case PORTFMT_EXIT_HELP:
		return EX_USAGE;
	case PORTFMT_EXIT_ERROR:
		return 1;
	case PORTFMT_EXIT_DIFFERENCES_FOUND:
		return 2;
	case PORTFMT_EXIT_SCAN_NO_CHANGES:
		return 2;
	}

	libias_panic("unreachable");
}

int
main(int argc, char *argv[])
{
	libias_attr_cleanup(portfmt_cleanup)
	auto this = portfmt_new();

	auto args = &(struct PortfmtArgs){ argc, argv, 0 };
	// Ignore command name in argv[0]
	args_pop(args);

	auto subcommand_arg = args_peek(args);
	if (subcommand_arg) {
		if (strcmp(subcommand_arg, "--version") == 0) {
			puts(portfmt_version());
			return 0;
		} else if (*subcommand_arg == '-' && args->argc == 0) {
			// If we have no extra argument or if the command is a flag we
			// assume that formatting is wanted.
			subcommand_arg = "format";
		}
	} else {
		subcommand_arg = "format";
	}
	auto subcommand = find_subcommand(subcommand_arg);
	if (subcommand) {
		args_pop(args);
	} else {
		if (access(subcommand_arg, R_OK) == 0) {
			// Check if command is a readable file in which case we assume that
			// we should format it.
			subcommand = find_subcommand("format");
		} else {
			subcommand = find_subcommand("help");
		}
	}

	libias_panic_unless(subcommand, "could not determine subcommand");

	enum PortfmtExitStatus status = PORTFMT_EXIT_OK;
	if (subcommand->read_arguments) {
		status = subcommand->read_arguments(this, args);
	}

	if (status == PORTFMT_EXIT_OK) {
		// For -h, --help dispatch to help subcommand instead
		if (this->flags.show_help) {
			this->help.cmd = subcommand;
			subcommand = find_subcommand("help");
			libias_panic_unless(subcommand, "could not find help subcommand");
		}
		status = run_subcommand(this, subcommand);
	}

	libias_scope_buffer(errbuf);
	diagnostic_render(this->diagnostic, errbuf);
	fputs(libias_buffer_data(errbuf), stderr);

	return PortfmtExitStatus_code(status);
}
