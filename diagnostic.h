// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_returns_nonnull
struct Diagnostic *diagnostic_new(void);

libias_attr_nonnull(1)
void diagnostic_cleanup(struct Diagnostic **);

libias_attr_nonnull(1)
void diagnostic_set_current_filename(struct Diagnostic *, const char *);

libias_attr_nonnull(1, 2)
libias_attr_printflike(5, 6)
void diagnostic_ast_error(struct Diagnostic *, struct AST *, struct ASTWord *, struct ASTWord *, const char *, ...);

libias_attr_nonnull(1, 2)
libias_attr_printflike(5, 6)
void diagnostic_ast_info(struct Diagnostic *, struct AST *, struct ASTWord *, struct ASTWord *, const char *, ...);

libias_attr_nonnull(1, 2)
libias_attr_printflike(5, 6)
void diagnostic_ast_warning(struct Diagnostic *, struct AST *, struct ASTWord *, struct ASTWord *, const char *, ...);

libias_attr_nonnull(1, 2, 5)
libias_attr_printflike(5, 6)
void diagnostic_args_error(struct Diagnostic *, char *[], size_t, size_t, const char *, ...);

libias_attr_nonnull(1, 2, 5)
libias_attr_printflike(5, 6)
void diagnostic_args_info(struct Diagnostic *, char *[], size_t, size_t, const char *, ...);

libias_attr_nonnull(1, 2, 5)
libias_attr_printflike(5, 6)
void diagnostic_args_warning(struct Diagnostic *, char *[], size_t, size_t, const char *, ...);

libias_attr_nonnull(1, 2, 3)
libias_attr_printflike(3, 4)
void diagnostic_io_error(struct Diagnostic *, const char *, const char *, ...);

libias_attr_nonnull(1, 2, 5)
libias_attr_printflike(5, 6)
void diagnostic_parse_error(struct Diagnostic *, struct ASTBuilder *, size_t, size_t, const char *, ...);

libias_attr_nonnull(1, 2, 5)
libias_attr_printflike(5, 6)
void diagnostic_parse_info(struct Diagnostic *, struct ASTBuilder *, size_t, size_t, const char *, ...);

libias_attr_nonnull(1, 2, 5)
libias_attr_printflike(5, 6)
void diagnostic_parse_warning(struct Diagnostic *, struct ASTBuilder *, size_t, size_t, const char *, ...);

libias_attr_nonnull(1)
void diagnostic_truncate(struct Diagnostic *this);

libias_attr_nonnull(1)
bool diagnostic_error_p(struct Diagnostic *);

libias_attr_nonnull(1)
bool diagnostic_warning_p(struct Diagnostic *);

libias_attr_nonnull(1, 2)
void diagnostic_move_reports(struct Diagnostic *, struct Diagnostic *);

libias_attr_nonnull(1, 2)
void diagnostic_render(struct Diagnostic *, struct LibiasBuffer *);
