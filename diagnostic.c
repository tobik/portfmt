// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <errno.h>
#include <string.h>
#include <sys/param.h>
#include <stdarg.h>
#include <stdio.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast/builder.h"
#include "diagnostic.h"

enum DiagnosticPriority {
	DIAGNOSTIC_INFO,
	DIAGNOSTIC_WARNING,
	DIAGNOSTIC_ERROR,
};

struct Diagnostic {
	struct LibiasArray *reports;
	enum DiagnosticPriority max_priority;
};

struct DiagnosticReport {
	enum DiagnosticPriority priority;
	char *message;
	struct {
		struct {
			char *filename;
			struct AST *node;
			struct ASTWord *start;
			struct ASTWord *end;
		} ast;

		struct {
			char *filename;
			int error;
		} io;

		struct {
			char *filename;
			struct LibiasBuffer *buf;
			size_t start;
			size_t end;
		} parse;

		struct {
			char **argv;
			size_t start;
			size_t end;
		} args;
	} location;
};

// Prototypes
static struct DiagnosticReport *diagnostic_add_args_report(struct Diagnostic *this, enum DiagnosticPriority priority, char *args[], size_t start, size_t end, const char *format, va_list ap);
static struct DiagnosticReport *diagnostic_add_ast_report(struct Diagnostic *this, enum DiagnosticPriority priority, struct AST *ast, struct ASTWord *start, struct ASTWord *end, const char *format, va_list ap);
static struct DiagnosticReport *diagnostic_add_io_report(struct Diagnostic *this, enum DiagnosticPriority priority, const char *filename, int error, const char *format, va_list ap);
static struct DiagnosticReport *diagnostic_add_parse_report(struct Diagnostic *this, enum DiagnosticPriority priority, struct ASTBuilder *builder, size_t start, size_t end, const char *format, va_list ap);
static struct DiagnosticReport *diagnostic_add_report(struct Diagnostic *this, enum DiagnosticPriority priority, const char *format, va_list ap);
static void diagnostic_render_args_report(struct DiagnosticReport *report, struct LibiasBuffer *target);
static void diagnostic_render_ast_report(struct DiagnosticReport *report, struct LibiasBuffer *target);
static void diagnostic_render_io_report(struct DiagnosticReport *this, struct LibiasBuffer *target);
static void diagnostic_render_parse_report(struct DiagnosticReport *report, struct LibiasBuffer *target);
static void diagnostic_report_cleanup(struct DiagnosticReport **this_ptr);

struct Diagnostic *
diagnostic_new(void)
{
	auto this = libias_alloc(struct Diagnostic);
	this->reports = libias_array_new();
	return this;
}

void
diagnostic_cleanup(struct Diagnostic **this_ptr)
{
	auto this = *this_ptr;
	if (this) {
		libias_array_foreach(this->reports, struct DiagnosticReport *, report) {
			diagnostic_report_cleanup(&report);
		}
		libias_cleanup(&this->reports);
		libias_free(*this_ptr);
		*this_ptr = NULL;
	}
}

struct DiagnosticReport *
diagnostic_add_ast_report(
	struct Diagnostic *this,
	enum DiagnosticPriority priority,
	struct AST *ast,
	struct ASTWord *start,
	struct ASTWord *end,
	const char *format,
	va_list ap)
{
	auto report = diagnostic_add_report(this, priority, format, ap);
	report->location.ast.node = ast;
	// TODO add panic if the word are not part of the node or if they
	// are part of another node.
	report->location.ast.start = start;
	report->location.ast.end = end;
	return report;
}

struct DiagnosticReport *
diagnostic_add_parse_report(
	struct Diagnostic *this,
	enum DiagnosticPriority priority,
	struct ASTBuilder *builder,
	size_t start,
	size_t end,
	const char *format,
	va_list ap)
{
	auto report = diagnostic_add_report(this, priority, format, ap);
	libias_panic_if(
		start < end,
		"DiagnosticReport with invalid location: %zu-%zu",
		start,
		end);
	report->location.parse.filename = libias_str_dup(ast_builder_filename(builder));
	report->location.parse.buf = libias_buffer_new();
	libias_buffer_puts(report->location.parse.buf, ast_builder_buffer(builder));
	report->location.parse.start = start;
	report->location.parse.end = end;
	return report;
}

struct DiagnosticReport *
diagnostic_add_report(
	struct Diagnostic *this,
	enum DiagnosticPriority priority,
	const char *format,
	va_list ap)
{
	auto report = libias_alloc(struct DiagnosticReport);

	report->priority = priority;
	this->max_priority = MAX(this->max_priority, priority);

	libias_panic_if(vasprintf(&report->message, format, ap) < 0, "vasprintf");

	libias_array_append(this->reports, report);

	return report;
}

struct DiagnosticReport *
diagnostic_add_args_report(
	struct Diagnostic *this,
	enum DiagnosticPriority priority,
	char *args[],
	size_t start,
	size_t end,
	const char *format,
	va_list ap)
{
	auto report = diagnostic_add_report(this, priority, format, ap);

	libias_panic_if(
		start < end,
		"DiagnosticReport with invalid location: %zu-%zu",
		start,
		end);
	report->location.args.argv = args;
	report->location.args.start = start;
	report->location.args.end = end;

	return report;
}

struct DiagnosticReport *
diagnostic_add_io_report(
	struct Diagnostic *this,
	enum DiagnosticPriority priority,
	const char *filename,
	int error,
	const char *format,
	va_list ap)
{
	auto report = diagnostic_add_report(this, priority, format, ap);
	report->location.io.filename = libias_str_dup(filename);
	report->location.io.error = error;
	return report;
}

void
diagnostic_ast_error(
	struct Diagnostic *this,
	struct AST *ast,
	struct ASTWord *start,
	struct ASTWord *end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_ast_report(
		this,
		DIAGNOSTIC_ERROR,
		ast,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_ast_info(
	struct Diagnostic *this,
	struct AST *ast,
	struct ASTWord *start,
	struct ASTWord *end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_ast_report(
		this,
		DIAGNOSTIC_INFO,
		ast,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_ast_warning(
	struct Diagnostic *this,
	struct AST *ast,
	struct ASTWord *start,
	struct ASTWord *end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_ast_report(
		this,
		DIAGNOSTIC_WARNING,
		ast,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_args_error(
	struct Diagnostic *this,
	char *args[],
	size_t start,
	size_t end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_args_report(
		this,
		DIAGNOSTIC_ERROR,
		args,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_args_info(
	struct Diagnostic *this,
	char *args[],
	size_t start,
	size_t end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_args_report(
		this,
		DIAGNOSTIC_INFO,
		args,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_args_warning(
	struct Diagnostic *this,
	char *args[],
	size_t start,
	size_t end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_args_report(
		this,
		DIAGNOSTIC_WARNING,
		args,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_parse_error(
	struct Diagnostic *this,
	struct ASTBuilder *builder,
	size_t start,
	size_t end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_parse_report(
		this,
		DIAGNOSTIC_ERROR,
		builder,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_parse_info(
	struct Diagnostic *this,
	struct ASTBuilder *builder,
	size_t start,
	size_t end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_parse_report(
		this,
		DIAGNOSTIC_INFO,
		builder,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_parse_warning(
	struct Diagnostic *this,
	struct ASTBuilder *builder,
	size_t start,
	size_t end,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	diagnostic_add_parse_report(
		this,
		DIAGNOSTIC_WARNING,
		builder,
		start, end,
		format,
		ap);
	va_end(ap);
}

void
diagnostic_io_error(
	struct Diagnostic *this,
	const char *filename,
	const char *format,
	...)
{
	if (errno > 0) {
		va_list ap;
		va_start(ap, format);
		diagnostic_add_io_report(
			this,
			DIAGNOSTIC_ERROR,
			filename,
			errno,
			format,
			ap);
		va_end(ap);
	}
}

void
diagnostic_truncate(struct Diagnostic *this)
{
	libias_array_foreach(this->reports, struct DiagnosticReport *, report) {
		diagnostic_report_cleanup(&report);
	}
	libias_array_truncate(this->reports);
	this->max_priority = DIAGNOSTIC_INFO;
}

bool
diagnostic_error_p(struct Diagnostic *this)
{
	return this->max_priority >= DIAGNOSTIC_ERROR;
}

bool
diagnostic_warning_p(struct Diagnostic *this)
{
	return this->max_priority >= DIAGNOSTIC_WARNING;
}

// Moves the reports from OTHER into THIS diagnostic. OTHER will have no
// reports afterwards.
void
diagnostic_move_reports(
	struct Diagnostic *this,
	struct Diagnostic *other)
{
	if (this == other) {
		return;
	}

	libias_array_foreach(other->reports, struct DiagnosticReport *, report) {
		this->max_priority = MAX(this->max_priority, report->priority);
		libias_array_append(this->reports, report);
	}
	libias_array_truncate(other->reports);
	other->max_priority = DIAGNOSTIC_INFO;
}

void
diagnostic_render_args_report(
	struct DiagnosticReport *report,
	struct LibiasBuffer *target)
{
	libias_buffer_puts(target, report->message);
	libias_buffer_puts(target, "\n");
}

void
diagnostic_render_ast_report(
	struct DiagnosticReport *report,
	struct LibiasBuffer *target)
{
	libias_buffer_puts(target, report->message);
	libias_buffer_puts(target, "\n");
}

void
diagnostic_render_parse_report(
	struct DiagnosticReport *report,
	struct LibiasBuffer *target)
{
	// Find line for location
	size_t start_line = 1;
	size_t end_line = 1;
	char *buf = libias_buffer_data(report->location.parse.buf);
	size_t len = libias_buffer_len(report->location.parse.buf);
	for (size_t i = 0; i < len; i++) {
		// TODO decode UTF-8 char
		char c = buf[i];
		//fprintf(stderr, "asdfas\n");
		if (c == '\n') {
			if (i < report->location.parse.start) {
				start_line++;
			}
			end_line++;
		}
		if (i >= report->location.parse.start) {
			break;
		}
	}

	libias_buffer_puts(target, report->location.parse.filename);
	libias_buffer_puts(target, ":");
	if (start_line == end_line) {
		libias_buffer_puts(target, start_line);
	} else {
		libias_buffer_puts(target, start_line);
		libias_buffer_puts(target, "-");
		libias_buffer_puts(target, end_line);
	}
	libias_buffer_puts(target, ":1: ");
	libias_buffer_puts(target, report->message);
	libias_buffer_puts(target, "\n");
}

void
diagnostic_render_io_report(
	struct DiagnosticReport *this,
	struct LibiasBuffer *target)
{
	libias_buffer_puts(target, this->message);
	libias_buffer_puts(target, ": ");
	libias_buffer_puts(target, this->location.io.filename);
	libias_buffer_puts(target, ": ");
	libias_buffer_puts(target, strerror(this->location.io.error));
	libias_buffer_puts(target, "\n");
}

// Renders the diagnostic to TARGET.
void
diagnostic_render(
	struct Diagnostic *this,
	struct LibiasBuffer *target)
{
	libias_array_foreach(this->reports, struct DiagnosticReport *, report) {
		if (report->location.args.argv) {
			diagnostic_render_args_report(report, target);
		} else if (report->location.ast.node) {
			diagnostic_render_ast_report(report, target);
		} else if (report->location.parse.buf) {
			diagnostic_render_parse_report(report, target);
		} else if (report->location.io.error != 0) {
			diagnostic_render_io_report(report, target);
		} else {
			libias_panic("Got unrenderable diagnostic report. This shouldn't happen.");
		}
	}
}

void
diagnostic_report_cleanup(struct DiagnosticReport **this_ptr)
{
	auto this = *this_ptr;
	if (this) {
		libias_cleanup(&this->location.ast.filename);
		libias_cleanup(&this->location.io.filename);
		libias_cleanup(&this->location.parse.filename);
		libias_cleanup(&this->location.parse.buf);
		libias_cleanup(&this->message);
		libias_free(*this_ptr);
		*this_ptr = NULL;
	}
}
