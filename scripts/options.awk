BEGIN {
	help_regexp = "^[A-Za-z0-9 \t\\{\\}\\[\\]\\(\\)]+=[ \t]+"
}

function cmd2cid(cmd,	name)
{
	name = cmd
	gsub(/-/, "_", name)
	return name
}

function Cindent(line,		buf, i)
{
	if (line == "\n") {
		return buf line
	} else if (line ~ /^[\)\}]/ ) {
		indent_level--
	}
	buf = ""
	for (i = 0; i < indent_level; i++) {
		buf = buf "\t"
	}
	if (line ~ /\)$/) {
		indent_level--
	} else if (line ~ /[\{\(]$/) {
		indent_level++
	}
	return buf line "\n"
}

function C(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15,	buf)
{
	buf = ""
	if (a0) {
		buf = buf Cindent(a0)
	}
	if (a1) {
		buf = buf Cindent(a1)
	}
	if (a2) {
		buf = buf Cindent(a2)
	}
	if (a3) {
		buf = buf Cindent(a3)
	}
	if (a4) {
		buf = buf Cindent(a4)
	}
	if (a5) {
		buf = buf Cindent(a5)
	}
	if (a6) {
		buf = buf Cindent(a6)
	}
	if (a7) {
		buf = buf Cindent(a7)
	}
	if (a8) {
		buf = buf Cindent(a8)
	}
	if (a9) {
		buf = buf Cindent(a9)
	}
	if (a10) {
		buf = buf Cindent(a10)
	}
	if (a11) {
		buf = buf Cindent(a11)
	}
	if (a12) {
		buf = buf Cindent(a12)
	}
	if (a13) {
		buf = buf Cindent(a13)
	}
	if (a14) {
		buf = buf Cindent(a14)
	}
	if (a15) {
		buf = buf Cindent(a15)
	}
	return buf
}

function err(msg) {
	printf("%s:%s:%s\n", FILENAME, NR, msg) >"/dev/stderr"
	exit(1)
}

# Skip comments and empty lines
$1 ~ /^#/ || $0 ~ /^[ 	]*$/{
	next
}

# New command
$1 ~ /^---/ {
	shortopt = ""
	longopt = ""
	posarg = ""
	command = ""
	next
}

# New flag
$1 ~ /^-/ {
	posarg = ""
	if ($1 ~ /^--/) {
		shortopt = ""
		longopt = substr($1, 3)
	} else if ($1 ~ /^-/) {
		shortopt = substr($1, 2)
		longopt = ""
		if ($2 ~ /^--/) {
			longopt = substr($2, 3)
		}
	} else {
		err("cannot parse flag spec");
	}


	if (longopt) {
		subcmds[command, "options"] = subcmds[command, "options"] " " longopt
		options[command, longopt] = 1
	}
	if (shortopt) {
		subcmds[command, "options"] = subcmds[command, "options"] " " shortopt
		options[command, shortopt] = 1
	}

	next
}

# New positional argument
$1 ~ /^:/ {
	shortopt = ""
	longopt = ""

	posarg = $2

	if ($1 == ":") { # required positional argument
		positionals[command, $2, "arg-greedy"] = 0
		positionals[command, $2, "arg-required"] = 1
	} else if ($1 == ":?") { # optional positional argument
		positionals[command, $2, "arg-greedy"] = 0
		positionals[command, $2, "arg-required"] = 0
	} else if ($1 == "::") { # zero or more positional arguments
		positionals[command, $2, "arg-greedy"] = 1
		positionals[command, $2, "arg-required"] = 0
	} else {
		err("cannot parse positional argument spec");
	}

	subcmds[command, "positionals"] = subcmds[command, "positionals"] " " $2
	positionals[command, $2] = 1

	next
}

# New kv pair
$2 == "=" {
	if (posarg) {
		value = $3
		if ($1 == "arg-descr") {
		} else if ($1 == "arg-read") {
		} else if ($1 == "flags") {
			for (i = 4; i <= NR; i++) {
				value = value " " $i
			}
		} else if ($1 == "help") {
			value = $0
			sub(help_regexp, "", value)
		} else {
			err("invalid option key: " $1)
		}
		positionals[command, posarg, $1] = value
	} else if (shortopt || longopt) {
		value = $3
		if ($1 == "arg-descr") {
		} else if ($1 == "arg-required") {
			if (value == "true") {
				value = 1
			} else if (value == "false") {
				value = 0
			} else {
				err("arg-required has to be either true or false")
			}
		} else if ($1 == "flags") {
			for (i = 4; i <= NR; i++) {
				value = value " " $i
			}
		} else if ($1 == "help") {
			value = $0
			sub(help_regexp, "", value)
		} else if ($1 == "arg-read") {
		} else {
			err("invalid option key: " $1)
		}

		if (shortopt) {
			options[command, shortopt, $1] = value
		}
		if (longopt) {
			options[command, longopt, $1] = value
		}
	} else if (command ~ /^\$/) {
		err("flag templates cannot have key-value pairs")
	} else {
		value = $3
		if ($1 == "filter") {
		} else if ($1 == "main") {
		} else if ($1 == "query") {
		} else if ($1 == "help") {
			value = $0
			sub(help_regexp, "", value)
		} else if ($1 == "manual") {
		} else if ($1 == "flags") {
			for (i = 4; i <= NR; i++) {
				value = value " " $i
			}
		} else {
			err("invalid command key: " $1)
		}

		subcmds[command, $1] = value
	}

	next
}

function copy_keys(\
	T,
	opts, i, option, arg, args)
{
	# Copy options from template T to new command
	split(subcmds[T, "options"], opts)
	for (i in opts) {
		option = opts[i]
		if (options[command, option]) {
			continue
		}
		subcmds[command, "options"]              = subcmds[command, "options"] " " option
		options[command, option]                 = 1
		options[command, option, "arg-descr"]    = options[T, option, "arg-descr"]
		options[command, option, "arg-read"]     = options[T, option, "arg-read"]
		options[command, option, "arg-required"] = options[T, option, "arg-required"]
		options[command, option, "flags"]        = options[T, option, "flags"]
		options[command, option, "help"]         = options[T, option, "help"]
	}

	# Copy positional arguments from template T to new command
	split(subcmds[T, "positionals"], args)
	for (i in args) {
		arg = args[i]
		if (positionals[command, arg]) {
			continue
		}
		subcmds[command, "positionals"]           = subcmds[command, "positionals"] " " arg
		positionals[command, arg]                 = 1
		positionals[command, arg, "arg-descr"]    = positionals[T, arg, "arg-descr"]
		positionals[command, arg, "arg-read"]     = positionals[T, arg, "arg-read"]
		positionals[command, arg, "arg-greedy"]   = positionals[T, arg, "arg-greedy"]
		positionals[command, arg, "arg-required"] = positionals[T, arg, "arg-required"]
		positionals[command, arg, "flags"]        = positionals[T, arg, "flags"]
		positionals[command, arg, "help"]         = positionals[T, arg, "help"]
	}
}

# Flag group
command && $1 ~ /^\$/ {
	copy_keys($1)
	next
}

!command {
	command = $1
	commands = commands " " command
	next
}

{
	err("invalid line type")
}

function generate_command_argparser_getopt(\
	cmd,
	buf, flagsbuf, statebuf, opts, i, j, option, flags, \
	read_arg, arg_required, arg, name)
{
	name = cmd2cid(cmd)
	buf = C(\
		"const char *arg = NULL;",
		"while ((arg = args_pop(args))) {")

	split(subcmds[cmd, "options"], opts)

	# Long options
	buf = buf C(\
		"if (strcmp(arg, \"--\") == 0) {",
		"// terminate option read",
		"break;",
		"} else if (libias_str_prefix_p(arg, \"--\")) {")
	longopts = 0
	for (i in opts) {
		option = opts[i]
		if (length(option) <= 1) {
			continue
		}
		longopts++
		split(options[cmd, option, "flags"], flags)
		read_arg = options[cmd, option, "arg-read"]
		arg_required = options[cmd, option, "arg-required"]

		if (read_arg) {
			buf = buf C(\
				"if (libias_str_prefix_p(arg, \"--" option "=\")) {",
				"status = " read_arg "(this, arg + " length("--" option "=") ");",
				"if (status != PORTFMT_EXIT_OK) {",
				"return status;",
				"}")
			for (j in flags) {
				buf = buf C("this->" flags[j] "++;")
			}
			buf = buf C("continue;", "}")
		}
		if (!read_arg || (read_arg && !arg_required)) {
			buf = buf C("if (strcmp(arg, \"--" option "\") == 0) {")
			for (j in flags) {
				buf = buf C("this->" flags[j] "++;")
			}
			buf = buf C("continue;", "}")
		}
	}
	buf = buf C("libias_panic(\"TODO diagnostic: %s\", arg);")

	# short options
	buf = buf C(\
		"} else if (strcmp(arg, \"-\") == 0) {",
		"libias_panic(\"unknown flag in option: %s\", arg); // TODO diagnostic",
		"return PORTFMT_EXIT_HELP;// TODO diagnostic",
		"} else if (libias_str_prefix_p(arg, \"-\")) {",
		"for (size_t i = 1; arg[i] != 0; i++) {",
		"switch (arg[i]) {")
	for (i in opts) {
		option = opts[i]
		if (length(option) != 1) {
			continue
		}
		read_arg = options[cmd, option, "arg-read"]
		arg_required = options[cmd, option, "arg-required"]
		split(options[cmd, option, "flags"], flags)
		buf = buf C("case '" option "': {")
		if (read_arg && !arg_required) {
			buf = buf C(\
				"if (arg[i + 1] != 0) {",
				"status = " read_arg "(this, arg + i + 1);",
				"if (status != PORTFMT_EXIT_OK) {",
				"return status;",
				"}",
				"}")
			for (j in flags) {
				buf = buf C("this->" flags[j] "++;")
			}
			buf = buf C(\
				"// Continue with next argument",
				"return read_arguments_" cmd2cid(cmd) "(this, args);")
		} else if (read_arg) {
			buf = buf C(\
				"if (arg[i + 1] == 0) {",
				"if (args_peek(args)) {",
				"status = " read_arg "(this, args_pop(args));",
				"if (status != PORTFMT_EXIT_OK) {",
				"return status;",
				"}")
			if (arg_required) {
				buf = buf C(\
					"} else {",
					"libias_panic(\"option non-optional: %s\", arg); // TODO diagnostic",
					"return PORTFMT_EXIT_HELP;// TODO diagnostic")
			}
			buf = buf C(\
				"}",
				"} else {",
				"libias_panic(\"option must appear last: %s\", arg); // TODO diagnostic",
				"return PORTFMT_EXIT_HELP;// TODO diagnostic",
				"}")
			for (j in flags) {
				buf = buf C("this->" flags[j] "++;")
			}
		} else {
			for (j in flags) {
				buf = buf C("this->" flags[j] "++;")
			}
		}
		buf = buf C("break;", "}")
	}
	buf = buf C(\
		"default: {",
		"libias_panic(\"unknown flag in option: %c\", arg[i]); // TODO diagnostic",
		"return PORTFMT_EXIT_HELP;// TODO diagnostic",
		"}")

	# end switch
	buf = buf C("}")
	# end for
	buf = buf C("}")

	# no match
	buf = buf C(\
		"} else {",
		"args_unwind(args);",
		"break;",
		"}")

	# close while loop
	buf = buf C("}")

	return buf
}

function generate_command_argparser(\
	cmd,
	buf, name, posargs, i, arg, read_arg, flag, flags)
{
	indent_level = 0

	name = cmd2cid(cmd)
	buf = C(\
		"enum PortfmtExitStatus",
		"read_arguments_" name "(",
		"struct Portfmt *this,",
		"struct PortfmtArgs *args)",
		"{")

	split(subcmds[cmd, "flags"], flags)
	for (i in flags) {
		buf = buf C("this->" flags[i] "++;")
	}

	if (subcmds[cmd, "options"]) {
		buf = buf C("enum PortfmtExitStatus status = PORTFMT_EXIT_OK;")
		buf = buf generate_command_argparser_getopt(cmd)
	} else if (subcmds[cmd, "positionals"]) {
		buf = buf C("enum PortfmtExitStatus status = PORTFMT_EXIT_OK;")
	}

	split(subcmds[cmd, "positionals"], posargs)
	for (i in posargs) {
		arg = posargs[i]
		buf = buf C("\n", "// " arg)

		read_arg = positionals[cmd, arg, "arg-read"]
		if (positionals[cmd, arg, "arg-greedy"]) {
			buf = buf C(\
				"while (args_peek(args)) {",
				"status = " read_arg "(this, args_pop(args));",
				"if (status != PORTFMT_EXIT_OK) {",
				"return status;",
				"}",
				"}")
		} else {
			buf = buf C(\
				"if (args_peek(args)) {",
				"status = " read_arg "(this, args_pop(args));",
				"if (status != PORTFMT_EXIT_OK) {",
				"return status;",
				"}")
			if (positionals[cmd, arg, "arg-required"]) {
				buf = buf C("} else {", "return PORTFMT_EXIT_HELP;")
			}
			buf = buf C("}")
		}
	}

	return buf C(\
		"\n",
		"// Leftover arguments?",
		"if (args_peek(args)) {",
		"libias_panic(\"leftover arguments\"); // TODO diagnostic",
		"return PORTFMT_EXIT_HELP;",
		"}",
		"\n",
		"return PORTFMT_EXIT_OK;",
		"}",
		"\n")
}

function generate_command_prototypes(\
	buf, cmds, i, cmd)
{
	indent_level = 0
	buf = C("// Prototypes")

	split(commands, cmds)
	for (i in cmds) {
		cmd = cmds[i]
		if (cmd ~ /^\$/) {
			continue
		}
		name = cmd2cid(cmd)
		buf = buf C("static enum PortfmtExitStatus read_arguments_" name "(struct Portfmt *this, struct PortfmtArgs *args);")
	}

	return buf
}

function generate_command_array(\
	buf, cmds, i, cmd, filter_cb, main_cb, query_cb, read_args, help, manual)
{
	indent_level = 0
	buf = C("struct PortfmtCommand subcommands[] = {")

	split(commands, cmds)
	for (i in cmds) {
		cmd = cmds[i]
		if (cmd ~ /^\$/) {
			continue
		}
		filter_cb = subcmds[cmd, "filter"]
		if (!filter_cb) {
			filter_cb = "NULL"
		}
		main_cb = subcmds[cmd, "main"]
		if (!main_cb) {
			main_cb = "NULL"
		}
		query_cb = subcmds[cmd, "query"]
		if (!query_cb) {
			query_cb = "NULL"
		}
		read_args = "NULL"
		read_args = "read_arguments_" cmd2cid(cmd)
		help = subcmds[cmd, "help"]
		manual = subcmds[cmd, "manual"]
		if (!manual) {
			manual = "portfmt"
		}

		buf = buf C(\
			"{",
			".name = \"" cmd "\",",
			".help = \"" help "\",",
			".manual = \"" manual "\",",
			".filter_callback = " filter_cb ",",
			".main_callback = " main_cb ",",
			".query_callback = " query_cb ",",
			".read_arguments = " read_args ",",
			"},")
	}
	buf = buf C("};", "const size_t subcommands_len = nitems(subcommands);")

	return buf
}

function generate_command_argparsers(\
	cmds, i, cmd, buf)
{
	split(commands, cmds)
	for (i in cmds) {
		cmd = cmds[i]
		if (cmd ~ /^\$/) {
			continue
		}
		buf = buf generate_command_argparser(cmd)
	}

	return buf
}

END {
	printf("// Generated file from %s. Do not edit.\n\n", FILENAME)
	print "#include \"config.h\""
	print "\n#include <stdlib.h>"
	print "#include <string.h>"
	print "\n#include <libias/str.h>"
	print "\n#include \"main.h\"\n"
	print generate_command_prototypes()
	print generate_command_argparsers()
	print generate_command_array()
}
