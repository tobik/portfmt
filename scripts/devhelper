#!/bin/sh
set -eu
: ${AWK:=awk}
: ${BSDTAR:=bsdtar}
: ${GIT:=git}
: ${GREP:=grep}
: ${MV:=mv}
: ${NINJA:=ninja}
: ${RM:=rm}
: ${SHA256:=sha256}
: ${SHA256SUM:=sha256sum}
: ${TEA:=tea}

configure() {
	CC="gcc"
	for arg in "$@"; do
		case "${arg}" in
			CC=*) CC="${arg#CC=*}" ;;
		esac
	done
	SANITIZE_FLAGS="-fsanitize=address,leak,undefined,bounds-strict"
	if ${CC} --version | ${AWK} '{exit($1 != "clang")}'; then
		SANITIZE_FLAGS="-fsanitize=address,leak,undefined -Wassign-enum"
	fi
	${RM} -rf _build/* _build/.ninja_deps _build/.ninja_log
	./configure \
		CC="${CC}" \
		LTO=0 \
		CFLAGS="-O0 -Werror ${SANITIZE_FLAGS} -fno-omit-frame-pointer" \
		LDFLAGS="${SANITIZE_FLAGS} -fno-omit-frame-pointer" \
		"$@"
	${NINJA} -C_build
}

tag() {
	V="$1"
	[ -z "${V}" ] && echo "must set version" && exit 1
	date=$(${GIT} log -1 --pretty=format:%cd --date=format:%Y-%m-%d HEAD)
	title="## [${V}] - ${date}"
	if ! ${GREP} -Fq "${title}" CHANGELOG.md; then
		${AWK} '/^## Unreleased$/{x=1;next}x{if($1=="##"){exit}else if($1=="###"){$1="##"};print}' \
			   CHANGELOG.md >RELNOTES.md.new
		${AWK} "/^## Unreleased$/{print;printf\"\n${title}\n\";next}{print}" \
			   CHANGELOG.md >CHANGELOG.md.new
		${MV} CHANGELOG.md.new CHANGELOG.md
		${MV} RELNOTES.md.new RELNOTES.md
	fi
	${GIT} commit -m "Release ${V}" CHANGELOG.md
	${GIT} tag -F RELNOTES.md "v${V}"
}

tarball() {
	tag=$(${GIT} tag --points-at HEAD)
	if [ -z "${tag}" ]; then
		echo "HEAD must point to a tag"
		exit 1
	fi
	V="$(echo ${tag} | sed 's,^v,,')"
	out="portfmt-${V}.tar.lz"
	${GIT} submodule update
	${GIT} ls-files --recurse-submodules . ':!:libias/tests' | \
		${BSDTAR} --files-from=- -s ",^,portfmt-${V}/," --options lzip:compression-level=9 \
			--uid 0 --gid 0 -caf "${out}"
	if ! ${SHA256} "${out}" >"${out}.SHA256"; then
		${SHA256SUM} --tag "${out}" >"${out}.SHA256"
	fi
	printf "SIZE (%s) = %s\n" "${out}" "$(wc -c <"${out}")" \
		>>"${out}.SHA256"
}

publish() {
	tag=$(${GIT} tag --points-at HEAD)
	if [ -z "${tag}" ]; then
		echo "HEAD must point to a tag"
		exit 1
	fi
	V="$(echo ${tag} | sed 's,^v,,')"
	out="portfmt-${V}.tar.lz"
	${GIT} push --follow-tags upstream
	${TEA} release create --tag "${tag}" --title "portfmt ${V}" \
		--note "$(cat RELNOTES.md)" \
		--asset "${out}" --asset "${out}.SHA256"
}

op="$1"; shift
case "${op}" in
	add-license) "$(dirname "$0")/../libias/scripts/add-license.sh" "$@" ;;
	configure|publish|tag|tarball) "${op}" "$@" ;;
	prototypes) "$(dirname "$0")/../libias/scripts/prototypes.sh" "$@" ;;
	*) exit 1 ;;
esac
