CSTD = gnu17
LDADD += -lm $LDADD_EXECINFO

default-features
	subpackages
if subpackages
	CPPFLAGS += -DPORTFMT_SUBPACKAGES=1
if !subpackages
	CPPFLAGS += -DPORTFMT_SUBPACKAGES=0
if mimalloc
	LDADD += $LDADD_mimalloc
pkg-config
	mimalloc if mimalloc

types
	LibiasArray
	LibiasBuffer
	LibiasDiff
	LibiasDirectory
	LibiasFile
	LibiasHashmap
	LibiasHashset
	LibiasIterator
	LibiasList
	LibiasListEntry         no-cleanup
	LibiasMap
	LibiasMempool
	LibiasPath
	LibiasQueue
	LibiasSet
	LibiasStack
	LibiasWorkqueue
	LibiasCompareTrait      no-cleanup
	LibiasHashTrait         no-cleanup
	#
	AST
	ASTBuilder                      prefix=ast_builder
	ASTBuilderToken                 no-cleanup
	ASTFormatSettings               no-cleanup
	ASTEdit                         prefix=ast_edit
	ASTQueryCache                   no-cleanup
	ASTVisit                        prefix=ast_visit
	ASTWordsEdit                    prefix=ast_words_edit
	ASTWord                         prefix=ast_word
	ASTWordExpandModifierBuilder    prefix=ast_word_expand_modifier_builder
	ASTWordExpandModifier           prefix=ast_word_expand_modifier
	Diagnostic
	LintUnknownVariable
	PortscanLog
	Regexp
	Rules
	SexpWriter

bundle libias.a
	subdir = $srcdir/libias
	libias/array.c
	libias/buffer.c
	libias/compats.c
	libias/diff.c
	libias/diffutil.c
	libias/hashmap.c
	libias/hashset.c
	libias/io.c
	libias/io/mkdirp.c
	libias/iterator.c
	libias/list.c
	libias/map.c
	libias/mempool.c
	libias/path.c
	libias/queue.c
	libias/runtime.c
	libias/set.c
	libias/stack.c
	libias/str.c
	libias/trait/compare.c
	libias/trait/hash.c
	libias/util.c
	libias/workqueue.c

bundle libportfmt.a
	ast.c
	ast/builder.c
	ast/builder/tokenizer.c
	ast/builder/word.c
	ast/format.c
	ast/format/normalize_append_modifier.c
	ast/loader.c
	ast/merge.c
	ast/query.c
	ast/word.c
	ast/word/expand/modifier.c
	$builddir/buildinfo.c
	constants.c
	edit/bump.c
	edit/version.c
	diagnostic.c
	$builddir/enum.c
	lint/bsd_port.c
	lint/clones.c
	lint/commented_portrevision.c
	lint/order.c
	lint/unknown_targets.c
	lint/unknown_variables.c
	portscan.c
	portscan/log.c
	portscan/status.c
	regexp.c
	rules.c
	sexp.c

gen $builddir/enum.c $srcdir/scripts/enum.awk
	ast.h
	ast/builder/enum.h
	ast/builder/word.h
	ast/word.h
	ast/word/expand/modifier.h
	mainutils.h
	portscan/log.h
	portscan/status.h
	rules.h

# To rebuild buildinfo.c on every commit add a pre-commit hook with
# touch CHANGELOG.md
gen $builddir/buildinfo.c $srcdir/scripts/buildinfo.awk
	CHANGELOG.md

gen $builddir/mainoptions.c $srcdir/scripts/options.awk
	options.spec

bin portfmt
	LDFLAGS += -pthread
	libias.a
	libportfmt.a
	main.c
	$builddir/mainoptions.c
	mainutils.c

install-man
	man/man1/portfmt.1
	man/man1/portfmt-lint.1
	man/man1/portfmt-scan.1

install-script
	scripts/portclippy
	scripts/portedit
	scripts/portscan

tests $builddir/libportfmt.a
	LDADD += $builddir/libias.a
	tests/unit-tests/ast.c

include $builddir/tests.ninja
