// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#if HAVE_CAPSICUM
# include <sys/capsicum.h>
#endif
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <libias/array.h>
#include <libias/io.h>
#include <libias/mempool.h>
#include <libias/path.h>
#include <libias/str.h>

#include "capsicum_helpers.h"
#include "mainutils.h"

// Prototypes
static struct LibiasFile *open_file_helper(const char *path, int flags, struct LibiasPath **retval);

void
enter_sandbox(void)
{
#if HAVE_CAPSICUM
	if (caph_limit_stderr() < 0) {
		libias_panic("caph_limit_stderr: %s", strerror(errno));
	}

	if (caph_enter() < 0) {
		libias_panic("caph_enter: %s", strerror(errno));
	}
#endif
#if HAVE_PLEDGE
	if (pledge("stdio", NULL) == -1) {
		libias_panic("pledge: %s", strerror(errno));
	}
#endif
}

struct LibiasFile *
open_file_helper(
	const char *path,
	int flags,
	struct LibiasPath **retval)
{
	libias_scope_mempool(pool);

	char pwd[PATH_MAX];
	if (getcwd(pwd, PATH_MAX) == NULL) {
		*retval = NULL;
		return NULL;
	}

	char *filename = libias_str_printf("%s/Makefile", path);
	libias_mempool_take(pool, filename);
	struct LibiasFile *f = libias_file_open(filename, flags, 0644);
	if (f == NULL) {
		f = libias_file_open(path, flags, 0644);
		if (f == NULL) {
			*retval = NULL;
			return NULL;
		}
		filename = libias_str_dup(path);
		libias_mempool_take(pool, filename);
	}
	libias_mempool_take(pool, f);

	filename = libias_mempool_take(pool, realpath(filename, NULL));
	if (filename == NULL) {
		*retval = NULL;
		return NULL;
	}

	if (libias_str_prefix_p(filename, pwd) && filename[strlen(pwd)] == '/') {
		filename = filename + strlen(pwd) + 1;
	}

	*retval = libias_path_from_cbuffer(filename, strlen(filename));
	libias_mempool_forget(pool, f);
	return f;
}

bool
open_file(
	uint32_t behavior,
	const char *path,
	struct LibiasFile **fp_in,
	struct LibiasFile **fp_out,
	struct LibiasPath **filename)
{
#if HAVE_CAPSICUM
	closefrom(STDERR_FILENO + 1);
#endif

	if (!path && (behavior & MAINUTILS_OPEN_FILE_INPLACE)) {
		return false;
	} else if (path) {
		if (behavior & MAINUTILS_OPEN_FILE_INPLACE) {
			if (!(behavior & MAINUTILS_OPEN_FILE_KEEP_STDIN)) {
				close(STDIN_FILENO);
			}
			close(STDOUT_FILENO);

			*fp_in = open_file_helper(path, O_RDWR, filename);
			*fp_out = *fp_in;
			if (*fp_in == NULL) {
				return false;
			}
#if HAVE_CAPSICUM
			if (caph_limit_stream(libias_file_fd(*fp_in), CAPH_READ | CAPH_WRITE | CAPH_FTRUNCATE) < 0) {
				libias_cleanup(fp_in);
				libias_cleanup(filename);
				return false;
			}
#endif
		} else  {
			if (!(behavior & MAINUTILS_OPEN_FILE_KEEP_STDIN)) {
				close(STDIN_FILENO);
			}
			*fp_out = libias_file_openfd(STDOUT_FILENO);
			if (*fp_out == NULL) {
				return false;
			}
			*fp_in = open_file_helper(path, O_RDONLY, filename);
			if (*fp_in == NULL) {
				libias_cleanup(fp_out);
				return false;
			}
#if HAVE_CAPSICUM
			if (caph_limit_stream(libias_file_fd(*fp_in), CAPH_READ) < 0) {
				libias_cleanup(filename);
				libias_cleanup(fp_in);
				libias_cleanup(fp_out);
				return false;
			}
			if (caph_limit_stdio() < 0) {
				libias_cleanup(filename);
				libias_cleanup(fp_in);
				libias_cleanup(fp_out);
				return false;
			}
#endif
		}
	} else {
		*fp_out = libias_file_openfd(STDOUT_FILENO);
		if (*fp_out == NULL) {
			return false;
		}
		*filename = libias_path_new("/dev/stdin");
		*fp_in = libias_file_openfd(STDIN_FILENO);
		if (*fp_in == NULL) {
			libias_cleanup(filename);
			libias_cleanup(fp_out);
			return false;
		}
#if HAVE_CAPSICUM
		if (caph_limit_stdio() < 0) {
			libias_cleanup(filename);
			libias_cleanup(fp_in);
			libias_cleanup(fp_out);
			return false;
		}
#endif
	}

	return true;
}
