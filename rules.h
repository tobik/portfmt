// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

/* Order is significant here and should match variable_order_ in rules.c */
enum BlockType {
	BLOCK_PORTNAME,			// human:"PORTNAME block"
	BLOCK_PATCHFILES,		// human:"Patch files"
	BLOCK_MAINTAINER,		// human:"Maintainer block"
	BLOCK_LICENSE,			// human:"License block"
	BLOCK_LICENSE_OLD,		// human:"Old-school license block (please replace with LICENSE)"
	BLOCK_BROKEN,			// human:"BROKEN/IGNORE/DEPRECATED messages"
	BLOCK_DEPENDS,			// human:"Dependencies"
	BLOCK_FLAVORS,			// human:"Flavors"
	BLOCK_FLAVORS_HELPER,	// human:"Flavors helpers"
#if PORTFMT_SUBPACKAGES
	BLOCK_SUBPACKAGES,		// human:"Subpackages block"
#endif
	BLOCK_USES,				// human:"USES block"
	BLOCK_SHEBANGFIX,		// human:"USES=shebangfix related variables"
	BLOCK_UNIQUEFILES,		// human:"USES=uniquefiles block"
	BLOCK_APACHE,			// human:"USES=apache related variables"
	BLOCK_ELIXIR,			// human:"USES=elixir related variables"
	BLOCK_EMACS,			// human:"USES=emacs related variables"
	BLOCK_ERLANG,			// human:"USES=erlang related variables"
	BLOCK_CMAKE,			// human:"USES=cmake related variables"
	BLOCK_CONFIGURE,		// human:"Configure block"
	BLOCK_QMAKE,			// human:"USES=qmake related variables"
	BLOCK_MESON,			// human:"USES=meson related variables"
	BLOCK_SCONS,			// human:"USES=scons related variables"
	BLOCK_CABAL,			// human:"USES=cabal related variables"
	BLOCK_CARGO,			// human:"USES=cargo related variables"
	BLOCK_GO,				// human:"USES=go related variables"
	BLOCK_LAZARUS,			// human:"USES=lazarus related variables"
	BLOCK_LINUX,			// human:"USES=linux related variables"
	BLOCK_NUGET,			// human:"USES=mono related variables"
	BLOCK_MAKE,				// human:"Make block"
	BLOCK_CFLAGS,			// human:"CFLAGS/CXXFLAGS/LDFLAGS block"
	BLOCK_CONFLICTS,		// human:"Conflicts"
	BLOCK_STANDARD,			// human:"Standard bsd.port.mk variables"
	BLOCK_WRKSRC,			// human:"WRKSRC block"
	BLOCK_USERS,			// human:"Users and groups block"
	BLOCK_PLIST,			// human:"Packaging list block"
	BLOCK_OPTDEF,			// human:"Options definitions"
	BLOCK_OPTDESC,			// human:"Options descriptions"
	BLOCK_OPTHELPER,		// human:"Options helpers"
	BLOCK_UNKNOWN,			// human:"Unknown variables"
};

const char *BlockType_human(enum BlockType);
const char *BlockType_tostring(enum BlockType);

struct RulesCompareTokens {
	struct Rules *rules;
	struct AST *root;
	const char *var;
};

struct RulesCompareOrder {
	struct Rules *rules;
	struct AST *root;
};

enum RulesBehavior {
	RULES_DEFAULT                   = 0,
	RULES_ALLOW_FUZZY_MATCHING      = 1 << 0,
	RULES_CHECK_VARIABLE_REFERENCES = 1 << 2,
};

enum ASTVariableModifier;

struct Rules *rules_new(uint32_t);

libias_attr_nonnull(1)
void rules_cleanup(struct Rules **);

int rules_compare_order(const void *, const void *, void *);
int rules_compare_target_order(const void *, const void *, void *);
int rules_compare_tokens(const void *, const void *, void *);

libias_attr_nonnull(1, 2, 3)
bool rules_ignore_wrap_col_p(struct Rules *, struct AST *, const char *, enum ASTVariableModifier);

int32_t rules_indent_goalcol(const char *, enum ASTVariableModifier);

libias_attr_nonnull(1, 2, 3)
bool rules_referenced_var_p(struct Rules *, struct AST *, const char *);

libias_attr_nonnull(1, 2, 3)
bool rules_known_target_p(struct Rules *, struct AST *, const char *);

bool rules_special_source_p(struct Rules *, const char *);
bool rules_special_target_p(struct Rules *, const char *);

libias_attr_nonnull(1, 2, 3)
bool rules_parse_variable_options_helper(struct Rules *, struct AST *, const char *, char **, char **, char **);

libias_attr_nonnull(1, 2, 3)
bool rules_leave_unformatted_p(struct Rules *, struct AST *, const char *);

libias_attr_nonnull(1, 2, 3)
bool rules_print_with_line_breaks_p(struct Rules *, struct AST *, const char *);

libias_attr_nonnull(1, 2, 3)
bool rules_should_sort_p(struct Rules *, struct AST *, const char *, enum ASTVariableModifier);

libias_attr_nonnull(1, 2, 3)
bool rules_skip_dedup_p(struct Rules *, struct AST *, const char *, enum ASTVariableModifier);

libias_attr_nonnull(1, 2, 3)
bool rules_skip_goalcol_p(struct Rules *, struct AST *, const char *);

bool rules_target_command_wrap_after_each_token_p(struct Rules *, const char *);
bool rules_target_command_should_wrap_p(struct Rules *, const char *);

libias_attr_nonnull(1, 2, 3)
enum BlockType rules_variable_order_block(struct Rules *, struct AST *, const char *, struct LibiasSet **);
