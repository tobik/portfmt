// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Generated by generate_rules.sh; do not edit
struct ConstantStaticFlavors {
	const char *uses;
	const char *flavor;
};
extern struct ConstantStaticFlavors static_flavors[];
extern size_t static_flavors_len;
extern const char *known_architectures[];
extern size_t known_architectures_len;
extern uint32_t freebsd_versions[];
extern size_t freebsd_versions_len;
extern uint32_t valid_ruby_versions[];
extern size_t valid_ruby_versions_len;
extern const char *valid_ssl_default[];
extern size_t valid_ssl_default_len;
extern const char *use_budgie_rel[];
extern const char *use_gnome_rel[];
extern const char *use_kde_rel[];
extern const char *use_pyqt_rel[];
extern const char *use_qt_rel[];
extern const char *static_shebang_langs[];
extern size_t use_budgie_rel_len;
extern size_t use_gnome_rel_len;
extern size_t use_kde_rel_len;
extern size_t use_pyqt_rel_len;
extern size_t use_qt_rel_len;
extern size_t static_shebang_langs_len;
