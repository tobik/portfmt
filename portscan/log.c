// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#if HAVE_CAPSICUM
# include <sys/capsicum.h>
#endif
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <libias/array.h>
#include <libias/diff.h>
#include <libias/hashset.h>
#include <libias/iterator.h>
#include <libias/io.h>
#include <libias/str.h>
#include <libias/trait/compare.h>

#include "capsicum_helpers.h"
#include "portscan/log.h"

struct PortscanLogDir {
	struct LibiasDirectory *dir;
	char *path;
	char *commit;
};

struct PortscanLog {
	struct LibiasArray *entries;
};

struct PortscanLogEntry {
	enum PortscanLogEntryType type;
	size_t index;
	char *origin;
	char *value;
};

// Prototypes
static libias_declare_compare(compare_log_entry);
static char *log_commit(struct LibiasDirectory *portsdir);
static struct PortscanLogEntry *log_entry_parse(const char *s);
static char *log_entry_tostring(const struct PortscanLogEntry *entry);
static char *log_filename(const char *commit);
static bool log_update_latest(struct PortscanLogDir *logdir, const char *log_path);
static void portscan_log_entry_free(struct PortscanLogEntry *entry);
static void portscan_log_sort(struct PortscanLog *log);

// Constants
static const char *PORTSCAN_LOG_DATE_FORMAT = "portscan-%Y%m%d%H%M%S";
static const char *PORTSCAN_LOG_INIT = "/dev/null";
static struct LibiasCompareTrait *log_entry_compare = &(struct LibiasCompareTrait){
	.compare = compare_log_entry,
	.context = NULL,
};

struct PortscanLog *
portscan_log_new(void)
{
	struct PortscanLog *log = libias_alloc(struct PortscanLog);
	log->entries = libias_array_new();
	return log;
}

void
portscan_log_cleanup(struct PortscanLog **log_ptr)
{
	struct PortscanLog *log = *log_ptr;

	if (log == NULL) {
		return;
	}
	libias_array_foreach(log->entries, struct PortscanLogEntry *, entry) {
		portscan_log_entry_free(entry);
	}
	libias_cleanup(&log->entries);
	libias_free(log);

	*log_ptr = NULL;
}

void
portscan_log_sort(struct PortscanLog *log)
{
	libias_array_sort(log->entries, log_entry_compare);
}

size_t
portscan_log_len(struct PortscanLog *log)
{
	return libias_array_len(log->entries);
}

char *
log_entry_tostring(const struct PortscanLogEntry *entry)
{
	switch (entry->type) {
	case PORTSCAN_LOG_ENTRY_UNKNOWN_VAR:
		return libias_str_printf("%-7c %-40s %s\n", 'V', entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_UNKNOWN_TARGET:
		return libias_str_printf("%-7c %-40s %s\n", 'T', entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_DUPLICATE_VAR:
		return libias_str_printf("%-7s %-40s %s\n", "Vc", entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_OPTION_DEFAULT_DESCRIPTION:
		return libias_str_printf("%-7s %-40s %s\n", "OD", entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_OPTION_GROUP:
		return libias_str_printf("%-7s %-40s %s\n", "OG", entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_OPTION:
		return libias_str_printf("%-7c %-40s %s\n", 'O', entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_CATEGORY_NONEXISTENT_PORT:
		return libias_str_printf("%-7s %-40s %s\n", "Ce", entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_CATEGORY_UNHOOKED_PORT:
		return libias_str_printf("%-7s %-40s %s\n", "Cu", entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_CATEGORY_UNSORTED:
		return libias_str_printf("%-7c %-40s %s\n", 'C', entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_ERROR:
		return libias_str_printf("%-7c %-40s %s\n", 'E', entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_VARIABLE_VALUE:
		return libias_str_printf("%-7s %-40s %s\n", "Vv", entry->origin, entry->value);
	case PORTSCAN_LOG_ENTRY_COMMENT:
		return libias_str_printf("%-7c %-40s %s\n", '#', entry->origin, entry->value);
	}

	libias_panic("unhandled portscan log entry type: %d", entry->type);
}

void
portscan_log_add_entries(
	struct PortscanLog *log,
	enum PortscanLogEntryType type,
	const char *origin,
	struct LibiasHashset *values)
{
	if (values == NULL) {
		return;
	}

	libias_hashset_foreach(values, const char *, value) {
		portscan_log_add_entry(log, type, origin, value);
	}
}

void
portscan_log_entry_free(struct PortscanLogEntry *entry)
{
	if (entry) {
		libias_free(entry->origin);
		libias_free(entry->value);
		libias_free(entry);
	}
}

void
portscan_log_add_entry(
	struct PortscanLog *log,
	enum PortscanLogEntryType type,
	const char *origin,
	const char *value)
{
	struct PortscanLogEntry *entry = libias_alloc(struct PortscanLogEntry);
	entry->type = type;
	entry->index = libias_array_len(log->entries);
	entry->origin = libias_str_dup(origin);
	entry->value = libias_str_dup(value);
	libias_array_append(log->entries, entry);
}

struct PortscanLogEntry *
log_entry_parse(const char *s)
{
	enum PortscanLogEntryType type = PORTSCAN_LOG_ENTRY_UNKNOWN_VAR;
	if (libias_str_prefix_p(s, "V ")) {
		type = PORTSCAN_LOG_ENTRY_UNKNOWN_VAR;
		s++;
	} else if (libias_str_prefix_p(s, "T ")) {
		type = PORTSCAN_LOG_ENTRY_UNKNOWN_TARGET;
		s++;
	} else if (libias_str_prefix_p(s, "Vc ")) {
		type = PORTSCAN_LOG_ENTRY_DUPLICATE_VAR;
		s += 2;
	} else if (libias_str_prefix_p(s, "OD ")) {
		type = PORTSCAN_LOG_ENTRY_OPTION_DEFAULT_DESCRIPTION;
		s += 2;
	} else if (libias_str_prefix_p(s, "OG ")) {
		type = PORTSCAN_LOG_ENTRY_OPTION_GROUP;
		s += 2;
	} else if (libias_str_prefix_p(s, "O ")) {
		type = PORTSCAN_LOG_ENTRY_OPTION;
		s++;
	} else if (libias_str_prefix_p(s, "Ce ")) {
		type = PORTSCAN_LOG_ENTRY_CATEGORY_NONEXISTENT_PORT;
		s += 2;
	} else if (libias_str_prefix_p(s, "Cu ")) {
		type = PORTSCAN_LOG_ENTRY_CATEGORY_UNHOOKED_PORT;
		s += 2;
	} else if (libias_str_prefix_p(s, "C ")) {
		type = PORTSCAN_LOG_ENTRY_CATEGORY_UNSORTED;
		s++;
	} else if (libias_str_prefix_p(s, "E ")) {
		type = PORTSCAN_LOG_ENTRY_ERROR;
		s++;
	} else if (libias_str_prefix_p(s, "Vv ")) {
		type = PORTSCAN_LOG_ENTRY_VARIABLE_VALUE;
		s += 2;
	} else if (libias_str_prefix_p(s, "# ")) {
		type = PORTSCAN_LOG_ENTRY_COMMENT;
		s++;
	} else {
		fprintf(stderr, "unable to parse log entry: %s\n", s);
		return NULL;
	}

	while (*s != 0 && isspace((unsigned char)*s)) {
		s++;
	}
	const char *origin_start = s;
	while (*s != 0 && !isspace((unsigned char)*s)) {
		s++;
	}
	const char *value = s;
	while (*value != 0 && isspace((unsigned char)*value)) {
		value++;
	}
	size_t value_len = strlen(value);
	if (value_len > 0 && value[value_len - 1] == '\n') {
		value_len--;
	}

	if ((s - origin_start) == 0 || value_len == 0) {
		fprintf(stderr, "unable to parse log entry: %s\n", s);
		return NULL;
	}

	struct PortscanLogEntry *e = libias_alloc(struct PortscanLogEntry);
	e->type = type;
	e->origin = libias_str_ndup(origin_start, s - origin_start);
	e->value = libias_str_ndup(value, value_len);
	return e;
}

libias_define_compare(compare_log_entry, struct PortscanLogEntry, void)
{
	int retval = strcmp(a->origin, b->origin);
	if (retval == 0) {
		if (a->type > b->type) {
			retval = 1;
		} else if (a->type < b->type) {
			retval = -1;
		} else {
			retval = strcmp(a->value, b->value);
		}
	}

	return retval;
}

int
portscan_log_compare(struct PortscanLog *prev, struct PortscanLog *log)
{
	portscan_log_sort(prev);
	portscan_log_sort(log);

	struct LibiasDiff *p = libias_array_diff(
		prev->entries,
		log->entries,
		log_entry_compare);
	if (p == NULL) {
		libias_panic("libias_array_diff() failed");
	}
	int equal = 1;
	for (size_t i = 0; i < p->sessz; i++) {
		if (p->ses[i].type != LIBIAS_DIFF_COMMON) {
			equal = 0;
			break;
		}
	}

	libias_cleanup(&p);
	return equal;
}

bool
portscan_log_serialize_to_file(
	struct PortscanLog *log,
	struct LibiasFile *out)
{
	portscan_log_sort(log);

	libias_array_foreach(log->entries, struct PortscanLogEntry *, entry) {
		char *line = log_entry_tostring(entry);
		ssize_t written = libias_file_puts(out, line);
		libias_free(line);
		if (written == -1) {
			return false;
		}
	}

	return true;
}

bool
log_update_latest(struct PortscanLogDir *logdir, const char *log_path)
{
	char *prev = NULL;
	bool ok = libias_symlink_update(logdir->dir, log_path, PORTSCAN_LOG_LATEST, &prev);
	if (!ok) {
		return false;
	}

	bool updated = false;
	if (prev) {
		updated = libias_symlink_update(
			logdir->dir,
			prev,
			PORTSCAN_LOG_PREVIOUS,
			NULL);
		libias_free(prev);
	}

	return updated;
}

char *
log_filename(const char *commit)
{
	time_t date = time(NULL);
	if (date == -1) {
		libias_panic("time: %s", strerror(errno));
	}
	struct tm *tm = gmtime(&date);

	char buf[PATH_MAX];
	if (strftime(buf, sizeof(buf), PORTSCAN_LOG_DATE_FORMAT, tm) == 0) {
		libias_panic("strftime: %s", strerror(errno));
	}

	return libias_str_printf("%s-%s.log", buf, commit);
}

bool
portscan_log_serialize_to_dir(struct PortscanLog *log, struct PortscanLogDir *logdir)
{
	char *log_path = log_filename(logdir->commit);
	libias_scope_free(log_path);
	struct LibiasFile *out = libias_file_openat(logdir->dir, log_path, O_WRONLY | O_CREAT, 0644);
	libias_scope_free(out);
	return out
		&& portscan_log_serialize_to_file(log, out)
		&& log_update_latest(logdir, log_path);
}

char *
log_commit(struct LibiasDirectory *portsdir)
{
	if (libias_directory_chdir(portsdir) == -1) {
		libias_panic("fchdir: %s", strerror(errno));
	}

	FILE *fp = popen("git rev-parse HEAD 2>/dev/null", "r");
	if (fp == NULL) {
		libias_panic("popen: %s", strerror(errno));
	}

	char *revision = NULL;

	struct LibiasFile *file = libias_file_openfd(fileno(fp));
	libias_file_line_foreach(file, line) {
		revision = libias_str_printf("%s", line);
		break;
	}
	libias_cleanup(&file);
	pclose(fp);

	if (revision == NULL) {
		revision = libias_str_dup("unknown");
	}
	return revision;
}

struct PortscanLogDir *
portscan_log_dir_open(
	const char *logdir_path,
	struct LibiasDirectory *portsdir)
{
	int created_dir = 0;
	struct LibiasDirectory *logdir;
	while ((logdir = libias_directory_open(logdir_path)) == NULL) {
		if (errno == ENOENT) {
			if (!libias_mkdirp(logdir_path)) {
				return NULL;
			}
			created_dir = 1;
		} else {
			return NULL;
		}
	}
	if (created_dir) {
		if (!libias_symlink_update(logdir, PORTSCAN_LOG_INIT, PORTSCAN_LOG_PREVIOUS, NULL)) {
			goto error;
		}
		if (!libias_symlink_update(logdir, PORTSCAN_LOG_INIT, PORTSCAN_LOG_LATEST, NULL)) {
			goto error;
		}
	} else {
		char *prev = libias_symlink_read(logdir, PORTSCAN_LOG_PREVIOUS);
		if (prev == NULL &&
		    !libias_symlink_update(logdir, PORTSCAN_LOG_INIT, PORTSCAN_LOG_PREVIOUS, NULL)) {
			goto error;
		}
		libias_free(prev);

		char *latest = libias_symlink_read(logdir, PORTSCAN_LOG_LATEST);
		if (latest == NULL &&
		    !libias_symlink_update(logdir, PORTSCAN_LOG_INIT, PORTSCAN_LOG_LATEST, NULL)) {
			goto error;
		}
		libias_free(latest);
	}

#if HAVE_CAPSICUM
	if (caph_limit_stream(libias_directory_fd(logdir), CAPH_CREATE | CAPH_FTRUNCATE | CAPH_READ | CAPH_SYMLINK) < 0) {
		libias_panic("caph_limit_stream: %s", strerror(errno));
	}
#endif
	struct PortscanLogDir *dir = libias_alloc(struct PortscanLogDir);
	dir->dir = logdir;
	dir->path = libias_str_dup(logdir_path);
	dir->commit = log_commit(portsdir);

	return dir;

error:
	libias_cleanup(&logdir);
	return NULL;
}

void
portscan_log_dir_close(struct PortscanLogDir *dir)
{
	if (dir == NULL) {
		return;
	}
	libias_cleanup(&dir->dir);
	libias_free(dir->path);
	libias_free(dir->commit);
	libias_free(dir);
}

struct PortscanLog *
portscan_log_read_all(
	struct PortscanLogDir *logdir,
	const char *log_path)
{
	struct PortscanLog *log = portscan_log_new();

	char *buf = libias_symlink_read(logdir->dir, log_path);
	libias_scope_free(buf);
	if (buf == NULL) {
		if (errno == ENOENT) {
			return log;
		} else if (errno != EINVAL) {
			libias_panic("symlink_read: %s: %s", log_path, strerror(errno));
		}
	} else if (strcmp(buf, PORTSCAN_LOG_INIT) == 0) {
		return log;
	}

	struct LibiasFile *fp = libias_file_openat(logdir->dir, log_path, O_RDONLY, 0);
	if (fp == NULL) {
		if (errno == ENOENT) {
			return log;
		}
		libias_panic("openat: %s: %s", log_path, strerror(errno));
	}

	libias_file_line_foreach(fp, line) {
		struct PortscanLogEntry *entry = log_entry_parse(line);
		if (entry != NULL) {
			libias_array_append(log->entries, entry);
		}
	}

	libias_cleanup(&fp);

	portscan_log_sort(log);

	return log;
}

