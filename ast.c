// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/hashmap.h>
#include <libias/hashset.h>
#include <libias/io.h>
#include <libias/iterator.h>
#include <libias/list.h>
#include <libias/map.h>
#include <libias/mempool.h>
#include <libias/queue.h>
#include <libias/str.h>
#include <libias/trait/compare.h>
#include <libias/trait/hash.h>
#include <libias/util.h>

#include "ast.h"
#include "ast/builder.h"
#include "ast/format.h"
#include "ast/word.h"
#include "diagnostic.h"
#include "rules.h"
#include "sexp.h"

enum ASTVisitState {
	AST_VISIT_STATE_CONTINUE,
	AST_VISIT_STATE_ABORT,
	AST_VISIT_STATE_STOP,
};

struct ASTEdit {
	struct Diagnostic *external_diagnostic;
	struct Diagnostic *diagnostic;
	enum ASTEditFlags flags;
	struct AST *root;
	struct LibiasQueue *queue;
	struct LibiasMempool *pool;
};

struct ASTWordsEdit {
	struct Diagnostic *external_diagnostic;
	struct Diagnostic *diagnostic;
	enum ASTWordsEditFlags flags;
	struct AST *node;
	struct LibiasQueue *queue;
	struct LibiasMempool *pool;
};

struct ASTChildrenIterator {
	struct AST *node;
	struct LibiasListEntry *entry;
	size_t i;
	size_t len;
};

struct ASTWordsIterator {
	struct AST *node;
	struct LibiasListEntry *entry;
	size_t i;
	size_t len;
};

struct ASTSortWordsData {
	struct LibiasCompareTrait *compare;
	struct ASTWord **words;
};

struct ASTVisit {
	struct ASTVisitTrait trait;
	struct Diagnostic *diagnostic;
	void *context;
	struct {
		struct LibiasHashset *pre;
		struct LibiasHashset *post;
	} visited_nodes;
	enum ASTVisitState state;
	struct AST *root;
	struct ASTEdit *root_ast_edit;

	struct {
		struct LibiasQueue *queue;
		struct ASTFormatSettings *settings;
		struct Rules *rules;
	} format;
};

struct AST {
	const enum ASTType type;

	struct AST *parent;
	struct {
		struct LibiasList *list; // [struct AST *]
		struct LibiasHashmap *child_to_list_entry_map; // Hashmap<AST,LibiasListEntry>
	} children;

	// We uphold the invariant that the word list has only one
	// AST_WORD_WHITESPACE (_) in between the other word types (W) like
	// _W_W_W_W_W_W_, _W_W_W_W, or W_W_W_W_W. We want to avoid something
	// like WWWWWW, W___W, or __W__. This means that when we add new
	// words we make sure there is a " " after it unless it's the last
	// word, or that we merge __ to a single _.
	struct {
		struct LibiasList *list; // [struct ASTWord *]
		struct LibiasHashmap *word_to_list_entry_map; // Hashmap<ASTWord,LibiasListEntry>
	} words;

	char *indent;
	struct ASTQueryCache *query_cache;
	void (*query_cache_cleanup)(struct ASTQueryCache **);

	struct AST *(*new)(void);
	void (*free)(struct AST *);
	void (*clone)(struct AST *, struct AST *);
	void (*render)(struct AST *, struct LibiasBuffer *);
	void (*sexp)(struct AST *, struct SexpWriter *);
	void (*words_validate)(struct AST *, struct Diagnostic *);

	struct {
		enum ASTExprType type;
	} expr;

	struct {
		enum ASTIfType type;
	} if_branch;

	struct {
		enum ASTIncludeType type;
		bool loaded;
	} include;

	struct {
		char *flags;
	} target_command;

	struct {
		enum ASTTargetRuleOperator operator;
	} target_rule;

	struct {
		enum ASTVariableModifier modifier;
	} variable;
};

#define MAKE_AST_NODE(type, fun) \
	struct AST *this = libias_alloc(struct AST); \
	this->indent = libias_str_dup(""); \
	this->clone = ast_clone_##fun; \
	this->new = ast_new_##fun; \
	this->free = ast_free_##fun; \
	this->render = ast_render_##fun; \
	this->sexp = ast_sexp_##fun; \
	this->words_validate = ast_words_validate_##fun; \
	this->parent = this; \
	SET_AST_TYPE(this, type); \
	ast_word_list_init(this);

#define SET_AST_TYPE(this, ast_type) \
	do { \
		enum ASTType type = (ast_type); \
		memcpy((void *)&(this)->type, &type, sizeof(enum ASTType)); \
	} while (false);

// Prototypes
static struct LibiasListEntry *ast_child_list_append(struct AST *node, struct AST *new_child);
static struct LibiasListEntry *ast_child_list_entry_for_node(struct AST *node, struct AST *child);
static struct LibiasListEntry *ast_child_list_entry_remove(struct AST *node, struct LibiasListEntry *entry);
static void ast_child_list_init(struct AST *this);
static struct LibiasListEntry *ast_child_list_insert_after(struct AST *node, struct LibiasListEntry *list_entry, struct AST *new_child);
static struct LibiasListEntry *ast_child_list_insert_before(struct AST *node, struct LibiasListEntry *list_entry, struct AST *new_child);
static void ast_children_iterator_cleanup(void **this_ptr);
static bool ast_children_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);
static void ast_clone_children(struct AST *node, struct AST *template);
static void ast_clone_comment(struct AST *node, struct AST *template);
static void ast_clone_expr(struct AST *node, struct AST *template);
static void ast_clone_for(struct AST *node, struct AST *template);
static void ast_clone_for_bindings(struct AST *node, struct AST *template);
static void ast_clone_for_body(struct AST *node, struct AST *template);
static void ast_clone_for_end(struct AST *node, struct AST *template);
static void ast_clone_for_words(struct AST *node, struct AST *template);
static struct AST *ast_clone_helper(struct AST *template, struct AST *parent);
static void ast_clone_if(struct AST *node, struct AST *template);
static void ast_clone_if_branch(struct AST *node, struct AST *template);
static void ast_clone_include(struct AST *node, struct AST *template);
static void ast_clone_root(struct AST *node, struct AST *template);
static void ast_clone_target_command(struct AST *node, struct AST *template);
static void ast_clone_target_rule(struct AST *node, struct AST *template);
static void ast_clone_target_rule_body(struct AST *node, struct AST *template);
static void ast_clone_target_rule_dependencies(struct AST *node, struct AST *template);
static void ast_clone_target_rule_targets(struct AST *node, struct AST *template);
static void ast_clone_variable(struct AST *node, struct AST *template);
static void ast_clone_variable_name(struct AST *node, struct AST *template);
static void ast_clone_words(struct AST *node, struct AST *template);
static void ast_edit_add_child_before_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_add_child_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_append_child_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_apply_words_edit_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_chain_apply_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_remove_all_children_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_remove_child_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_replace_child_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_set_target_rule_operator_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static void ast_edit_set_variable_modifier_impl(struct ASTEdit *edit, struct LibiasQueue *args);
static struct LibiasListEntry *ast_find_child(struct AST *root, struct AST *child, struct AST **parent);
static void ast_free_comment(struct AST *node);
static void ast_free_expr(struct AST *node);
static void ast_free_for(struct AST *node);
static void ast_free_for_bindings(struct AST *node);
static void ast_free_for_body(struct AST *node);
static void ast_free_for_end(struct AST *node);
static void ast_free_for_words(struct AST *node);
static void ast_free_if(struct AST *node);
static void ast_free_if_branch(struct AST *node);
static void ast_free_include(struct AST *node);
static void ast_free_root(struct AST *node);
static void ast_free_target_command(struct AST *node);
static void ast_free_target_rule(struct AST *node);
static void ast_free_target_rule_body(struct AST *node);
static void ast_free_target_rule_dependencies(struct AST *node);
static void ast_free_target_rule_targets(struct AST *node);
static void ast_free_variable(struct AST *node);
static void ast_free_variable_name(struct AST *node);
static struct AST *ast_new_for_bindings(void);
static struct AST *ast_new_for_body(void);
static struct AST *ast_new_for_end(void);
static struct AST *ast_new_for_words(void);
static struct AST *ast_new_target_rule_body(void);
static struct AST *ast_new_target_rule_dependencies(void);
static struct AST *ast_new_target_rule_targets(void);
static struct AST *ast_new_variable_name(void);
static void ast_remove_from_parent(struct AST *node);
static void ast_render_comment(struct AST *node, struct LibiasBuffer *f);
static void ast_render_expr(struct AST *node, struct LibiasBuffer *f);
static void ast_render_for(struct AST *node, struct LibiasBuffer *f);
static void ast_render_for_bindings(struct AST *node, struct LibiasBuffer *f);
static void ast_render_for_body(struct AST *node, struct LibiasBuffer *f);
static void ast_render_for_end(struct AST *node, struct LibiasBuffer *f);
static void ast_render_for_words(struct AST *node, struct LibiasBuffer *f);
static void ast_render_if(struct AST *node, struct LibiasBuffer *f);
static void ast_render_if_branch(struct AST *node, struct LibiasBuffer *f);
static void ast_render_include(struct AST *node, struct LibiasBuffer *f);
static void ast_render_root(struct AST *node, struct LibiasBuffer *f);
static void ast_render_target_command(struct AST *node, struct LibiasBuffer *f);
static void ast_render_target_rule(struct AST *node, struct LibiasBuffer *f);
static void ast_render_target_rule_body(struct AST *node, struct LibiasBuffer *f);
static void ast_render_target_rule_dependencies(struct AST *node, struct LibiasBuffer *f);
static void ast_render_target_rule_targets(struct AST *node, struct LibiasBuffer *f);
static void ast_render_variable(struct AST *node, struct LibiasBuffer *f);
static void ast_render_variable_name(struct AST *node, struct LibiasBuffer *f);
static void ast_sexp_comment(struct AST *node, struct SexpWriter *w);
static void ast_sexp_expr(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_bindings(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_body(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_end(struct AST *node, struct SexpWriter *w);
static void ast_sexp_for_words(struct AST *node, struct SexpWriter *w);
static void ast_sexp_helper(struct AST *node, struct SexpWriter *w);
static void ast_sexp_if(struct AST *node, struct SexpWriter *w);
static void ast_sexp_if_branch(struct AST *node, struct SexpWriter *w);
static void ast_sexp_include(struct AST *node, struct SexpWriter *w);
static void ast_sexp_root(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_command(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule_body(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule_dependencies(struct AST *node, struct SexpWriter *w);
static void ast_sexp_target_rule_targets(struct AST *node, struct SexpWriter *w);
static void ast_sexp_variable(struct AST *node, struct SexpWriter *w);
static void ast_sexp_variable_name(struct AST *node, struct SexpWriter *w);
static void ast_sexp_words(struct AST *node, const char *name, struct SexpWriter *w);
static void ast_visit_dispatch(struct ASTVisit *visit, struct AST *node);
static void ast_visit_done(struct ASTVisit *visit);
static void ast_visit_helper(struct ASTVisit *visit, struct AST *node);
static void ast_visit_pre_dispatch(struct ASTVisit *visit, struct AST *node);
static void ast_visit_reinit(struct ASTVisit *visit);
static ASTVisitFn ast_visit_trait_get_pre_visit_fn(struct ASTVisitTrait *trait, enum ASTType type);
static ASTVisitFn ast_visit_trait_get_visit_fn(struct ASTVisitTrait *trait, enum ASTType type);
static struct LibiasListEntry *ast_word_list_append(struct AST *node, struct Diagnostic *diagnostic, struct ASTWord *word);
static struct LibiasListEntry *ast_word_list_entry_for_word(struct AST *node, struct ASTWord *word);
static struct LibiasListEntry *ast_word_list_entry_remove(struct AST *node, struct LibiasListEntry *list_entry);
static void ast_word_list_entry_replace_word(struct AST *node, struct LibiasListEntry *list_entry, struct ASTWord *new_word);
static void ast_word_list_init(struct AST *this);
static struct LibiasListEntry *ast_word_list_insert_after(struct AST *node, struct Diagnostic *diagnostic, struct LibiasListEntry *head, struct ASTWord *word, bool replace);
static struct LibiasListEntry *ast_word_list_insert_before(struct AST *node, struct Diagnostic *diagnostic, struct LibiasListEntry *head, struct ASTWord *word, bool replace);
static void ast_words_edit_add_word_before_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_add_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_append_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_remove_all_words_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_remove_beginning_whitespace_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_remove_trailing_whitespace_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_remove_whitespace_after_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_remove_whitespace_before_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_remove_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_replace_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_set_whitespace_after_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_edit_set_whitespace_before_word_impl(struct ASTWordsEdit *edit, struct LibiasQueue *args);
static void ast_words_iterator_cleanup(void **this_ptr);
static bool ast_words_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);
static void ast_words_validate(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_comment(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_expr(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_for(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_for_bindings(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_for_body(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_for_end(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_for_words(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_if(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_if_branch(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_include(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_root(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_target_command(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_target_rule(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_target_rule_body(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_target_rule_dependencies(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_target_rule_targets(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_variable(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_variable_name(struct AST *node, struct Diagnostic *diagnostic);
static void ast_words_validate_whitespace(struct AST *node, struct Diagnostic *diagnostic);
static int compare_ast_sort_words(const void *ap, const void *bp, void *userdata);

void
ast_child_list_init(struct AST *this)
{
	this->children.list = libias_list_new();
	this->children.child_to_list_entry_map = libias_hashmap_new(
		libias_compare_ptr,
		libias_hash_ptr);
}

void
ast_word_list_init(struct AST *this)
{
	this->words.list = libias_list_new();
	this->words.word_to_list_entry_map = libias_hashmap_new(
		libias_compare_ptr,
		libias_hash_ptr);
}

// Tries to parse and generate an AST from the contents of
// BUFFER. Returns the generated AST or NULL in case of error in which
// case DIAGNOSTIC will be filled with parsing errors.
struct AST *
ast_from_buffer(
	struct LibiasBuffer *buffer,
	struct Diagnostic *diagnostic,
	const char *filename)
{
	struct ASTBuilder *builder = ast_builder_new(buffer, diagnostic, filename);
	libias_scope_free(builder);
	struct AST *ast = ast_builder_finish(builder);
	if (ast) {
		return ast;
	} else {
		return NULL;
	}
}

// Tries to parse and generate an AST from the contents of
// FILE. Returns the generated AST or NULL in case of error in which
// case DIAGNOSTIC will be filled with parsing or IO errors.
struct AST *
ast_from_file(
	struct LibiasFile *file,
	struct Diagnostic *diagnostic,
	const char *filename)
{
	auto buf = libias_file_slurp(file);
	if (buf) {
		auto ast = ast_from_buffer(buf, diagnostic, filename);
		libias_cleanup(&buf);
		return ast;
	} else {
		diagnostic_io_error(diagnostic, filename, "failed to read file");
		return NULL;
	}
}

struct AST *
ast_new_comment(void)
{
	MAKE_AST_NODE(AST_COMMENT, comment);

	return this;
}

void
ast_free_comment(struct AST *node)
{
}

struct AST *
ast_new_expr(void)
{
	MAKE_AST_NODE(AST_EXPR, expr);

	this->expr.type = AST_EXPR_ERROR;

	return this;
}

void
ast_free_expr(struct AST *node)
{
}

struct AST *
ast_new_for(void)
{
	MAKE_AST_NODE(AST_FOR, for);

	ast_child_list_init(this);

	struct AST *bindings = ast_new_for_bindings();
	bindings->parent = this;
	ast_child_list_append(this, bindings);

	struct AST *words = ast_new_for_words();
	words->parent = this;
	ast_child_list_append(this, words);

	struct AST *body = ast_new_for_body();
	body->parent = this;
	ast_child_list_append(this, body);

	struct AST *end = ast_new_for_end();
	end->parent = this;
	ast_child_list_append(this, end);

	return this;
}

void
ast_free_for(struct AST *node)
{
}

struct AST *
ast_new_for_bindings(void)
{
	MAKE_AST_NODE(AST_FOR_BINDINGS, for_bindings);

	return this;
}

void
ast_free_for_bindings(struct AST *node)
{
}

struct AST *
ast_new_for_body(void)
{
	MAKE_AST_NODE(AST_FOR_BODY, for_body);

	ast_child_list_init(this);

	return this;
}

void
ast_free_for_body(struct AST *node)
{
}

struct AST *
ast_new_for_end(void)
{
	MAKE_AST_NODE(AST_FOR_END, for_end);

	return this;
}

void
ast_free_for_end(struct AST *node)
{
}

struct AST *
ast_new_for_words(void)
{
	MAKE_AST_NODE(AST_FOR_WORDS, for_words);

	return this;
}

void
ast_free_for_words(struct AST *node)
{
}

struct AST *
ast_new_if(void)
{
	MAKE_AST_NODE(AST_IF, if);

	ast_child_list_init(this);

	return this;
}

void
ast_free_if(struct AST *node)
{
}

struct AST *
ast_new_if_branch(void)
{
	MAKE_AST_NODE(AST_IF_BRANCH, if_branch);

	this->if_branch.type = AST_IF_IF;
	ast_child_list_init(this);

	return this;
}

void
ast_free_if_branch(struct AST *node)
{
}

struct AST *
ast_new_include(void)
{
	MAKE_AST_NODE(AST_INCLUDE, include);

	this->include.type = AST_INCLUDE_BMAKE;
	ast_child_list_init(this);

	return this;
}

void
ast_free_include(struct AST *node)
{
}

struct AST *
ast_new_root(void)
{
	MAKE_AST_NODE(AST_ROOT, root);

	ast_child_list_init(this);

	return this;
}

void
ast_free_root(struct AST *node)
{
}

struct AST *
ast_new_target_command(void)
{
	MAKE_AST_NODE(AST_TARGET_COMMAND, target_command);

	this->target_command.flags = libias_str_dup("");

	return this;
}

void
ast_free_target_command(struct AST *node)
{
	libias_free(node->target_command.flags);
}

struct AST *
ast_new_target_rule(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE, target_rule);

	this->target_rule.operator = AST_TARGET_RULE_OPERATOR_1;
	ast_child_list_init(this);

	struct AST *targets = ast_new_target_rule_targets();
	targets->parent = this;
	ast_child_list_append(this, targets);

	struct AST *dependencies = ast_new_target_rule_dependencies();
	dependencies->parent = this;
	ast_child_list_append(this, dependencies);

	struct AST *body = ast_new_target_rule_body();
	body->parent = this;
	ast_child_list_append(this, body);

	return this;
}

void
ast_free_target_rule(struct AST *node)
{
}

struct AST *
ast_new_target_rule_targets(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE_TARGETS, target_rule_targets);

	return this;
}

void
ast_free_target_rule_targets(struct AST *node)
{
}

struct AST *
ast_new_target_rule_dependencies(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE_DEPENDENCIES, target_rule_dependencies);

	return this;
}

void
ast_free_target_rule_dependencies(struct AST *node)
{
}

struct AST *
ast_new_target_rule_body(void)
{
	MAKE_AST_NODE(AST_TARGET_RULE_BODY, target_rule_body);

	ast_child_list_init(this);

	return this;
}

void
ast_free_target_rule_body(struct AST *node)
{
}

struct AST *
ast_new_variable(void)
{
	MAKE_AST_NODE(AST_VARIABLE, variable);

	this->variable.modifier = AST_VARIABLE_MODIFIER_ASSIGN;
	ast_child_list_init(this);

	struct AST *name = ast_new_variable_name();
	name->parent = this;
	ast_child_list_append(this, name);

	return this;
}

void
ast_free_variable(struct AST *node)
{
}

struct AST *
ast_new_variable_name(void)
{
	MAKE_AST_NODE(AST_VARIABLE_NAME, variable_name);

	return this;
}

void
ast_free_variable_name(struct AST *node)
{
}

void
ast_clone_comment(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_expr(struct AST *node, struct AST *template)
{
	ast_set_expr_type(node, ast_expr_type(template));
	ast_clone_words(node, template);
}

void
ast_clone_for(struct AST *node, struct AST *template)
{
	libias_scope_mempool(pool);
	ast_children_foreach(node, child) {
		libias_mempool_take(pool, child);
	}
	ast_clone_children(node, template);
}

void
ast_clone_for_bindings(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_for_body(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
}

void
ast_clone_for_words(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_for_end(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_if(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_if_branch(struct AST *node, struct AST *template)
{
	ast_set_if_branch_type(node, ast_if_branch_type(template));
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_include(struct AST *node, struct AST *template)
{
	node->include.type = template->include.type;
	node->include.loaded = ast_include_loaded_p(template);
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_root(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
}

void
ast_clone_target_command(struct AST *node, struct AST *template)
{
	ast_set_target_command_flags(node, ast_target_command_flags(template));
	ast_clone_words(node, template);
}

void
ast_clone_target_rule(struct AST *node, struct AST *template)
{
	libias_scope_mempool(pool);

	node->target_rule.operator = template->target_rule.operator;

	ast_children_foreach(node, child) {
		libias_mempool_take(pool, child);
	}
	ast_clone_children(node, template);
	ast_clone_words(node, template);
}

void
ast_clone_target_rule_targets(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_target_rule_dependencies(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_target_rule_body(struct AST *node, struct AST *template)
{
	ast_clone_children(node, template);
}

void
ast_clone_variable(struct AST *node, struct AST *template)
{
	libias_scope_mempool(pool);

	node->variable.modifier = template->variable.modifier;
	ast_clone_words(node, template);

	ast_children_foreach(node, child) {
		libias_mempool_take(pool, child);
	}
	ast_clone_children(node, template);
}

void
ast_clone_variable_name(struct AST *node, struct AST *template)
{
	ast_clone_words(node, template);
}

void
ast_clone_children(struct AST *node, struct AST *template)
{
	ast_children_foreach(template, child) {
		struct AST *clone = ast_clone_helper(child, node);
		ast_child_list_append(node, clone);
	}
}

void
ast_clone_words(struct AST *node, struct AST *template)
{
	auto diagnostic = diagnostic_new();
	ast_words_foreach(template, template_word) {
		ast_word_list_append(node, diagnostic, ast_word_clone(template_word));
	}
	libias_cleanup(&diagnostic);
}

struct AST *
ast_clone_helper(struct AST *template, struct AST *parent)
{
	struct AST *node = template->new();
	node->parent = parent;

	ast_set_expr_indent(node, ast_expr_indent(template));

	node->clone(node, template);

	return node;
}

struct AST *
ast_clone(struct AST *template)
{
	struct AST *node = ast_clone_helper(template, NULL);
	node->parent = node;
	return node;
}

void
ast_render_comment(struct AST *node, struct LibiasBuffer *f)
{
	ast_words_foreach(node, word) {
		if (ast_word_whitespace_p(word)) {
			// Special case only for this node type
			libias_buffer_puts(f, ast_word_value(word));
		} else {
			ast_word_render(word, f);
		}
		libias_buffer_puts(f, "\n");
	}
}

void
ast_render_expr(struct AST *node, struct LibiasBuffer *f)
{
	const char *name = ASTExprType_identifier(ast_expr_type(node));
	libias_buffer_puts(f, ".");
	libias_buffer_puts(f, ast_expr_indent(node));
	libias_buffer_puts(f, name + 1);
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
	libias_buffer_puts(f, "\n");
}

void
ast_render_for(struct AST *node, struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ".");
	libias_buffer_puts(f, ast_expr_indent(node));
	libias_buffer_puts(f, "for");

	ast_children_foreach(node, child) {
		child->render(child, f);
	}
}

void
ast_render_for_bindings(struct AST *node, struct LibiasBuffer *f)
{
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}

	libias_buffer_puts(f, "in");
}

void
ast_render_for_words(struct AST *node, struct LibiasBuffer *f)
{
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}

	libias_buffer_puts(f, "\n");
}

void
ast_render_for_body(struct AST *node, struct LibiasBuffer *f)
{
	ast_children_foreach(node, child) {
		child->render(child, f);
	}
}

void
ast_render_for_end(struct AST *node, struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ".");
	libias_buffer_puts(f, ast_expr_indent(node));
	libias_buffer_puts(f, "endfor");

	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
	libias_buffer_puts(f, "\n");
}

void
ast_render_if(struct AST *node, struct LibiasBuffer *f)
{
	ast_children_foreach(node, child) {
		child->render(child, f);
	}

	libias_buffer_puts(f, ".");
	libias_buffer_puts(f, ast_expr_indent(node));
	libias_buffer_puts(f, "endif");

	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
	libias_buffer_puts(f, "\n");
}

void
ast_render_if_branch(struct AST *node, struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ".");
	libias_buffer_puts(f, ast_expr_indent(node));
	bool else_branch = ast_if_branch_type(node) == AST_IF_ELSE
		|| ast_get_child(ast_parent(node), 0) == node;
	if (!else_branch) {
		libias_buffer_puts(f, "el");
	}
	libias_buffer_puts(f, ASTIfType_human(ast_if_branch_type(node)));

	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}

	libias_buffer_puts(f, "\n");

	ast_children_foreach(node, child) {
		child->render(child, f);
	}
}

void
ast_render_include(struct AST *node, struct LibiasBuffer *f)
{
	const char *name = ASTIncludeType_identifier(ast_include_type(node));
	if (*name == '.') {
		libias_buffer_puts(f, ".");
		libias_buffer_puts(f, ast_expr_indent(node));
		libias_buffer_puts(f, name + 1);
	} else {
		libias_buffer_puts(f, name);
	}
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
	libias_buffer_puts(f, "\n");
}

void
ast_render_root(struct AST *node, struct LibiasBuffer *f)
{
	ast_children_foreach(node, child) {
		child->render(child, f);
	}
}

void
ast_render_target_command(struct AST *node, struct LibiasBuffer *f)
{
	libias_buffer_puts(f, "\t");
	libias_buffer_puts(f, ast_target_command_flags(node));
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
	libias_buffer_puts(f, "\n");
}

void
ast_render_target_rule(struct AST *node, struct LibiasBuffer *f)
{
	ast_children_foreach(node, child) {
		if (ast_target_rule_body_p(child)) {
			ast_words_foreach(node, word) {
				ast_word_render(word, f);
			}
			libias_buffer_puts(f, "\n");
		}
		child->render(child, f);
		if (ast_target_rule_targets_p(child)) {
			libias_buffer_puts(
				f,
				ASTTargetRuleOperator_bmake(ast_target_rule_operator(node)));
		}
	}
}

void
ast_render_target_rule_targets(struct AST *node, struct LibiasBuffer *f)
{
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
}

void
ast_render_target_rule_dependencies(struct AST *node, struct LibiasBuffer *f)
{
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
}

void
ast_render_target_rule_body(struct AST *node, struct LibiasBuffer *f)
{
	ast_children_foreach(node, child) {
		child->render(child, f);
	}
}

void
ast_render_variable(struct AST *node, struct LibiasBuffer *f)
{
	ast_children_foreach(node, child) {
		child->render(child, f);
	}

	libias_buffer_puts(f, ASTVariableModifier_human(ast_variable_modifier(node)));

	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}

	libias_buffer_puts(f, "\n");
}

void
ast_render_variable_name(struct AST *node, struct LibiasBuffer *f)
{
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
}

void
ast_render(
	struct AST *ast,
	struct LibiasBuffer *f)
{
	ast->render(ast, f);
}

void
ast_sexp_comment(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_expr(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "type", ASTExprType_sexp(ast_expr_type(node)));
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_for(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "indent", ast_expr_indent(node));
}

void
ast_sexp_for_bindings(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "bindings", w);
}

void
ast_sexp_for_words(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_for_body(struct AST *node, struct SexpWriter *w)
{
}

void
ast_sexp_for_end(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_if(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "end-indent", ast_expr_indent(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_if_branch(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "type", ASTIfType_human(ast_if_branch_type(node)));
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	ast_sexp_words(node, "test", w);
}

void
ast_sexp_include(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "type", ASTIncludeType_sexp(ast_include_type(node)));
	sexp_writer_string(w, "indent", ast_expr_indent(node));
	sexp_writer_bool(w, "sys?", ast_include_system_p(node));
	sexp_writer_bool(w, "loaded?", ast_include_loaded_p(node));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_root(struct AST *node, struct SexpWriter *w)
{
}

void
ast_sexp_target_command(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
	const char *flags = ast_target_command_flags(node);
	if (*flags) {
		sexp_writer_string(w, "flags", flags);
	}
}

void
ast_sexp_target_rule(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_string(w, "operator", ASTTargetRuleOperator_bmake(ast_target_rule_operator(node)));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_target_rule_targets(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_target_rule_dependencies(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_target_rule_body(struct AST *node, struct SexpWriter *w)
{
}

void
ast_sexp_variable(struct AST *node, struct SexpWriter *w)
{
	sexp_writer_keyword(w, "modifier", ASTVariableModifier_sexp(ast_variable_modifier(node)));
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_variable_name(struct AST *node, struct SexpWriter *w)
{
	ast_sexp_words(node, "words", w);
}

void
ast_sexp_helper(
	struct AST *node,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, ASTType_sexp(ast_type(node)));
	node->sexp(node, w);
	if (node->children.list && libias_list_len(node->children.list) > 0) {
		if (!ast_root_p(node)) {
			sexp_writer_open_tree(w, "children");
		}
		ast_children_foreach(node, child) {
			ast_sexp_helper(child, w);
		}
		if (!ast_root_p(node)) {
			sexp_writer_close_tree(w);
		}
	}
	sexp_writer_close_tree(w);
}

void
ast_sexp(
	struct AST *node,
	struct LibiasBuffer *f,
	bool color)
{
	struct SexpWriter *w = sexp_writer_new(color);
	ast_sexp_helper(node, w);
	sexp_writer_finish(w, f);
	libias_cleanup(&w);
}

void
ast_words_validate_whitespace(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	bool last_was_whitespace = false;
	ast_words_foreach(node, word) {
		if (last_was_whitespace) {
			if (ast_word_whitespace_p(word)) {
				// This is a bug because ast_word_list_insert_* should
				// have merged them.
				libias_panic("word list with >2 consecutive AST_WORD_WHITESPACE");
			}
			last_was_whitespace = false;
		} else {
			last_was_whitespace = ast_word_whitespace_p(word);
		}
	}
}

void
ast_words_validate_root(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	if (libias_list_len(node->words.list) > 0) {
		diagnostic_ast_error(
			diagnostic,
			node,
			NULL, NULL,
			"AST_ROOT should not have any words");
	}
}

void
ast_words_validate_comment(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_foreach(node, word) {
		if (ast_word_string_p(word)) {
			// ok
		} else if (ast_word_whitespace_p(word)) {
			// ok
		} else {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, word,
				"AST_COMMENT with non-static word");
		}
	}
}

void
ast_words_validate_expr(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
	// TODO
}

void
ast_words_validate_if(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_foreach(node, word) {
		if (ast_word_comment_p(word)) {
			// ok
		} else if (ast_word_whitespace_p(word)) {
			// ok
		} else {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, word,
				"AST_IF with non-static word");
		}
	}
}

void
ast_words_validate_if_branch(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
}

void
ast_words_validate_for(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	if (libias_list_len(node->words.list) > 0) {
		diagnostic_ast_error(
			diagnostic,
			node,
			NULL, NULL,
			"AST_FOR should not have any words");
	}
}

void
ast_words_validate_for_body(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	if (libias_list_len(node->words.list) > 0) {
		diagnostic_ast_error(
			diagnostic,
			node,
			NULL, NULL,
			"AST_FOR_BODY should not have any words");
	}

}

void
ast_words_validate_for_bindings(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);

	ast_words_foreach(node, word) {
		if (ast_word_string_p(word)) {
			// ok
		} else if (ast_word_whitespace_p(word)) {
			// ok
		} else {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, word,
				"AST_FOR_BINDING with non-static word");
		}
	}
}

void
ast_words_validate_for_words(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
}

void
ast_words_validate_for_end(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
	// TODO
}

void
ast_words_validate_include(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	struct LibiasListEntry *current = libias_list_head(node->words.list);
	// Word list must be of the following forms
	bool ok = false;
	struct ASTWord *path_word = NULL;
	if (libias_list_len(node->words.list) == 0) {
		// empty
		ok = true;
	} else if (libias_list_len(node->words.list) == 1) {
		// P
		struct ASTWord *word = libias_list_entry_value(current);
		path_word = word;
		ok = !ast_word_whitespace_p(word)
			&& !ast_word_comment_p(word);
		if (!ok) {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, word,
				"AST_INCLUDE has no path");
		}
	} else if (libias_list_len(node->words.list) == 2) {
		struct ASTWord *word = libias_list_entry_value(current);
		struct ASTWord *next_word = libias_list_entry_value(libias_list_entry_next(current));
		if (ast_node_comment(node)) {
			// P#
			path_word = word;
			ok = !ast_word_whitespace_p(path_word)
				&& ast_word_comment_p(next_word);
		} else {
			// _P or P_
			if (ast_word_whitespace_p(word)) {
				path_word = next_word;
				ok = !ast_word_whitespace_p(path_word);
			} else {
				path_word = word;
				ok = ast_word_whitespace_p(next_word);
			}
		}
		if (!ok) {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, next_word,
				"AST_INCLUDE has invalid path");
		}
	} else if (libias_list_len(node->words.list) == 3) {
		struct ASTWord *word = libias_list_entry_value(current);
		struct ASTWord *next_word = libias_list_entry_value(libias_list_entry_next(current));
		struct ASTWord *next_next_word = libias_list_entry_value(
			libias_list_entry_next(libias_list_entry_next(current)));

		if (ast_node_comment(node)) {
			// _P#
			path_word = next_word;
			ok = ast_word_whitespace_p(word)
				&& !ast_word_whitespace_p(next_word)
				&& ast_word_comment_p(next_next_word);
		} else {
			// _P_
			path_word = next_word;
			ok = ast_word_whitespace_p(word)
				&& !ast_word_whitespace_p(next_word)
				&& ast_word_whitespace_p(next_next_word);
		}
		if (!ok) {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, next_next_word,
				"AST_INCLUDE has invalid path");
		}
	} else if (libias_list_len(node->words.list) == 4) {
		// _P_#
		struct ASTWord *word = libias_list_entry_value(current);
		struct ASTWord *next_word = libias_list_entry_value(libias_list_entry_next(current));
		struct ASTWord *next_next_word = libias_list_entry_value(
			libias_list_entry_next(libias_list_entry_next(current)));
		struct ASTWord *next_next_next_word = libias_list_entry_value(
			libias_list_entry_next(libias_list_entry_next(libias_list_entry_next(current))));

		path_word = next_word;
		ok = ast_word_whitespace_p(word)
			&& !ast_word_whitespace_p(next_word)
			&& !ast_word_comment_p(next_word)
			&& ast_word_whitespace_p(next_next_word)
			&& ast_word_comment_p(next_next_next_word);
		if (!ok) {
			diagnostic_ast_error(
				diagnostic,
				node,
				word, next_next_next_word,
				"AST_INCLUDE has invalid path");
		}
	}

	if (!ok) {
		return;
	}

	// Recompute the path and system properties
	if (!path_word) {
		// No path is allowed for the sake of allowing easier
		// construction of AST_INCLUDE
		return;
	}

	char *path = ast_word_flatten(path_word);
	libias_scope_free(path);
	bool invalid = false;
	const char *hint = "";
	if (*ASTIncludeType_identifier(ast_include_type(node)) == '.') {
		if (*path == '<') {
			path++;
			if (libias_str_suffix_p(path, ">")) {
				path[strlen(path) - 1] = 0;
			} else {
				invalid = true;
				hint = ": missing > at the end";
			}
		} else if (*path == '"') {
			path++;
			if (libias_str_suffix_p(path, "\"")) {
				path[strlen(path) - 1] = 0;
			} else {
				invalid = true;
				hint = ": missing \" at the end";
			}
		} else {
			invalid = true;
			hint = ": must start with < or \"";
		}
	}
	if (strlen(path) == 0 || invalid) {
		diagnostic_ast_error(
			diagnostic,
			node,
			path_word, path_word,
			"AST_INCLUDE with invalid path for %s%s",
			ASTIncludeType_identifier(ast_include_type(node)),
			hint);
	}
}

void
ast_words_validate_target_rule(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	libias_panic_unless(
		libias_list_len(node->words.list) == 0,
		"AST_TARGET_RULE should not have any words");
}

void
ast_words_validate_target_rule_dependencies(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
}

void
ast_words_validate_target_rule_targets(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
}

void
ast_words_validate_target_rule_body(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	libias_panic_unless(
		libias_list_len(node->words.list) == 0,
		"AST_TARGET_BODY should not have any words");
}

void
ast_words_validate_target_command(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
}

void
ast_words_validate_variable(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	ast_words_validate_whitespace(node, diagnostic);
}

void
ast_words_validate_variable_name(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	struct LibiasListEntry *current = libias_list_head(node->words.list);
	// Word list must be of the following forms
	bool ok = false;
	if (libias_list_len(node->words.list) == 0) {
		// empty
		ok = true;
	} else if (libias_list_len(node->words.list) == 1) {
		// X
		struct ASTWord *word = libias_list_entry_value(current);
		ok = !ast_word_whitespace_p(word)
			&& !ast_word_comment_p(word);
	} else if (libias_list_len(node->words.list) == 2) {
		// _X or X_
		struct ASTWord *word = libias_list_entry_value(current);
		struct ASTWord *next_word = libias_list_entry_value(libias_list_entry_next(current));
		ok = (ast_word_whitespace_p(word)
			  && !ast_word_whitespace_p(next_word))
			|| (!ast_word_whitespace_p(word)
				&& ast_word_whitespace_p(next_word));
		ok = ok
			&& !ast_word_comment_p(word)
			&& !ast_word_comment_p(next_word);
	} else if (libias_list_len(node->words.list) == 3) {
		// _X_
		struct ASTWord *word = libias_list_entry_value(current);
		struct ASTWord *next_word = libias_list_entry_value(libias_list_entry_next(current));
		struct ASTWord *next_next_word = libias_list_entry_value(
			libias_list_entry_next(libias_list_entry_next(current)));
		ok = ast_word_whitespace_p(word)
			&& !ast_word_whitespace_p(next_word)
			&& !ast_word_comment_p(next_word)
			&& ast_word_whitespace_p(next_next_word);
	}

	if (!ok) {
		libias_panic("invalid AST_VARIABLE_NAME word list");
	}
}

void
ast_words_validate(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	node->words_validate(node, diagnostic);
}

// Remove NODE from its parent. NODE is *not* freed.
void
ast_remove_from_parent(struct AST *node)
{
	struct AST *parent = node->parent;
	if (parent == node) {
		return;
	}

	if (parent->children.list) {
		struct LibiasListEntry *entry = ast_child_list_entry_for_node(parent, node);
		if (entry) {
			ast_child_list_entry_remove(parent, entry);
		}
	}

	node->parent = node;
}

bool
ast_child_p(struct AST *node, struct AST *needle)
{
	if (node == needle) {
		return true;
	} else {
		return node->children.list
			&& ast_child_list_entry_for_node(node, needle);
	}
}

bool
ast_ports_framework_include_p(struct AST *node)
{
	if (
		ast_include_p(node)
		&& ast_include_type(node) == AST_INCLUDE_BMAKE
		&& ast_include_path(node)
		&& ast_include_system_p(node)
		)
	{
		char *path = ast_word_flatten(ast_include_path(node));
		libias_scope_free(path);
		return strcmp(path, "<bsd.port.options.mk>") == 0
			|| strcmp(path, "<bsd.port.pre.mk>") == 0
			|| strcmp(path, "<bsd.port.post.mk>") == 0
			|| strcmp(path, "<bsd.port.mk>") == 0;
	}

	return false;
}

enum ASTType
ast_type(const struct AST *node)
{
	return node->type;
}

// Return the root of NODE.
struct AST *
ast_root(struct AST *node)
{
	if (ast_root_p(node)) {
		return node;
	} else {
		return ast_root(ast_parent(node));
	}
}

bool
ast_root_p(const struct AST *node)
{
	return node->type == AST_ROOT;
}

bool
ast_comment_p(const struct AST *node)
{
	return node->type == AST_COMMENT;
}

bool
ast_expr_p(const struct AST *node)
{
	return node->type == AST_EXPR;
}

bool
ast_if_p(const struct AST *node)
{
	return node->type == AST_IF;
}

bool
ast_if_branch_p(const struct AST *node)
{
	return node->type == AST_IF_BRANCH;
}

bool
ast_for_p(const struct AST *node)
{
	return node->type == AST_FOR;
}

bool
ast_for_bindings_p(const struct AST *node)
{
	return node->type == AST_FOR_BINDINGS;
}

bool
ast_for_words_p(const struct AST *node)
{
	return node->type == AST_FOR_WORDS;
}

bool
ast_for_body_p(const struct AST *node)
{
	return node->type == AST_FOR_BODY;
}

bool
ast_for_end_p(const struct AST *node)
{
	return node->type == AST_FOR_END;
}

bool
ast_include_p(const struct AST *node)
{
	return node->type == AST_INCLUDE;
}

bool
ast_target_rule_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE;
}

bool
ast_target_rule_dependencies_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE_DEPENDENCIES;
}

bool
ast_target_rule_targets_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE_TARGETS;
}

bool
ast_target_rule_body_p(const struct AST *node) {
	return node->type == AST_TARGET_RULE_BODY;
}

bool
ast_target_command_p(const struct AST *node) {
	return node->type == AST_TARGET_COMMAND;
}

bool
ast_variable_p(const struct AST *node) {
	return node->type == AST_VARIABLE;
}

bool
ast_variable_name_p(const struct AST *node) {
	return node->type == AST_VARIABLE_NAME;
}

int
compare_ast_sort_words(
	const void *ap,
	const void *bp,
	void *userdata)
{
	struct ASTSortWordsData *this = userdata;
	struct ASTWord *a = this->words[*((size_t *)ap)];
	struct ASTWord *b = this->words[*((size_t *)bp)];

	// AST_WORD_COMMENT is always last and there can only be one of it
	if (ast_word_comment_p(a) && ast_word_comment_p(b)) {
		return 0;
	} else if (ast_word_comment_p(a)) {
		return 1;
	} else if (ast_word_comment_p(b)) {
		return -1;
	} else {
		return this->compare->compare(
			&a,
			&b,
			this->compare->context);
	}
}

// Sort the words in NODE based on COMPARE. This ignores
// AST_WORD_WHITESPACE and only reorders the other words. The whitespace
// in between them is preserved.
void
ast_sort_words(
	struct AST *node,
	struct LibiasCompareTrait *compare)
{
	if (!libias_list_head(node->words.list)) {
		return;
	}

	// This code doesn't handle AST_WORD_BOOL but sorting if expression
	// words doesn't make much sense anyway.
	if (ast_if_branch_p(node)) {
		return;
	}

	size_t *unsorted_index_map = NULL;
	libias_realloc_array(unsorted_index_map, libias_list_len(node->words.list));

	size_t *sorted_index_map = NULL;
	libias_realloc_array(sorted_index_map, libias_list_len(node->words.list));

	struct ASTWord **unsorted_words = NULL;
	libias_realloc_array(unsorted_words, libias_list_len(node->words.list));

	struct ASTWord **sorted_words = NULL;
	libias_realloc_array(sorted_words, libias_list_len(node->words.list));

	size_t index_map_len = 0;
	ast_words_foreach(node, word) {
		unsorted_words[word_index] = word;
		sorted_words[word_index] = word;
		if (!ast_word_whitespace_p(word)) {
			unsorted_index_map[index_map_len] = word_index;
			sorted_index_map[index_map_len] = word_index;
			index_map_len++;
		}
	}

	struct ASTSortWordsData this = {
		.compare = compare,
		.words = sorted_words,
	};
	struct LibiasCompareTrait ast_sort_words_compare = {
		.compare = compare_ast_sort_words,
		.context = &this,
	};
	libias_sort(sorted_index_map, index_map_len, sizeof(size_t), &ast_sort_words_compare);

	// Swap the words according to our sort result
	for (size_t i = 0; i < index_map_len; i++) {
		sorted_words[unsorted_index_map[i]] = unsorted_words[sorted_index_map[i]];
	}

	// Apply to our word list
	size_t i = 0;
	libias_list_foreach(node->words.list, struct ASTWord *, current_word) {
		struct ASTWord *word = sorted_words[i++];
		if (word != current_word) {
			libias_list_entry_set_value(current_word_list_entry, word);
		}
	}

	libias_hashmap_truncate(node->words.word_to_list_entry_map);
	libias_list_foreach(node->words.list, struct ASTWord *, word) {
		libias_hashmap_add(node->words.word_to_list_entry_map, word, word_list_entry);
	}

	libias_free(unsorted_words);
	libias_free(sorted_words);
	libias_free(sorted_index_map);
	libias_free(unsorted_index_map);
}

// Returns the NODE's parent. We are at the root node if NODE itself is
// returned.
struct AST *
ast_parent(struct AST *node)
{
	return node->parent;
}

// Return the parent AST_FOR of NODE. Returns NULL if there is no such
// node.
struct AST *
ast_parent_for(struct AST *node)
{
	if (ast_for_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_for(parent);
	}
}

// Return the parent AST_IF of NODE. Returns NULL if there is no such
// node.
struct AST *
ast_parent_if(struct AST *node)
{
	if (ast_if_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_if(parent);
	}
}

// Return the parent AST_INCLUDE of NODE. Returns NULL if there is no
// such node.
struct AST *
ast_parent_include(struct AST *node)
{
	if (ast_include_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_include(parent);
	}
}

// Return the parent AST_TARGET_RULE of NODE. Returns NULL if there is
// no such node.
struct AST *
ast_parent_target_rule(struct AST *node)
{
	if (ast_target_rule_p(node)) {
		return node;
	}

	struct AST *parent = ast_parent(node);
	if (parent == node) {
		return NULL;
	} else {
		return ast_parent_target_rule(parent);
	}
}

void
ast_sexp_words(struct AST *node, const char *name, struct SexpWriter *w)
{
	if (libias_list_head(node->words.list)) {
		sexp_writer_open_tree(w, name);
		ast_words_foreach(node, word) {
			ast_word_sexp(word, w);
		}
		sexp_writer_close_tree(w);
	}
}

void
ast_cleanup(struct AST **node_ptr)
{
	struct AST *node = *node_ptr;
	if (node) {
		ast_remove_from_parent(node);

		if (node->children.list) {
			struct LibiasMempool *pool = libias_mempool_new();
			ast_children_foreach(node, child) {
				libias_mempool_take(pool, child);
			}
			libias_cleanup(&pool);
			libias_cleanup(&node->children.list);
			node->children.list = NULL;
			libias_cleanup(&node->children.child_to_list_entry_map);
			node->children.child_to_list_entry_map = NULL;
		}

		ast_words_foreach(node, word) {
			libias_cleanup(&word);
		}
		libias_cleanup(&node->words.list);
		libias_cleanup(&node->words.word_to_list_entry_map);

		libias_free(node->indent);
		if (node->query_cache_cleanup) {
			node->query_cache_cleanup(&node->query_cache);
			node->query_cache_cleanup = NULL;
		}
		node->free(node);
		libias_free(node);
		*node_ptr = NULL;
	}
}

// Return the NODE's nth child. INDEX is zero-based. If INDEX is
// negative then the nth last child is returned instead. -1 returns the
// last child, -2 the second to last child, 0 the first child, etc.
//
// Returns NULL if there's no nth child.
struct AST *
ast_get_child(struct AST *node, ssize_t index)
{
	if (node->children.list) {
		if (index < 0) {
			index = MAX(0, libias_list_len(node->children.list) + index);
		}

		ssize_t i = 0;
		ast_children_foreach(node, child) {
			if (i == index) {
				return child;
			}
			i++;
		}
	}

	return NULL;
}

bool
ast_include_loaded_p(struct AST *node)
{
	if (ast_include_p(node)) {
		return node->include.loaded;
	} else {
		return false;
	}
}

void
ast_set_include_loaded(struct AST *node, bool loaded)
{
	if (ast_include_p(node)) {
		node->include.loaded = loaded;
	}
}

bool
ast_include_system_p(struct AST *node)
{
	if (!ast_include_p(node)) {
		return false;
	}

	struct ASTWord *path_word = NULL;
	ast_words_foreach(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		} else {
			path_word = word;
			break;
		}
	}
	if (!path_word) {
		return false;
	}

	char *path = ast_word_flatten(path_word);
	libias_scope_free(path);
	if (*ASTIncludeType_identifier(ast_include_type(node)) == '.') {
		if (*path == '<') {
			return true;
		} else if (*path == '"') {
			return false;
		}
	}

	return false;
}

struct ASTWord *
ast_include_path(struct AST *node)
{
	if (!ast_include_p(node)) {
		return NULL;
	}

	ast_words_foreach(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		} else {
			return word;
		}
	}

	return NULL;
}

enum ASTIncludeType
ast_include_type(struct AST *node)
{
	return node->include.type;
}

void
ast_set_include_type(
	struct AST *node,
	const enum ASTIncludeType type)
{
	if (!ast_include_p(node)) {
		return;
	}

	node->include.type = type;
}

// Return the node's comment. This is the end of line comment
// immediately after an expression, variable, target command, or target
// rule. NULL is returned if no comment is set NULL.
//
// The node comment is also available as an AST_WORD_COMMENT in the
// node's word list. It's always the last word.
//
// To change the node comment use ast_words_edit_add_word().
struct ASTWord *
ast_node_comment(struct AST *node)
{
	struct LibiasListEntry *tail = libias_list_tail(node->words.list);
	if (tail) {
		struct ASTWord *comment = libias_list_entry_value(tail);
		if (ast_word_comment_p(comment)) {
			return comment;
		}
	}

	return NULL;
}

const char *
ast_expr_indent(struct AST *node)
{
	switch (ast_type(node)) {
	case AST_EXPR:
	case AST_FOR:
	case AST_FOR_END:
	case AST_IF:
	case AST_IF_BRANCH:
	case AST_INCLUDE:
		break;
	default:
		return "";
	}

	return node->indent;
}

void
ast_set_expr_indent(
	struct AST *node,
	const char *indent)
{
	switch (ast_type(node)) {
	case AST_EXPR:
	case AST_FOR:
	case AST_FOR_END:
	case AST_IF:
	case AST_IF_BRANCH:
	case AST_INCLUDE:
		break;
	default:
		return;
	}

	libias_free(node->indent);
	node->indent = libias_str_dup(indent);
}

enum ASTExprType
ast_expr_type(struct AST *node)
{
	if (ast_expr_p(node)) {
		return node->expr.type;
	} else {
		return AST_EXPR_ERROR;
	}
}

void
ast_set_expr_type(
	struct AST *node,
	const enum ASTExprType type)
{
	if (!ast_expr_p(node)) {
		return;
	}

	switch (type) {
	case AST_EXPR_ERROR:
	case AST_EXPR_EXPORT_ENV:
	case AST_EXPR_EXPORT_ENV_DOT:
	case AST_EXPR_EXPORT_LITERAL:
	case AST_EXPR_EXPORT:
	case AST_EXPR_INFO:
	case AST_EXPR_UNDEF:
	case AST_EXPR_UNEXPORT_ENV:
	case AST_EXPR_UNEXPORT:
	case AST_EXPR_WARNING:
		node->expr.type = type;
		return;
	}

	libias_panic("invalid ASTExprType: %d", type);
}

enum ASTIfType
ast_if_branch_type(struct AST *node)
{
	if (ast_if_branch_p(node)) {
		return node->if_branch.type;
	} else {
		return AST_IF_IF;
	}
}

void
ast_set_if_branch_type(
	struct AST *node,
	const enum ASTIfType type)
{
	if (!ast_if_branch_p(node)) {
		return;
	}

	switch (type) {
	case AST_IF_IF:
	case AST_IF_DEF:
	case AST_IF_ELSE:
	case AST_IF_MAKE:
	case AST_IF_NDEF:
	case AST_IF_NMAKE:
		node->if_branch.type = type;
		return;
	}

	libias_panic("invalid ASTIfType: %d", type);
}

const char *
ast_target_command_flags(struct AST *node)
{
	if (ast_target_command_p(node)) {
		return node->target_command.flags;
	} else {
		return "";
	}
}

enum ASTTargetRuleOperator
ast_target_rule_operator(struct AST *node)
{
	if (ast_target_rule_p(node)) {
		return node->target_rule.operator;
	} else {
		return AST_TARGET_RULE_OPERATOR_1;
	}
}

void
ast_set_target_command_flags(
	struct AST *node,
	const char *flags)
{
	if (!ast_target_command_p(node)) {
		return;
	}

	// TODO only keep valid flags

	libias_free(node->target_command.flags);
	node->target_command.flags = libias_str_dup(flags);
}

enum ASTVariableModifier
ast_variable_modifier(struct AST *node)
{
	if (ast_variable_p(node)) {
		return node->variable.modifier;
	} else {
		return AST_VARIABLE_MODIFIER_ASSIGN;
	}
}

struct ASTWord *
ast_variable_name(struct AST *node)
{
	switch (ast_type(node)) {
	case AST_VARIABLE:
		node = ast_get_child(node, AST_CHILD_VARIABLE_NAME);
		break;
	case AST_VARIABLE_NAME:
		break;
	default:
		return NULL;
	}

	ast_words_foreach(node, word) {
		if (!ast_word_whitespace_p(word)) {
			return word;
		}
	}

	libias_panic("variable name has no non-whitespace word");
}

struct ASTQueryCache *
ast_get_query_cache(struct AST *node)
{
	auto root_node = ast_root(node);
	return root_node->query_cache;
}

void
ast_set_query_cache(
	struct AST *node,
	struct ASTQueryCache *query_cache,
	void (*query_cache_cleanup)(struct ASTQueryCache **))
{
	auto root_node = ast_root(node);
	if (root_node->query_cache_cleanup) {
		root_node->query_cache_cleanup(&root_node->query_cache);
	}
	root_node->query_cache = query_cache;
	root_node->query_cache_cleanup = query_cache_cleanup;
}

struct LibiasListEntry *
ast_word_list_append(
	struct AST *node,
	struct Diagnostic *diagnostic,
	struct ASTWord *word)
{
	struct LibiasListEntry *tail = libias_list_tail(node->words.list);
	if (tail && ast_word_comment_p(libias_list_entry_value(tail))) {
		if (ast_word_comment_p(word)) {
			ast_word_list_entry_remove(node, tail);
			tail = libias_list_tail(node->words.list);
		} else {
			return ast_word_list_insert_before(node, diagnostic, tail, word, false);
		}
	}

	return ast_word_list_insert_after(node, diagnostic, tail, word, false);
}

struct LibiasListEntry *
ast_word_list_insert_after(
	struct AST *node,
	struct Diagnostic *diagnostic,
	struct LibiasListEntry *head,
	struct ASTWord *word,
	bool replace)
{
	if (ast_word_bool_p(word) && !ast_if_branch_p(node)) {
		diagnostic_ast_warning(
			diagnostic,
			node,
			word, word,
			"AST_WORD_BOOL can only be added to if branches");
		return head;
	}

	if (head) {
		if (ast_word_whitespace_p(word) && !ast_comment_p(node)) {
			struct LibiasListEntry *merge_entry = NULL;
			if (ast_word_whitespace_p(libias_list_entry_value(head))) {
				merge_entry = head;
			} else if (
				libias_list_entry_next(head)
				&& ast_word_whitespace_p(libias_list_entry_value(libias_list_entry_next(head)))
				)
			{
				merge_entry = libias_list_entry_next(head);
			}

			if (merge_entry) {
				struct ASTWord *old_word = libias_list_entry_value(merge_entry);
				if (!replace) {
					char *value = libias_str_printf(
						"%s%s",
						ast_word_value(old_word),
						ast_word_value(word));
					libias_cleanup(&word);
					word = ast_word_new_whitespace(value);
					libias_free(value);
				}
				libias_hashmap_remove(node->words.word_to_list_entry_map, old_word);
				libias_cleanup(&old_word);
				libias_list_entry_set_value(merge_entry, word);
				libias_hashmap_add(node->words.word_to_list_entry_map, word, merge_entry);
				return merge_entry;
			}
		}
	}

	struct LibiasListEntry *new_list_entry = libias_list_insert_after(
		node->words.list,
		head,
		word);
	libias_hashmap_add(node->words.word_to_list_entry_map, word, new_list_entry);
	return new_list_entry;
}

struct LibiasListEntry *
ast_word_list_insert_before(
	struct AST *node,
	struct Diagnostic *diagnostic,
	struct LibiasListEntry *head,
	struct ASTWord *word,
	bool replace)
{
	if (ast_word_bool_p(word) && !ast_if_branch_p(node)) {
		diagnostic_ast_warning(
			diagnostic,
			node,
			word, word,
			"AST_WORD_BOOL can only be added to if branches");
		return head;
	}

	if (head) {
		if (ast_word_whitespace_p(word) && !ast_comment_p(node)) {
			struct LibiasListEntry *merge_entry = NULL;
			if (ast_word_whitespace_p(libias_list_entry_value(head))) {
				merge_entry = head;
			} else if (
				libias_list_entry_prev(head)
				&& ast_word_whitespace_p(libias_list_entry_value(libias_list_entry_prev(head)))
				)
			{
				merge_entry = libias_list_entry_prev(head);
			}

			if (merge_entry) {
				struct ASTWord *old_word = libias_list_entry_value(merge_entry);
				if (!replace) {
					char *value = libias_str_printf(
						"%s%s",
						ast_word_value(old_word),
						ast_word_value(word));
					libias_cleanup(&word);
					word = ast_word_new_whitespace(value);
					libias_free(value);
				}
				libias_hashmap_remove(node->words.word_to_list_entry_map, old_word);
				libias_cleanup(&old_word);
				libias_list_entry_set_value(merge_entry, word);
				libias_hashmap_add(node->words.word_to_list_entry_map, word, merge_entry);
				return merge_entry;
			}
		}
	}

	struct LibiasListEntry *new_list_entry = libias_list_insert_before(
		node->words.list,
		head,
		word);
	libias_hashmap_add(node->words.word_to_list_entry_map, word, new_list_entry);
	return new_list_entry;
}

void
ast_word_list_entry_replace_word(
	struct AST *node,
	struct LibiasListEntry *list_entry,
	struct ASTWord *new_word)
{
	struct ASTWord *old_word = libias_list_entry_value(list_entry);
	libias_hashmap_remove(node->words.word_to_list_entry_map, old_word);
	libias_hashmap_add(node->words.word_to_list_entry_map, new_word, list_entry);
	libias_list_entry_set_value(list_entry, new_word);
	libias_cleanup(&old_word);
}

struct LibiasListEntry *
ast_word_list_entry_remove(
	struct AST *node,
	struct LibiasListEntry *list_entry)
{
	struct ASTWord *word = libias_list_entry_value(list_entry);
	libias_cleanup(&word);
	list_entry = libias_list_entry_remove(list_entry);
	// Remove whitespace after the entry too to maintain the word list invariant
	if (list_entry && ast_word_whitespace_p(libias_list_entry_value(list_entry))) {
		libias_hashmap_remove(
			node->words.word_to_list_entry_map,
			libias_list_entry_value(list_entry));
		struct ASTWord *word = libias_list_entry_value(list_entry);
		libias_cleanup(&word);
		list_entry = libias_list_entry_remove(list_entry);
	}
	return list_entry;
}

struct LibiasListEntry *
ast_word_list_entry_for_word(
	struct AST *node,
	struct ASTWord *word)
{
	return libias_hashmap_get(node->words.word_to_list_entry_map, word);
}

struct ASTWordsEdit *
ast_words_edit_new(
	struct AST *node,
	struct Diagnostic *diagnostic,
	enum ASTWordsEditFlags flags)
{
	struct ASTWordsEdit *edit = libias_alloc(struct ASTWordsEdit);
	edit->external_diagnostic = diagnostic;
	edit->diagnostic = diagnostic_new();
	edit->flags = flags;
	edit->node = node;
	edit->queue = libias_queue_new();
	edit->pool = libias_mempool_new();
	return edit;
}

void
ast_words_edit_cleanup(struct ASTWordsEdit **edit_ptr)
{
	struct ASTWordsEdit *edit = *edit_ptr;
	if (edit) {
		libias_cleanup(&edit->diagnostic);
		libias_cleanup(&edit->queue);
		libias_cleanup(&edit->pool);
		libias_free(edit);
		*edit_ptr = NULL;
	}
}

// Applies the queued up edit operations. Returns true iff EDIT's
// diagnostic object has no queued up error messages.
bool
ast_words_edit_apply(struct ASTWordsEdit *edit)
{
	void (*fn)(struct ASTWordsEdit *, struct LibiasQueue *) = NULL;
	while ((fn = libias_queue_pop(edit->queue)) != NULL) {
		fn(edit, edit->queue);
	}

	// Clear query cache since it might no longer be valid
	if (fn) {
		ast_set_query_cache(edit->node, NULL, NULL);
	}

	// Check word list invariants are still ok
	ast_words_validate(edit->node, edit->diagnostic);

	bool status = !diagnostic_error_p(edit->diagnostic);

	diagnostic_move_reports(edit->external_diagnostic, edit->diagnostic);

	libias_cleanup(&edit);

	return status;
}

void
ast_words_edit_add_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);
	struct ASTWord *other = libias_queue_pop(args);

	if (ast_word_comment_p(word)) {
		ast_word_list_append(edit->node, edit->diagnostic, word);
	} else {
		struct LibiasListEntry *current;
		current = ast_word_list_entry_for_word(edit->node, other);
		if (current) {
			// insert after other word
			ast_word_list_insert_after(edit->node, edit->diagnostic, current, word, false);
		} else {
			// append at the end
			ast_word_list_append(edit->node, edit->diagnostic, word);
		}
	}

	libias_mempool_forget(edit->pool, word);
}

// Adds WORD before the OTHER word in the NODE. NODE takes ownership of
// WORD. Appends WORD if OTHER is NULL or can't be found.
void
ast_words_edit_add_word_before_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);
	struct ASTWord *other = libias_queue_pop(args);

	if (ast_word_comment_p(word)) {
		ast_word_list_append(edit->node, edit->diagnostic, word);
	} else {
		struct LibiasListEntry *current;
		current = ast_word_list_entry_for_word(edit->node, other);
		if (current) {
			// insert before other word
			ast_word_list_insert_before(edit->node, edit->diagnostic, current, word, false);
		} else {
			// append at the end
			ast_word_list_append(edit->node, edit->diagnostic, word);
		}
	}

	libias_mempool_forget(edit->pool, word);
}

void
ast_words_edit_append_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);
	ast_word_list_append(edit->node, edit->diagnostic, word);
	libias_mempool_forget(edit->pool, word);
}

void
ast_words_edit_remove_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);

	struct LibiasListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	if (list_entry) {
		struct ASTWord *word = libias_list_entry_value(list_entry);
		if (
			ast_word_whitespace_p(word)
			&& !ast_comment_p(edit->node)
			&& libias_list_entry_next(list_entry) != NULL
			)
		{
			// Can't remove whitespace since we must maintain our words
			// invariant but reset to default whitespace of " ".
			ast_word_list_entry_replace_word(
				edit->node,
				list_entry, ast_word_new_whitespace(" "));
		} else {
			ast_word_list_entry_remove(edit->node, list_entry);
		}
	}
}

void
ast_words_edit_replace_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *old_word = libias_queue_pop(args);
	struct ASTWord *new_word = libias_queue_pop(args);

	// Make sure the types of the words are compatible
	switch (ast_word_type(old_word)) {
        case AST_WORD_STRING:
        case AST_WORD_APPEND:
        case AST_WORD_EXPAND:
			switch (ast_word_type(new_word)) {
			case AST_WORD_STRING:
			case AST_WORD_APPEND:
			case AST_WORD_EXPAND:
				break;
			default:
				return;
			}
			break;
        case AST_WORD_BOOL:
			if (!ast_word_bool_p(new_word)) {
				return;
			}
			break;
        case AST_WORD_WHITESPACE:
			if (!ast_word_whitespace_p(new_word)) {
				return;
			}
			break;
        case AST_WORD_COMMENT:
			if (!ast_word_comment_p(new_word)) {
				return;
			}
			break;
	}

	auto list_entry = ast_word_list_entry_for_word(edit->node, old_word);
	if (!list_entry) {
		return;
	}

	// If the new word is already part of the AST we don't do anything
	if (ast_word_list_entry_for_word(edit->node, new_word)) {
		return;
	}

	ast_word_list_entry_replace_word(edit->node, list_entry, new_word);
	libias_mempool_forget(edit->pool, new_word);
}

void
ast_words_edit_remove_all_words_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	for (struct LibiasListEntry *current = libias_list_head(edit->node->words.list);
		 current;
		 current = ast_word_list_entry_remove(edit->node, current));
}

void
ast_words_edit_remove_beginning_whitespace_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	while (
		libias_list_head(edit->node->words.list) &&
		ast_word_whitespace_p(libias_list_entry_value(libias_list_head(edit->node->words.list)))
		)
	{
		ast_word_list_entry_remove(edit->node, libias_list_head(edit->node->words.list));
	}
}

void
ast_words_edit_remove_trailing_whitespace_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	while (
		libias_list_tail(edit->node->words.list)
		&& ast_word_whitespace_p(libias_list_entry_value(libias_list_tail(edit->node->words.list)))
		)
	{
		ast_word_list_entry_remove(edit->node, libias_list_tail(edit->node->words.list));
	}
}

void
ast_words_edit_remove_whitespace_after_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);

	struct LibiasListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	if (!list_entry) {
		return;
	}
	if (
		!ast_word_bool_p(libias_list_entry_value(list_entry))
		&& libias_list_tail(edit->node->words.list) != libias_list_entry_next(list_entry)
		)
	{
		return;
	}
	if (
		libias_list_entry_next(list_entry)
		&& ast_word_whitespace_p(libias_list_entry_value(libias_list_entry_next(list_entry)))
		)
	{
		ast_word_list_entry_remove(edit->node, libias_list_entry_next(list_entry));
	}
}

void
ast_words_edit_remove_whitespace_before_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);

	struct LibiasListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	if (!list_entry) {
		return;
	}
	if (
		!ast_word_bool_p(libias_list_entry_value(list_entry))
		&& libias_list_head(edit->node->words.list) != libias_list_entry_prev(list_entry)
		)
	{
		return;
	}
	if (
		libias_list_entry_prev(list_entry)
		&& ast_word_whitespace_p(libias_list_entry_value(libias_list_entry_prev(list_entry)))
		)
	{
		ast_word_list_entry_remove(edit->node, libias_list_entry_prev(list_entry));
	}
}

void
ast_words_edit_set_whitespace_after_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);
	struct ASTWord *whitespace = libias_queue_pop(args);

	struct LibiasListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	if (!list_entry) {
		return;
	}

	// Can't add whitespace after an AST_WORD_COMMENT
	if (ast_word_comment_p(libias_list_entry_value(list_entry))) {
		return;
	}

	ast_word_list_insert_after(edit->node, edit->diagnostic, list_entry, whitespace, true);
	libias_mempool_forget(edit->pool, whitespace);
}

void
ast_words_edit_set_whitespace_before_word_impl(
	struct ASTWordsEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWord *word = libias_queue_pop(args);
	struct ASTWord *whitespace = libias_queue_pop(args);

	if (!ast_word_whitespace_p(whitespace)) {
		return;
	}
	struct LibiasListEntry *list_entry;
	list_entry = ast_word_list_entry_for_word(edit->node, word);
	if (!list_entry) {
		return;
	}

	ast_word_list_insert_before(edit->node, edit->diagnostic, list_entry, whitespace, true);
	libias_mempool_forget(edit->pool, whitespace);
}

// Adds WORD after the OTHER word in the NODE. OTHER is searched for by
// identity not by value. NODE takes ownership of WORD.
void
ast_words_edit_add_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *word,
	struct ASTWord *other)
{
	libias_queue_push(edit->queue, ast_words_edit_add_word_impl);
	libias_queue_push(edit->queue, word);
	libias_mempool_take(edit->pool, word);
	libias_queue_push(edit->queue, other);
}

// Adds WORD before the OTHER word in the NODE. OTHER is searched for by
// identity not by value. NODE takes ownership of WORD.
void
ast_words_edit_add_word_before(
	struct ASTWordsEdit *edit,
	struct ASTWord *word,
	struct ASTWord *other)
{
	libias_queue_push(edit->queue, ast_words_edit_add_word_before_impl);
	libias_queue_push(edit->queue, word);
	libias_mempool_take(edit->pool, word);
	libias_queue_push(edit->queue, other);
}

// Appends WORD to the NODE's word list. NODE takes ownership of WORD.
void
ast_words_edit_append_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *word)
{
	libias_queue_push(edit->queue, ast_words_edit_append_word_impl);
	libias_queue_push(edit->queue, word);
	libias_mempool_take(edit->pool, word);
}

// Remove WORD from NODE. WORD is searched for by identity not by value.
// If there is whitespace after WORD it is removed too. All removed
// words are freed immediately.
void
ast_words_edit_remove_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *word)
{
	libias_queue_push(edit->queue, ast_words_edit_remove_word_impl);
	libias_queue_push(edit->queue, word);
}

void
ast_words_edit_replace_word(
	struct ASTWordsEdit *edit,
	struct ASTWord *old_word,
	struct ASTWord *new_word)
{
	libias_queue_push(edit->queue, ast_words_edit_replace_word_impl);
	libias_queue_push(edit->queue, old_word);
	libias_queue_push(edit->queue, new_word);
	libias_mempool_take(edit->pool, new_word);
}

void
ast_words_edit_remove_all_words(
	struct ASTWordsEdit *edit)
{
	libias_queue_push(edit->queue, ast_words_edit_remove_all_words_impl);
}

void
ast_words_edit_remove_beginning_whitespace(
	struct ASTWordsEdit *edit)
{
	libias_queue_push(edit->queue, ast_words_edit_remove_beginning_whitespace_impl);
}

void
ast_words_edit_remove_trailing_whitespace(
	struct ASTWordsEdit *edit)
{
	libias_queue_push(edit->queue, ast_words_edit_remove_trailing_whitespace_impl);
}

void
ast_words_edit_remove_whitespace_after_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word)
{
	libias_queue_push(edit->queue, ast_words_edit_remove_whitespace_after_word_impl);
	libias_queue_push(edit->queue, word);
}

void
ast_words_edit_remove_whitespace_before_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word)
{
	libias_queue_push(edit->queue, ast_words_edit_remove_whitespace_before_word_impl);
	libias_queue_push(edit->queue, word);
}

void
ast_words_edit_set_whitespace_after_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word,
	struct ASTWord *ws)
{
	libias_queue_push(edit->queue, ast_words_edit_set_whitespace_after_word_impl);
	libias_queue_push(edit->queue, word);
	libias_queue_push(edit->queue, ws);
	libias_mempool_take(edit->pool, ws);
}

void
ast_words_edit_set_whitespace_before_word(
	struct ASTWordsEdit *edit,
	const struct ASTWord *word,
	struct ASTWord *ws)
{
	libias_queue_push(edit->queue, ast_words_edit_set_whitespace_before_word_impl);
	libias_queue_push(edit->queue, word);
	libias_queue_push(edit->queue, ws);
	libias_mempool_take(edit->pool, ws);
}

// Render the NODE's words into a newly allocated string. The caller is
// responsible for freeing it.
char *
ast_words_flatten(struct AST *node)
{
	if (!node->words.list) {
		return libias_str_dup("");
	}

	libias_scope_buffer(f);
	ast_words_foreach(node, word) {
		ast_word_render(word, f);
	}
	return libias_buffer_data_disown(f);
}

// Render the NODE's words into a newly allocated string. Meta words are
// skipped and each word is separated by SEPARATOR. The caller is
// responsible for freeing the returned value.
char *
ast_words_no_meta_flatten(
	struct AST *node,
	const char *separator)
{
	if (!node->words.list) {
		return libias_str_dup("");
	}

	libias_scope_buffer(f);
	size_t print_sep = false;
	ast_words_foreach(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		}
		if (print_sep) {
			libias_buffer_puts(f, separator);
		} else {
			print_sep = true;
		}
		ast_word_render(word, f);
	}
	return libias_buffer_data_disown(f);
}

// Return the number of words of NODE.
size_t
ast_words_len(struct AST *node)
{
	if (node->words.list) {
		return libias_list_len(node->words.list);
	} else {
		return 0;
	}
}

// Return the number of non-meta words of NODE.
size_t
ast_words_no_meta_len(struct AST *node)
{
	if (!node->words.list) {
		return 0;
	}

	size_t len = 0;
	ast_words_foreach(node, word) {
		if (ast_word_meta_p(word)) {
			// nothing
		} else {
			len++;
		}
	}
	return len;
}

// Return the sibling just before NODE. Return NULL if there is no such
// sibling. Return NULL if NODE has no next sibling.
struct AST *
ast_next_sibling(struct AST *node)
{
	if (node->parent == node) {
		return NULL;
	} else if (!node->parent->children.list) {
		return NULL;
	}

	struct LibiasListEntry *list_entry;
	list_entry = ast_child_list_entry_for_node(node->parent, node);
	if (!list_entry) {
		return NULL;
	}

	list_entry = libias_list_entry_next(list_entry);
	if (!list_entry) {
		return NULL;
	}

	return libias_list_entry_value(list_entry);
}

// Return the sibling just after NODE. Return NULL if there is no such
// sibling. Return NULL if NODE has no previous sibling.
struct AST *
ast_prev_sibling(struct AST *node)
{
	if (node->parent == node) {
		return NULL;
	} else if (!node->parent->children.list) {
		return NULL;
	}

	struct LibiasListEntry *list_entry;
	list_entry = ast_child_list_entry_for_node(node->parent, node);
	if (!list_entry) {
		return NULL;
	}

	list_entry = libias_list_entry_prev(list_entry);
	if (!list_entry) {
		return NULL;
	}

	return libias_list_entry_value(list_entry);
}

// Return the word just after CURRENT_WORD. Return NULL if there is no
// such word or if CURRENT_WORD isn't part of NODE. If CURRENT_WORD is
// NULL return the first word of NODE or NULL if NODE has no words.
struct ASTWord *
ast_next_word(
	struct AST *node,
	struct ASTWord *current_word)
{
	if (!node->words.list) {
		return NULL;
	}

	struct LibiasListEntry *list_entry;
	if (current_word) {
		list_entry = ast_word_list_entry_for_word(node, current_word);
	} else {
		list_entry = libias_list_head(node->words.list);
	}
	if (!list_entry) {
		return NULL;
	}

	list_entry = libias_list_entry_next(list_entry);
	if (!list_entry) {
		return NULL;
	}

	return libias_list_entry_value(list_entry);
}

// Return the word just before CURRENT_WORD. Return NULL if there is no
// such word or if CURRENT_WORD isn't part of NODE. If CURRENT_WORD is
// NULL return the first word of NODE or NULL if NODE has no words.
struct ASTWord *
ast_prev_word(
	struct AST *node,
	struct ASTWord *current_word)
{
	if (!node->words.list) {
		return NULL;
	}

	struct LibiasListEntry *list_entry;
	if (current_word) {
		list_entry = ast_word_list_entry_for_word(node, current_word);
	} else {
		list_entry = libias_list_head(node->words.list);
	}
	if (!list_entry) {
		return NULL;
	}

	list_entry = libias_list_entry_prev(list_entry);
	if (!list_entry) {
		return NULL;
	}

	return libias_list_entry_value(list_entry);
}

struct LibiasListEntry *
ast_child_list_append(
	struct AST *node,
	struct AST *new_child)
{
	libias_panic_if(
		libias_hashmap_contains_p(node->children.child_to_list_entry_map, new_child),
		"cannot add child twice to the same tree");
	struct LibiasListEntry *new_entry = libias_list_append(node->children.list, new_child);
	libias_hashmap_add(node->children.child_to_list_entry_map, new_child, new_entry);
	new_child->parent = node;
	return new_entry;
}

struct LibiasListEntry *
ast_child_list_insert_before(
	struct AST *node,
	struct LibiasListEntry *list_entry,
	struct AST *new_child)
{
	libias_panic_if(
		libias_hashmap_contains_p(node->children.child_to_list_entry_map, new_child),
		"cannot add child twice to the same tree");
	struct LibiasListEntry *new_entry = libias_list_insert_before(
		node->children.list,
		list_entry,
		new_child);
	libias_hashmap_add(node->children.child_to_list_entry_map, new_child, new_entry);
	new_child->parent = node;
	return new_entry;
}

struct LibiasListEntry *
ast_child_list_insert_after(
	struct AST *node,
	struct LibiasListEntry *list_entry,
	struct AST *new_child)
{
	libias_panic_if(
		libias_hashmap_contains_p(node->children.child_to_list_entry_map, new_child),
		"cannot add child twice to the same tree");
	struct LibiasListEntry *new_entry = libias_list_insert_after(
		node->children.list,
		list_entry,
		new_child);
	libias_hashmap_add(node->children.child_to_list_entry_map, new_child, new_entry);
	new_child->parent = node;
	return new_entry;
}

struct LibiasListEntry *
ast_child_list_entry_for_node(
	struct AST *node,
	struct AST *child)
{
	return libias_hashmap_get(node->children.child_to_list_entry_map, child);
}

struct LibiasListEntry *
ast_child_list_entry_remove(
	struct AST *node,
	struct LibiasListEntry *entry)
{
	libias_hashmap_remove(
		node->children.child_to_list_entry_map,
		libias_list_entry_value(entry));
	return libias_list_entry_remove(entry);
}

struct ASTEdit *
ast_edit_new(
	struct AST *node,
	struct Diagnostic *diagnostic,
	enum ASTEditFlags flags)
{
	struct ASTEdit *edit = libias_alloc(struct ASTEdit);
	edit->external_diagnostic = diagnostic;
	edit->diagnostic = diagnostic_new();
	edit->flags = flags;
	edit->root = node;
	edit->queue = libias_queue_new();
	edit->pool = libias_mempool_new();
	return edit;
}

void
ast_edit_cleanup(struct ASTEdit **edit_ptr)
{
	struct ASTEdit *edit = *edit_ptr;
	if (edit) {
		libias_cleanup(&edit->diagnostic);
		libias_cleanup(&edit->queue);
		libias_cleanup(&edit->pool);
		libias_free(edit);
		*edit_ptr = NULL;
	}
}

// Applies the queued up edit operations. Returns true iff EDIT's
// diagnostic object has no queued up error messages.
bool
ast_edit_apply(struct ASTEdit *edit)
{
	void (*fn)(struct ASTEdit *, struct LibiasQueue *) = NULL;
	while ((fn = libias_queue_pop(edit->queue)) != NULL) {
		fn(edit, edit->queue);
	}

	// Clear query cache since it might no longer be valid
	if (fn) {
		ast_set_query_cache(edit->root, NULL, NULL);
	}

	// Check node invariants are still ok
	// ast_validate(edit->node);

	bool status = !diagnostic_error_p(edit->diagnostic);

	diagnostic_move_reports(edit->external_diagnostic, edit->diagnostic);

	libias_cleanup(&edit);

	return status;
}

struct LibiasListEntry *
ast_find_child(
	struct AST *root,
	struct AST *child,
	struct AST **parent)
{
	if (!root->children.list) {
		return NULL;
	}

	struct LibiasListEntry *entry = ast_child_list_entry_for_node(root, child);
	if (entry) {
		*parent = root;
		return entry;
	}

	ast_children_foreach(root, node) {
		struct LibiasListEntry *entry = ast_find_child(node, child, parent);
		if (entry) {
			return entry;
		}
	}

	return NULL;
}

void
ast_edit_add_child_before_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	struct AST *new_child = libias_queue_pop(args);
	struct AST *other = libias_queue_pop(args);

	struct AST *parent = NULL;
	struct LibiasListEntry *list_entry = ast_find_child(
		edit->root,
		other,
		&parent);
	if (list_entry) {
		ast_child_list_insert_before(parent, list_entry, new_child);
	} else {
		ast_child_list_append(edit->root, new_child);
	}
	libias_mempool_forget(edit->pool, new_child);
}

void
ast_edit_add_child_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	struct AST *new_child = libias_queue_pop(args);
	struct AST *other = libias_queue_pop(args);

	struct AST *parent = NULL;
	struct LibiasListEntry *list_entry = ast_find_child(
		edit->root,
		other,
		&parent);
	if (list_entry) {
		ast_child_list_insert_after(parent, list_entry, new_child);
	} else {
		ast_child_list_append(edit->root, new_child);
	}
	libias_mempool_forget(edit->pool, new_child);
}

void
ast_edit_append_child_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	struct AST *new_child = libias_queue_pop(args);
	ast_child_list_append(edit->root, new_child);
	libias_mempool_forget(edit->pool, new_child);
}

void
ast_edit_remove_child_impl(
	struct ASTEdit  *edit,
	struct LibiasQueue *args)
{
	struct AST *child = libias_queue_pop(args);

	struct AST *parent = NULL;
	struct LibiasListEntry *list_entry = ast_find_child(
		edit->root,
		child,
		&parent);
	if (list_entry) {
		struct AST *child = libias_list_entry_value(list_entry);
		libias_cleanup(&child);
	}
}

void
ast_edit_remove_all_children_impl(
	struct ASTEdit  *edit,
	struct LibiasQueue *args)
{
	libias_scope_mempool(pool);
	ast_children_foreach(edit->root, child) {
		libias_mempool_take(pool, child);
	}
}

void
ast_edit_replace_child_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	struct AST *child = libias_queue_pop(args);
	struct AST *replacement = libias_queue_pop(args);

	struct AST *parent = NULL;
	struct LibiasListEntry *list_entry = ast_find_child(
		edit->root,
		child,
		&parent);
	if (list_entry) {
		ast_child_list_insert_after(parent, list_entry, replacement);
		libias_mempool_forget(edit->pool, replacement);
		libias_cleanup(&child);
	}
}

void
ast_edit_chain_apply_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTEdit *other = libias_queue_pop(args);
	ast_edit_apply(other);
	libias_mempool_forget(edit->pool, other);
}

void
ast_edit_apply_words_edit_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	struct ASTWordsEdit *words_edit = libias_queue_pop(args);
	ast_words_edit_apply(words_edit);
	libias_mempool_forget(edit->pool, words_edit);
}

void
ast_edit_set_variable_modifier_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	enum ASTVariableModifier *modifier = libias_queue_pop(args);
	edit->root->variable.modifier = *modifier;
	libias_mempool_release(edit->pool, modifier);
}

void
ast_edit_set_target_rule_operator_impl(
	struct ASTEdit *edit,
	struct LibiasQueue *args)
{
	enum ASTTargetRuleOperator *operator = libias_queue_pop(args);
	edit->root->target_rule.operator = *operator;
	libias_mempool_release(edit->pool, operator);
}

// Add NEW_NODE to OTHER's parent just above OTHER. NEW_NODE becomes a
// new sibling of OTHER.
void
ast_edit_add_child_before(
	struct ASTEdit *edit,
	struct AST *new_child,
	struct AST *other)
{
	libias_queue_push(edit->queue, ast_edit_add_child_before_impl);
	libias_queue_push(edit->queue, new_child);
	libias_mempool_take(edit->pool, new_child);
	libias_queue_push(edit->queue, other);
}

// Adds NEW_CHILD to the tree. NEW_CHILD will be added after OTHER. The
// AST will take ownership of NEW_CHILD.
void
ast_edit_add_child(
	struct ASTEdit *edit,
	struct AST *new_child,
	struct AST *other)
{
	libias_queue_push(edit->queue, ast_edit_add_child_impl);
	libias_queue_push(edit->queue, new_child);
	libias_mempool_take(edit->pool, new_child);
	libias_queue_push(edit->queue, other);
}

// Appends NEW_CHILD to the tree. The AST will take ownership of
// NEW_CHILD.
void
ast_edit_append_child(
	struct ASTEdit *edit,
	struct AST *new_child)
{
	libias_queue_push(edit->queue, ast_edit_append_child_impl);
	libias_queue_push(edit->queue, new_child);
	libias_mempool_take(edit->pool, new_child);
}

// Removes CHILD from the edited tree. CHILD will be freed.
void
ast_edit_remove_child(
	struct ASTEdit *edit,
	struct AST *child)
{
	libias_queue_push(edit->queue, ast_edit_remove_child_impl);
	libias_queue_push(edit->queue, child);
}

// Remove all children of the edited tree.
void
ast_edit_remove_all_children(struct ASTEdit *edit)
{
	libias_queue_push(edit->queue, ast_edit_remove_all_children_impl);
}

// Replaces CHILD with REPLACEMENT. CHILD will be freed.
void
ast_edit_replace_child(
	struct ASTEdit *edit,
	struct AST *child,
	struct AST *replacement)
{
	libias_queue_push(edit->queue, ast_edit_replace_child_impl);
	libias_queue_push(edit->queue, child);
	libias_queue_push(edit->queue, replacement);
	libias_mempool_take(edit->pool, replacement);
}

// Apply OTHER together with EDIT.
void
ast_edit_chain_apply(
	struct ASTEdit *edit,
	struct ASTEdit *other)
{
	libias_queue_push(edit->queue, ast_edit_chain_apply_impl);
	libias_queue_push(edit->queue, other);
	libias_mempool_take(edit->pool, other);
}

// Apply WORDS_EDIT together with EDIT. Editing words doesn't change the
// tree structure, so it could safely be applied immediately. With this
// function you can delay it and only apply it when the entire edit
// operation is applied.
//
// EDIT takes ownership of WORDS_EDIT.
void
ast_edit_apply_words_edit(
	struct ASTEdit *edit,
	struct ASTWordsEdit *words_edit)
{
	libias_queue_push(edit->queue, ast_edit_apply_words_edit_impl);
	libias_queue_push(edit->queue, words_edit);
	libias_mempool_take(edit->pool, words_edit);
}

// Set the node's variable modifier. The node must be a
// AST_VARIABLE or this is a no-op.
void
ast_edit_set_variable_modifier(
	struct ASTEdit *edit,
	const enum ASTVariableModifier modifier)
{
	if (!ast_variable_p(edit->root)) {
		return;
	}

	libias_queue_push(edit->queue, ast_edit_set_variable_modifier_impl);
	enum ASTVariableModifier *mod = libias_mempool_alloc(
		edit->pool,
		enum ASTVariableModifier);
	*mod = modifier;
	libias_queue_push(edit->queue, mod);
}

// Set the node's target rule operator. The node must be a
// AST_TARGET_RULE or this is a no-op.
void
ast_edit_set_target_rule_operator(
	struct ASTEdit *edit,
	const enum ASTTargetRuleOperator operator)
{
	if (!ast_target_rule_p(edit->root)) {
		return;
	}

	libias_queue_push(edit->queue, ast_edit_set_target_rule_operator_impl);
	enum ASTTargetRuleOperator *op = libias_mempool_alloc(
		edit->pool,
		enum ASTTargetRuleOperator);
	*op = operator;
	libias_queue_push(edit->queue, op);
}

void
ast_visit_trait_init(struct ASTVisitTrait *this)
{
	memset(this, 0, sizeof(struct ASTVisitTrait));
}

ASTVisitFn
ast_visit_trait_get_pre_visit_fn(
	struct ASTVisitTrait *trait,
	enum ASTType type)
{
	switch (type) {
	case AST_ROOT:
		return trait->visit_pre_root;
	case AST_COMMENT:
		return trait->visit_pre_comment;
	case AST_EXPR:
		return trait->visit_pre_expr;
	case AST_FOR:
		return trait->visit_pre_for;
	case AST_FOR_BINDINGS:
		return trait->visit_pre_for_bindings;
	case AST_FOR_WORDS:
		return trait->visit_pre_for_words;
	case AST_FOR_BODY:
		return trait->visit_pre_for_body;
	case AST_FOR_END:
		return trait->visit_pre_for_end;
	case AST_IF:
		return trait->visit_pre_if;
	case AST_IF_BRANCH:
		return trait->visit_pre_if_branch;
	case AST_INCLUDE:
		return trait->visit_pre_include;
	case AST_TARGET_COMMAND:
		return trait->visit_pre_target_command;
	case AST_TARGET_RULE:
		return trait->visit_pre_target_rule;
	case AST_TARGET_RULE_TARGETS:
		return trait->visit_pre_target_rule_targets;
	case AST_TARGET_RULE_DEPENDENCIES:
		return trait->visit_pre_target_rule_dependencies;
	case AST_TARGET_RULE_BODY:
		return trait->visit_pre_target_rule_body;
	case AST_VARIABLE:
		return trait->visit_pre_variable;
	case AST_VARIABLE_NAME:
		return trait->visit_pre_variable_name;
	}

	return NULL;
}

ASTVisitFn
ast_visit_trait_get_visit_fn(
	struct ASTVisitTrait *trait,
	enum ASTType type)
{
	switch (type) {
	case AST_ROOT:
		return trait->visit_root;
	case AST_COMMENT:
		return trait->visit_comment;
	case AST_EXPR:
		return trait->visit_expr;
	case AST_FOR:
		return trait->visit_for;
	case AST_FOR_BINDINGS:
		return trait->visit_for_bindings;
	case AST_FOR_WORDS:
		return trait->visit_for_words;
	case AST_FOR_BODY:
		return trait->visit_for_body;
	case AST_FOR_END:
		return trait->visit_for_end;
	case AST_IF:
		return trait->visit_if;
	case AST_IF_BRANCH:
		return trait->visit_if_branch;
	case AST_INCLUDE:
		return trait->visit_include;
	case AST_TARGET_COMMAND:
		return trait->visit_target_command;
	case AST_TARGET_RULE:
		return trait->visit_target_rule;
	case AST_TARGET_RULE_TARGETS:
		return trait->visit_target_rule_targets;
	case AST_TARGET_RULE_DEPENDENCIES:
		return trait->visit_target_rule_dependencies;
	case AST_TARGET_RULE_BODY:
		return trait->visit_target_rule_body;
	case AST_VARIABLE:
		return trait->visit_variable;
	case AST_VARIABLE_NAME:
		return trait->visit_variable_name;
	}

	return NULL;
}

void
ast_visit_pre_dispatch(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (libias_hashset_contains_p(visit->visited_nodes.pre, node)) {
		return;
	}

	ASTVisitPredicateFn visit_pre_node_p = visit->trait.visit_pre_node_p;
	if (visit_pre_node_p) {
		if (!visit_pre_node_p(visit, node)) {
			goto done;
		}
	}

	ASTVisitFn visit_fn = ast_visit_trait_get_pre_visit_fn(
		&visit->trait,
		ast_type(node));
	if (visit_fn) {
		visit_fn(visit, node);
	} else if (visit->trait.default_pre_visit_fn) {
		visit->trait.default_pre_visit_fn(visit, node);
	}

done:
	libias_hashset_add(visit->visited_nodes.pre, node);
}

void
ast_visit_dispatch(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (libias_hashset_contains_p(visit->visited_nodes.post, node)) {
		return;
	}

	ASTVisitPredicateFn visit_node_p = visit->trait.visit_node_p;
	if (visit_node_p) {
		if (!visit_node_p(visit, node)) {
			goto done;
		}
	}

	ASTVisitFn visit_fn = ast_visit_trait_get_visit_fn(
		&visit->trait,
		ast_type(node));
	if (visit_fn) {
		visit_fn(visit, node);
	} else if (visit->trait.default_visit_fn) {
		visit->trait.default_visit_fn(visit, node);
	}

done:
	libias_hashset_add(visit->visited_nodes.post, node);
}

void
ast_visit_helper(
	struct ASTVisit *visit,
	struct AST *node)
{
	ast_visit_pre_dispatch(visit, node);

	switch (visit->state) {
	case AST_VISIT_STATE_CONTINUE:
	{
		ASTVisitPredicateFn visit_children_p = visit->trait.visit_children_p;
		if (!visit_children_p || visit_children_p(visit, node)) {
			if (node->children.list) {
				ast_children_foreach(node, child) {
					ast_visit_helper(visit, child);
					switch (visit->state) {
					case AST_VISIT_STATE_ABORT:
					case AST_VISIT_STATE_STOP:
						return;
					case AST_VISIT_STATE_CONTINUE:
						break;
					}
				}
			}
		}
		ast_visit_dispatch(visit, node);
		return;
	}
	case AST_VISIT_STATE_ABORT:
	case AST_VISIT_STATE_STOP:
		return;
	}
}

void
ast_visit_run(struct ASTVisit *visit)
{
	ast_visit_reinit(visit);
	ast_visit_helper(visit, visit->root);
	ast_visit_done(visit);
}

// Set the current formatting settings. This determines the formatting
// settings of all subsequent calls to ast_vist_format() until
// ast_visit_set format_settings() is called again. Calls to
// ast_visit_set_format_settings() will not override the settings of
// already scheduled formats.
//
// VISIT will _not_ take ownership of RULES and FORMAT_SETTINGS. The
// lifetime of both must at least as long as VISIT.
void
ast_visit_set_format_settings(
	struct ASTVisit *visit,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings)
{
	visit->format.settings = format_settings;
	visit->format.rules = rules;
}

// Schedule a format of NODE with the current formatting
// settings. ast_visit_set_format_settings() must have been called
// first. The formatting will happen after the visit.
void
ast_visit_format(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (visit->format.settings && visit->format.rules) {
		libias_queue_push(visit->format.queue, node);
		libias_queue_push(visit->format.queue, visit->format.settings);
		libias_queue_push(visit->format.queue, visit->format.rules);
	}
}

struct ASTVisit *
ast_visit_new(
	struct AST *node,
	struct ASTVisitTrait *trait)
{
	struct ASTVisit *visit = libias_alloc(struct ASTVisit);

	visit->format.queue = libias_queue_new();
	visit->visited_nodes.pre = libias_hashset_new(libias_compare_ptr, libias_hash_ptr);
	visit->visited_nodes.post = libias_hashset_new(libias_compare_ptr, libias_hash_ptr);
	visit->root = node;
	visit->trait = *trait;
	visit->diagnostic = diagnostic_new();

	return visit;
}

void
ast_visit_cleanup(struct ASTVisit **visit_ptr)
{
	struct ASTVisit *visit = *visit_ptr;
	if (visit) {
		libias_cleanup(&visit->format.queue);
		libias_cleanup(&visit->visited_nodes.pre);
		libias_cleanup(&visit->visited_nodes.post);
		libias_cleanup(&visit->root_ast_edit);
		libias_cleanup(&visit->diagnostic);
		libias_free(visit);
		*visit_ptr = NULL;
	}
}

void
ast_visit_reinit(struct ASTVisit *visit)
{
	diagnostic_truncate(visit->diagnostic);
	libias_queue_truncate(visit->format.queue);
	libias_hashset_truncate(visit->visited_nodes.pre);
	libias_hashset_truncate(visit->visited_nodes.post);
	visit->state = AST_VISIT_STATE_CONTINUE;

	if (!visit->root_ast_edit) {
		visit->root_ast_edit = ast_edit_new(
			visit->root,
			visit->diagnostic,
			AST_EDIT_DEFAULT);
	}
}

void
ast_visit_done(struct ASTVisit *visit)
{
	switch (visit->state) {
	case AST_VISIT_STATE_STOP:
	case AST_VISIT_STATE_CONTINUE:
	{
		ast_edit_apply(visit->root_ast_edit);
		visit->root_ast_edit = NULL;
		struct AST *node;
		while ((node = libias_queue_pop(visit->format.queue)) != NULL) {
			struct ASTFormatSettings *settings = libias_queue_pop(visit->format.queue);
			struct Rules *rules = libias_queue_pop(visit->format.queue);
			ast_format(node, rules, settings, visit->diagnostic);
		}
		break;
	}
	case AST_VISIT_STATE_ABORT:
		libias_cleanup(&visit->root_ast_edit);
		break;
	}
}

void
ast_visit_set_context(
	struct ASTVisit *visit,
	void *context)
{
	visit->context = context;
}

void *
ast_visit_context(struct ASTVisit *visit)
{
	return visit->context;
}

struct Diagnostic *
ast_visit_diagnostic(struct ASTVisit *visit)
{
	return visit->diagnostic;
}

// Return "root" node of VISIT. This is the node that was passed to
// ast_visit_new() and might not be the actual root of the tree.
struct AST *
ast_visit_root(struct ASTVisit *visit)
{
	return visit->root;
}

struct ASTEdit *
ast_visit_root_edit(struct ASTVisit *visit)
{
	return visit->root_ast_edit;
}

// Tells VISIT to abort. Same as ast_visit_stop() but the queued up
// edits and formatting requests will be discarded.
void
ast_visit_abort(struct ASTVisit *visit)
{
	visit->state = AST_VISIT_STATE_ABORT;
}

// Tells VISIT to continue. This can be used to overwrite a previous
// ast_vist_stop() or ast_vist_abort().
void
ast_visit_continue(struct ASTVisit *visit)
{
	visit->state = AST_VISIT_STATE_CONTINUE;
}

// Tells VISIT to stop. No more nodes will be visited. Any queued up
// edits and formatting requests will still be applied.
void
ast_visit_stop(struct ASTVisit *visit)
{
	visit->state = AST_VISIT_STATE_STOP;
}

// Skip the visit of NODE. NODE is added to the set of already visited
// nodes. This is useless if you have already processed NODE and don't
// need to do it again. The visit_* methods will not be called for NODE.
void
ast_visit_skip(
	struct ASTVisit *visit,
	struct AST *node)
{
	libias_hashset_add(visit->visited_nodes.post, node);
	libias_hashset_add(visit->visited_nodes.pre, node);
}

bool
ast_visit_visited_node_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	return libias_hashset_contains_p(visit->visited_nodes.post, node);
}

bool
ast_visit_pre_visited_node_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	return libias_hashset_contains_p(visit->visited_nodes.pre, node);
}

struct LibiasIterator *
ast_children_iterator(struct AST *node, ssize_t a, ssize_t b)
{
	struct ASTChildrenIterator *this = libias_alloc(struct ASTChildrenIterator);
	this->node = node;
	size_t len = 0;
	if (node->children.list) {
		this->entry = libias_list_head(node->children.list);
		len = libias_list_len(node->children.list);
	}
	libias_slice_to_range(len, a, b, &this->i, &this->len);

	for (size_t i = 0; this->entry && i < this->i; i++) {
		this->entry = libias_list_entry_next(this->entry);
	}

	return libias_iterator_new(
		ast_children_iterator_next,
		this,
		ast_children_iterator_cleanup);
}

void
ast_children_iterator_cleanup(void **this_ptr)
{
	struct ASTChildrenIterator *this = *this_ptr;
	if (this) {
		libias_free(this);
		*this_ptr = NULL;
	}
}

bool
ast_children_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct ASTChildrenIterator *this = libias_iterator_context(*it_ptr);
	if (this->entry && this->i < this->len) {
		*index = this->i++;
		*key = libias_list_entry_value(this->entry);
		*value = libias_list_entry_value(this->entry);
		this->entry = libias_list_entry_next(this->entry);
		return true;
	} else {
		return false;
	}
}

struct LibiasIterator *
ast_words_iterator(struct AST *node, ssize_t a, ssize_t b)
{
	struct ASTWordsIterator *this = libias_alloc(struct ASTWordsIterator);
	this->node = node;
	size_t len = 0;
	if (node->words.list) {
		this->entry = libias_list_head(node->words.list);
		len = libias_list_len(node->words.list);
	}
	libias_slice_to_range(len, a, b, &this->i, &this->len);

	for (size_t i = 0; this->entry && i < this->i; i++) {
		this->entry = libias_list_entry_next(this->entry);
	}

	return libias_iterator_new(
		ast_words_iterator_next,
		this,
		ast_words_iterator_cleanup);
}

void
ast_words_iterator_cleanup(void **this_ptr)
{
	struct ASTWordsIterator *this = *this_ptr;
	if (this) {
		libias_free(this);
		*this_ptr = NULL;
	}
}

bool
ast_words_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct ASTWordsIterator *this = libias_iterator_context(*it_ptr);
	if (this->entry && this->i < this->len) {
		*index = this->i++;
		*key = libias_list_entry_value(this->entry);
		*value = libias_list_entry_value(this->entry);
		this->entry = libias_list_entry_next(this->entry);
		return true;
	} else {
		return false;
	}
}
