// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/hashset.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/query.h"
#include "ast/word.h"
#include "constants.h"
#include "rules.h"

enum VariableOrderFlag {
	VAR_DEFAULT                           = 0,
	VAR_CASE_SENSITIVE_SORT               = 1 << 0,
	// Lines that are best not wrapped to 80 columns
	VAR_IGNORE_WRAPCOL                    = 1 << 1,
	VAR_LEAVE_UNFORMATTED                 = 1 << 2,
	VAR_NOT_COMPARABLE                    = 1 << 3,
	VAR_PRINT_AS_NEWLINES                 = 1 << 4,
	// Do not indent with the rest of the variables in a paragraph
	VAR_SKIP_GOALCOL                      = 1 << 5,
	VAR_SORTED                            = 1 << 6,
	VAR_SUBPKG_HELPER                     = 1 << 7,
	VAR_DEDUP                             = 1 << 8,
	VAR_FOR_EACH_ARCH                     = 1 << 9,
	VAR_FOR_EACH_FREEBSD_VERSION          = 1 << 10,
	VAR_FOR_EACH_FREEBSD_VERSION_AND_ARCH = 1 << 11,
	VAR_FOR_EACH_RUBY                     = 1 << 12,
	VAR_FOR_EACH_SSL                      = 1 << 13,
};

struct TargetOrderEntry {
	const char *name;
	int opthelper;
};

struct VariableOrderEntry {
	enum BlockType block;
	const char *var;
	uint32_t flags; // see enum VariableOrderFlag
	const char *uses[2];
};

struct Rules {
	struct LibiasMempool *pool;

	uint32_t behavior; // enum RulesBehavior

	struct LibiasArray *special_variables; // [const char *]
	struct LibiasArray *special_sources; // [const char *]
	struct LibiasArray *special_targets; // [const char *]
	struct LibiasArray *target_order; // [struct TargetOrderEntry *]
	struct LibiasArray *variable_order; // [struct VariableOrderEntry *]

	struct LibiasArray *license_perms_rel; // [const char *]
	struct LibiasArray *use_budgie_rel; // [const char *]
	struct LibiasArray *use_gnome_rel; // [const char *]
	struct LibiasArray *use_kde_rel; // [const char *]
	struct LibiasArray *use_pyqt_rel; // [const char *]
	struct LibiasArray *use_qt_rel; // [const char *]

	struct LibiasArray *known_architectures; // [const char *]
	struct LibiasArray *static_shebang_langs; // [const char *]

	struct LibiasArray *target_command_wrap_after_each_token; // [const char *]

	// TODO: Convert these too?
	// struct LibiasArray *static_flavors; // [struct ConstantStaticFlavors *]
	// extern uint32_t freebsd_versions[];
	// extern size_t freebsd_versions_len;
};

struct RulesReferencedVarCollectTokensData {
	struct LibiasMempool *pool;
	struct LibiasArray *tokens;
};

// Prototypes
static void add_referenced_var_candidates(struct LibiasMempool *pool, struct LibiasArray *candidates, struct LibiasArray *cond_candidates, const char *stem, const char *ref);
static bool case_sensitive_sort(struct Rules *rules, struct AST *root, const char *var);
static bool compare_license_perms(struct Rules *rules, struct AST *root, const char *varname, const char *a, const char *b, int *result);
static bool compare_plist_files(struct Rules *rules, struct AST *root, const char *varname, const char *a, const char *b, int *result);
static int compare_rel(struct LibiasArray *rel, const char *a, const char *b);
static bool compare_use_budgie(struct Rules *rules, const char *var, const char *a, const char *b, int *result);
static bool compare_use_gnome(struct Rules *rules, const char *var, const char *a, const char *b, int *result);
static bool compare_use_kde(struct Rules *rules, const char *var, const char *a, const char *b, int *result);
static bool compare_use_pyqt(struct Rules *rules, const char *var, const char *a, const char *b, int *result);
static bool compare_use_qt(struct Rules *rules, const char *var, const char *a, const char *b, int *result);
static bool extract_arch_prefix(struct Rules *rules, struct LibiasMempool *pool, const char *var, char **prefix_without_arch, char **prefix_without_arch_osrel);
static bool extract_osrel_prefix(struct LibiasMempool *pool, const char *var, char **prefix);
static bool matches_license_name_p(struct Rules *rules, struct AST *root, const char *var);
static bool matches_options_group(struct LibiasMempool *pool, struct Rules *rules, struct AST *node, const char *s, char **prefix);
static bool parse_cabal_datadir_vars(struct LibiasMempool *pool, struct Rules *rules, struct AST *root, const char *var, char **prefix, char **suffix);
static bool parse_cabal_datadir_vars_helper(struct LibiasMempool *pool, const char *var, const char *exe, char **prefix, char **suffix);
static bool parse_flavors_helper(struct LibiasMempool *extpool, struct Rules *rules, struct AST *node, const char *var, char **prefix_ret, char **helper_ret);
static bool parse_shebang_lang(struct LibiasMempool *extpool, struct Rules *rules, struct AST *root, const char *var, char **prefix, char **suffix);
static bool parse_shebang_lang_helper(struct LibiasMempool *extpool, const char *var, const char *lang, char **prefix, char **suffix);
static char *parse_subpkg(struct LibiasMempool *extpool, struct Rules *rules, struct AST *node, const char *var_, char **subpkg_ret);
static void parse_target_options_helper(struct LibiasMempool *extpool, struct Rules *rules, struct AST *root, const char *target, char **target_out, char **opt_out, bool *state);
static char *remove_plist_keyword(const char *s, struct LibiasMempool *pool);
static void rules_referenced_var_p_collect_tokens(struct ASTVisit *visit, struct AST *node);
static bool valid_license_p(struct Rules *rules, struct AST *root, const char *license);
static bool variable_flag_p(struct Rules *rules, struct AST *root, const char *var, int flag);
static struct VariableOrderEntry *variable_order_entry_with_suffix(struct VariableOrderEntry *template, const char *suffix, struct LibiasMempool *pool);

// Constants
static const char *license_perms_rel_[] = {
	"dist-mirror",
	"no-dist-mirror",
	"dist-sell",
	"no-dist-sell",
	"pkg-mirror",
	"no-pkg-mirror",
	"pkg-sell",
	"no-pkg-sell",
	"auto-accept",
	"no-auto-accept",
	"none",
};

static const char *target_command_wrap_after_each_token_[] = {
	"${INSTALL_DATA}",
	"${INSTALL_LIB}",
	"${INSTALL_MAN}",
	"${INSTALL_PROGRAM}",
	"${INSTALL_SCRIPT}",
	"${INSTALL}",
	"${MKDIR}",
	"${MV}",
	"${REINPLACE_CMD}",
	"${RMDIR}",
	"${SED}",
	"${STRIP_CMD}",
};

static struct TargetOrderEntry target_order_[] = {
	{ "all", 0 },
	{ "post-chroot", 0 },
	{ "pre-everything", 0 },
	{ "fetch", 0 },
	{ "fetch-list", 0 },
	{ "fetch-recursive-list", 0 },
	{ "fetch-recursive", 0 },
	{ "fetch-required-list", 0 },
	{ "fetch-required", 0 },
	{ "fetch-specials", 0 },
	{ "fetch-url-list-int", 0 },
	{ "fetch-url-list", 0 },
	{ "fetch-urlall-list", 0 },
	{ "pre-fetch", 1 },
	{ "pre-fetch-script", 0 },
	{ "do-fetch", 1 },
	{ "post-fetch", 1 },
	{ "post-fetch-script", 0 },
	{ "checksum", 0 },
	{ "checksum-recursive", 0 },
	{ "extract", 0 },
	{ "pre-extract", 1 },
	{ "pre-extract-script", 0 },
	{ "do-extract", 1 },
	{ "post-extract", 1 },
	{ "post-extract-script", 0 },
	{ "patch", 0 },
	{ "pre-patch", 1 },
	{ "pre-patch-script", 0 },
	{ "do-patch", 1 },
	{ "post-patch", 1 },
	{ "post-patch-script", 0 },
	{ "configure", 0 },
	{ "pre-configure", 1 },
	{ "pre-configure-script", 0 },
	{ "do-configure", 1 },
	{ "post-configure", 1 },
	{ "post-configure-script", 0 },
	{ "build", 0 },
	{ "pre-build", 1 },
	{ "pre-build-script", 0 },
	{ "do-build", 1 },
	{ "post-build", 1 },
	{ "post-build-script", 0 },
	{ "install", 0 },
	{ "install-desktop-entries", 0 },
	{ "install-ldconfig-file", 0 },
	{ "install-mtree", 0 },
	{ "install-package", 0 },
	{ "install-rc-script", 0 },
	{ "pre-install", 1 },
	{ "pre-install-script", 0 },
	{ "pre-su-install", 0 },
	{ "do-install", 1 },
	{ "post-install", 1 },
	{ "post-install-script", 0 },
	{ "stage", 0 },
	{ "post-stage", 1 },
	{ "test", 0 },
	{ "pre-test", 1 },
	{ "do-test", 1 },
	{ "post-test", 1 },
	{ "package-name", 0 },
	{ "package-noinstall", 0 },
	{ "pre-package", 1 },
	{ "pre-package-script", 0 },
	{ "do-package", 1 },
	{ "post-package", 1 },
	{ "post-package-script", 0 },
	{ "pre-pkg-script", 0 },
	{ "pkg", 0 },
	{ "post-pkg-script", 0 },
	{ "clean", 0 },
	{ "pre-clean", 0 },
	{ "do-clean", 0 },
	{ "post-clean", 0 },

	{ "add-plist-data", 0 },
	{ "add-plist-docs", 0 },
	{ "add-plist-examples", 0 },
	{ "add-plist-info", 0 },
	{ "add-plist-post", 0 },
	{ "apply-slist", 0 },
	{ "check-already-installed", 0 },
	{ "check-build-conflicts", 0 },
	{ "check-config", 0 },
	{ "check-conflicts", 0 },
	{ "check-deprecated", 0 },
	{ "check-install-conflicts", 0 },
	{ "check-man", 0 },
	{ "check-orphans", 0 },
	{ "check-plist", 0 },
	{ "check-sanity", 0 },
	{ "check-umask", 0 },
	{ "checkpatch", 0 },
	{ "clean-depends", 0 },
	{ "compress-man", 0 },
	{ "config-conditional", 0 },
	{ "config-recursive", 0 },
	{ "config", 0 },
	{ "create-binary-alias", 0 },
	{ "create-binary-wrappers", 0 },
	{ "create-users-groups", 0 },
	{ "deinstall-all", 0 },
	{ "deinstall-depends", 0 },
	{ "deinstall", 0 },
	{ "delete-distfiles-list", 0 },
	{ "delete-distfiles", 0 },
	{ "delete-package-list", 0 },
	{ "delete-package", 0 },
	{ "depends", 0 },
	{ "describe", 0 },
	{ "distclean", 0 },
	{ "fake-pkg", 0 },
	{ "fix-shebang", 0 },
	{ "fixup-lib-pkgconfig", 0 },
	{ "generate-plist", 0 },
	{ "identify-install-conflicts", 0 },
	{ "limited-clean-depends", 0 },
	{ "maintainer", 0 },
	{ "makepatch", 0 },
	{ "makeplist", 0 },
	{ "makesum", 0 },
	{ "post-check-sanity-script", 0 },
	{ "pre-check-config", 0 },
	{ "pre-check-sanity-script", 0 },
	{ "pre-config", 0 },
	{ "pretty-print-build-depends-list", 0 },
	{ "pretty-print-config", 0 },
	{ "pretty-print-run-depends-list", 0 },
	{ "pretty-print-www-site", 0 },
	{ "readme", 0 },
	{ "readmes", 0 },
	{ "reinstall", 0 },
	{ "repackage", 0 },
	{ "restage", 0 },
	{ "rmconfig-recursive", 0 },
	{ "rmconfig", 0 },
	{ "run-autotools-fixup", 0 },
	{ "sanity-config", 0 },
	{ "security-check", 0 },
	{ "showconfig-recursive", 0 },
	{ "showconfig", 0 },
	{ "stage-dir", 0 },
	{ "stage-qa", 0 },
};

static const char *special_sources_[] = {
	".EXEC",
	".IGNORE",
	".MADE",
	".MAKE",
	".META",
	".NOMETA",
	".NOMETA_CMP",
	".NOPATH",
	".NOTMAIN",
	".OPTIONAL",
	".PHONY",
	".PRECIOUS",
	".SILENT",
	".USE",
	".USEBEFORE",
	".WAIT",
};

static const char *special_targets_[] = {
	".BEGIN",
	".DEFAULT",
	".DELETE_ON_ERROR",
	".END",
	".ERROR",
	".EXEC",
	".IGNORE",
	".INTERRUPT",
	".MADE",
	".MAIN",
	".MAKE",
	".MAKEFLAGS",
	".META",
	".NO_PARALLEL",
	".NOMAIN",
	".NOMETA_CMP",
	".NOMETA",
	".NOPATH",
	".NOTPARALLEL",
	".OBJDIR",
	".OPTIONAL",
	".ORDER",
	".PATH",
	".PHONY",
	".PRECIOUS",
	".RECURSIVE",
	".SHELL",
	".SILENT",
	".STALE",
	".SUFFIXES",
	".USE",
	".USEBEFORE",
	".WAIT",
};

// Based on: https://www.freebsd.org/doc/en/books/porters-handbook/porting-order.html
static struct VariableOrderEntry variable_order_[] = {
	{ BLOCK_PORTNAME, "PORTNAME", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "PORTVERSION", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "DISTVERSIONPREFIX", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_PORTNAME, "DISTVERSION", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "DISTVERSIONSUFFIX", VAR_SKIP_GOALCOL, {} },
	/* XXX: hack to fix inserting PORTREVISION in aspell ports */
	{ BLOCK_PORTNAME, "SPELLVERSION", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "PORTREVISION", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "PORTEPOCH", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "CATEGORIES", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "MASTER_SITES", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_PORTNAME, "MASTER_SITE_SUBDIR", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_PORTNAME, "PKGNAMEPREFIX", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "PKGNAMESUFFIX", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "DISTNAME", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "DISTNAME_aarch64", VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_PORTNAME, "DISTNAME_amd64", VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_PORTNAME, "DISTNAME_i386", VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_PORTNAME, "EXTRACT_SUFX", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "DISTFILES", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_PORTNAME, "DISTFILES_aarch64", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_PORTNAME, "DISTFILES_amd64", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_PORTNAME, "DISTFILES_i386", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_PORTNAME, "DIST_SUBDIR", VAR_DEFAULT, {} },
	{ BLOCK_PORTNAME, "EXTRACT_ONLY", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_PORTNAME, "EXTRACT_ONLY_7z", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, {} },

	{ BLOCK_PATCHFILES, "PATCH_SITES", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_PATCHFILES, "PATCH_SITE_SUBDIR", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_PATCHFILES, "PATCHFILES", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_PATCHFILES, "PATCH_DIST_STRIP", VAR_SKIP_GOALCOL, {} },

	{ BLOCK_MAINTAINER, "MAINTAINER", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_MAINTAINER, "COMMENT", VAR_IGNORE_WRAPCOL | VAR_SUBPKG_HELPER, {} },
	{ BLOCK_MAINTAINER, "WWW", VAR_PRINT_AS_NEWLINES, {} },

	{ BLOCK_LICENSE, "LICENSE", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_LICENSE, "LICENSE_COMB", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_LICENSE, "LICENSE_GROUPS", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_LICENSE, "LICENSE_NAME", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_LICENSE, "LICENSE_TEXT", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_LICENSE, "LICENSE_FILE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_LICENSE, "LICENSE_PERMS", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_LICENSE, "LICENSE_DISTFILES", VAR_SKIP_GOALCOL, {} },

	{ BLOCK_LICENSE_OLD, "RESTRICTED", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_LICENSE_OLD, "RESTRICTED_FILES", VAR_DEFAULT, {} },
	{ BLOCK_LICENSE_OLD, "NO_CDROM", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_LICENSE_OLD, "NO_PACKAGE", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_LICENSE_OLD, "LEGAL_PACKAGE", VAR_DEFAULT, {} },
	{ BLOCK_LICENSE_OLD, "LEGAL_TEXT", VAR_IGNORE_WRAPCOL, {} },

	{ BLOCK_BROKEN, "DEPRECATED", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_BROKEN, "EXPIRATION_DATE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "FORBIDDEN", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_BROKEN, "MANUAL_PACKAGE_BUILD", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },

	{ BLOCK_BROKEN, "BROKEN", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_BROKEN, "BROKEN_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_BROKEN, "BROKEN_DragonFly", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "BROKEN_FreeBSD", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "BROKEN_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_FREEBSD_VERSION_AND_ARCH, {} },
	{ BLOCK_BROKEN, "IGNORE", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "IGNORE_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_BROKEN, "IGNORE_DragonFly", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "IGNORE_FreeBSD", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "IGNORE_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_FREEBSD_VERSION_AND_ARCH, {} },
	{ BLOCK_BROKEN, "ONLY_FOR_ARCHS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "ONLY_FOR_ARCHS_REASON", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "ONLY_FOR_ARCHS_REASON_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_BROKEN, "NOT_FOR_ARCHS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "NOT_FOR_ARCHS_REASON", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_BROKEN, "NOT_FOR_ARCHS_REASON_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_ARCH, {} },

	{ BLOCK_DEPENDS, "FETCH_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "FETCH_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_DEPENDS, "EXTRACT_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "EXTRACT_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_DEPENDS, "PATCH_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "PATCH_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_DEPENDS, "CRAN_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "BUILD_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "BUILD_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_DEPENDS, "LIB_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "LIB_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_DEPENDS, "RUN_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "RUN_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_DEPENDS, "TEST_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_DEPENDS, "TEST_DEPENDS_", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
#if PORTFMT_SUBPACKAGES
	{ BLOCK_DEPENDS, "SELF_DEPENDS", VAR_SUBPKG_HELPER | VAR_SORTED, {} },
#endif

	{ BLOCK_FLAVORS, "FLAVORS", VAR_DEFAULT, {} },
	{ BLOCK_FLAVORS, "FLAVOR", VAR_DEFAULT, {} },
	{ BLOCK_FLAVORS, "FLAVORS_SUB", VAR_DEFAULT, {} },

#if PORTFMT_SUBPACKAGES
	{ BLOCK_SUBPACKAGES, "SUBPACKAGES", VAR_SORTED, {} },
#endif

	{ BLOCK_FLAVORS_HELPER, "PKGNAMEPREFIX", VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "PKGNAMESUFFIX", VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "PKG_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "EXTRACT_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "PATCH_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "FETCH_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "BUILD_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "LIB_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "RUN_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "TEST_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "CONFLICTS", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "CONFLICTS_BUILD", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "CONFLICTS_INSTALL", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "DESCR", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_FLAVORS_HELPER, "PLIST", VAR_NOT_COMPARABLE, {} },

	{ BLOCK_USES, "USES", VAR_SORTED, {} },
	{ BLOCK_USES, "BROKEN_SSL", VAR_IGNORE_WRAPCOL | VAR_SORTED, { "ssl" } },
	{ BLOCK_USES, "BROKEN_SSL_REASON", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, { "ssl" } },
	{ BLOCK_USES, "BROKEN_SSL_REASON_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_SSL, { "ssl" } },
	{ BLOCK_USES, "IGNORE_SSL", VAR_IGNORE_WRAPCOL | VAR_SORTED, { "ssl" } },
	{ BLOCK_USES, "IGNORE_SSL_REASON", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL, { "ssl" } },
	{ BLOCK_USES, "IGNORE_SSL_REASON_", VAR_IGNORE_WRAPCOL | VAR_SKIP_GOALCOL | VAR_FOR_EACH_SSL, { "ssl" } },
	{ BLOCK_USES, "IGNORE_WITH_MYSQL", VAR_SKIP_GOALCOL | VAR_SORTED, { "mysql" } },
	{ BLOCK_USES, "ANSIBLE_CMD", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_DOC_CMD", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_RUN_DEPENDS", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_DATADIR", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_ETCDIR", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_PLUGINS_PREFIX", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_MODULESDIR", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_PLUGINSDIR", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "ANSIBLE_PLUGIN_TYPE", VAR_SKIP_GOALCOL, { "ansible" } },
	{ BLOCK_USES, "INVALID_BDB_VER", VAR_SKIP_GOALCOL, { "bdb" } },
	{ BLOCK_USES, "OBSOLETE_BDB_VAR", VAR_SKIP_GOALCOL | VAR_SORTED, { "bdb" } },
	{ BLOCK_USES, "WITH_BDB_HIGHEST", VAR_SKIP_GOALCOL, { "bdb" } },
	{ BLOCK_USES, "WITH_BDB6_PERMITTED", VAR_SKIP_GOALCOL, { "bdb" } },
	{ BLOCK_USES, "CHARSETFIX_MAKEFILEIN", VAR_SKIP_GOALCOL, { "charsetfix" } },
	{ BLOCK_USES, "CPE_PART", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_VENDOR", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_PRODUCT", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_VERSION", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_UPDATE", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_EDITION", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_LANG", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_SW_EDITION", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_TARGET_SW", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_TARGET_HW", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "CPE_OTHER", VAR_DEFAULT, { "cpe" } },
	{ BLOCK_USES, "DOS2UNIX_REGEX", VAR_SORTED, { "dos2unix" } },
	{ BLOCK_USES, "DOS2UNIX_FILES", VAR_SORTED, { "dos2unix" } },
	{ BLOCK_USES, "DOS2UNIX_GLOB", VAR_SORTED, { "dos2unix" } },
	{ BLOCK_USES, "DOS2UNIX_WRKSRC", VAR_DEFAULT, { "dos2unix" } },
	{ BLOCK_USES, "FONTNAME", VAR_DEFAULT, { "fonts", "xorg-cat" /* :fonts */ } },
	{ BLOCK_USES, "FONTSDIR", VAR_DEFAULT, { "fonts", "xorg-cat" /* :fonts */ } },
	{ BLOCK_USES, "FONTPATHD", VAR_DEFAULT, { "fonts", "xorg-cat", /* :fonts */ } },
	{ BLOCK_USES, "FONTPATHSPEC", VAR_DEFAULT, { "fonts", "xorg-cat", /* :fonts */ } },
	{ BLOCK_USES, "KMODDIR", VAR_DEFAULT, { "kmod" } },
	{ BLOCK_USES, "KERN_DEBUGDIR", VAR_DEFAULT, { "kmod" } },
	{ BLOCK_USES, "NCURSES_IMPL", VAR_DEFAULT, { "ncurses" } },
	{ BLOCK_USES, "NOFONT", VAR_DEFAULT, { "xorg-cat" } },
	{ BLOCK_USES, "PATHFIX_CMAKELISTSTXT", VAR_SKIP_GOALCOL | VAR_SORTED, { "pathfix" } },
	{ BLOCK_USES, "PATHFIX_MAKEFILEIN", VAR_SKIP_GOALCOL | VAR_SORTED, { "pathfix" } },
	{ BLOCK_USES, "PATHFIX_WRKSRC", VAR_DEFAULT, { "pathfix" } },
	{ BLOCK_USES, "QMAIL_PREFIX", VAR_DEFAULT, { "qmail" } },
	{ BLOCK_USES, "QMAIL_SLAVEPORT", VAR_DEFAULT, { "qmail" } },
	{ BLOCK_USES, "TCL_PKG", VAR_DEFAULT, { "tcl", "tk" } },
	{ BLOCK_USES, "TRIGGERS", VAR_DEFAULT, { "trigger" } },
	{ BLOCK_USES, "WANT_PGSQL", VAR_SORTED, { "pgsql" } },
	{ BLOCK_USES, "USE_ANT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_ASDF", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_ASDF_FASL", VAR_DEFAULT, {} },
	{ BLOCK_USES, "FASL_BUILD", VAR_DEFAULT, {} },
	{ BLOCK_USES, "ASDF_MODULES", VAR_SORTED, {} },
	{ BLOCK_USES, "USE_BINUTILS", VAR_SORTED, {} },
	{ BLOCK_USES, "DISABLE_BINUTILS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_BUDGIE", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_CLISP", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_CSTD", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_CXXSTD", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_FPC", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_GCC", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_GECKO", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_GENERIC_PKGMESSAGE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_GITHUB", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GH_ACCOUNT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GH_PROJECT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GH_SUBDIR", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GH_TAGNAME", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GH_TUPLE", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_USES, "USE_GITLAB", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_SITE", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_ACCOUNT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_PROJECT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_COMMIT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_SUBDIR", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_TAGNAME", VAR_DEFAULT, {} },
	{ BLOCK_USES, "GL_TUPLE", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_USES, "USE_GL", VAR_SORTED, { "gl" } },
	{ BLOCK_USES, "USE_GNOME", VAR_SORTED, { "gnome" } },
	{ BLOCK_USES, "USE_GNOME_SUBR", VAR_DEFAULT, { "gnome" } },
	{ BLOCK_USES, "GCONF_CONFIG_OPTIONS", VAR_SKIP_GOALCOL, { "gnome" } },
	{ BLOCK_USES, "GCONF_CONFIG_DIRECTORY", VAR_SKIP_GOALCOL, { "gnome" } },
	{ BLOCK_USES, "GCONF_CONFIG_SOURCE", VAR_SKIP_GOALCOL, { "gnome" } },
	{ BLOCK_USES, "GCONF_SCHEMAS", VAR_SORTED, { "gnome" } },
	{ BLOCK_USES, "GLIB_SCHEMAS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, { "gnome" } },
	{ BLOCK_USES, "GNOME_HTML_DIR", VAR_DEFAULT, { "gnome" } },
	{ BLOCK_USES, "GNOME_LOCALSTATEDIR", VAR_SKIP_GOALCOL, { "gnome" } },
	{ BLOCK_USES, "GNOME_MAKEFILEIN", VAR_SKIP_GOALCOL, { "gnome" } },
	{ BLOCK_USES, "INSTALLS_OMF", VAR_DEFAULT, { "gnome" } },
	{ BLOCK_USES, "USE_GNUSTEP", VAR_SORTED, { "gnustep" } },
	{ BLOCK_USES, "GNUSTEP_PREFIX", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "DEFAULT_LIBVERSION", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_CFLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_CPPFLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_CXXFLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_OBJCCFLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_OBJCFLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_LDFLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_FLAGS", VAR_DEFAULT, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_INCLUDE_DIRS", VAR_SORTED, { "gnustep" } },
	{ BLOCK_USES, "ADDITIONAL_LIB_DIRS", VAR_SORTED, { "gnustep" } },
	{ BLOCK_USES, "USE_GSTREAMER", VAR_SORTED, { "gstreamer" } },
	{ BLOCK_USES, "USE_HORDE_BUILD", VAR_SKIP_GOALCOL, { "horde" } },
	{ BLOCK_USES, "USE_HORDE_RUN", VAR_DEFAULT, { "horde" } },
	{ BLOCK_USES, "HORDE_DIR", VAR_DEFAULT, { "horde" } },
	{ BLOCK_USES, "USE_JAVA", VAR_DEFAULT, {} },
	{ BLOCK_USES, "JAVA_VERSION", VAR_DEFAULT, {} },
	{ BLOCK_USES, "JAVA_OS", VAR_DEFAULT, {} },
	{ BLOCK_USES, "JAVA_VENDOR", VAR_DEFAULT, {} },
	{ BLOCK_USES, "JAVA_EXTRACT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "JAVA_BUILD", VAR_DEFAULT, {} },
	{ BLOCK_USES, "JAVA_RUN", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_KDE", VAR_SORTED, { "kde" } },
	{ BLOCK_USES, "KDE_INVENT", VAR_DEFAULT, { "kde" } },
	{ BLOCK_USES, "KDE_MAN_PREFIX", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_PLASMA_VERSION", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_PLASMA_BRANCH", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_FRAMEWORKS_VERSION", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_FRAMEWORKS_BRANCH", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_APPLICATIONS_VERSION", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_APPLICATIONS_SHLIB_VER", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "KDE_APPLICATIONS_BRANCH", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "CALLIGRA_VERSION", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "CALLIGRA_BRANCH", VAR_SKIP_GOALCOL, { "kde" } },
	{ BLOCK_USES, "USE_LDCONFIG", VAR_SORTED, {} },
	{ BLOCK_USES, "USE_LDCONFIG32", VAR_SORTED, {} },
	{ BLOCK_USES, "USE_LINUX", VAR_SORTED, { "linux" } },
	{ BLOCK_USES, "USE_LINUX_PREFIX", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_LINUX_RPM", VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_USES, "USE_LINUX_RPM_BAD_PERMS", VAR_SKIP_GOALCOL, { "linux" } },
	{ BLOCK_USES, "USE_LOCALE", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_LXQT", VAR_SORTED, { "lxqt" } },
	{ BLOCK_USES, "USE_MATE", VAR_SORTED, { "mate" } },
	{ BLOCK_USES, "USE_MOZILLA", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_MYSQL", VAR_DEFAULT, { "mysql" } },
	{ BLOCK_USES, "USE_OCAML", VAR_DEFAULT, {} },
	{ BLOCK_USES, "NO_OCAML_BUILDDEPENDS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "NO_OCAML_RUNDEPENDS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OCAML_FINDLIB", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OCAML_CAMLP4", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OCAML_TK", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "NO_OCAMLTK_BUILDDEPENDS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "NO_OCAMLTK_RUNDEPENDS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OCAML_LDCONFIG", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OCAMLFIND_PLIST", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OCAML_WASH", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "OCAML_PKGDIRS", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_USES, "OCAML_LDLIBS", VAR_SORTED, {} },
	{ BLOCK_USES, "OCAMLFIND", VAR_DEFAULT, {} },
	{ BLOCK_USES, "OCAMLFIND_DEPEND", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "OCAMLFIND_DESTDIR", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "OCAMLFIND_LDCONF", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "OCAMLFIND_PORT", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_OPENLDAP", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WANT_OPENLDAP_SASL", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "WANT_OPENLDAP_VER", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_PERL5", VAR_SORTED, { "perl5" } },
	{ BLOCK_USES, "PL_BUILD", VAR_DEFAULT, { "perl5" } },
	{ BLOCK_USES, "USE_PHP", VAR_SORTED, { "pear", "php" } },
	{ BLOCK_USES, "IGNORE_WITH_PHP", VAR_SKIP_GOALCOL, { "pear", "php" } },
	{ BLOCK_USES, "PHP_MODNAME", VAR_DEFAULT, { "pear", "php" } },
	{ BLOCK_USES, "PHP_MOD_PRIO", VAR_DEFAULT, { "pear", "php" } },
	{ BLOCK_USES, "PHP_VER", VAR_DEFAULT, { "pear", "php" } },
	{ BLOCK_USES, "PEAR_CHANNEL", VAR_DEFAULT, { "pear" } },
	{ BLOCK_USES, "PEAR_CHANNEL_VER", VAR_SKIP_GOALCOL, { "pear" } },
	{ BLOCK_USES, "USE_PYQT", VAR_SORTED, { "pyqt" } },
	{ BLOCK_USES, "PYQT_DIST", VAR_DEFAULT, { "pyqt" } },
	{ BLOCK_USES, "PYQT_SIPDIR", VAR_DEFAULT, { "pyqt" } },
	{ BLOCK_USES, "USE_PYTHON", VAR_SORTED, { "python", "waf" } },
	{ BLOCK_USES, "PYTHON_NO_DEPENDS", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYTHON_CMD", VAR_DEFAULT, { "python", "waf" } },
	{ BLOCK_USES, "PEP517_BUILD_DEPEND", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PEP517_BUILD_CMD", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PEP517_INSTALL_DEPEND", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PEP517_INSTALL_CMD", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYSETUP", VAR_DEFAULT, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_SETUP", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_CONFIGURE_TARGET", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_BUILD_TARGET", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_INSTALL_TARGET", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_CONFIGUREARGS", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_BUILDARGS", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_INSTALLARGS", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_INSTALLNOSINGLE", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_PKGNAME", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_PKGVERSION", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_EGGINFO", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYDISTUTILS_EGGINFODIR", VAR_SKIP_GOALCOL, { "python", "waf" } },
	{ BLOCK_USES, "PYTEST_ARGS", VAR_SKIP_GOALCOL, { "pytest" } },
	{ BLOCK_USES, "PYTEST_BROKEN_TESTS", VAR_SKIP_GOALCOL, { "python", "pytest" } },\
	{ BLOCK_USES, "PYTEST_IGNORED_TESTS", VAR_SKIP_GOALCOL, { "python", "pytest" } }, \
	// XXX: PYTEST_ENABLE_* shouldn't be set by ports, so maybe they shouldn't be here.
	{ BLOCK_USES, "PYTEST_ENABLE_ALL_TESTS", VAR_SKIP_GOALCOL, { "pytest" } },
	{ BLOCK_USES, "PYTEST_ENABLE_BROKEN_TESTS", VAR_SKIP_GOALCOL, { "pytest" } },
	{ BLOCK_USES, "PYTEST_ENABLE_IGNORED_TESTS", VAR_SKIP_GOALCOL, { "pytest" } },
	{ BLOCK_USES, "USE_QT", VAR_SORTED, { "qt", "qt-dist" } },
	{ BLOCK_USES, "QT_BINARIES", VAR_DEFAULT, { "qt", "qt-dist" } },
	{ BLOCK_USES, "QT_CONFIG", VAR_DEFAULT, { "qt", "qt-dist" } },
	{ BLOCK_USES, "QT_DEFINES", VAR_DEFAULT, { "qt", "qt-dist" } },
	{ BLOCK_USES, "QT5_VERSION", VAR_DEFAULT, { "qt", "qt-dist" } },
	{ BLOCK_USES, "USE_RC_SUBR", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_RUBY", VAR_DEFAULT, {} },
	{ BLOCK_USES, "BROKEN_RUBY", VAR_IGNORE_WRAPCOL | VAR_FOR_EACH_RUBY, {} },
	{ BLOCK_USES, "RUBY_MODNAME", VAR_DEFAULT, {} },
	{ BLOCK_USES, "RUBY_MODDOCDIR", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_MODEXAMPLESDIR", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_NO_BUILD_DEPENDS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_NO_RUN_DEPENDS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_RUBY_EXTCONF", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_EXTCONF", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_EXTCONF_SUBDIRS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_RUBY_SETUP", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_SETUP", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_RUBY_RDOC", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "RUBY_REQUIRE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "USE_RUBYGEMS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_USES, "GEM_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_USES, "USE_SBCL", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_SDL", VAR_SORTED, { "sdl" } },
	{ BLOCK_USES, "USE_SM_COMPAT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_SUBMAKE", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_TEX", VAR_SORTED, {} },
	{ BLOCK_USES, "USE_WX", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_WX_NOT", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WANT_WX", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WANT_WX_VER", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WANT_WXGTK_VER", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WITH_WX_VER", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WX_COMPS", VAR_SORTED, {} },
	{ BLOCK_USES, "WX_CONF_ARGS", VAR_DEFAULT, {} },
	{ BLOCK_USES, "WX_PREMK", VAR_DEFAULT, {} },
	{ BLOCK_USES, "USE_XFCE", VAR_SORTED, { "xfce" } },
	{ BLOCK_USES, "USE_XORG", VAR_SORTED, { "xorg", "motif" } },
	{ BLOCK_USES, "WAF_CMD", VAR_DEFAULT, { "waf" } },
	{ BLOCK_USES, "WEBPLUGIN_NAME", VAR_SKIP_GOALCOL, { "webplugin" } },
	{ BLOCK_USES, "WEBPLUGIN_FILES", VAR_SKIP_GOALCOL, { "webplugin" } },
	{ BLOCK_USES, "WEBPLUGIN_DIR", VAR_SKIP_GOALCOL, { "webplugin" } },
	{ BLOCK_USES, "XMKMF_ARGS", VAR_DEFAULT, { "imake" } },

	{ BLOCK_SHEBANGFIX, "SHEBANG_FILES", VAR_SORTED, { "shebangfix" } },
	{ BLOCK_SHEBANGFIX, "SHEBANG_GLOB", VAR_SORTED, { "shebangfix" } },
	{ BLOCK_SHEBANGFIX, "SHEBANG_REGEX", VAR_SORTED, { "shebangfix" } },
	{ BLOCK_SHEBANGFIX, "SHEBANG_LANG", VAR_SORTED, { "shebangfix" } },
	{ BLOCK_SHEBANGFIX, "OLD_CMD", VAR_NOT_COMPARABLE, { "shebangfix" } },
	{ BLOCK_SHEBANGFIX, "CMD", VAR_NOT_COMPARABLE, { "shebangfix" } },

	{ BLOCK_UNIQUEFILES, "UNIQUE_PREFIX", VAR_DEFAULT, { "uniquefiles" } },
	{ BLOCK_UNIQUEFILES, "UNIQUE_PREFIX_FILES", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED, { "uniquefiles" } },
	{ BLOCK_UNIQUEFILES, "UNIQUE_SUFFIX", VAR_DEFAULT, { "uniquefiles" } },
	{ BLOCK_UNIQUEFILES, "UNIQUE_SUFFIX_FILES", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED, { "uniquefiles" } },

	{ BLOCK_APACHE, "AP_EXTRAS", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "AP_INC", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "AP_LIB", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "AP_FAST_BUILD", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "AP_GENPLIST", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "MODULENAME", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "SHORTMODNAME", VAR_DEFAULT, { "apache" } },
	{ BLOCK_APACHE, "SRC_FILE", VAR_DEFAULT, { "apache" } },

	{ BLOCK_ELIXIR, "ELIXIR_APP_NAME", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "ELIXIR_LIB_ROOT", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "ELIXIR_APP_ROOT", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "ELIXIR_HIDDEN", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "ELIXIR_LOCALE", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_CMD", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_COMPILE", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_REWRITE", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_BUILD_DEPS", VAR_SORTED, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_RUN_DEPS", VAR_SORTED, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_DOC_DIRS", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_DOC_FILES", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_ENV", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_ENV_NAME", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_BUILD_NAME", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_TARGET", VAR_DEFAULT, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_EXTRA_APPS", VAR_SORTED, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_EXTRA_DIRS", VAR_SORTED, { "elixir" } },
	{ BLOCK_ELIXIR, "MIX_EXTRA_FILES", VAR_SORTED, { "elixir" } },

	{ BLOCK_EMACS, "EMACS_FLAVORS_EXCLUDE", VAR_DEFAULT, { "emacs" } },
	{ BLOCK_EMACS, "EMACS_NO_DEPENDS", VAR_DEFAULT, { "emacs" } },

	{ BLOCK_ERLANG, "ERL_APP_NAME", VAR_DEFAULT, { "erlang" } },
	{ BLOCK_ERLANG, "ERL_APP_ROOT", VAR_DEFAULT, { "erlang" } },
	{ BLOCK_ERLANG, "REBAR_CMD", VAR_DEFAULT, { "erlang" } },
	{ BLOCK_ERLANG, "REBAR3_CMD", VAR_DEFAULT, { "erlang" } },
	{ BLOCK_ERLANG, "REBAR_PROFILE", VAR_DEFAULT, { "erlang" } },
	{ BLOCK_ERLANG, "REBAR_TARGETS", VAR_SORTED, { "erlang" } },
	{ BLOCK_ERLANG, "ERL_BUILD_NAME", VAR_DEFAULT, { "erlang" } },
	{ BLOCK_ERLANG, "ERL_BUILD_DEPS", VAR_SORTED, { "erlang" } },
	{ BLOCK_ERLANG, "ERL_RUN_DEPS", VAR_SORTED, { "erlang" } },
	{ BLOCK_ERLANG, "ERL_DOCS", VAR_DEFAULT, { "erlang" } },

	{ BLOCK_CMAKE, "CMAKE_ARGS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_ON", VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_OFF", VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_TESTING_ARGS", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_TESTING_ON", VAR_SKIP_GOALCOL | VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_TESTING_OFF", VAR_SKIP_GOALCOL | VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_TESTING_TARGET", VAR_SKIP_GOALCOL | VAR_SORTED, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_BUILD_TYPE", VAR_SKIP_GOALCOL, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_INSTALL_PREFIX", VAR_SKIP_GOALCOL, { "cmake" } },
	{ BLOCK_CMAKE, "CMAKE_SOURCE_PATH", VAR_SKIP_GOALCOL, { "cmake" } },

	{ BLOCK_CONFIGURE, "HAS_CONFIGURE", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "GNU_CONFIGURE", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "GNU_CONFIGURE_MANPREFIX", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_CONFIGURE, "GNU_CONFIGURE_PREFIX", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_CMD", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_LOG", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_SCRIPT", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_SHELL", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_ARGS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_OUTSOURCE", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "CONFIGURE_TARGET", VAR_DEFAULT, {} },
	{ BLOCK_CONFIGURE, "WITHOUT_FBSD10_FIX", VAR_SKIP_GOALCOL, {} },

	{ BLOCK_QMAKE, "QMAKE_ARGS", VAR_SORTED, { "qmake" } },
	{ BLOCK_QMAKE, "QMAKE_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, { "qmake" } },
	{ BLOCK_QMAKE, "QMAKE_CONFIGURE_ARGS", VAR_SORTED, { "qmake" } },
	{ BLOCK_QMAKE, "QMAKE_SOURCE_PATH", VAR_DEFAULT, { "qmake" } },

	{ BLOCK_MESON, "MESON_ARGS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, { "meson" } },
	{ BLOCK_MESON, "MESON_BUILD_DIR", VAR_DEFAULT, { "meson" } },

	{ BLOCK_SCONS, "CCFLAGS", VAR_DEFAULT, { "scons" } },
	{ BLOCK_SCONS, "CPPPATH", VAR_SORTED, { "scons" } },
	{ BLOCK_SCONS, "LINKFLAGS", VAR_DEFAULT, { "scons" } },
	{ BLOCK_SCONS, "LIBPATH", VAR_DEFAULT, { "scons" } },

	{ BLOCK_CABAL, "USE_CABAL", VAR_CASE_SENSITIVE_SORT | VAR_PRINT_AS_NEWLINES | VAR_SORTED, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_BOOTSTRAP", VAR_SKIP_GOALCOL, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_FLAGS", VAR_DEFAULT, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_PROJECT", VAR_DEFAULT, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_EXECUTABLES", VAR_SORTED, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_REPOSITORIES", VAR_DEFAULT, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_REVISION", VAR_DEFAULT, { "cabal" } },
	{ BLOCK_CABAL, "CABAL_WRAPPER_SCRIPTS", VAR_SORTED, { "cabal" } },
	{ BLOCK_CABAL, "DATADIR_VARS", VAR_NOT_COMPARABLE | VAR_SKIP_GOALCOL | VAR_SORTED, { "cabal" } },
	{ BLOCK_CABAL, "SKIP_CABAL_EXTRACT", VAR_SKIP_GOALCOL | VAR_SORTED, { "cabal" } },
	{ BLOCK_CABAL, "SKIP_CABAL_PLIST", VAR_SKIP_GOALCOL | VAR_SORTED, { "cabal" } },

	{ BLOCK_CARGO, "CARGO_CRATES", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_CARGOLOCK", VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_CARGOTOML", VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_FEATURES", VAR_SORTED, { "cargo" } },

	{ BLOCK_CARGO, "CARGO_BUILDDEP", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_BUILD", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_BUILD_ARGS", VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_BUILD_TARGET", VAR_SKIP_GOALCOL, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_INSTALL", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_INSTALL_ARGS", VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_INSTALL_PATH", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_TEST", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_TEST_ARGS", VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_UPDATE_ARGS", VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_CARGO_BIN", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_DIST_SUBDIR", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_TARGET_DIR", VAR_DEFAULT, { "cargo" } },
	{ BLOCK_CARGO, "CARGO_VENDOR_DIR", VAR_DEFAULT, { "cargo" } },

	{ BLOCK_GO, "GO_PORT", VAR_DEFAULT, { "go" } },
	{ BLOCK_GO, "GO_MODULE", VAR_DEFAULT, { "go" } },
	{ BLOCK_GO, "GO_PKGNAME", VAR_DEFAULT, { "go" } },
	{ BLOCK_GO, "GO_TARGET", VAR_SORTED, { "go" } },
	{ BLOCK_GO, "GO_BUILDFLAGS", VAR_LEAVE_UNFORMATTED, { "go" } },
	{ BLOCK_GO, "GO_TESTTARGET", VAR_DEFAULT, { "go" } },
	{ BLOCK_GO, "GO_TESTFLAGS", VAR_LEAVE_UNFORMATTED, { "go" } },
	{ BLOCK_GO, "CGO_ENABLED", VAR_DEFAULT, { "go" } },
	{ BLOCK_GO, "CGO_CFLAGS", VAR_SORTED, { "go" } },
	{ BLOCK_GO, "CGO_LDFLAGS", VAR_DEFAULT, { "go" } },

	{ BLOCK_LAZARUS, "NO_LAZBUILD", VAR_DEFAULT, { "lazarus" } },
	{ BLOCK_LAZARUS, "LAZARUS_PROJECT_FILES", VAR_DEFAULT, { "lazarus" } },
	{ BLOCK_LAZARUS, "LAZARUS_DIR", VAR_DEFAULT, { "lazarus" } },
	{ BLOCK_LAZARUS, "LAZBUILD_ARGS", VAR_SORTED, { "lazarus" } },
	{ BLOCK_LAZARUS, "LAZARUS_NO_FLAVORS", VAR_DEFAULT, { "lazarus" } },

	{ BLOCK_LINUX, "BIN_DISTNAMES", VAR_DEFAULT, { "linux" } },
	{ BLOCK_LINUX, "LIB_DISTNAMES", VAR_DEFAULT, { "linux" } },
	{ BLOCK_LINUX, "LIB_DISTNAMES_aarch64", VAR_DEFAULT, { "linux" } },
	{ BLOCK_LINUX, "LIB_DISTNAMES_amd64", VAR_DEFAULT, { "linux" } },
	{ BLOCK_LINUX, "LIB_DISTNAMES_i386", VAR_DEFAULT, { "linux" } },
	{ BLOCK_LINUX, "SHARE_DISTNAMES", VAR_DEFAULT, { "linux" } },
	{ BLOCK_LINUX, "SRC_DISTFILES", VAR_DEFAULT, { "linux" } },

	{ BLOCK_NUGET, "NUGET_DEPENDS", VAR_SORTED, { "mono" } },
	{ BLOCK_NUGET, "NUGET_PACKAGEDIR", VAR_DEFAULT, { "mono" } },
	{ BLOCK_NUGET, "NUGET_LAYOUT", VAR_DEFAULT, { "mono" } },
	{ BLOCK_NUGET, "NUGET_FEEDS", VAR_DEFAULT, { "mono" } },
	// TODO: These need to be handled specially
	//{ BLOCK_NUGET, "_URL", VAR_DEFAULT, { "mono" } },
	//{ BLOCK_NUGET, "_FILE", VAR_DEFAULT, { "mono" } },
	//{ BLOCK_NUGET, "_DEPENDS", VAR_DEFAULT, { "mono" } },
	{ BLOCK_NUGET, "PAKET_PACKAGEDIR", VAR_DEFAULT, { "mono" } },
	{ BLOCK_NUGET, "PAKET_DEPENDS", VAR_SORTED, { "mono" } },

	{ BLOCK_MAKE, "MAKEFILE", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "MAKE_CMD", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "MAKE_SHELL", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "MAKE_ARGS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_MAKE, "MAKE_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_MAKE, "SCRIPTS_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_MAKE, "DESTDIRNAME", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "MAKE_FLAGS", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "MAKE_JOBS_UNSAFE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_MAKE, "ALL_TARGET", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "INSTALL_TARGET", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "LATE_INSTALL_ARGS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_MAKE, "TEST_ARGS", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_MAKE, "TEST_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_MAKE, "TEST_TARGET", VAR_DEFAULT, {} },
	{ BLOCK_MAKE, "QA_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_MAKE, "DO_MAKE_BUILD", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_MAKE, "DO_MAKE_TEST", VAR_IGNORE_WRAPCOL, {} },

	{ BLOCK_CFLAGS, "CFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "CFLAGS_", VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_CFLAGS, "CPPFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "CXXFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "CXXFLAGS_", VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_CFLAGS, "DEBUG_FLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "DPADD", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "FFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "FCFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "OBJCFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "RUSTFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "LDADD", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "LDFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "LDFLAGS_", VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_CFLAGS, "LIBS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "ELF_FEATURES", VAR_DEFAULT, { "elfctl" } },
	{ BLOCK_CFLAGS, "BIND_NOW_UNSAFE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_CFLAGS, "LLD_UNSAFE", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "LTO_UNSAFE", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "PIE_UNSAFE", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "SSP_UNSAFE", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "SSP_CFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITH_BIND_NOW", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITH_DEBUG", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITH_LTO", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITH_RELRO", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITH_SSP", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_BIND_NOW", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_CPU_CFLAGS", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_DEBUG", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_LTO", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_NO_STRICT_ALIASING", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_RELRO", VAR_DEFAULT, {} },
	{ BLOCK_CFLAGS, "WITHOUT_SSP", VAR_DEFAULT, {} },

	{ BLOCK_CONFLICTS, "CONFLICTS", VAR_SORTED, {} },
	{ BLOCK_CONFLICTS, "CONFLICTS_BUILD", VAR_SORTED, {} },
	{ BLOCK_CONFLICTS, "CONFLICTS_INSTALL", VAR_SORTED, {} },

	{ BLOCK_STANDARD, "AR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "AS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "CC", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "CPP", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "CXX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "LD", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "STRIP", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "BINDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "ETCDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "ETCDIR_REL", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "DATADIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "DATADIR_REL", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "DOCSDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "DOCSDIR_REL", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "EXAMPLESDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "FILESDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "LIB_DIRS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MASTERDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MANDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MANDIRS", VAR_SORTED, {} },
	{ BLOCK_STANDARD, "MANPREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN1PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN2PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN3PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN4PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN5PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN6PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN7PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN8PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MAN9PREFIX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "PATCHDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "PKGDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "SCRIPTDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "STAGEDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "SRC_BASE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "TMPDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "WWWDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "WWWDIR_REL", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "BINARY_ALIAS", VAR_SORTED, {} },
	{ BLOCK_STANDARD, "BINARY_WRAPPERS", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_STANDARD, "BINOWN", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "BINGRP", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "BINMODE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MANMODE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "SHAREOWN", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "SHAREGRP", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "_SHAREMODE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "SHAREMODE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "WWWOWN", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "WWWGRP", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "BUNDLE_LIBS", VAR_SORTED, {} },
	{ BLOCK_STANDARD, "DESKTOP_ENTRIES", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "DESKTOPDIR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "EXTRA_PATCHES", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "EXTRACT_CMD", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "EXTRACT_BEFORE_ARGS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "EXTRACT_AFTER_ARGS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "FETCH_CMD", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "FETCH_ARGS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "FETCH_REGET", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "FETCH_ENV", VAR_SORTED, {} },
	{ BLOCK_STANDARD, "FETCH_BEFORE_ARGS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "FETCH_AFTER_ARGS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "PATCH_STRIP", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "PATCH_ARGS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "PATCH_DIST_ARGS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "REINPLACE_CMD", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "REINPLACE_ARGS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "DISTORIG", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "IA32_BINARY_PORT", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "INSTALL", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "IS_INTERACTIVE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NO_ARCH", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NO_ARCH_IGNORE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NO_BUILD", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NOCCACHE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NO_CCACHE", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_STANDARD, "NO_CCACHE_DEPEND", VAR_IGNORE_WRAPCOL, {} },
	{ BLOCK_STANDARD, "NO_CHECKSUM", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NO_INSTALL", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NO_LICENSES_INSTALL", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "NO_MTREE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NOT_REPRODUCIBLE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "MASTER_SORT", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MASTER_SORT_REGEX", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MTREE_CMD", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MTREE_ARGS", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "MTREE_FILE", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "NOPRECIOUSMAKEVARS", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "NO_TEST", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "PORTSCOUT", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "SUB_FILES", VAR_SORTED, {} },
	{ BLOCK_STANDARD, "SUB_LIST", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_STANDARD, "TARGET_ORDER_OVERRIDE", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_STANDARD, "UID_FILES", VAR_SORTED, {} },
	// XXX: Really add them here?
	{ BLOCK_STANDARD, "ERROR", VAR_DEFAULT, {} },
	{ BLOCK_STANDARD, "WARNING", VAR_DEFAULT, {} },

	{ BLOCK_WRKSRC, "NO_WRKSUBDIR", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "AUTORECONF_WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "BUILD_WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "CONFIGURE_WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "INSTALL_WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "PATCH_WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "TEST_WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "WRKDIR", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "WRKSRC", VAR_DEFAULT, {} },
	{ BLOCK_WRKSRC, "WRKSRC_SUBDIR", VAR_DEFAULT, {} },

	{ BLOCK_USERS, "USERS", VAR_SORTED, {} },
	{ BLOCK_USERS, "GROUPS", VAR_SORTED, {} },

	{ BLOCK_PLIST, "DESCR", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "DISTINFO_FILE", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "PKGHELP", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "PKGPREINSTALL", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKGINSTALL", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKGPOSTINSTALL", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKGPREDEINSTALL", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKGDEINSTALL", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKGPOSTDEINSTALL", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKGMESSAGE", VAR_SUBPKG_HELPER, {} },
	{ BLOCK_PLIST, "PKG_DBDIR", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "PKG_SUFX", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "PLIST", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "POST_PLIST", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "TMPPLIST", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "INFO", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "INFO_PATH", VAR_DEFAULT, {} },
	{ BLOCK_PLIST, "PLIST_DIRS", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_PLIST, "PLIST_FILES", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_PLIST, "PLIST_SUB", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_PLIST, "PORTDATA", VAR_CASE_SENSITIVE_SORT | VAR_SORTED, {} },
	{ BLOCK_PLIST, "PORTDOCS", VAR_CASE_SENSITIVE_SORT | VAR_SORTED, {} },
	{ BLOCK_PLIST, "PORTEXAMPLES", VAR_CASE_SENSITIVE_SORT | VAR_SORTED, {} },

	{ BLOCK_OPTDEF, "OPTIONS_DEFINE", VAR_SORTED, {} },
	// These do not exist in the framework but some ports
	// define them themselves
	{ BLOCK_OPTDEF, "OPTIONS_DEFINE_DragonFly", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFINE_FreeBSD", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFINE_", VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_FREEBSD_VERSION, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFINE_", VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFAULT", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFAULT_DragonFly", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFAULT_FreeBSD", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFAULT_", VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_FREEBSD_VERSION, {} },
	{ BLOCK_OPTDEF, "OPTIONS_DEFAULT_", VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_OPTDEF, "OPTIONS_GROUP", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_MULTI", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_RADIO", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_SINGLE", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_EXCLUDE", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_EXCLUDE_DragonFly", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_EXCLUDE_FreeBSD", VAR_SKIP_GOALCOL | VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_EXCLUDE_", VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_FREEBSD_VERSION, {} },
	{ BLOCK_OPTDEF, "OPTIONS_EXCLUDE_", VAR_SKIP_GOALCOL | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_OPTDEF, "OPTIONS_SLAVE", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "OPTIONS_OVERRIDE", VAR_SORTED, {} },
	{ BLOCK_OPTDEF, "NO_OPTIONS_SORT", VAR_SKIP_GOALCOL, {} },
	{ BLOCK_OPTDEF, "OPTIONS_FILE", VAR_DEFAULT, {} },
	{ BLOCK_OPTDEF, "OPTIONS_SUB", VAR_DEFAULT, {} },

	{ BLOCK_OPTDESC, "DESC", VAR_IGNORE_WRAPCOL | VAR_NOT_COMPARABLE, {} },

	{ BLOCK_OPTHELPER, "IMPLIES", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PREVENTS", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PREVENTS_MSG", VAR_NOT_COMPARABLE, {} },
#if PORTFMT_SUBPACKAGES
	{ BLOCK_OPTHELPER, "SUBPACKAGES", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
#endif
	{ BLOCK_OPTHELPER, "CATEGORIES", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CATEGORIES_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "MASTER_SITES", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "MASTER_SITES_OFF", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "DISTFILES", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "DISTFILES_OFF", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "EXTRACT_ONLY", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "EXTRACT_ONLY_OFF", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PATCH_SITES", VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PATCH_SITES_OFF", VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PATCHFILES", VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PATCHFILES_OFF", VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "BROKEN", VAR_IGNORE_WRAPCOL | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "BROKEN_OFF", VAR_IGNORE_WRAPCOL | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "IGNORE", VAR_IGNORE_WRAPCOL | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "IGNORE_OFF", VAR_IGNORE_WRAPCOL | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PKG_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PKG_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "FETCH_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "FETCH_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "EXTRACT_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "EXTRACT_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PATCH_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PATCH_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "BUILD_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "BUILD_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "LIB_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "LIB_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "RUN_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "RUN_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "TEST_DEPENDS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "TEST_DEPENDS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_SUBPKG_HELPER | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "USES", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "USES_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "USE", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "USE_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_ACCOUNT", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_ACCOUNT_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_PROJECT", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_PROJECT_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_SUBDIR", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_SUBDIR_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_TAGNAME", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_TAGNAME_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_TUPLE", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GH_TUPLE_OFF", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_ACCOUNT", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_ACCOUNT_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_COMMIT", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_COMMIT_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_PROJECT", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_PROJECT_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_SITE", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_SITE_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_SUBDIR", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_SUBDIR_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_TAGNAME", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_TAGNAME_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_TUPLE", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "GL_TUPLE_OFF", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CMAKE_BOOL", VAR_SORTED | VAR_NOT_COMPARABLE, { "cmake" } },
	{ BLOCK_OPTHELPER, "CMAKE_BOOL_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, { "cmake" } },
	{ BLOCK_OPTHELPER, "CMAKE_ON", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, { "cmake" } },
	{ BLOCK_OPTHELPER, "CMAKE_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, { "cmake" } },
	{ BLOCK_OPTHELPER, "CONFIGURE_ON", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFIGURE_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFIGURE_ENABLE", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFIGURE_WITH", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFIGURE_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFIGURE_ENV_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "QMAKE_ON", VAR_SORTED | VAR_NOT_COMPARABLE, { "qmake" } },
	{ BLOCK_OPTHELPER, "QMAKE_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, { "qmake" } },
	{ BLOCK_OPTHELPER, "MESON_ENABLED", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_DISABLED", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_ON", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_TRUE", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_FALSE", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_YES", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "MESON_NO", VAR_SORTED | VAR_NOT_COMPARABLE, { "meson" } },
	{ BLOCK_OPTHELPER, "USE_CABAL", VAR_CASE_SENSITIVE_SORT | VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_SORTED | VAR_NOT_COMPARABLE, { "cabal" } },
	{ BLOCK_OPTHELPER, "CABAL_FLAGS", VAR_NOT_COMPARABLE, { "cabal" } },
	{ BLOCK_OPTHELPER, "CABAL_EXECUTABLES", VAR_SORTED | VAR_NOT_COMPARABLE, { "cabal" } },
	{ BLOCK_OPTHELPER, "MAKE_ARGS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "MAKE_ARGS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "MAKE_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "MAKE_ENV_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "ALL_TARGET", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "ALL_TARGET_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "INSTALL_TARGET", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "INSTALL_TARGET_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "TEST_TARGET", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "TEST_TARGET_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CFLAGS", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CFLAGS_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CPPFLAGS", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CPPFLAGS_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CXXFLAGS", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CXXFLAGS_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "LDFLAGS", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "LDFLAGS_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "LIBS", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "LIBS_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFLICTS", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFLICTS_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFLICTS_BUILD", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFLICTS_BUILD_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFLICTS_INSTALL", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "CONFLICTS_INSTALL_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "BINARY_ALIAS", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "BINARY_ALIAS_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "DESKTOP_ENTRIES", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "DESKTOP_ENTRIES_OFF", VAR_PRINT_AS_NEWLINES | VAR_SKIP_GOALCOL | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "EXTRA_PATCHES", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "EXTRA_PATCHES_OFF", VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "SUB_FILES", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "SUB_FILES_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "SUB_LIST", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "SUB_LIST_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "INFO", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "INFO_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PLIST_DIRS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PLIST_DIRS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PLIST_FILES", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PLIST_FILES_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PLIST_SUB", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PLIST_SUB_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PORTDOCS", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PORTDOCS_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PORTEXAMPLES", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "PORTEXAMPLES_OFF", VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "VARS", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
	{ BLOCK_OPTHELPER, "VARS_OFF", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_NOT_COMPARABLE, {} },
};

// Variables that are somewhere in the ports framework but that
// ports do not usually set.  Portclippy will flag them as "unknown".
// We can set special formatting rules for them here instead of in
// variable_order_.
static struct VariableOrderEntry special_variables_[] = {
	{ BLOCK_UNKNOWN, "_DISABLE_TESTS", VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "_IPXE_BUILDCFG", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "_PARFETCH_ENV", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "_SRHT_TUPLE", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "CARGO_CARGO_RUN", VAR_IGNORE_WRAPCOL, { "cargo" } },
	{ BLOCK_UNKNOWN, "CFLAGS_clang", VAR_DEFAULT, {} },
	{ BLOCK_UNKNOWN, "CFLAGS_gcc", VAR_DEFAULT, {} },
	{ BLOCK_UNKNOWN, "CPPFLAGS_clang", VAR_DEFAULT, {} },
	{ BLOCK_UNKNOWN, "CPPFLAGS_gcc", VAR_DEFAULT, {} },
	{ BLOCK_UNKNOWN, "CONFIGURE_ARGS_", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_UNKNOWN, "CONFIGURE_ENV_", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_FOR_EACH_ARCH, {}},
	{ BLOCK_UNKNOWN, "CXXFLAGS_clang", VAR_DEFAULT, {} },
	{ BLOCK_UNKNOWN, "CXXFLAGS_gcc", VAR_DEFAULT, {} },
	{ BLOCK_UNKNOWN, "CO_ENV", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "D4P_ENV", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "DEV_ERROR", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "DEV_WARNING", VAR_IGNORE_WRAPCOL | VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "EXTRA_PATCHES_", VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_UNKNOWN, "GN_ARGS", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "GO_ENV", VAR_PRINT_AS_NEWLINES, { "go" } },
	{ BLOCK_UNKNOWN, "IPXE_BUILDCFG", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "MAKE_ARGS_", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_UNKNOWN, "MAKE_ARGS_clang", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "MAKE_ARGS_gcc", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "MAKE_ENV_", VAR_PRINT_AS_NEWLINES | VAR_SORTED | VAR_FOR_EACH_ARCH, {} },
	{ BLOCK_UNKNOWN, "MAKE_ENV_clang", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "MAKE_ENV_gcc", VAR_PRINT_AS_NEWLINES | VAR_SORTED, {} },
	{ BLOCK_UNKNOWN, "MASTER_SITES_ABBREVS", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "MOZ_OPTIONS", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "QA_ENV", VAR_PRINT_AS_NEWLINES, {} },
	{ BLOCK_UNKNOWN, "SUBDIR", VAR_DEDUP | VAR_PRINT_AS_NEWLINES, {} },
};

struct VariableOrderEntry *
variable_order_entry_with_suffix(
	struct VariableOrderEntry *template,
	const char *suffix,
	struct LibiasMempool *pool)
{
	auto entry = libias_mempool_alloc(pool, struct VariableOrderEntry);
	*entry = *template;
	auto var = libias_str_printf("%s%s", entry->var, suffix);
	libias_mempool_take(pool, var);
	entry->var = var;
	return entry;
}

struct Rules *
rules_new(uint32_t flags)
{
	struct Rules *rules = libias_alloc(struct Rules);
	rules->behavior = flags;
	rules->pool = libias_mempool_new();

	rules->variable_order = libias_mempool_array(rules->pool);
	for (size_t variable_order_entry_index = 0; variable_order_entry_index < nitems(variable_order_); variable_order_entry_index++) {
		if (variable_order_[variable_order_entry_index].flags & VAR_FOR_EACH_ARCH) {
			for (size_t arch_index = 0; arch_index < known_architectures_len; arch_index++) {
				auto entry = variable_order_entry_with_suffix(
					&variable_order_[variable_order_entry_index],
					known_architectures[arch_index],
					rules->pool);
				libias_array_append(rules->variable_order, entry);
			}
		} else if (variable_order_[variable_order_entry_index].flags & VAR_FOR_EACH_FREEBSD_VERSION) {
			for (size_t version_index = 0; version_index < freebsd_versions_len; version_index++) {
				auto suffix = libias_str_printf("FreeBSD_%u", freebsd_versions[version_index]);
				auto entry = variable_order_entry_with_suffix(
					&variable_order_[variable_order_entry_index],
					suffix,
					rules->pool);
				libias_cleanup(&suffix);
				libias_array_append(rules->variable_order, entry);
			}
		} else if (variable_order_[variable_order_entry_index].flags & VAR_FOR_EACH_FREEBSD_VERSION_AND_ARCH) {
			for (size_t version_index = 0; version_index < freebsd_versions_len; version_index++) {
				auto version = libias_str_printf("FreeBSD_%u", freebsd_versions[version_index]);
				libias_scope_free(version);
				auto entry = variable_order_entry_with_suffix(
					&variable_order_[variable_order_entry_index],
					version,
					rules->pool);
				libias_array_append(rules->variable_order, entry);

				for (size_t arch_index = 0; arch_index < known_architectures_len; arch_index++) {
					auto suffix = libias_str_printf(
						"%s_%s",
						version,
						known_architectures[arch_index]);
					auto entry = variable_order_entry_with_suffix(
						&variable_order_[variable_order_entry_index],
						suffix,
						rules->pool);
					libias_cleanup(&suffix);
					libias_array_append(rules->variable_order, entry);
				}
			}
		} else if (variable_order_[variable_order_entry_index].flags & VAR_FOR_EACH_SSL) {
			for (size_t ssl_index = 0; ssl_index < valid_ssl_default_len; ssl_index++) {
				auto entry = variable_order_entry_with_suffix(
					&variable_order_[variable_order_entry_index],
					valid_ssl_default[ssl_index],
					rules->pool);
				libias_array_append(rules->variable_order, entry);
			}
		} else if (variable_order_[variable_order_entry_index].flags & VAR_FOR_EACH_RUBY) {
			for (size_t ruby_index = 0; ruby_index < valid_ruby_versions_len; ruby_index++) {
				auto suffix = libias_str_printf("%u", valid_ruby_versions[ruby_index]);
				auto entry = variable_order_entry_with_suffix(
					&variable_order_[variable_order_entry_index],
					suffix,
					rules->pool);
				libias_cleanup(&suffix);
				libias_array_append(rules->variable_order, entry);
			}

		} else {
			libias_array_append(rules->variable_order, &variable_order_[variable_order_entry_index]);
		}
	}

	rules->special_variables = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < nitems(special_variables_); i++) {
		libias_array_append(rules->special_variables, &special_variables_[i]);
	}

	rules->special_sources = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < nitems(special_sources_); i++) {
		libias_array_append(rules->special_sources, special_sources_[i]);
	}

	rules->special_targets = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < nitems(special_targets_); i++) {
		libias_array_append(rules->special_targets, special_targets_[i]);
	}

	rules->target_order = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < nitems(target_order_); i++) {
		libias_array_append(rules->target_order, &target_order_[i]);
	}

	rules->license_perms_rel = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < nitems(license_perms_rel_); i++) {
		libias_array_append(rules->license_perms_rel, license_perms_rel_[i]);
	}

	rules->use_budgie_rel = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < use_budgie_rel_len; i++) {
		libias_array_append(rules->use_budgie_rel, use_budgie_rel[i]);
	}

	rules->use_gnome_rel = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < use_gnome_rel_len; i++) {
		libias_array_append(rules->use_gnome_rel, use_gnome_rel[i]);
	}

	rules->use_kde_rel = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < use_kde_rel_len; i++) {
		libias_array_append(rules->use_kde_rel, use_kde_rel[i]);
	}

	rules->use_pyqt_rel = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < use_pyqt_rel_len; i++) {
		libias_array_append(rules->use_pyqt_rel, use_pyqt_rel[i]);
	}

	rules->use_qt_rel = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < use_qt_rel_len; i++) {
		libias_array_append(rules->use_qt_rel, use_qt_rel[i]);
	}

	rules->static_shebang_langs = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < static_shebang_langs_len; i++) {
		libias_array_append(rules->static_shebang_langs, static_shebang_langs[i]);
	}

	rules->known_architectures = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < known_architectures_len; i++) {
		libias_array_append(rules->known_architectures, known_architectures[i]);
	}

	rules->target_command_wrap_after_each_token = libias_mempool_array(rules->pool);
	for (size_t i = 0; i < nitems(target_command_wrap_after_each_token_); i++) {
		libias_array_append(rules->target_command_wrap_after_each_token, target_command_wrap_after_each_token_[i]);
	}

	return rules;
}

void
rules_cleanup(struct Rules **rules_ptr)
{
	struct Rules *rules = *rules_ptr;
	if (rules) {
		libias_cleanup(&rules->pool);
		libias_free(rules);
		*rules_ptr = NULL;
	}
}

bool
variable_flag_p(
	struct Rules *rules,
	struct AST *root,
	const char *var,
	int flag)
{
	libias_scope_mempool(pool);

	char *helper;
	if (rules_parse_variable_options_helper(rules, root, var, NULL, &helper, NULL)) {
		libias_scope_free(helper);
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if ((entry->block == BLOCK_OPTHELPER || entry->block == BLOCK_OPTDESC) &&
			    (entry->flags & flag) && strcmp(helper, entry->var) == 0) {
				return true;
			}
		}
	}

	if (parse_flavors_helper(pool, rules, root, var, NULL, &helper)) {
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block == BLOCK_FLAVORS_HELPER &&
			    (entry->flags & flag) &&
			    strcmp(helper, entry->var) == 0) {
				return true;
			}
		}
	}

	char *suffix;
	if (parse_shebang_lang(pool, rules, root, var, NULL, &suffix)) {
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block == BLOCK_SHEBANGFIX &&
			    (entry->flags & VAR_NOT_COMPARABLE) &&
			    (entry->flags & flag) &&
			    strcmp(suffix, entry->var) == 0) {
				return true;
			}
		}
	}

	if (parse_cabal_datadir_vars(pool, rules, root, var, NULL, &suffix)) {
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block == BLOCK_CABAL &&
			    (entry->flags & VAR_NOT_COMPARABLE) &&
			    (entry->flags & flag) &&
			    strcmp(suffix, entry->var) == 0) {
				return true;
			}
		}
	}

	char *prefix;
	if (matches_options_group(pool, rules, root, var, &prefix)) {
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block == BLOCK_OPTDEF &&
			    (entry->flags & flag) &&
			    strcmp(prefix, entry->var) == 0) {
				return true;
			}
		}
	}

	libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
		if ((!(entry->flags & VAR_NOT_COMPARABLE)) &&
		    (entry->flags & flag) &&
		    strcmp(var, entry->var) == 0) {
			return true;
		}
	}

	libias_array_foreach(rules->special_variables, struct VariableOrderEntry *, entry) {
		if ((entry->flags & flag) &&
		    strcmp(var, entry->var) == 0) {
			return true;
		}
	}

	return false;
}

bool
extract_arch_prefix(
	struct Rules *rules,
	struct LibiasMempool *pool,
	const char *var,
	char **prefix_without_arch,
	char **prefix_without_arch_osrel)
{
	libias_array_foreach(rules->known_architectures, const char *, arch) {
		char *suffix = libias_str_printf("_%s", arch);
		if (libias_str_suffix_p(var, suffix)) {
			*prefix_without_arch = libias_str_ndup(var, strlen(var) - strlen(suffix));
			*prefix_without_arch_osrel = NULL;
			libias_mempool_take(pool, *prefix_without_arch);
			libias_free(suffix);
			return true;
		} else {
			libias_free(suffix);
		}
	}
	libias_array_foreach(rules->known_architectures, const char *, arch) {
		for (size_t j = 0; j < freebsd_versions_len; j++) {
			char *suffix = libias_str_printf("_%s_%" PRIu32, arch, freebsd_versions[j]);
			if (libias_str_suffix_p(var, suffix)) {
				*prefix_without_arch = libias_str_ndup(var, strlen(var) - strlen(suffix));
				*prefix_without_arch_osrel = libias_str_ndup(var, strlen(var) - strlen(suffix) + strlen(arch) + 1);
				libias_mempool_take(pool, *prefix_without_arch);
				libias_mempool_take(pool, *prefix_without_arch_osrel);
				libias_free(suffix);
				return true;
			} else {
				libias_free(suffix);
			}
		}
	}
	return false;
}

bool
extract_osrel_prefix(
	struct LibiasMempool *pool,
	const char *var,
	char **prefix)
{
	for (size_t i = 0; i < freebsd_versions_len; i++) {
		char *suffix = libias_str_printf("_%s_%" PRIu32, "FreeBSD", freebsd_versions[i]);
		if (libias_str_suffix_p(var, suffix)) {
			*prefix = libias_str_ndup(var, strlen(var) - strlen(suffix));
			libias_mempool_take(pool, *prefix);
			libias_free(suffix);
			return true;
		} else {
			libias_free(suffix);
		}
	}
	return false;
}

void
add_referenced_var_candidates(
	struct LibiasMempool *pool,
	struct LibiasArray *candidates,
	struct LibiasArray *cond_candidates,
	const char *stem,
	const char *ref)
{
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("${%s_${%s}}", stem, ref)));
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("$(%s_${%s})", stem, ref)));
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("${%s_${%s}:", stem, ref)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("defined(%s_${%s})", stem, ref)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("empty(%s_${%s})", stem, ref)));

	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("${${%s}_%s}", ref, stem)));
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("$(${%s}_%s)", ref, stem)));
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("${${%s}_%s:", ref, stem)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("defined(${%s}_%s)", ref, stem)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("defined(${%s}_%s:", ref, stem)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("empty(${%s}_%s)", ref, stem)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("empty(${%s}_%s:", ref, stem)));
}

void
rules_referenced_var_p_collect_tokens(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RulesReferencedVarCollectTokensData *this = ast_visit_context(visit);
	ast_words_foreach(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		}
		char *value = ast_word_flatten(word);
		libias_mempool_take(this->pool, value);
		libias_array_append(this->tokens, value);
	}
}

bool
rules_referenced_var_p(
	struct Rules *rules,
	struct AST *root,
	const char *var)
{
	if (!(rules->behavior & RULES_CHECK_VARIABLE_REFERENCES)) {
		return false;
	}

	libias_scope_mempool(pool);

	// TODO: This is broken in many ways but will reduce
	// the number of false positives from portclippy/portscan

	struct LibiasArray *candidates = libias_mempool_array(pool);
	struct LibiasArray *cond_candidates = libias_mempool_array(pool);
	size_t varlen = strlen(var);

	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("${%s}", var)));
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("$(%s)", var)));
	libias_array_append(
		candidates,
		libias_mempool_take(pool, libias_str_printf("${%s:", var)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("defined(%s)", var)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("defined(%s:", var)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("empty(%s)", var)));
	libias_array_append(
		cond_candidates,
		libias_mempool_take(pool, libias_str_printf("empty(%s:", var)));

	{
		char *var_without_arch = NULL;
		char *var_without_arch_osrel = NULL;
		if (extract_arch_prefix(rules, pool, var, &var_without_arch, &var_without_arch_osrel)) {
			add_referenced_var_candidates(pool, candidates, cond_candidates, var_without_arch, "ARCH");
			if (var_without_arch_osrel) {
				add_referenced_var_candidates(pool, candidates, cond_candidates, var_without_arch, "ARCH}_${OSREL:R");
				add_referenced_var_candidates(pool, candidates, cond_candidates, var_without_arch_osrel, "OSREL:R");
			}
		}
	}

	{
		char *prefix = NULL;
		if (extract_osrel_prefix(pool, var, &prefix)) {
			add_referenced_var_candidates(pool, candidates, cond_candidates, prefix, "OPSYS}_${OSREL:R");
		}
	}

	ast_lookup_flavors_foreach(root, flavor) {
		size_t flavorlen = strlen(flavor);
		char *var_without_flavor;
		if (varlen > flavorlen && libias_str_suffix_p(var, flavor) && *(var + varlen - flavorlen - 1) == '_') {
			var_without_flavor = libias_str_slice(var, 0, varlen - flavorlen - 1);
			libias_mempool_take(pool, var_without_flavor);
		} else if (libias_str_prefix_p(var, flavor) && *(var + flavorlen) == '_') {
			var_without_flavor = libias_str_slice(var, flavorlen + 1, varlen);
			libias_mempool_take(pool, var_without_flavor);
		} else {
			continue;
		}

		add_referenced_var_candidates(pool, candidates, cond_candidates, var_without_flavor, "FLAVOR");
	}

	bool uses_compiler = (libias_str_suffix_p(var, "_clang") || libias_str_suffix_p(var, "_gcc"))
		&& ast_lookup_uses_p(root, "compiler");
	if (uses_compiler) {
		char *var_without_compiler_type;
		if (libias_str_suffix_p(var, "_clang")) {
			var_without_compiler_type = libias_str_slice(var, 0, varlen - strlen("_clang"));
			libias_mempool_take(pool, var_without_compiler_type);
		} else {
			var_without_compiler_type = libias_str_slice(var, 0, varlen - strlen("_gcc"));
			libias_mempool_take(pool, var_without_compiler_type);
		}
		add_referenced_var_candidates(pool, candidates, cond_candidates, var_without_compiler_type, "CHOSEN_COMPILER_TYPE");
	}

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_target_command = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_target_rule_dependencies = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_target_rule_targets = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_variable = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_variable_name = rules_referenced_var_p_collect_tokens;

	libias_scope_array(tokens);
	struct RulesReferencedVarCollectTokensData data = {
		.pool = pool,
		.tokens = tokens,
	};
	auto visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &data);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	libias_array_foreach(tokens, char *, token) {
		libias_array_foreach(candidates, const char *, candidate) {
			if (strstr(token, candidate)) {
				return true;
			}
		}
	}
	libias_array_truncate(tokens);

	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_expr = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_for_words = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_if_branch = rules_referenced_var_p_collect_tokens;
	visit_trait.visit_include = rules_referenced_var_p_collect_tokens;

	visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &data);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	libias_array_foreach(tokens, char *, token) {
		libias_array_foreach(candidates, const char *, candidate) {
			if (strstr(token, candidate)) {
				return true;
			}
		}
		libias_array_foreach(cond_candidates, const char *, candidate) {
			if (strstr(token, candidate)) {
				return true;
			}
		}
	}

	return false;
}

bool
valid_license_p(
	struct Rules *rules,
	struct AST *root,
	const char *license)
{
	if (rules->behavior & RULES_ALLOW_FUZZY_MATCHING) {
		if (strlen(license) == 0) {
			return false;
		}
		size_t i = 0;
		for (; license[i] != 0; i++) {
			char c = license[i];
			switch (c) {
			case '-':
			case '.':
			case '_':
			case '+':
				break;
			default:
				if (!isalnum((unsigned char)c)) {
					return false;
				}
				break;
			}
		}
		return i > 0;
	} else {
		return ast_lookup_license_p(root, license);
	}
}

bool
matches_license_name_p(
	struct Rules *rules,
	struct AST *root,
	const char *var)
{
	if (strcmp(var, "LICENSE_NAME") == 0 ||
	    strcmp(var, "LICENSE_TEXT") == 0) {
		return true;
	}

	if (*var == '_') {
		var++;
	}

	bool has_license_prefix = libias_str_prefix_p(var, "LICENSE_NAME_")
		|| libias_str_prefix_p(var, "LICENSE_TEXT_")
	    || libias_str_prefix_p(var, "LICENSE_FILE_");
	if (has_license_prefix) {
		return valid_license_p(rules, root, var + strlen("LICENSE_NAME_"));
	} else {
		return false;
	}
}

bool
rules_ignore_wrap_col_p(
	struct Rules *rules,
	struct AST *root,
	const char *varname,
	enum ASTVariableModifier modifier)
{
	if (modifier == AST_VARIABLE_MODIFIER_SHELL ||
	    matches_license_name_p(rules, root, varname)) {
		return true;
	}

	return variable_flag_p(rules, root, varname, VAR_IGNORE_WRAPCOL);
}

int32_t
rules_indent_goalcol(const char *var, enum ASTVariableModifier modifier)
{
	size_t varlength = strlen(var) + 1;
	if (libias_str_suffix_p(var, "+")) {
		varlength += 1; // " " before modifier
	}
	switch (modifier) {
	case AST_VARIABLE_MODIFIER_ASSIGN:
		varlength += 1;
		break;
	case AST_VARIABLE_MODIFIER_APPEND:
	case AST_VARIABLE_MODIFIER_EXPAND:
	case AST_VARIABLE_MODIFIER_OPTIONAL:
	case AST_VARIABLE_MODIFIER_SHELL:
		varlength += 2;
		break;
	default:
		libias_panic("unhandled variable modifier: %d", modifier);
	}
	if (((varlength + 1) % 8) == 0) {
		varlength++;
	}
	return ceil(varlength / 8.0) * 8;
}

bool
case_sensitive_sort(
	struct Rules *rules,
	struct AST *root,
	const char *var)
{
	return variable_flag_p(rules, root, var, VAR_CASE_SENSITIVE_SORT);
}

bool
rules_leave_unformatted_p(
	struct Rules *rules,
	struct AST *root,
	const char *var)
{
	return variable_flag_p(rules, root, var, VAR_LEAVE_UNFORMATTED);
}

bool
rules_should_sort_p(
	struct Rules *rules,
	struct AST *root,
	const char *var,
	enum ASTVariableModifier modifier)
{
	if (modifier == AST_VARIABLE_MODIFIER_SHELL) {
		return false;
	}
	return variable_flag_p(rules, root, var, VAR_SORTED);
}

bool
rules_print_with_line_breaks_p(
	struct Rules *rules,
	struct AST *root,
	const char *var)
{
	return variable_flag_p(rules, root, var, VAR_PRINT_AS_NEWLINES);
}

bool
rules_skip_dedup_p(
	struct Rules *rules,
	struct AST *root,
	const char *var,
	enum ASTVariableModifier modifier)
{
	return !rules_should_sort_p(rules, root, var, modifier)
		&& !variable_flag_p(rules, root, var, VAR_DEDUP);
}

bool
rules_skip_goalcol_p(
	struct Rules *rules,
	struct AST *root,
	const char *varname)
{
	if (matches_license_name_p(rules, root, varname)) {
		return true;
	}

	return variable_flag_p(rules, root, varname, VAR_SKIP_GOALCOL);
}

int
compare_rel(struct LibiasArray *rel, const char *a, const char *b)
{
	ssize_t ai = -1;
	ssize_t bi = -1;
	libias_array_foreach(rel, const char *, e) {
		if (ai == -1 && strcmp(a, e) == 0) {
			ai = e_index;
		}
		if (bi == -1 && strcmp(b, e) == 0) {
			bi = e_index;
		}
		if (ai != -1 && bi != -1) {
			if (bi > ai) {
				return -1;
			} else if (ai > bi) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	return strcasecmp(a, b);
}

int
rules_compare_tokens(const void *ap, const void *bp, void *userdata)
{
	struct RulesCompareTokens *data = userdata;
	const char *a = *(const char**)ap;
	const char *b = *(const char**)bp;

	int result;
	if (
		compare_license_perms(data->rules, data->root, data->var, a, b, &result)
		|| compare_plist_files(data->rules, data->root, data->var, a, b, &result)
		|| compare_use_budgie(data->rules, data->var, a, b, &result)
		|| compare_use_gnome(data->rules, data->var, a, b, &result)
		|| compare_use_kde(data->rules, data->var, a, b, &result)
		|| compare_use_pyqt(data->rules, data->var, a, b, &result)
		|| compare_use_qt(data->rules, data->var, a, b, &result)
		)
	{
		return result;
	}

	if (case_sensitive_sort(data->rules, data->root, data->var)) {
		return strcmp(a, b);
	} else {
		return strcasecmp(a, b);
	}
}

bool
compare_license_perms(
	struct Rules *rules,
	struct AST *root,
	const char *varname,
	const char *a,
	const char *b,
	int *result)
{
	// ^(_?LICENSE_PERMS_(-|[A-Z0-9\\._+ ])+|_LICENSE_LIST_PERMS|LICENSE_PERMS)
	if (strcmp(varname, "_LICENSE_LIST_PERMS") != 0 &&
	    strcmp(varname, "LICENSE_PERMS") != 0) {
		if (libias_str_prefix_p(varname, "_LICENSE_PERMS_")) {
			varname++;
		}
		if (!libias_str_prefix_p(varname, "LICENSE_PERMS_")) {
			return false;
		}
		const char *license = varname + strlen("LICENSE_PERMS_");
		if (!valid_license_p(rules, root, license)) {
			return false;
		}
	}

	if (result) {
		*result = compare_rel(rules->license_perms_rel, a, b);
	}

	return true;
}

char *
remove_plist_keyword(const char *s, struct LibiasMempool *pool)
{
	if (!libias_str_suffix_p(s, "\"")) {
		return libias_mempool_take(pool, libias_str_dup(s));
	}

	// "^\"@([a-z]|-)+ "
	const char *ptr = s;
	if (*ptr != '"') {
		return libias_mempool_take(pool, libias_str_dup(s));
	}
	ptr++;
	if (*ptr != '@') {
		return libias_mempool_take(pool, libias_str_dup(s));
	}
	ptr++;

	const char *prev = ptr;
	for (; *ptr != 0 && (islower((unsigned char)*ptr) || *ptr == '-'); ptr++);
	if (*ptr == 0 || prev == ptr || *ptr != ' ') {
		return libias_mempool_take(pool, libias_str_dup(s));
	}
	ptr++;

	return libias_mempool_take(pool, libias_str_ndup(ptr, strlen(ptr) - 1));
}

bool
compare_plist_files(
	struct Rules *rules,
	struct AST *root,
	const char *varname,
	const char *a,
	const char *b,
	int *result)
{
	libias_scope_mempool(pool);

	char *helper = NULL;
	if (rules_parse_variable_options_helper(rules, root, varname, NULL, &helper, NULL)) {
		libias_scope_free(helper);
		if (strcmp(helper, "PLIST_FILES_OFF") != 0 &&
		    strcmp(helper, "PLIST_FILES") != 0 &&
		    strcmp(helper, "PLIST_DIRS_OFF") != 0 &&
		    strcmp(helper, "PLIST_DIRS") != 0) {
			return false;
		}
	} else if (strcmp(varname, "PLIST_FILES") != 0 && strcmp(varname, "PLIST_DIRS") != 0) {
		return false;
	}

	/* Ignore plist keywords */
	char *as = remove_plist_keyword(a, pool);
	char *bs = remove_plist_keyword(b, pool);
	if (result) {
		*result = strcasecmp(as, bs);
	}
	return true;
}

bool
compare_use_budgie(
	struct Rules *rules,
	const char *var,
	const char *a,
	const char *b,
	int *result)
{
	if (strcmp(var, "USE_BUDGIE") != 0) {
		return false;
	}

	if (result) {
		*result = compare_rel(rules->use_budgie_rel, a, b);
	}
	return true;
}

bool
compare_use_gnome(
	struct Rules *rules,
	const char *var,
	const char *a,
	const char *b,
	int *result)
{
	if (strcmp(var, "USE_GNOME") != 0) {
		return false;
	}

	if (result) {
		*result = compare_rel(rules->use_gnome_rel, a, b);
	}
	return true;
}

bool
compare_use_kde(
	struct Rules *rules,
	const char *var,
	const char *a,
	const char *b,
	int *result)
{
	if (strcmp(var, "USE_KDE") != 0) {
		return false;
	}

	if (result) {
		*result = compare_rel(rules->use_kde_rel, a, b);
	}
	return true;
}

bool
compare_use_pyqt(
	struct Rules *rules,
	const char *var,
	const char *a,
	const char *b,
	int *result)
{
	if (strcmp(var, "USE_PYQT") != 0) {
		return false;
	}

	if (result) {
		*result = compare_rel(rules->use_pyqt_rel, a, b);
	}

	return true;
}

bool
compare_use_qt(
	struct Rules *rules,
	const char *var,
	const char *a,
	const char *b,
	int *result)
{
	if (strcmp(var, "USE_QT") != 0) {
		return false;
	}

	if (result) {
		*result = compare_rel(rules->use_qt_rel, a, b);
	}

	return true;
}

bool
parse_flavors_helper(
	struct LibiasMempool *extpool,
	struct Rules *rules,
	struct AST *node,
	const char *var,
	char **prefix_ret,
	char **helper_ret)
{
	const char *suffix = NULL;
	libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
		if (entry->block != BLOCK_FLAVORS_HELPER) {
			continue;
		}
		const char *helper = entry->var;
		if (libias_str_suffix_p(var, helper) &&
		    strlen(var) > strlen(helper) &&
		    var[strlen(var) - strlen(helper) - 1] == '_') {
			suffix = helper;
			break;
		}
	}
	if (suffix == NULL) {
		return false;
	}

	// ^[-_[:lower:][:digit:]]+_
	size_t len = strlen(var) - strlen(suffix);
	if (len == 0) {
		return false;
	}
	if (var[len - 1] != '_') {
		return false;
	}
	for (size_t i = 0; i < len; i++) {
		char c = var[i];
		if (c != '-' && c != '_' && !islower((unsigned char)c) && !isdigit((unsigned char)c)) {
			return false;
		}
	}

	if (rules->behavior & RULES_ALLOW_FUZZY_MATCHING) {
		goto done;
	}

	bool has_flavor_prefix;
	{
		char *prefix = libias_str_ndup(var, len - 1);
		has_flavor_prefix = ast_lookup_flavor_p(node, prefix);
		libias_free(prefix);
	}
	if (!has_flavor_prefix) {
		return false;
	}

done:
	if (prefix_ret) {
		*prefix_ret = libias_str_ndup(var, len);
		libias_mempool_take(extpool, *prefix_ret);
	}
	if (helper_ret) {
		*helper_ret = libias_str_dup(suffix);
		libias_mempool_take(extpool, *helper_ret);
	}

	return true;
}

char *
parse_subpkg(
	struct LibiasMempool *extpool,
	struct Rules *rules,
	struct AST *node,
	const char *var_,
	char **subpkg_ret)
{
	char *var = NULL;
	const char *subpkg = NULL;
	for (ssize_t i = strlen(var_) - 1; i > -1; i--) {
		char c = var_[i];
		if (c != '-' && c != '_' && !islower((unsigned char)c) && !isdigit((unsigned char)c)) {
			if (c == '.') {
				subpkg = var_ + i + 1;
				var = libias_str_ndup(var_, i);
			} else {
				var = libias_str_dup(var_);
			}
			break;
		}
	}

	if (var == NULL) {
		if (subpkg_ret) {
			*subpkg_ret = NULL;
		}
		return NULL;
	}

	if (subpkg && !(rules->behavior & RULES_ALLOW_FUZZY_MATCHING)) {
		bool found = ast_lookup_subpackage_p(node, subpkg);
		if (!found) {
			if (subpkg_ret) {
				*subpkg_ret = NULL;
			}
			libias_free(var);
			return NULL;
		}
	}

	if (subpkg_ret) {
		if (subpkg) {
			*subpkg_ret = libias_str_dup(subpkg);
			libias_mempool_take(extpool, *subpkg_ret);
		} else {
			*subpkg_ret = NULL;
		}
	}

	libias_mempool_take(extpool, var);
	return var;
}

// Checks whether VARIABLE is an options helper and splits it into
// prefix, helper, and subpackage stems. The caller is responsible to
// free the return values but that is only required if this function
// returns true.
bool
rules_parse_variable_options_helper(
	struct Rules *rules,
	struct AST *node,
	const char *variable,
	char **prefix_ret,
	char **helper_ret,
	char **subpkg_ret)
{
	libias_scope_mempool(pool);

	char *subpkg;
	char *var = parse_subpkg(pool, rules, node, variable, &subpkg);
	if (!var) {
		return false;
	}

	const char *suffix = NULL;
	if (libias_str_suffix_p(var, "DESC")) {
		suffix = "DESC";
	} else {
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block != BLOCK_OPTHELPER) {
				continue;
			}
			const char *helper = entry->var;
			if (libias_str_suffix_p(var, helper) &&
			    strlen(var) > strlen(helper) &&
			    var[strlen(var) - strlen(helper) - 1] == '_') {
				suffix = helper;
				break;
			}
		}
	}
	if (suffix == NULL) {
		return false;
	}

	if (subpkg) {
		bool found = false;
#if PORTFMT_SUBPACKAGES
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block != BLOCK_OPTHELPER ||
			    !(entry->flags & VAR_SUBPKG_HELPER)) {
				continue;
			}
			if (strcmp(entry->var, suffix) == 0) {
				found = true;
			}
		}
#endif
		if (!found) {
			return false;
		}
	}


	// ^[-_[:upper:][:digit:]]+_
	size_t len = strlen(var) - strlen(suffix);
	if (len == 0) {
		return false;
	}
	if (var[len - 1] != '_') {
		return false;
	}
	for (size_t i = 0; i < len; i++) {
		char c = var[i];
		if (c != '-' && c != '_' && !isupper((unsigned char)c) && !isdigit((unsigned char)c)) {
			return false;
		}
	}

	if (rules->behavior & RULES_ALLOW_FUZZY_MATCHING) {
		goto done;
	}

	char *prefix = libias_str_ndup(var, len - 1);
	if (strcmp(suffix, "DESC") == 0) {
		if (ast_lookup_option_group_p(node, prefix)) {
			libias_free(prefix);
			goto done;
		}
	}
	if (!ast_lookup_option_p(node, prefix)) {
		libias_free(prefix);
		return false;
	}
	libias_free(prefix);

done:
	if (prefix_ret) {
		*prefix_ret = libias_str_ndup(var, len);
	}
	if (helper_ret) {
		*helper_ret = libias_str_dup(suffix);
	}
	if (subpkg_ret) {
		if (subpkg) {
			*subpkg_ret = libias_str_dup(subpkg);
		} else {
			*subpkg_ret = NULL;
		}
	}

	return true;
}

bool
matches_options_group(
	struct LibiasMempool *pool,
	struct Rules *rules,
	struct AST *node,
	const char *s,
	char **prefix)
{
	size_t i = 0;
	// ^_?
	if (s[i] == '_') {
		i++;
	}

	const char *var = NULL;
	// OPTIONS_(GROUP|MULTI|RADIO|SINGLE)_
	const char *opts[] = {
		"OPTIONS_GROUP_",
		"OPTIONS_MULTI_",
		"OPTIONS_RADIO_",
		"OPTIONS_SINGLE_",
	};
	bool matched = false;
	for (size_t j = 0; j < nitems(opts); j++) {
		if (libias_str_prefix_p(s + i, opts[j])) {
			matched = true;
			i += strlen(opts[j]);
			var = opts[j];
			break;
		}
	}
	if (!matched) {
		return false;
	}

	if (rules->behavior & RULES_ALLOW_FUZZY_MATCHING) {
		// [-_[:upper:][:digit:]]+
		if (!(isupper((unsigned char)s[i]) || isdigit((unsigned char)s[i]) || s[i] == '-' || s[i] == '_')) {
			return false;
		}
		for (size_t len = strlen(s); i < len; i++) {
			if (!(isupper((unsigned char)s[i]) || isdigit((unsigned char)s[i]) || s[i] == '-' || s[i] == '_')) {
				return false;
			}
		}
		if (prefix) {
			*prefix = libias_str_ndup(var, strlen(var) - 1);
			libias_mempool_take(pool, *prefix);
		}
		return true;
	} else {
		// XXX: This could be stricter by checking the group type too
		if (ast_lookup_option_group_p(node, s + i)) {
			if (prefix) {
				*prefix = libias_str_ndup(var, strlen(var) - 1);
				libias_mempool_take(pool, *prefix);
			}
			return true;
		} else {
			return false;
		}
	}
}

bool
parse_cabal_datadir_vars_helper(
	struct LibiasMempool *pool,
	const char *var,
	const char *exe,
	char **prefix,
	char **suffix)
{
	char *buf = libias_str_printf("%s_DATADIR_VARS", exe);
	if (strcmp(var, buf) == 0) {
		if (prefix) {
			*prefix = libias_str_dup(exe);
			libias_mempool_take(pool, *prefix);
		}
		if (suffix) {
			*suffix = libias_str_dup("DATADIR_VARS");
			libias_mempool_take(pool, *suffix);
		}
		libias_free(buf);
		return true;
	} else {
		libias_free(buf);
		return false;
	}
}

bool
parse_cabal_datadir_vars(
	struct LibiasMempool *pool,
	struct Rules *rules,
	struct AST *root,
	const char *var,
	char **prefix,
	char **suffix)
{
	if (rules->behavior & RULES_ALLOW_FUZZY_MATCHING) {
		if (libias_str_suffix_p(var, "_DATADIR_VARS")) {
			if (prefix) {
				*prefix = libias_str_ndup(var, strlen(var) - strlen("_DATADIR_VARS"));
				libias_mempool_take(pool, *prefix);
			}
			if (suffix) {
				*suffix = libias_str_dup("DATADIR_VARS");
				libias_mempool_take(pool, *suffix);
			}
			return true;
		}
	}

	// Do we have USES=cabal?
	if (!ast_lookup_uses_p(root, "cabal")) {
		return false;
	}

	ast_lookup_cabal_executables_foreach(root, exe) {
		if (parse_cabal_datadir_vars_helper(pool, var, exe, prefix, suffix)) {
			return true;
		}
	}

	return false;
}

bool
parse_shebang_lang_helper(
	struct LibiasMempool *extpool,
	const char *var,
	const char *lang,
	char **prefix,
	char **suffix)
{
	char *buf = libias_str_printf("%s_OLD_CMD", lang);
	if (strcmp(var, buf) == 0) {
		libias_free(buf);
		if (prefix) {
			*prefix = libias_str_dup(lang);
			libias_mempool_take(extpool, *prefix);
		}
		if (suffix) {
			*suffix = libias_str_dup("OLD_CMD");
			libias_mempool_take(extpool, *suffix);
		}
		return true;
	}

	libias_free(buf);
	buf = libias_str_printf("%s_CMD", lang);
	if (strcmp(var, buf) == 0) {
		libias_free(buf);
		if (prefix) {
			*prefix = libias_str_dup(lang);
			libias_mempool_take(extpool, *prefix);
		}
		if (suffix) {
			*suffix = libias_str_dup("CMD");
			libias_mempool_take(extpool, *suffix);
		}
		return true;
	}
	libias_free(buf);

	return false;
}

bool
parse_shebang_lang(
	struct LibiasMempool *extpool,
	struct Rules *rules,
	struct AST *root,
	const char *var,
	char **prefix,
	char **suffix)
{
	if (rules->behavior & RULES_ALLOW_FUZZY_MATCHING) {
		if (libias_str_suffix_p(var, "_OLD_CMD")) {
			if (prefix) {
				*prefix = libias_str_ndup(var, strlen(var) - strlen("_OLD_CMD"));
				libias_mempool_take(extpool, *prefix);
			}
			if (suffix) {
				*suffix = libias_str_dup("OLD_CMD");
				libias_mempool_take(extpool, *suffix);
			}
			return true;
		}
		if (libias_str_suffix_p(var, "_CMD")) {
			if (prefix) {
				*prefix = libias_str_ndup(var, strlen(var) - strlen("_CMD"));
				libias_mempool_take(extpool, *prefix);
			}
			if (suffix) {
				*suffix = libias_str_dup("CMD");
				libias_mempool_take(extpool, *suffix);
			}
			return true;
		}
	}

	// Do we have USES=shebangfix?
	if (!ast_lookup_uses_p(root, "shebangfix")) {
		return false;
	}

	libias_array_foreach(rules->static_shebang_langs, const char *, lang) {
		if (parse_shebang_lang_helper(extpool, var, lang, prefix, suffix)) {
			return true;
		}
	}

	bool ok = false;
	ast_lookup_shebang_langs_foreach(root, lang) {
		if (parse_shebang_lang_helper(extpool, var, lang, prefix, suffix)) {
			ok = true;
			break;
		}
	}

	return ok;
}

// Returns the block VAR should be in according to RULES. If
// USES_CANDIDATES is non-NULL a set of required USES for VAR is
// returned. The caller is responsible to free USES_CANDIDATES itself.
// Its elements are static strings and do not need to be freed.
enum BlockType
rules_variable_order_block(
	struct Rules *rules,
	struct AST *root,
	const char *var,
	struct LibiasSet **uses_candidates)
{
	libias_scope_mempool(pool);

	if (uses_candidates) {
		*uses_candidates = NULL;
	}

	if (strcmp(var, "LICENSE") == 0) {
		return BLOCK_LICENSE;
	}
	libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
		if (entry->block != BLOCK_LICENSE ||
		    strcmp(entry->var, "LICENSE") == 0) {
			continue;
		}
		if (strcmp(entry->var, var) == 0) {
			return BLOCK_LICENSE;
		}
		if (libias_str_prefix_p(var, entry->var)) {
			const char *suffix = var + strlen(entry->var);
			if (*suffix == '_' && valid_license_p(rules, root, suffix + 1)) {
				return BLOCK_LICENSE;
			}
		}
	}

	if (parse_flavors_helper(pool, rules, root, var, NULL, NULL)) {
		return BLOCK_FLAVORS_HELPER;
	}

	if (parse_shebang_lang(pool, rules, root, var, NULL, NULL)) {
		return BLOCK_SHEBANGFIX;
	}

	if (parse_cabal_datadir_vars(pool, rules, root, var, NULL, NULL)) {
		return BLOCK_CABAL;
	}

	if (rules_parse_variable_options_helper(rules, root, var, NULL, NULL, NULL)) {
		if (libias_str_suffix_p(var, "_DESC")) {
			return BLOCK_OPTDESC;
		} else {
			return BLOCK_OPTHELPER;
		}
	}

	if (matches_options_group(pool, rules, root, var, NULL)) {
		return BLOCK_OPTDEF;
	}

	const char *tmp = var;
	char *var_without_subpkg = parse_subpkg(pool, rules, root, var, NULL);
	if (var_without_subpkg) {
		tmp = var_without_subpkg;
	}
	libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
		switch (entry->block) {
		case BLOCK_FLAVORS_HELPER:
		case BLOCK_OPTHELPER:
		case BLOCK_OPTDESC:
			continue;
		case BLOCK_LICENSE:
		case BLOCK_OPTDEF:
			// RE_LICENSE_*, matches_options_group() do not
			// cover all cases.
		default:
			break;
		}
		if (strcmp(tmp, entry->var) == 0) {
			size_t count = 0;
			bool satisfies_uses = true;
			// We skip the USES check if the port is a
			// slave port since often USES only appears
			// in the master.  Since we do not recurse
			// down in the master Makefile we would
			// get many false positives otherwise.
			if (!(rules->behavior & RULES_ALLOW_FUZZY_MATCHING) &&
			    !ast_lookup_masterdir(root)) {
				for (; count < nitems(entry->uses) && entry->uses[count]; count++);
				if (count > 0) {
					satisfies_uses = false;
					for (size_t j = 0; j < count; j++) {
						const char *requses = entry->uses[j];
						if (ast_lookup_uses_p(root, requses)) {
							satisfies_uses = true;
							break;
						}
					}
				}
			}
			if (satisfies_uses) {
				return entry->block;
			} else if (count > 0 && uses_candidates) {
				*uses_candidates = libias_set_new(libias_str_compare);
				for (size_t j = 0; j < count; j++) {
					libias_set_add(*uses_candidates, entry->uses[j]);
				}
			}
		}
	}

	return BLOCK_UNKNOWN;
}

int
rules_compare_order(const void *ap, const void *bp, void *userdata)
{
	libias_scope_mempool(pool);
	struct RulesCompareOrder *this = userdata;
	auto rules = this->rules;
	const char *a = *(const char **)ap;
	const char *b = *(const char **)bp;

	if (strcmp(a, b) == 0) {
		return 0;
	}
	enum BlockType ablock = rules_variable_order_block(rules, this->root, a, NULL);
	enum BlockType bblock = rules_variable_order_block(rules, this->root, b, NULL);
	if (ablock < bblock) {
		return -1;
	} else if (ablock > bblock) {
		return 1;
	}

	if (ablock == BLOCK_LICENSE) {
		int ascore = -1;
		int bscore = -1;
		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block != BLOCK_LICENSE) {
				continue;
			}
			if (strcmp(entry->var, "LICENSE") == 0) {
				continue;
			}
			if (libias_str_prefix_p(a, entry->var)) {
				ascore = entry_index;
			}
			if (libias_str_prefix_p(b, entry->var)) {
				bscore = entry_index;
			}
		}
		if (ascore < bscore) {
			return -1;
		} else if (ascore > bscore) {
			return 1;
		}
	} else if (ablock == BLOCK_FLAVORS_HELPER) {
		int ascore = -1;
		int bscore = -1;

		char *ahelper = NULL;
		char *aprefix = NULL;
		char *bhelper = NULL;
		char *bprefix = NULL;
		libias_panic_unless(
			parse_flavors_helper(pool, rules, this->root, a, &aprefix, &ahelper)
			&& parse_flavors_helper(pool, rules, this->root, b, &bprefix, &bhelper),
			"is_flavors_helper() failed");
		libias_panic_unless(
			ahelper && aprefix && bhelper && bprefix,
			"is_flavors_helper() returned invalid values");

		// Only compare if common prefix (helper for the same flavor)
		int prefix_score = strcmp(aprefix, bprefix);
		if (prefix_score == 0) {
			libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
				if (entry->block != BLOCK_FLAVORS_HELPER) {
					continue;
				}
				if (strcmp(ahelper, entry->var) == 0) {
					ascore = entry_index;
				}
				if (strcmp(bhelper, entry->var) == 0) {
					bscore = entry_index;
				}
			}
		}

		if (prefix_score != 0) {
			return prefix_score;
		} else if (ascore < bscore) {
			return -1;
		} else if (ascore > bscore) {
			return 1;
		} else {
			return strcmp(a, b);
		}
	} else if (ablock == BLOCK_SHEBANGFIX) {
		if (libias_str_suffix_p(a, "_CMD") && !libias_str_suffix_p(b, "_CMD")) {
			return 1;
		} else if (!libias_str_suffix_p(a, "_CMD") && libias_str_suffix_p(b, "_CMD")) {
			return -1;
		} else if (libias_str_suffix_p(a, "_CMD") && libias_str_suffix_p(b, "_CMD")) {
			char *alang = NULL;
			char *asuffix = NULL;
			char *blang = NULL;
			char *bsuffix = NULL;
			parse_shebang_lang(pool, rules, this->root, a, &alang, &asuffix);
			parse_shebang_lang(pool, rules, this->root, b, &blang, &bsuffix);
			libias_panic_unless(
				alang && asuffix && blang && bsuffix,
				"is_shebang_lang() returned invalid values");

			ssize_t ascore = -1;
			ssize_t bscore = -1;
			libias_array_foreach(rules->static_shebang_langs, const char *, lang) {
				if (strcmp(alang, lang) == 0) {
					ascore = lang_index;
				}
				if (strcmp(blang, lang) == 0) {
					bscore = lang_index;
				}
			}
			ast_lookup_shebang_langs_foreach(this->root, lang) {
				if (strcmp(alang, lang) == 0) {
					ascore = lang_index;
				}
				if (strcmp(blang, lang) == 0) {
					bscore = lang_index;
				}
			}

			bool aold = strcmp(asuffix, "OLD_CMD") == 0;
			bool bold = strcmp(bsuffix, "OLD_CMD") == 0;
			if (ascore == bscore) {
				if (aold && !bold) {
					return -1;
				} else if (!aold && bold) {
					return 1;
				} else {
					return 0;
				}
			} else if (ascore < bscore) {
				return -1;
			} else {
				return 1;
			}
		}
	} else if (ablock == BLOCK_CABAL) {
		// XXX: Yikes!
		if (strcmp(a, "SKIP_CABAL_PLIST") == 0) {
			return 1;
		} else if (strcmp(b, "SKIP_CABAL_PLIST") == 0) {
			return -1;
		} else if (libias_str_suffix_p(a, "_DATADIR_VARS") && !libias_str_suffix_p(b, "_DATADIR_VARS")) {
			return 1;
		} else if (!libias_str_suffix_p(a, "_DATADIR_VARS") && libias_str_suffix_p(b, "_DATADIR_VARS")) {
			return -1;
		} else if (libias_str_suffix_p(a, "_DATADIR_VARS") && libias_str_suffix_p(b, "_DATADIR_VARS")) {
			char *aexe = NULL;
			char *asuffix = NULL;
			char *bexe = NULL;
			char *bsuffix = NULL;
			parse_cabal_datadir_vars(pool, rules, this->root, a, &aexe, &asuffix);
			parse_cabal_datadir_vars(pool, rules, this->root, b, &bexe, &bsuffix);
			libias_panic_unless(aexe && asuffix && bexe && bsuffix,
				     "is_cabal_datadir_vars() returned invalid values");

			ssize_t ascore = -1;
			ssize_t bscore = -1;
			ast_lookup_cabal_executables_foreach(this->root, exe) {
				if (strcmp(aexe, exe) == 0) {
					ascore = exe_index;
				}
				if (strcmp(bexe, exe) == 0) {
					bscore = exe_index;
				}
			}

			bool aold = strcmp(asuffix, "DATADIR_VARS") == 0;
			bool bold = strcmp(bsuffix, "DATADIR_VARS") == 0;
			if (ascore == bscore) {
				if (aold && !bold) {
					return -1;
				} else if (!aold && bold) {
					return 1;
				} else {
					return 0;
				}
			} else if (ascore < bscore) {
				return -1;
			} else {
				return 1;
			}
		}
	} else if (ablock == BLOCK_OPTDESC) {
		return strcmp(a, b);
	} else if (ablock == BLOCK_OPTHELPER) {
		int ascore = -1;
		int bscore = -1;

		char *ahelper = NULL;
		char *aprefix = NULL;
		char *bhelper = NULL;
		char *bprefix = NULL;
		// TODO SUBPKG
		libias_panic_unless(
			rules_parse_variable_options_helper(rules, this->root, a, &aprefix, &ahelper, NULL)
			&& rules_parse_variable_options_helper(rules, this->root, b, &bprefix, &bhelper, NULL),
			"is_options_helper() failed");
		libias_panic_unless(
			ahelper && aprefix && bhelper && bprefix,
			"is_options_helper() returned invalid values");
		libias_scope_free(aprefix);
		libias_scope_free(ahelper);
		libias_scope_free(bprefix);
		libias_scope_free(bhelper);

		// Only compare if common prefix (helper for the same option)
		int prefix_score = strcmp(aprefix, bprefix);
		if (prefix_score == 0) {
			libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
				if (entry->block != BLOCK_OPTHELPER) {
					continue;
				}
				if (strcmp(ahelper, entry->var) == 0) {
					ascore = entry_index;
				}
				if (strcmp(bhelper, entry->var) == 0) {
					bscore = entry_index;
				}
			}
		}

		if (prefix_score != 0) {
			return prefix_score;
		} else if (ascore < bscore) {
			return -1;
		} else if (ascore > bscore) {
			return 1;
		} else {
			return strcmp(a, b);
		}
	} else if (ablock == BLOCK_OPTDEF) {
		int ascore = -1;
		int bscore = -1;

		libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
			if (entry->block != BLOCK_OPTDEF) {
				continue;
			}
			if (libias_str_prefix_p(a, entry->var)) {
				ascore = entry_index;
			}
			if (libias_str_prefix_p(b, entry->var)) {
				bscore = entry_index;
			}
		}

		if (ascore < bscore) {
			return -1;
		} else if (ascore > bscore) {
			return 1;
		} else {
			return strcmp(a, b);
		}
	}

	char *asubpkg = NULL;
	char *a_without_subpkg = parse_subpkg(pool, rules, this->root, a, &asubpkg);
	if (a_without_subpkg == NULL) {
		a_without_subpkg = libias_str_dup( a);
		libias_mempool_take(pool, a_without_subpkg);
	}
	char *bsubpkg = NULL;
	char *b_without_subpkg = parse_subpkg(pool, rules, this->root, b, &bsubpkg);
	if (b_without_subpkg == NULL) {
		b_without_subpkg = libias_str_dup(b);
		libias_mempool_take(pool, b_without_subpkg);
	}
	int ascore = -1;
	int bscore = -1;
	libias_array_foreach(rules->variable_order, struct VariableOrderEntry *, entry) {
		if (ascore != -1 && bscore != -1) {
			break;
		}
		if (strcmp(a_without_subpkg, entry->var) == 0) {
			ascore = entry_index;
		}
		if (strcmp(b_without_subpkg, entry->var) == 0) {
			bscore = entry_index;
		}
	}

	if (strcmp(a_without_subpkg, b_without_subpkg) == 0 && asubpkg && bsubpkg) {
		return strcmp(asubpkg, bsubpkg);
	} else if (asubpkg && !bsubpkg) {
		return 1;
	} else if (!asubpkg && bsubpkg) {
		return -1;
	} else if (ascore < bscore) {
		return -1;
	} else if (ascore > bscore) {
		return 1;
	} else {
		return strcmp(a_without_subpkg, b_without_subpkg);
	}
}

void
parse_target_options_helper(
	struct LibiasMempool *extpool,
	struct Rules *rules,
	struct AST *root,
	const char *target,
	char **target_out,
	char **opt_out,
	bool *state)
{
	bool on;
	if (((on = libias_str_suffix_p(target, "-on")) || libias_str_suffix_p(target, "-off"))) {
		const char *p = target;
		for (; *p == '-' || (islower((unsigned char)*p) && isalpha((unsigned char)*p)); p++);
		size_t opt_suffix_len;
		if (on) {
			opt_suffix_len = strlen("-on");
		} else {
			opt_suffix_len = strlen("-off");
		}
		char *opt = libias_str_ndup(p, strlen(p) - opt_suffix_len);
		libias_scope_free(opt);
		char *tmp = libias_str_printf("%s_USES", opt);
		libias_scope_free(tmp);
		if (rules_parse_variable_options_helper(rules, root, tmp, NULL, NULL, NULL)) {
			char *target_root = libias_str_ndup(target, strlen(target) - strlen(p) - 1);
			libias_scope_free(target_root);
			libias_array_foreach(rules->target_order, struct TargetOrderEntry *, entry) {
				if (entry->opthelper && strcmp(entry->name, target_root) == 0) {
					*state = on;
					if (opt_out) {
						*opt_out = libias_str_dup(opt);
						libias_mempool_take(extpool, *opt_out);
					}
					if (target_out) {
						*target_out = libias_str_dup(target_root);
						libias_mempool_take(extpool, *target_out);
					}
					return;
				}
			}
		}
	}

	if (opt_out) {
		*opt_out = NULL;
	}
	*state = false;
	if (target_out) {
		*target_out = libias_str_dup(target);
		libias_mempool_take(extpool, *target_out);
	}
}

bool
rules_known_target_p(
	struct Rules *rules,
	struct AST *root_node,
	const char *target)
{
	libias_scope_mempool(pool);

	char *root;
	bool state;
	parse_target_options_helper(pool, rules, root_node, target, &root, NULL, &state);

	libias_array_foreach(rules->target_order, struct TargetOrderEntry *, entry) {
		if (strcmp(entry->name, root) == 0) {
			return true;
		}
	}

	return false;
}

bool
rules_special_source_p(struct Rules *rules, const char *source)
{
	libias_array_foreach(rules->special_sources, const char *, special_source) {
		if (strcmp(source, special_source) == 0) {
			return true;
		}
	}
	return false;
}

bool
rules_special_target_p(struct Rules *rules, const char *target)
{
	libias_array_foreach(rules->special_targets, const char *, special_target) {
		if (strcmp(target, special_target) == 0) {
			return true;
		}
	}
	return false;
}

int
rules_compare_target_order(const void *ap, const void *bp, void *userdata)
{
	struct RulesCompareOrder *this = userdata;
	auto rules = this->rules;

	libias_scope_mempool(pool);

	const char *a_ = *(const char **)ap;
	const char *b_ = *(const char **)bp;

	if (strcmp(a_, b_) == 0) {
		return 0;
	}

	char *a, *b, *aopt, *bopt;
	bool aoptstate, boptstate;
	parse_target_options_helper(pool, rules, this->root, a_, &a, &aopt, &aoptstate);
	parse_target_options_helper(pool, rules, this->root, b_, &b, &bopt, &boptstate);

	ssize_t aindex = -1;
	ssize_t bindex = -1;
	libias_array_foreach(rules->target_order, struct TargetOrderEntry *, entry) {
		if (aindex != -1 && bindex != -1) {
			break;
		}
		if (aindex == -1 && strcmp(entry->name, a) == 0) {
			aindex = entry_index;
		}
		if (bindex == -1 && strcmp(entry->name, b) == 0) {
			bindex = entry_index;
		}
	}

	if (aindex == -1) {
		return 1;
	} else if (bindex == -1) {
		return -1;
	} else if (aindex == bindex) {
		if (aopt == NULL) {
			return -1;
		}
		if (bopt == NULL) {
			return 1;
		}

		int c = strcmp(aopt, bopt);
		if (c < 0) {
			return -1;
		} else if (c > 0) {
			return 1;
		}

		if (aoptstate && !boptstate) {
			return -1;
		} else if (!aoptstate && boptstate) {
			return 1;
		} else {
			libias_panic("should not happen");
		}
	} else if (aindex < bindex) {
		return -1;
	} else if (aindex > bindex) {
		return 1;
	} else {
		libias_panic("should not happen");
	}
}

bool
rules_target_command_wrap_after_each_token_p(
	struct Rules *rules,
	const char *command)
{
	libias_array_foreach(
		rules->target_command_wrap_after_each_token,
		const char *, token)
	{
		if (strcmp(command, token) == 0) {
			return true;
		}
	}
	return false;
}

bool
rules_target_command_should_wrap_p(
	struct Rules *rules,
	const char *word)
{
	if (strcmp(word, "&&") == 0 ||
	    strcmp(word, "||") == 0 ||
	    strcmp(word, "then") == 0 ||
	    (libias_str_suffix_p(word, ";") && !libias_str_suffix_p(word, "\\;")) ||
	    strcmp(word, "|") == 0) {
		return true;
	}

	return false;
}
