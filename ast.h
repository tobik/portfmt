// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTType {
	AST_ROOT,                     // sexp:"ast"
	AST_COMMENT,                  // sexp:"comment"
	AST_EXPR,                     // sexp:"expr"
	AST_IF,                       // sexp:"if"
	AST_IF_BRANCH,                // sexp:"branch"
	AST_FOR,                      // sexp:"for"
	AST_FOR_BINDINGS,             // sexp:"for-bindings"
	AST_FOR_WORDS,                // sexp:"for-words"
	AST_FOR_BODY,                 // sexp:"for-body"
	AST_FOR_END,                  // sexp:"for-end"
	AST_INCLUDE,                  // sexp:"include"
	AST_TARGET_RULE,              // sexp:"target-rule"
	AST_TARGET_RULE_DEPENDENCIES, // sexp:"target-rule-dependencies"
	AST_TARGET_RULE_TARGETS,      // sexp:"target-rule-targets"
	AST_TARGET_RULE_BODY,         // sexp:"target-rule-body"
	AST_TARGET_COMMAND,           // sexp:"target-command"
	AST_VARIABLE,                 // sexp:"variable"
	AST_VARIABLE_NAME,            // sexp:"variable-name"
};

// Special child indices for AST_FOR nodes and ast_get_child
enum ASTForChildIndices {
	AST_CHILD_FOR_BINDINGS = 0,
	AST_CHILD_FOR_WORDS    = 1,
	AST_CHILD_FOR_BODY     = 2,
	AST_CHILD_FOR_END      = 3,
};

// Special child indices for AST_TARGET_RULE nodes and ast_get_child
enum ASTTargetRuleChildIndices {
	AST_CHILD_TARGET_RULE_TARGETS      = 0,
	AST_CHILD_TARGET_RULE_DEPENDENCIES = 1,
	AST_CHILD_TARGET_RULE_BODY         = 2,
};

// Special child indices for AST_VARIABLE nodes and ast_get_child
enum ASTVariableChildIndices {
	AST_CHILD_VARIABLE_NAME = 0,
};

libias_attr_returns_nonnull
const char *ASTType_sexp(enum ASTType);

libias_attr_returns_nonnull
const char *ASTType_tostring(enum ASTType);

enum ASTExprType {
	AST_EXPR_ERROR,          // identifier:".error"          sexp:"error"
	AST_EXPR_EXPORT_ENV,     // identifier:".export-env"     sexp:"export-env"
	AST_EXPR_EXPORT_ENV_DOT, // identifier:".export.env"     sexp:"export.env"
	AST_EXPR_EXPORT_LITERAL, // identifier:".export-literal" sexp:"export-literal"
	AST_EXPR_EXPORT,         // identifier:".export"         sexp:"export"
	AST_EXPR_INFO,           // identifier:".info"           sexp:"info"
	AST_EXPR_UNDEF,          // identifier:".undef"          sexp:"undef"
	AST_EXPR_UNEXPORT_ENV,   // identifier:".unexport-env"   sexp:"unexport-env"
	AST_EXPR_UNEXPORT,       // identifier:".unexport"       sexp:"unexport"
	AST_EXPR_WARNING,        // identifier:".warning"        sexp:"warning"
};

libias_attr_returns_nonnull
const char *ASTExprType_identifier(enum ASTExprType);

libias_attr_returns_nonnull
const char *ASTExprType_sexp(enum ASTExprType);

libias_attr_returns_nonnull
const char *ASTExprType_tostring(enum ASTExprType);

enum ASTIfType {
	AST_IF_IF,    // human:"if"
	AST_IF_DEF,   // human:"ifdef"
	AST_IF_ELSE,  // human:"else"
	AST_IF_MAKE,  // human:"ifmake"
	AST_IF_NDEF,  // human:"ifndef"
	AST_IF_NMAKE, // human:"ifnmake"
};

libias_attr_returns_nonnull
const char *ASTIfType_human(enum ASTIfType);

libias_attr_returns_nonnull
const char *ASTIfType_tostring(enum ASTIfType);

enum ASTIncludeType {
	AST_INCLUDE_BMAKE,            // identifier:".include"  sexp:"bmake"
	AST_INCLUDE_POSIX,            // identifier:"include"   sexp:"posix"
	AST_INCLUDE_POSIX_OPTIONAL,   // identifier:"-include"  sexp:"posix-optional"
	AST_INCLUDE_POSIX_OPTIONAL_S, // identifier:"sinclude"  sexp:"posix-optional-s"
	AST_INCLUDE_OPTIONAL,         // identifier:".-include" sexp:"optional"
	AST_INCLUDE_OPTIONAL_D,       // identifier:".dinclude" sexp:"optional-d"
	AST_INCLUDE_OPTIONAL_S,       // identifier:".sinclude" sexp:"optional-s"
};

libias_attr_returns_nonnull
const char *ASTIncludeType_identifier(enum ASTIncludeType);

libias_attr_returns_nonnull
const char *ASTIncludeType_sexp(enum ASTIncludeType);

libias_attr_returns_nonnull
const char *ASTIncludeType_tostring(enum ASTIncludeType);

enum ASTTargetRuleOperator {
	AST_TARGET_RULE_OPERATOR_1, // bmake:":"
	AST_TARGET_RULE_OPERATOR_2, // bmake:"!"
	AST_TARGET_RULE_OPERATOR_3, // bmake:"::"
};

libias_attr_nonnull(1)
bool ASTTargetRuleOperator_from_bmake(const char *, const size_t, enum ASTTargetRuleOperator *);

libias_attr_returns_nonnull
const char *ASTTargetRuleOperator_bmake(enum ASTTargetRuleOperator);

enum ASTVariableModifier {
	AST_VARIABLE_MODIFIER_APPEND,   // human:"+=" sexp:"append"
	AST_VARIABLE_MODIFIER_ASSIGN,   // human:"="  sexp:"assign"
	AST_VARIABLE_MODIFIER_EXPAND,   // human:":=" sexp:"expand"
	AST_VARIABLE_MODIFIER_OPTIONAL, // human:"?=" sexp:"optional"
	AST_VARIABLE_MODIFIER_SHELL,    // human:"!=" sexp:"shell"
};

libias_attr_nonnull(1)
bool ASTVariableModifier_from_human(const char *, const size_t, enum ASTVariableModifier *);

libias_attr_returns_nonnull
const char *ASTVariableModifier_human(enum ASTVariableModifier);

libias_attr_returns_nonnull
const char *ASTVariableModifier_sexp(enum ASTVariableModifier);

libias_attr_returns_nonnull
const char *ASTVariableModifier_tostring(enum ASTVariableModifier);

enum ASTEditFlags {
	AST_EDIT_DEFAULT = 0,
};

enum ASTWordsEditFlags {
	AST_WORDS_EDIT_DEFAULT = 0,
};

typedef bool (*ASTVisitPredicateFn)(struct ASTVisit *, struct AST *);
typedef void (*ASTVisitFn)(struct ASTVisit *, struct AST *);

struct ASTVisitTrait {
	ASTVisitFn default_pre_visit_fn;
	ASTVisitFn default_visit_fn;

	// Should we call visit_pre_*() for this node? Default is true. If
	// this predicate returns false visit_pre_*() will NOT be
	// called. The children of the node will always still be visited and
	// the node will be considered as visited.
	ASTVisitPredicateFn visit_pre_node_p;

	// Should we call visit_*() for this node? Default is true. If this
	// predicate returns false visit_*() will NOT be called. The
	// children of the node will always still be visited and the node
	// will be considered as visited.
	ASTVisitPredicateFn visit_node_p;

	// Should we visit the children of this node?
	ASTVisitPredicateFn visit_children_p;

	// These functions are called before visiting the node's children.
	ASTVisitFn visit_pre_root;
	ASTVisitFn visit_pre_comment;
	ASTVisitFn visit_pre_expr;
	ASTVisitFn visit_pre_if;
	ASTVisitFn visit_pre_if_branch;
	ASTVisitFn visit_pre_for;
	ASTVisitFn visit_pre_for_bindings;
	ASTVisitFn visit_pre_for_words;
	ASTVisitFn visit_pre_for_body;
	ASTVisitFn visit_pre_for_end;
	ASTVisitFn visit_pre_include;
	ASTVisitFn visit_pre_target_rule;
	ASTVisitFn visit_pre_target_rule_dependencies;
	ASTVisitFn visit_pre_target_rule_targets;
	ASTVisitFn visit_pre_target_rule_body;
	ASTVisitFn visit_pre_target_command;
	ASTVisitFn visit_pre_variable;
	ASTVisitFn visit_pre_variable_name;

	// These functions are called after visiting a node's children.
	ASTVisitFn visit_root;
	ASTVisitFn visit_comment;
	ASTVisitFn visit_expr;
	ASTVisitFn visit_if;
	ASTVisitFn visit_if_branch;
	ASTVisitFn visit_for;
	ASTVisitFn visit_for_bindings;
	ASTVisitFn visit_for_words;
	ASTVisitFn visit_for_body;
	ASTVisitFn visit_for_end;
	ASTVisitFn visit_include;
	ASTVisitFn visit_target_rule;
	ASTVisitFn visit_target_rule_dependencies;
	ASTVisitFn visit_target_rule_targets;
	ASTVisitFn visit_target_rule_body;
	ASTVisitFn visit_target_command;
	ASTVisitFn visit_variable;
	ASTVisitFn visit_variable_name;
};

libias_attr_nonnull(1)
void ast_cleanup(struct AST **);

libias_attr_nonnull(1, 2, 3)
struct AST *ast_from_buffer(struct LibiasBuffer *, struct Diagnostic *, const char *);

libias_attr_nonnull(1, 2, 3)
struct AST *ast_from_file(struct LibiasFile *, struct Diagnostic *, const char *);

libias_attr_returns_nonnull
struct AST *ast_new_comment(void);

libias_attr_returns_nonnull
struct AST *ast_new_expr(void);

libias_attr_returns_nonnull
struct AST *ast_new_for(void);

libias_attr_returns_nonnull
struct AST *ast_new_if(void);

libias_attr_returns_nonnull
struct AST *ast_new_if_branch(void);

libias_attr_returns_nonnull
struct AST *ast_new_include(void);

libias_attr_returns_nonnull
struct AST *ast_new_root(void);

libias_attr_returns_nonnull
struct AST *ast_new_target_command(void);

libias_attr_returns_nonnull
struct AST *ast_new_target_rule(void);

libias_attr_returns_nonnull
struct AST *ast_new_variable(void);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct AST *ast_clone(struct AST *);

libias_attr_nonnull(1, 2)
void ast_sort_words(struct AST *, struct LibiasCompareTrait *);

libias_attr_nonnull(1, 2)
bool ast_child_p(struct AST *, struct AST *);

libias_attr_nonnull(1)
bool ast_ports_framework_include_p(struct AST *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct AST *ast_parent(struct AST *);

libias_attr_nonnull(1)
struct AST *ast_parent_for(struct AST *);

libias_attr_nonnull(1)
struct AST *ast_parent_if(struct AST *);

libias_attr_nonnull(1)
struct AST *ast_parent_include(struct AST *);

libias_attr_nonnull(1)
struct AST *ast_parent_target_rule(struct AST *);

libias_attr_nonnull(1)
enum ASTType ast_type(const struct AST *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct AST *ast_root(struct AST *);

libias_attr_nonnull(1)
bool ast_root_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_comment_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_expr_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_if_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_if_branch_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_for_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_for_bindings_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_for_words_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_for_body_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_for_end_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_include_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_target_rule_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_target_rule_dependencies_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_target_rule_targets_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_target_rule_body_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_target_command_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_variable_p(const struct AST *);

libias_attr_nonnull(1)
bool ast_variable_name_p(const struct AST *);

libias_attr_nonnull(1, 2)
void ast_sexp(struct AST *, struct LibiasBuffer *, bool);

libias_attr_nonnull(1, 2)
void ast_render(struct AST *, struct LibiasBuffer *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
const char *ast_expr_indent(struct AST *);

libias_attr_nonnull(1, 2)
void ast_set_expr_indent(struct AST *, const char *);

libias_attr_nonnull(1)
enum ASTExprType ast_expr_type(struct AST *);

libias_attr_nonnull(1)
void ast_set_expr_type(struct AST *, const enum ASTExprType);

libias_attr_nonnull(1)
struct AST *ast_get_child(struct AST *, ssize_t);

libias_attr_nonnull(1)
enum ASTIfType ast_if_branch_type(struct AST *);

libias_attr_nonnull(1)
void ast_set_if_branch_type(struct AST *, const enum ASTIfType);

libias_attr_nonnull(1)
bool ast_include_loaded_p(struct AST *);

libias_attr_nonnull(1)
void ast_set_include_loaded(struct AST *, bool);

libias_attr_nonnull(1)
bool ast_include_system_p(struct AST *);

libias_attr_nonnull(1)
struct ASTWord *ast_include_path(struct AST *);

libias_attr_nonnull(1)
enum ASTIncludeType ast_include_type(struct AST *);

libias_attr_nonnull(1)
void ast_set_include_type(struct AST *, const enum ASTIncludeType);

libias_attr_nonnull(1)
struct ASTWord *ast_node_comment(struct AST *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
const char *ast_target_command_flags(struct AST *);

libias_attr_nonnull(1, 2)
void ast_set_target_command_flags(struct AST *, const char *);

libias_attr_nonnull(1)
enum ASTTargetRuleOperator ast_target_rule_operator(struct AST *);

libias_attr_nonnull(1)
enum ASTVariableModifier ast_variable_modifier(struct AST *);

libias_attr_nonnull(1)
struct ASTWord *ast_variable_name(struct AST *);

libias_attr_nonnull(1)
struct ASTQueryCache *ast_get_query_cache(struct AST *);

libias_attr_nonnull(1)
void ast_set_query_cache(struct AST *, struct ASTQueryCache *, void (*)(struct ASTQueryCache **));

libias_attr_nonnull(1)
void ast_visit_trait_init(struct ASTVisitTrait *);

libias_attr_nonnull(1, 2)
struct ASTVisit *ast_visit_new(struct AST *, struct ASTVisitTrait *);

libias_attr_nonnull(1)
void ast_visit_cleanup(struct ASTVisit **);

libias_attr_nonnull(1)
void ast_visit_run(struct ASTVisit *);

libias_attr_nonnull(1, 2, 3)
void ast_visit_set_format_settings(struct ASTVisit *, struct Rules *, struct ASTFormatSettings *);

libias_attr_nonnull(1, 2)
void ast_visit_format(struct ASTVisit *, struct AST *);

libias_attr_nonnull(1)
void ast_visit_set_context(struct ASTVisit *, void *);

libias_attr_nonnull(1)
void *ast_visit_context(struct ASTVisit *);

libias_attr_nonnull(1)
struct Diagnostic *ast_visit_diagnostic(struct ASTVisit *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct AST *ast_visit_root(struct ASTVisit *);

libias_attr_nonnull(1)
struct ASTEdit *ast_visit_root_edit(struct ASTVisit *);

libias_attr_nonnull(1)
void ast_visit_abort(struct ASTVisit *);

libias_attr_nonnull(1)
void ast_visit_continue(struct ASTVisit *);

libias_attr_nonnull(1)
void ast_visit_stop(struct ASTVisit *);

libias_attr_nonnull(1, 2)
bool ast_visit_visited_node_p(struct ASTVisit *, struct AST *);

libias_attr_nonnull(1, 2)
bool ast_visit_pre_visited_node_p(struct ASTVisit *, struct AST *);

libias_attr_nonnull(1, 2)
void ast_visit_skip(struct ASTVisit *, struct AST *);

libias_attr_nonnull(1, 2)
libias_attr_returns_nonnull
struct ASTEdit *ast_edit_new(struct AST *, struct Diagnostic *, enum ASTEditFlags);

libias_attr_nonnull(1)
void ast_edit_cleanup(struct ASTEdit **);

libias_attr_nonnull(1, 2, 3)
void ast_edit_add_child(struct ASTEdit *, struct AST *, struct AST *);

libias_attr_nonnull(1, 2, 3)
void ast_edit_add_child_before(struct ASTEdit *, struct AST *, struct AST *);

libias_attr_nonnull(1, 2)
void ast_edit_append_child(struct ASTEdit *, struct AST *);

libias_attr_nonnull(1, 2)
void ast_edit_remove_child(struct ASTEdit *, struct AST *);

libias_attr_nonnull(1)
void ast_edit_remove_all_children(struct ASTEdit *);

libias_attr_nonnull(1, 2, 3)
void ast_edit_replace_child(struct ASTEdit *, struct AST *, struct AST *);

libias_attr_nonnull(1)
void ast_edit_set_variable_modifier(struct ASTEdit *, const enum ASTVariableModifier);

libias_attr_nonnull(1)
void ast_edit_set_target_rule_operator(struct ASTEdit *, const enum ASTTargetRuleOperator);

libias_attr_nonnull(1, 2)
void ast_edit_chain_apply(struct ASTEdit *, struct ASTEdit *);

libias_attr_nonnull(1, 2)
void ast_edit_apply_words_edit(struct ASTEdit *, struct ASTWordsEdit *);

libias_attr_nonnull(1)
bool ast_edit_apply(struct ASTEdit *);

libias_attr_nonnull(1, 2)
libias_attr_returns_nonnull
struct ASTWordsEdit *ast_words_edit_new(struct AST *, struct Diagnostic *, enum ASTWordsEditFlags);

libias_attr_nonnull(1)
void ast_words_edit_cleanup(struct ASTWordsEdit **);

libias_attr_nonnull(1, 2, 3)
void ast_words_edit_add_word(struct ASTWordsEdit *, struct ASTWord *, struct ASTWord *);

libias_attr_nonnull(1, 2, 3)
void ast_words_edit_add_word_before(struct ASTWordsEdit *, struct ASTWord *, struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_words_edit_append_word(struct ASTWordsEdit *, struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_words_edit_remove_word(struct ASTWordsEdit *, struct ASTWord *);

libias_attr_nonnull(1, 2, 3)
void ast_words_edit_replace_word(struct ASTWordsEdit *, struct ASTWord *, struct ASTWord *);

libias_attr_nonnull(1)
void ast_words_edit_remove_all_words(struct ASTWordsEdit *);

libias_attr_nonnull(1)
void ast_words_edit_remove_beginning_whitespace(struct ASTWordsEdit *);

libias_attr_nonnull(1)
void ast_words_edit_remove_trailing_whitespace(struct ASTWordsEdit *);

libias_attr_nonnull(1, 2)
void ast_words_edit_remove_whitespace_after_word(struct ASTWordsEdit *, const struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_words_edit_remove_whitespace_before_word(struct ASTWordsEdit *, const struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_words_edit_set_whitespace_after_word(struct ASTWordsEdit *, const struct ASTWord *, struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_words_edit_set_whitespace_before_word(struct ASTWordsEdit *, const struct ASTWord *, struct ASTWord *);

libias_attr_nonnull(1)
bool ast_words_edit_apply(struct ASTWordsEdit *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *ast_words_flatten(struct AST *);

libias_attr_nonnull(1, 2)
libias_attr_returns_nonnull
char *ast_words_no_meta_flatten(struct AST *, const char *);

libias_attr_nonnull(1)
size_t ast_words_len(struct AST *);

libias_attr_nonnull(1)
size_t ast_words_no_meta_len(struct AST *);

libias_attr_nonnull(1)
struct AST *ast_next_sibling(struct AST *);

libias_attr_nonnull(1)
struct AST *ast_prev_sibling(struct AST *);

libias_attr_nonnull(1)
struct ASTWord *ast_next_word(struct AST *, struct ASTWord *);

libias_attr_nonnull(1)
struct ASTWord *ast_prev_word(struct AST *, struct ASTWord *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_children_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_words_iterator(struct AST *, ssize_t, ssize_t);

#define ast_children_foreach_slice(NODE, A, B, VAR) \
	libias_iterator_foreach(ast_children_iterator(NODE, A, B), struct AST *, VAR, struct AST *, libias_macro_gensym(VAR))
#define ast_children_foreach(NODE, VAR) \
	ast_children_foreach_slice(NODE, 0, -1, VAR)

#define ast_words_foreach_slice(NODE, A, B, VAR) \
	libias_iterator_foreach(ast_words_iterator(NODE, A, B), struct ASTWord *, VAR, struct ASTWord *, libias_macro_gensym(VAR))
#define ast_words_foreach(NODE, VAR) \
	ast_words_foreach_slice(NODE, 0, -1, VAR)
