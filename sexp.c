// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/color.h>
#include <libias/iterator.h>
#include <libias/stack.h>
#include <libias/str.h>

#include "sexp.h"

struct SexpWriterParenthesisPair {
	const char *open;
	const char *close;
};

struct SexpWriter {
	struct LibiasBuffer *f;

	struct SexpWriterParenthesisPair *parenthesis_pairs;
	size_t parenthesis_pairs_len;
	size_t parenthesis_pairs_pos;
	struct LibiasStack *parenthesis_stack;

	int32_t level;
};

// Constants
static const size_t sexp_writer_indent_step = 1;
static struct SexpWriterParenthesisPair parenthesis_pairs_monochrome[] = {
	{ "(", ")" },
};
static struct SexpWriterParenthesisPair parenthesis_pairs_color[] = {
	{ "(", ")" },
	{ LIBIAS_ANSI_COLOR_RED "(" LIBIAS_ANSI_COLOR_RESET,
	  LIBIAS_ANSI_COLOR_RED ")" LIBIAS_ANSI_COLOR_RESET },
	{ LIBIAS_ANSI_COLOR_GREEN "(" LIBIAS_ANSI_COLOR_RESET,
	  LIBIAS_ANSI_COLOR_GREEN ")" LIBIAS_ANSI_COLOR_RESET },
	{ LIBIAS_ANSI_COLOR_YELLOW "(" LIBIAS_ANSI_COLOR_RESET,
	  LIBIAS_ANSI_COLOR_YELLOW ")" LIBIAS_ANSI_COLOR_RESET },
	{ LIBIAS_ANSI_COLOR_BLUE "(" LIBIAS_ANSI_COLOR_RESET,
	  LIBIAS_ANSI_COLOR_BLUE ")" LIBIAS_ANSI_COLOR_RESET },
	{ LIBIAS_ANSI_COLOR_MAGENTA "(" LIBIAS_ANSI_COLOR_RESET,
	  LIBIAS_ANSI_COLOR_MAGENTA ")" LIBIAS_ANSI_COLOR_RESET },
	{ LIBIAS_ANSI_COLOR_CYAN "(" LIBIAS_ANSI_COLOR_RESET,
	  LIBIAS_ANSI_COLOR_CYAN ")" LIBIAS_ANSI_COLOR_RESET },
};

struct SexpWriter *
sexp_writer_new(bool rainbow)
{
	struct SexpWriter *w = libias_alloc(struct SexpWriter);

	if (rainbow) {
		w->parenthesis_pairs = parenthesis_pairs_color;
		w->parenthesis_pairs_len = nitems(parenthesis_pairs_color);
	} else {
		w->parenthesis_pairs = parenthesis_pairs_monochrome;
		w->parenthesis_pairs_len = nitems(parenthesis_pairs_monochrome);
	}
	w->parenthesis_stack = libias_stack_new();

	w->f = libias_buffer_new();
	return w;
}

void
sexp_writer_cleanup(struct SexpWriter **w_ptr)
{
	struct SexpWriter *w = *w_ptr;
	if (w) {
		libias_cleanup(&w->parenthesis_stack);
		libias_cleanup(&w->f);
		libias_free(w);
		*w_ptr = NULL;
	}
}

void
sexp_writer_open_tree(struct SexpWriter *w, const char *name)
{
	if (w->level > 0) {
		libias_buffer_puts(w->f, "\n");
	}
	{
		size_t indent = w->level * sexp_writer_indent_step;
		char *buf = libias_str_repeat(" ", indent);
		libias_buffer_puts(w->f, buf);
		libias_free(buf);
	}

	const struct SexpWriterParenthesisPair pair = w->parenthesis_pairs[w->parenthesis_pairs_pos++ % w->parenthesis_pairs_len];
	libias_buffer_puts(w->f, pair.open);
	libias_stack_push(w->parenthesis_stack, pair.close);

	libias_buffer_puts(w->f, name);
	w->level++;
}

void
sexp_writer_close_tree(struct SexpWriter *w)
{
	libias_panic_unless(
		libias_stack_peek(w->parenthesis_stack),
		"no open parentheses");
	const char *s = libias_stack_pop(w->parenthesis_stack);
	libias_buffer_puts(w->f, s);
	w->level--;
}

void
sexp_writer_bool(struct SexpWriter *w, const char *name, bool value)
{
	const char *s = "#f";
	if (value) {
		s = "#t";
	}
	sexp_writer_open_tree(w, name);
	libias_buffer_puts(w->f, " ");
	libias_buffer_puts(w->f, s);
	sexp_writer_close_tree(w);
}

void
sexp_writer_int64_array(struct SexpWriter *w, const char *name, int64_t values[], size_t len)
{
	if (len > 0) {
		sexp_writer_open_tree(w, name);
		for (size_t i = 0; i < len; i++) {
			libias_buffer_puts(w->f, " ");
			libias_buffer_puts(w->f, values[i]);
		}
		sexp_writer_close_tree(w);
	}
}

void
sexp_writer_keyword(struct SexpWriter *w, const char *name, const char *value)
{
	if (value) {
		sexp_writer_open_tree(w, name);
		libias_buffer_puts(w->f, " #:");
		libias_buffer_puts(w->f, value);
		sexp_writer_close_tree(w);
	}
}

void
sexp_writer_keywords(
	struct SexpWriter *w,
	const char *name,
	struct LibiasArray *values)
{
	if (libias_array_len(values) > 0) {
		sexp_writer_open_tree(w, name);
		libias_array_foreach(values, const char *, value) {
			// TODO: Check if valid symbol!!! This goes for all key
			// names!!!
			libias_buffer_puts(w->f, " #:");
			libias_buffer_puts(w->f, value);
		}
		sexp_writer_close_tree(w);
	}
}

void
sexp_writer_string(struct SexpWriter *w, const char *name, const char *value)
{
	if (!value) {
		return;
	}

	sexp_writer_open_tree(w, name);

	libias_scope_buffer(buf);
	libias_buffer_puts(buf, " \"");
	size_t valuelen = strlen(value);
	for (size_t i = 0; i < valuelen; i++) {
		switch (value[i]) {
		case '\\':
			libias_buffer_puts(buf, "\\\\");
			break;
		case '"':
			libias_buffer_puts(buf, "\\\"");
			break;
		case '\n':
			libias_buffer_puts(buf, "\\n");
			break;
		case '\t':
			libias_buffer_puts(buf, "\\t");
			break;
		default:
			libias_buffer_write(buf, value + i, 1, 1);
			break;
		}
	}
	libias_buffer_puts(buf, "\"");
	libias_buffer_puts(w->f, buf);

	sexp_writer_close_tree(w);
}

void
sexp_writer_symbol(struct SexpWriter *w, const char *name, const char *value)
{
	if (value) {
		// TODO: Check if valid symbol!!! This goes for all key names!!!
		sexp_writer_open_tree(w, name);
		libias_buffer_puts(w->f, " ");
		libias_buffer_puts(w->f, value);
		sexp_writer_close_tree(w);
	}
}

// Close the tree and write the result F.
void
sexp_writer_finish(
	struct SexpWriter *w,
	struct LibiasBuffer *f)
{
	const char *p;
	while ((p = libias_stack_pop(w->parenthesis_stack))) {
		libias_buffer_puts(w->f, p);
	}
	w->level = 0;
	libias_buffer_puts(w->f, "\n");

	libias_buffer_puts(f, w->f);
}
