# This is the spec file for portfmt's command line arguments.
# scripts/flags.awk will generate the CLI interface from it.

# Template with common arguments
$common_arguments

-D --diff
help         = Generate a diff that shows what would be done
flags        = flags.show_diff
arg-descr    = context
arg-required = false
arg-read     = read_diff_context

-d
help  = Dump the AST to stdout
flags = flags.dump_ast

-i --in-place-edit
help  = Edit Makefile in place
flags = flags.edit_in_place

-h --help
help  = Show command help
flags = flags.show_help

------------------------------------------------------------------------

# Template with common formatting arguments
$format_arguments

$common_arguments

-t --format-target-commands
help  = Format target commands
flags = flags.format_target_commands

-u --format-variable-unsorted
help  = Keep variables unsorted
flags = flags.keep_variables_unsorted

-U --format-variable-always-sort
help  = Always sort variables
flags = flags.always_sort_variables

-w --format-variable-wrapcol
arg-read     = read_format_variable_wrapcol
arg-required = true
arg-descr    = wrapcol
help         = Sets the variable wrapping column. Set to -1 to ignore for all variables

------------------------------------------------------------------------

# Template for positional Makefile argument
$makefile

:? Makefile
help     = Makefile
arg-read = read_makefile_arg

------------------------------------------------------------------------

bump-epoch
help   = Bump PORTEPOCH
filter = do_bump_epoch

$format_arguments

$makefile

------------------------------------------------------------------------

bump-revision
help   = Bump PORTREVISION
filter = do_bump_revision

$format_arguments

$makefile

------------------------------------------------------------------------

ast
help  = Show parsed AST before any edits
query = do_ast

-h --help
help  = Show command help
flags = flags.show_help

$makefile

------------------------------------------------------------------------

cat
help   = Parse Makefile and rerender from AST (AST completeness test)
filter = do_cat

$common_arguments

$makefile

------------------------------------------------------------------------

format
help   = Format Makefile
filter = do_format
flags  = flags.fuzzy_variable_matching

$format_arguments

$makefile

------------------------------------------------------------------------

get
help  = Get raw variable words
query = do_get_variable

-h --help
help  = Show command help
flags = flags.show_help

: variable_regexp
help     = Regular expression to match variables against
arg-read = read_get_variable_regexp

$makefile

------------------------------------------------------------------------

help
help = Subcommand help
main = do_help

-h --help
help  = Show command help
flags = flags.show_help

:? topic
help     = Show help screen of subcommand
arg-read = read_help_topic

------------------------------------------------------------------------

lint clippy
help  = Lint Makefile (alias: clippy)
flags = lint.flags.check_variable_references
query = do_lint

-h --help
help  = Show command help
flags = flags.show_help

--strict-variable-references
flags = lint.flags.strict_variable_references
help =

$makefile

------------------------------------------------------------------------

merge
help   = Merge variables into the Makefile
flags  = flags.keep_stdin_open flags.fuzzy_variable_matching
filter = do_merge

$format_arguments

-e
arg-read     = read_merge_expression
arg-required = true
arg-descr    = expr
help         = Merge expression

$makefile

------------------------------------------------------------------------

sanitize-append
help   = Sanitize += in Makefiles before bsd.port.{options,pre}.mk
filter = do_sanitize_append

$common_arguments

$makefile

------------------------------------------------------------------------

scan
help = Scan the FreeBSD Ports Collection
main = do_scan

-h --help
help  = Show command help
flags = flags.show_help

-l
arg-read     = read_scan_logdir_path
arg-required = true
help         =

-q
arg-read     = read_scan_query
arg-required = true
help         =

-p
arg-read     = read_scan_portsdir
arg-required = true
help         =

--categories
flags = scan.flags.categories
help  = Check category Makefiles

--clones
flags = scan.flags.clones
help  =

--comments
flags = scan.flags.comments
help  =

--option-default-descriptions
flags        = scan.flags.option_default_descriptions
arg-read     = read_scan_option_default_descriptions_editdist
arg-required = false
arg-descr    = [editdist]
help         =

--options
flags = scan.flags.options
help  =

--progress
flags        = scan.flags.progress
arg-read     = read_scan_progress_interval
arg-required = false
help         =

--strict
flags = scan.flags.strict_variables
help =

--unknown-targets
flags = scan.flags.unknown_targets
help =

--unknown-variables
flags = scan.flags.unknown_variables
help  =

--variable-values
flags        = scan.flags.variable_values
arg-read     = read_scan_variable_values
arg-required = false
arg-descr    = [keyquery]
help         =

:: origin
help     = origin
arg-read = read_scan_origin_arg

------------------------------------------------------------------------

set-version
help   = Manage {PORT,DIST}VERSION and PORTREVISION with Git tag support
filter = do_set_version

$format_arguments

: version
help     = Version (git describe --tags compatible)
arg-read = read_set_version_arg

$makefile
