// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

struct SexpWriter *sexp_writer_new(bool);

libias_attr_nonnull(1)
void sexp_writer_cleanup(struct SexpWriter **);

void sexp_writer_open_tree(struct SexpWriter *, const char *);
void sexp_writer_close_tree(struct SexpWriter *);

void sexp_writer_bool(struct SexpWriter *, const char *, bool);
void sexp_writer_int64_array(struct SexpWriter *, const char *, int64_t[], size_t);
void sexp_writer_keyword(struct SexpWriter *, const char *, const char *);
void sexp_writer_keywords(struct SexpWriter *, const char *, struct LibiasArray *);
void sexp_writer_string(struct SexpWriter *, const char *, const char *);
void sexp_writer_symbol(struct SexpWriter *, const char *, const char *);
void sexp_writer_finish(struct SexpWriter *, struct LibiasBuffer *);

#define sexp_writer_int64(w, name, ...)	\
	sexp_writer_int64_array(w, name, (int64_t[]){__VA_ARGS__}, nitems(((int64_t[]){__VA_ARGS__})))
