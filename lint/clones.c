// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include <libias/array.h>
#include <libias/color.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/word.h"
#include "lint/clones.h"

struct LintClonesState {
	struct LibiasMempool *pool;
	struct LibiasSet *seen;
	struct LibiasSet *seen_in_cond;
	struct LibiasSet *clones;
	struct LibiasMempool *clones_pool;
};

// Prototypes
static void add_clones(struct LintClonesState *this);
static void lint_clones_visit_variable(struct ASTVisit *visit, struct AST *node);

void
add_clones(struct LintClonesState *this)
{
	libias_set_foreach(this->seen_in_cond, const char *, name) {
		bool valid_clone = libias_set_contains_p(this->seen, name)
			&& !libias_set_contains_p(this->clones, name);
		if (valid_clone) {
			char *name_copy = libias_str_dup(name);
			libias_mempool_take(this->clones_pool, name_copy);
			libias_set_add(this->clones, name_copy);
		}
	}
	libias_set_truncate(this->seen_in_cond);
}

void
lint_clones_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct LintClonesState *this = ast_visit_context(visit);

	bool in_conditional =
		ast_parent_if(node)
		|| ast_parent_for(node)
		|| ast_parent_include(node);

	if (ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_ASSIGN) {
		char *name = ast_word_flatten(ast_variable_name(node));
		libias_mempool_take(this->pool, name);
		if (in_conditional) {
			libias_set_add(this->seen_in_cond, name);
		} else if (libias_set_contains_p(this->seen, name)) {
			if (!libias_set_contains_p(this->clones, name)) {
				char *name_copy = libias_str_dup(name);
				libias_mempool_take(this->clones_pool, name_copy);
				libias_set_add(this->clones, name_copy);
			}
		} else {
			libias_set_add(this->seen, name);
		}
	}

	if (!in_conditional) {
		add_clones(this);
	}
}

struct LibiasIterator *
lint_clones_iterator(struct AST *root)
{
	auto pool = libias_mempool_new();
	struct LintClonesState this = {
		.pool = pool,
		.seen = libias_mempool_set(pool, libias_str_compare),
		.seen_in_cond = libias_mempool_set(pool, libias_str_compare),
		.clones_pool = pool,
		.clones = libias_mempool_set(pool, libias_str_compare),
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = lint_clones_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	add_clones(&this);

	auto iter = libias_set_iterator(this.clones, 0, -1);
	libias_mempool_take(libias_iterator_pool(iter), pool);
	return iter;
}
