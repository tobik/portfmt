// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/color.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/query.h"
#include "ast/word.h"
#include "diagnostic.h"
#include "lint/commented_portrevision.h"

struct LintCommentedPortrevisionState {
	struct LibiasMempool *comments_pool;
	struct LibiasSet *comments;
};

// Prototypes
static void lint_commented_portrevision_visit_comment(struct ASTVisit *visit, struct AST *node);

void
lint_commented_portrevision_visit_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct LintCommentedPortrevisionState *this = ast_visit_context(visit);

	ast_words_foreach(node, word) {
		char *value = ast_word_flatten(word);
		libias_scope_free(value);
		char *comment = libias_str_trim(value);
		libias_scope_free(comment);

		if (strlen(comment) <= 1) {
			continue;
		}

		libias_scope_buffer(buf);
		libias_buffer_puts(buf, comment + 1);
		libias_buffer_puts(buf, "\n");

		auto diagnostic = diagnostic_new();
		libias_scope_free(diagnostic);
		auto ast = ast_from_buffer(buf, diagnostic, "<lint-comment-portrevision>");
		libias_scope_free(ast);
		if (!ast) {
			continue;
		}

		if (ast_lookup_variable(ast, "PORTEPOCH", AST_LOOKUP_VARIABLE_FIRST) ||
			ast_lookup_variable(ast, "PORTREVISION", AST_LOOKUP_VARIABLE_FIRST)) {
			if (!libias_set_contains_p(this->comments, comment)) {
				libias_set_add(
					this->comments,
					libias_mempool_take(this->comments_pool, libias_str_dup(comment)));
			}
		}
	}
}

struct LibiasIterator *
lint_commented_portrevision_iterator(struct AST *root)
{
	struct LibiasMempool *comments_pool = libias_mempool_new();
	struct LintCommentedPortrevisionState this = {
		.comments_pool = comments_pool,
		.comments = libias_mempool_set(comments_pool, libias_str_compare),
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_comment = lint_commented_portrevision_visit_comment;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	auto iter = libias_set_iterator(this.comments, 0, -1);
	libias_mempool_take(libias_iterator_pool(iter), comments_pool);
	return iter;
}
