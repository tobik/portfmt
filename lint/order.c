// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/color.h>
#include <libias/diff.h>
#include <libias/iterator.h>
#include <libias/map.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>

#include "ast.h"
#include "ast/word.h"
#include "buildinfo.h"
#include "lint/unknown_variables.h"
#include "lint/order.h"
#include "rules.h"

enum OutputDiffResult {
	OUTPUT_DIFF_OK,
	OUTPUT_DIFF_NO_EDITS,
	OUTPUT_DIFF_ERROR,
};

struct GetVariablesState {
	struct AST *root;
	struct LibiasMempool *pool;
	struct Rules *rules;
	struct LibiasArray *vars;
};

struct GetTargetsState {
	struct AST *root;
	struct LibiasMempool *pool;
	struct Rules *rules;
	struct LibiasArray *targets;
};

struct Row {
	char *name;
	char *hint;
};

// Prototypes
static libias_declare_compare(compare_get_all_unknown_variables_row);
static libias_declare_compare(compare_row);
static enum OutputDiffResult check_target_order(struct LibiasBuffer *outbuf, struct Rules *rules, struct AST *root, bool no_color, enum OutputDiffResult status_var);
static enum OutputDiffResult check_variable_order(struct LibiasBuffer *outbuf, struct Rules *rules, struct AST *root, bool no_color);
static struct LibiasSet *get_all_unknown_variables(struct LibiasMempool *pool, struct AST *root, struct Rules *rules);
static char *get_hint(struct LibiasMempool *pool, struct Rules *rules, struct AST *root, const char *var, enum BlockType block, struct LibiasSet *uses_candidates);
static void get_targets(struct GetTargetsState *this);
static bool get_targets_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void get_targets_visit_target_rule_targets(struct ASTVisit *visit, struct AST *node);
static void get_variables(struct GetVariablesState *this);
static bool get_variables_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void get_variables_visit_variable(struct ASTVisit *visit, struct AST *node);
static enum OutputDiffResult output_diff(struct LibiasBuffer *outbuf, struct LibiasArray *origin, struct LibiasArray *target, bool no_color);
static void output_row(struct LibiasBuffer *outbuf, struct Row *row, size_t maxlen);
static void row(struct LibiasMempool *pool, struct LibiasArray *output, const char *name, const char *hint);
static void row_cleanup(struct Row **row_ptr);
static struct LibiasArray *variable_list(struct LibiasMempool *pool, struct Rules *rules, struct AST *root);

// Constants
static struct LibiasCompareTrait *row_compare = &(struct LibiasCompareTrait){
	.compare = compare_row,
	.context = NULL,
};
static struct LibiasCompareTrait *get_all_unknown_variables_compare = &(struct LibiasCompareTrait){
	.compare = compare_get_all_unknown_variables_row,
	.context = NULL,
};

void
row_cleanup(struct Row **row_ptr)
{
	struct Row *row = *row_ptr;
	if (row) {
		libias_cleanup(&row->name);
		libias_cleanup(&row->hint);
		libias_free(row);
		*row_ptr = NULL;
	}
}

void
row(
	struct LibiasMempool *pool,
	struct LibiasArray *output,
	const char *name,
	const char *hint)
{
	struct Row *row = libias_alloc(struct Row);
	row->name = libias_str_dup(name);
	if (hint) {
		row->hint = libias_str_dup(hint);
	}
	libias_mempool_add(pool, row, row_cleanup);
	libias_array_append(output, row);
}

libias_define_compare(compare_row, struct Row, void)
{
	return strcmp(a->name, b->name);
}

bool
get_variables_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	switch (ast_type(node)) {
	case AST_IF_BRANCH: {
		libias_scope_mempool(pool);
		libias_scope_array(test);
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			libias_array_append(test, libias_mempool_take(pool, ast_word_flatten(word)));
		}
		if (ast_if_branch_type(node) == AST_IF_NMAKE && libias_array_len(test) == 1) {
			if (strcmp(libias_array_get(test, 0), "portclippy") == 0) {
				return false;
			}
		} else if (ast_if_branch_type(node) == AST_IF_IF && libias_array_len(test) == 1) {
			const char *word = libias_array_get(test, 0);
			if (strcmp(word, "defined(DEVELOPER)") == 0 ||
				strcmp(word, "defined(MAINTAINER_MODE)") == 0) {
				return false;
			} else if (strcmp(word, "make(makesum)") == 0) {
				return false;
			}
		}
		break;
	} case AST_INCLUDE:
		if (ast_ports_framework_include_p(node)) {
			ast_visit_stop(visit);
			return false;
		} else {
			// XXX: Should we recurse down into includes?
			return false;
		}
		break;
	default:
		break;
	}

	return true;
}

void
get_variables_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct GetVariablesState *this = ast_visit_context(visit);

	char *name = ast_word_flatten(ast_variable_name(node));
	libias_mempool_take(this->pool, name);
	// Ignore port local variables that start with an _
	if (name[0] != '_' && libias_array_find(this->vars, name, libias_str_compare) == -1) {
		if (rules_referenced_var_p(this->rules, this->root, name)) {
			if (rules_variable_order_block(this->rules, node, name, NULL) != BLOCK_UNKNOWN) {
				libias_array_append(this->vars, name);
			}
		} else {
			libias_array_append(this->vars, name);
		}
	}
}

void
get_variables(struct GetVariablesState *this)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = get_variables_visit_children_p;
	visit_trait.visit_variable = get_variables_visit_variable;

	struct ASTVisit *visit = ast_visit_new(this->root, &visit_trait);
	ast_visit_set_context(visit, this);
	ast_visit_run(visit);
	libias_cleanup(&visit);
}

libias_define_compare(compare_get_all_unknown_variables_row, struct Row, void)
{
	int retval = strcmp(a->name, b->name);
	if (retval == 0) {
		if (a->hint && b->hint) {
			return strcmp(a->hint, b->hint);
		} else if (a->hint) {
			return -1;
		} else {
			return 1;
		}
	} else {
		return retval;
	}
}

struct LibiasSet *
get_all_unknown_variables(
	struct LibiasMempool *pool,
	struct AST *root,
	struct Rules *rules)
{
	auto unknowns = libias_mempool_set(pool, get_all_unknown_variables_compare);
	lint_unknown_variables_foreach(root, rules, var) {
		auto name = lint_unknown_variable_name(var);
		auto hint = lint_unknown_variable_hint(var);
		if (*name != '_') {
			struct Row rowkey = { .name = (char *)name, .hint = (char *)hint };
			if (name && hint && !libias_set_contains_p(unknowns, &rowkey)) {
				struct Row *row = libias_alloc(struct Row);
				row->name = libias_str_dup(name);
				row->hint = libias_str_dup(hint);
				libias_mempool_add(pool, row, row_cleanup);
				libias_set_add(unknowns, row);
			}
		}
	}
	return unknowns;
}

char *
get_hint(
	struct LibiasMempool *pool,
	struct Rules *rules,
	struct AST *root,
	const char *var,
	enum BlockType block,
	struct LibiasSet *uses_candidates)
{
	char *hint = NULL;
	if (uses_candidates) {
		struct LibiasArray *uses = libias_set_values(uses_candidates);
		char *buf = libias_str_join(uses, " ");
		if (libias_set_len(uses_candidates) > 1) {
			hint = libias_str_printf("missing one of USES=%s ?", buf);
		} else {
			hint = libias_str_printf("missing USES=%s ?", buf);
		}
		libias_cleanup(&uses);
		libias_free(buf);
	} else if (block == BLOCK_UNKNOWN) {
		char *uppervar = str_map(var, strlen(var), toupper);
		if (rules_variable_order_block(rules, root, uppervar, NULL) != BLOCK_UNKNOWN) {
			hint = libias_str_printf("did you mean %s ?", uppervar);
		}
		libias_free(uppervar);
	}

	if (hint) {
		libias_mempool_take(pool, hint);
	}
	return hint;
}

struct LibiasArray *
variable_list(
	struct LibiasMempool *pool,
	struct Rules *rules,
	struct AST *root)
{
	struct LibiasArray *output = libias_mempool_array(pool);
	libias_scope_array(vars);
	get_variables(&(struct GetVariablesState){
		.root = root,
		.pool = pool,
		.rules = rules,
		.vars = vars,
	});

	enum BlockType block = BLOCK_UNKNOWN;
	enum BlockType last_block = BLOCK_UNKNOWN;
	bool flag = false;
	libias_array_foreach(vars, char *, var) {
		struct LibiasSet *uses_candidates = NULL;
		block = rules_variable_order_block(rules, root, var, &uses_candidates);
		if (block != last_block) {
			if (flag && block != last_block) {
				row(pool, output, "", NULL);
			}
			char *name = libias_str_printf("# %s", BlockType_human(block));
			row(pool, output, name, NULL);
			libias_free(name);
		}
		flag = true;
		char *hint = get_hint(pool, rules, root, var, block, uses_candidates);
		row(pool, output, var, hint);
		last_block = block;
		libias_cleanup(&uses_candidates);
	}

	return output;
}

bool
get_targets_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{

	switch (ast_type(node)) {
	case AST_IF_BRANCH: {
		libias_scope_array(test);
		libias_scope_mempool(pool);
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			libias_array_append(test, libias_mempool_take(pool, ast_word_flatten(word)));
		}
		if (ast_if_branch_type(node) == AST_IF_NMAKE && libias_array_len(test) == 1) {
			if (strcmp(libias_array_get(test, 0), "portclippy") == 0) {
				return false;
			}
		} else if (ast_if_branch_type(node) == AST_IF_IF && libias_array_len(test) == 1) {
			const char *word = libias_array_get(test, 0);
			if (strcmp(word, "defined(DEVELOPER)") == 0 ||
				strcmp(word, "defined(MAINTAINER_MODE)") == 0) {
				return false;
			} else if (strcmp(word, "make(makesum)") == 0) {
				return false;
			}
		}
		break;
	} case AST_INCLUDE:
		if (ast_ports_framework_include_p(node)) {
			ast_visit_stop(visit);
			return false;
		} else {
			// XXX: Should we recurse down into includes?
			return false;
		}
		break;
	default:
		break;
	}

	return true;
}

void
get_targets_visit_target_rule_targets(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct GetTargetsState *this = ast_visit_context(visit);

	ast_words_foreach(node, source) {
		if (ast_word_meta_p(source)) {
			continue;
		}
		const char *target = libias_mempool_take(this->pool, ast_word_flatten(source));
		// Ignore port local targets that start with an _
		if (target[0] != '_' && !rules_special_target_p(this->rules, target) &&
			libias_array_find(this->targets, target, libias_str_compare) == -1) {
			libias_array_append(this->targets, target);
		}
	}
}

void
get_targets(struct GetTargetsState *this)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = get_targets_visit_children_p;
	visit_trait.visit_target_rule_targets = get_targets_visit_target_rule_targets;

	struct ASTVisit *visit = ast_visit_new(this->root, &visit_trait);
	ast_visit_set_context(visit, this);
	ast_visit_run(visit);
	libias_cleanup(&visit);
}

enum OutputDiffResult
check_variable_order(
	struct LibiasBuffer *outbuf,
	struct Rules *rules,
	struct AST *root,
	bool no_color)
{
	libias_scope_mempool(pool);
	struct LibiasArray *origin = variable_list(pool, rules, root);

	libias_scope_array(vars);
	get_variables(&(struct GetVariablesState){
		.root = root,
		.pool = pool,
		.rules = rules,
		.vars = vars,
	});
	libias_array_sort(
		vars,
		&(struct LibiasCompareTrait){
			rules_compare_order,
			&(struct RulesCompareOrder){ rules, root }});

	libias_scope_array(target);
	libias_scope_array(unknowns);
	enum BlockType block = BLOCK_UNKNOWN;
	enum BlockType last_block = BLOCK_UNKNOWN;
	bool flag = false;
	libias_array_foreach(vars, char *, var) {
		struct LibiasSet *uses_candidates = NULL;
		block = rules_variable_order_block(rules, root, var, &uses_candidates);
		libias_scope_free(uses_candidates);
		if (block != BLOCK_UNKNOWN) {
			if (block != last_block) {
				if (flag && block != last_block) {
					row(pool, target, "", NULL);
				}
				char *name = libias_str_printf("# %s", BlockType_human(block));
				row(pool, target, name, NULL);
				libias_free(name);
			}
			flag = true;
			row(pool, target, var, NULL);
			last_block = block;
		} else {
			libias_array_append(unknowns, var);
			last_block = BLOCK_UNKNOWN;
		}
	}

	libias_array_sort(unknowns, libias_str_compare);

	struct LibiasSet *all_unknown_variables = get_all_unknown_variables(pool, root, rules);
	libias_array_foreach(unknowns, char *, var) {
		struct Row key = { .name = var, .hint = NULL };
		libias_set_remove(all_unknown_variables, &key);
	}

	bool have_unknown_variables = libias_array_len(vars) > 0
		&& (libias_array_len(unknowns) > 0
			|| libias_set_len(all_unknown_variables) > 0);
	if (have_unknown_variables) {
		row(pool, target, "", NULL);
		char *name = libias_str_printf("# %s", BlockType_human(BLOCK_UNKNOWN));
		row(pool, target, name, NULL);
		libias_free(name);
		row(pool, target, "# WARNING:", NULL);
		row(pool, target, "# The following variables were not recognized.", NULL);
		row(pool, target, "# They could just be typos or Portclippy needs to be made aware of them.", NULL);
		row(pool, target, "# Please double check them.", NULL);
		row(pool, target, "#", NULL);
		row(pool, target, "# Prefix them with an _ or wrap in '.ifnmake portclippy' to tell", NULL);
		row(pool, target, "# Portclippy to ignore them.", NULL);
		row(pool, target, "#", NULL);
		row(pool, target, "# If in doubt please report this on portfmt's bug tracker:", NULL);
		name = libias_str_printf("# %s", portfmt_bug_tracker_url());
		row(pool, target, name, NULL);
		libias_free(name);
	}
	libias_array_foreach(unknowns, char *, var) {
		struct LibiasSet *uses_candidates = NULL;
		enum BlockType block = rules_variable_order_block(rules, root, var, &uses_candidates);
		char *hint = get_hint(pool, rules, root, var, block, uses_candidates);
		libias_cleanup(&uses_candidates);
		row(pool, target, var, hint);
	}

	enum OutputDiffResult retval = output_diff(outbuf, origin, target, no_color);

	if (libias_array_len(vars) > 0 && libias_set_len(all_unknown_variables) > 0) {
		struct LibiasMap *group = libias_mempool_map(pool, libias_str_compare);
		size_t maxlen = 0;
		libias_set_foreach(all_unknown_variables, struct Row *, var) {
			struct LibiasArray *hints = libias_map_get(group, var->name);
			maxlen = MAX(maxlen, strlen(var->name));
			if (!hints) {
				hints = libias_mempool_array(pool);
				libias_map_add(group, var->name, hints);
			}
			if (var->hint) {
				char *hint = libias_str_printf("in %s", var->hint);
				libias_mempool_take(pool, hint);
				libias_array_append(hints, hint);
			}
		}
		libias_buffer_puts(outbuf, "\n");
		if (!no_color) {
			libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_CYAN);
		}
		libias_buffer_puts(outbuf, "# Unknown variables in options helpers\n");
		if (!no_color) {
			libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_RESET);
		}
		libias_map_foreach(group, char *, name, struct LibiasArray *, hints) {
			struct LibiasSet *uses_candidates = NULL;
			rules_variable_order_block(rules, root, name, &uses_candidates);
			libias_scope_free(uses_candidates);
			if (uses_candidates) {
				struct LibiasArray *uses = libias_set_values(uses_candidates);
				char *buf = libias_str_join(uses, " ");
				char *hint = NULL;
				if (libias_set_len(uses_candidates) > 1) {
					hint = libias_str_printf("missing one of USES=%s ?", buf);
				} else {
					hint = libias_str_printf("missing USES=%s ?", buf);
				}
				libias_mempool_take(pool, hint);
				libias_array_append(hints, hint);
				libias_cleanup(&uses);
				libias_free(buf);
			}
			if (libias_array_len(hints) > 0) {
				struct Row row = { .name = name, .hint = libias_array_get(hints, 0) };
				output_row(outbuf, &row, maxlen + 1);
				libias_array_foreach(hints, char *, hint) {
					if (hint_index > 0) {
						struct Row row = { .name = (char *)"", .hint = hint };
						output_row(outbuf, &row, maxlen + 1);
					}
				}
			} else {
				libias_buffer_puts(outbuf, name);
				libias_buffer_puts(outbuf, "\n");
			}
		}
	}

	return retval;
}

enum OutputDiffResult
check_target_order(
	struct LibiasBuffer *outbuf,
	struct Rules *rules,
	struct AST *root,
	bool no_color,
	enum OutputDiffResult status_var)
{
	libias_scope_mempool(pool);

	libias_scope_array(targets);
	get_targets(&(struct GetTargetsState){
		.root = root,
		.pool = pool,
		.rules = rules,
		.targets = targets,
	});

	libias_scope_array(origin);
	if (status_var == OUTPUT_DIFF_OK) {
		row(pool, origin, "", NULL);
	}
	row(pool, origin, "# Out of order targets", NULL);
	libias_array_foreach(targets, char *, name) {
		if (rules_known_target_p(rules, root, name)) {
			char *name_with_colon = libias_str_printf("%s:", name);
			row(pool, origin, name_with_colon, NULL);
			libias_free(name_with_colon);
		}
	}

	libias_array_sort(
		targets,
		&(struct LibiasCompareTrait){
			rules_compare_target_order,
			&(struct RulesCompareOrder){ rules, root }});

	libias_scope_array(target);
	if (status_var == OUTPUT_DIFF_OK) {
		row(pool, target, "", NULL);
	}
	row(pool, target, "# Out of order targets", NULL);
	libias_array_foreach(targets, char *, name) {
		if (rules_known_target_p(rules, root, name)) {
			char *name_with_colon = libias_str_printf("%s:", name);
			row(pool, target, name_with_colon, NULL);
			libias_free(name_with_colon);
		}
	}

	libias_scope_array(unknowns);
	libias_array_foreach(targets, char *, name) {
		if (!rules_known_target_p(rules, root, name) && name[0] != '_') {
			char *name_with_colon = libias_str_printf("%s:", name);
			libias_mempool_take(pool, name_with_colon);
			libias_array_append(unknowns, name_with_colon);
		}
	}

	enum OutputDiffResult status_target = OUTPUT_DIFF_ERROR;
	if ((status_target = output_diff(outbuf, origin, target, no_color)) == OUTPUT_DIFF_ERROR) {
		return status_target;
	}

	if (libias_array_len(unknowns) > 0) {
		if (status_var == OUTPUT_DIFF_OK || status_target == OUTPUT_DIFF_OK) {
			libias_buffer_puts(outbuf, "\n");
		}
		status_target = OUTPUT_DIFF_OK;
		if (!no_color) {
			libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_CYAN);
		}
		libias_buffer_puts(outbuf, "# Unknown targets");
		if (!no_color) {
			libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_RESET);
		}
		libias_buffer_puts(outbuf, "\n");
		libias_array_foreach(unknowns, char *, name) {
			libias_buffer_puts(outbuf, name);
			libias_buffer_puts(outbuf, "\n");
		}
	}

	return status_target;
}

void
output_row(
	struct LibiasBuffer *outbuf,
	struct Row *row,
	size_t maxlen)
{
	libias_buffer_puts(outbuf, row->name);
	if (row->hint && maxlen > 0) {
		size_t len = maxlen - strlen(row->name);
		char *spaces = libias_str_repeat(" ", len + 4);
		libias_buffer_puts(outbuf, spaces);
		libias_free(spaces);
		libias_buffer_puts(outbuf, row->hint);
	}
	libias_buffer_puts(outbuf, "\n");
}

enum OutputDiffResult
output_diff(
	struct LibiasBuffer *outbuf,
	struct LibiasArray *origin,
	struct LibiasArray *target,
	bool no_color)
{
	libias_scope_mempool(pool);

	struct LibiasDiff *p = libias_array_diff(origin, target, row_compare);
	libias_mempool_take(pool, p);
	if (p == NULL) {
		return OUTPUT_DIFF_ERROR;
	}

	size_t edits = 0;
	for (size_t i = 0; i < p->sessz; i++) {
		switch (p->ses[i].type) {
		case LIBIAS_DIFF_ADD:
		case LIBIAS_DIFF_DELETE:
			edits++;
			break;
		default:
			break;
		}
	}
	if (edits == 0) {
		return OUTPUT_DIFF_NO_EDITS;
	}

	size_t maxlen = 0;
	libias_array_foreach(origin, struct Row *, row) {
		if (row->name[0] != '#') {
			maxlen = MAX(maxlen, strlen(row->name));
		}
	}

	for (size_t i = 0; i < p->sessz; i++) {
		struct Row *row = *(struct Row **)p->ses[i].e;
		if (strlen(row->name) == 0) {
			libias_buffer_puts(outbuf, "\n");
			continue;
		} else if (row->name[0] == '#') {
			if (p->ses[i].type != LIBIAS_DIFF_DELETE) {
				if (!no_color) {
					libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_CYAN);
				}
				output_row(outbuf, row, 0);
				if (!no_color) {
					libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_RESET);
				}
			}
			continue;
		}
		switch (p->ses[i].type) {
		case LIBIAS_DIFF_ADD:
			if (!no_color) {
				libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_GREEN);
			}
			libias_buffer_puts(outbuf, "+");
			output_row(outbuf, row, maxlen);
			break;
		case LIBIAS_DIFF_DELETE:
			if (!no_color) {
				libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_RED);
			}
			libias_buffer_puts(outbuf, "-");
			output_row(outbuf, row, 0);
			break;
		default:
			output_row(outbuf, row, maxlen + 1);
			break;
		}
		if (!no_color) {
			libias_buffer_puts(outbuf, LIBIAS_ANSI_COLOR_RESET);
		}
	}

	return OUTPUT_DIFF_OK;
}

struct LibiasBuffer *
lint_order(
	struct AST *root,
	struct Rules *rules,
	bool color)
{
	auto outbuf = libias_buffer_new();

	enum OutputDiffResult status_var;
	if ((status_var = check_variable_order(outbuf, rules, root, !color)) == OUTPUT_DIFF_ERROR) {
		libias_cleanup(&outbuf);
		return NULL;
	}

	enum OutputDiffResult status_target;
	if ((status_target = check_target_order(outbuf, rules, root, !color, status_var)) == OUTPUT_DIFF_ERROR) {
		libias_cleanup(&outbuf);
		return NULL;
	}

	if (status_var == OUTPUT_DIFF_OK || status_target == OUTPUT_DIFF_OK) {
		return outbuf;
	}

	libias_cleanup(&outbuf);
	return NULL;
}
