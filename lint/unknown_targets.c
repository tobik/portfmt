// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/hashset.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/hash.h>

#include "ast.h"
#include "ast/query.h"
#include "ast/word.h"
#include "lint/unknown_targets.h"
#include "rules.h"

struct WalkerData {
	struct AST *root;
	struct Rules *rules;
	struct LibiasMempool *pool;
	struct LibiasSet *unknown_targets;
	struct LibiasSet *targets;
	struct LibiasSet *deps;
};

// Prototypes
static void check_target(struct WalkerData *this, const char *name, bool deps);
static void output_unknown_targets_visit_target_rule(struct ASTVisit *visit, struct AST *node);

void
check_target(struct WalkerData *this, const char *name, bool deps)
{
	if (deps) {
		if (rules_special_source_p(this->rules, name)) {
			return;
		}
		if (rules_known_target_p(this->rules, this->root, name)) {
			return;
		}
		if (libias_set_contains_p(this->targets, name)) {
			return;
		}
		if (ast_lookup_post_plist_target_p(this->root, name)) {
			return;
		}
	} else {
		if (rules_special_target_p(this->rules, name)) {
			return;
		}
		if (rules_known_target_p(this->rules, this->root, name)) {
			return;
		}
		if (libias_set_contains_p(this->deps, name)) {
			return;
		}
		if (ast_lookup_post_plist_target_p(this->root, name)) {
			return;
		}
	}

	libias_set_add(this->unknown_targets, name);
}

void
output_unknown_targets_visit_target_rule(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct WalkerData *this = ast_visit_context(visit);
	bool skip_deps = false;
	struct AST *targets = ast_get_child(
		node,
		AST_CHILD_TARGET_RULE_TARGETS);
	ast_words_foreach(targets, name) {
		if (ast_word_meta_p(name)) {
			continue;
		}
		const char *source = libias_mempool_take(this->pool, ast_word_flatten(name));
		if (rules_special_target_p(this->rules, source)) {
			skip_deps = true;
		}
		libias_set_add(this->targets, source);
	}
	if (!skip_deps) {
		struct AST *dependencies = ast_get_child(
			node,
			AST_CHILD_TARGET_RULE_DEPENDENCIES);
		ast_words_foreach(dependencies, name) {
			if (ast_word_meta_p(name)) {
				continue;
			}
			libias_set_add(this->deps, libias_mempool_take(this->pool, ast_word_flatten(name)));
		}
	}
}

struct LibiasIterator *
lint_unknown_targets_iterator(
	struct AST *root,
	struct Rules *rules)
{
	auto unknown_targets = libias_set_new(libias_str_compare);
	auto pool = libias_mempool_new();
	libias_mempool_take(pool, unknown_targets);

	libias_scope_set(targets, libias_str_compare);
	libias_scope_set(deps, libias_str_compare);

	struct WalkerData this = {
		.root = root,
		.rules = rules,
		.pool = pool,
		.unknown_targets = unknown_targets,
		.targets = targets,
		.deps = deps,
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_target_rule = output_unknown_targets_visit_target_rule;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	libias_set_foreach(targets, const char *, name) {
		check_target(&this, name, false);
	}
	libias_set_foreach(deps, const char *, name) {
		check_target(&this, name, true);
	}

	auto iter = libias_set_iterator(unknown_targets, 0, -1);
	libias_mempool_take(libias_iterator_pool(iter), pool);
	return iter;
}
