// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/hashset.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>
#include <libias/trait/hash.h>

#include "ast.h"
#include "ast/query.h"
#include "ast/word.h"
#include "lint/unknown_variables.h"
#include "rules.h"

struct LintUnknownVariable {
	char *name;
	char *hint;
};

struct LintUnknownVariablesVisitContext {
	struct LibiasMempool *pool;
	struct AST *root;
	struct Rules *rules;
	struct LibiasSet *vars;
};

// Prototypes
static libias_declare_compare(compare_lint_unknown_variable);
static void check_opthelper(struct LintUnknownVariablesVisitContext *this, const char *option, bool optuse, bool optoff);
static struct LintUnknownVariable *lint_unknown_variable_new(struct LibiasMempool *pool, const char *name, const char *hint);
static void output_unknown_variables_visit_variable(struct ASTVisit *visit, struct AST *node);

// Constants
static struct LibiasCompareTrait *lint_unknown_variable_compare = &(struct LibiasCompareTrait){
	.compare = compare_lint_unknown_variable,
	.context = NULL,
};

struct LintUnknownVariable *
lint_unknown_variable_new(
	struct LibiasMempool *pool,
	const char *name,
	const char *hint)
{
	auto this = libias_alloc(struct LintUnknownVariable);
	this->name = libias_str_dup(name);
	if (hint) {
		this->hint = libias_str_dup(hint);
	}
	libias_mempool_take(pool, this);
	return this;
}

void
lint_unknown_variable_cleanup(struct LintUnknownVariable **this_ptr)
{
	auto this = *this_ptr;
	if (this) {
		libias_cleanup(&this->name);
		libias_cleanup(&this->hint);
		libias_free(this);
		*this_ptr = NULL;
	}
}

const char *
lint_unknown_variable_name(struct LintUnknownVariable *this)
{
	return this->name;
}

const char *
lint_unknown_variable_hint(struct LintUnknownVariable *this)
{
	return this->hint;
}

libias_define_compare(compare_lint_unknown_variable, struct LintUnknownVariable, void)
{
	int retval = strcmp(a->name, b->name);
	if (retval == 0) {
		if (a->hint && b->hint) {
			return strcmp(a->hint, b->hint);
		} else if (a->hint) {
			return -1;
		} else {
			return 1;
		}
	} else {
		return retval;
	}
}

void
check_opthelper(
	struct LintUnknownVariablesVisitContext *this,
	const char *option,
	bool optuse,
	bool optoff)
{
	const char *suffix;
	if (optoff) {
		suffix = "_OFF";
	} else {
		suffix = "";
	}
	char *var;
	if (optuse) {
		var = libias_str_printf("%s_USE%s", option, suffix);
	} else {
		var = libias_str_printf("%s_VARS%s", option, suffix);
	}
	libias_scope_free(var);
	auto optvars = ast_lookup_variables(
		this->root,
		var,
		AST_LOOKUP_VARIABLE_DEFAULT);
	if (!optvars) {
		return;
	}
	libias_scope_free(optvars);
	libias_array_foreach(optvars, struct AST *, node) {
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			char *token = ast_word_flatten(word);
			libias_scope_free(token);
			char *suffix = strchr(token, '+');
			if (!suffix) {
				suffix = strchr(token, '=');
				if (!suffix) {
					continue;
				}
			} else if (*(suffix + 1) != '=') {
				continue;
			}
			char *name = str_map(token, suffix - token, toupper);
			if (optuse) {
				char *orig_name = name;
				name = libias_str_printf("USE_%s", name);
				libias_free(orig_name);
			}
			struct LintUnknownVariable varskey = { .name = name, .hint = var };
			if (rules_variable_order_block(this->rules, this->root, name, NULL) == BLOCK_UNKNOWN &&
			    !rules_referenced_var_p(this->rules, this->root, name) &&
			    !libias_set_contains_p(this->vars, &varskey))
			{
				libias_set_add(this->vars, lint_unknown_variable_new(this->pool, name, var));
			}
			libias_free(name);
		}
	}
}

void
output_unknown_variables_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct LintUnknownVariablesVisitContext *this = ast_visit_context(visit);

	char *name = ast_word_flatten(ast_variable_name(node));
	libias_scope_free(name);
	struct LintUnknownVariable varskey = { .name = (char *)name, .hint = NULL };
	if (rules_variable_order_block(this->rules, node, name, NULL) == BLOCK_UNKNOWN
		&& !rules_referenced_var_p(this->rules, this->root, name)
		&& !libias_set_contains_p(this->vars, &varskey))
	{
		libias_set_add(this->vars, lint_unknown_variable_new(this->pool, name, NULL));
	}
}

struct LibiasIterator *
lint_unknown_variables_iterator(
	struct AST *root,
	struct Rules *rules)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = output_unknown_variables_visit_variable;

	struct LintUnknownVariablesVisitContext this = {
		.pool = libias_mempool_new(),
		.root = root,
		.rules = rules,
		.vars = libias_set_new(lint_unknown_variable_compare),
	};
	libias_mempool_take(this.pool, this.vars);

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	ast_lookup_options_foreach(root, option) {
		check_opthelper(&this, option, true, false);
		check_opthelper(&this, option, false, false);
		check_opthelper(&this, option, true, true);
		check_opthelper(&this, option, false, true);
	}

	auto it = libias_set_iterator(this.vars, 0, -1);
	auto pool = libias_iterator_pool(it);
	libias_mempool_take(pool, this.pool);
	return it;
}
