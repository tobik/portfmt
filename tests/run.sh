#!/bin/sh
set -eu
ROOT="${PWD}"
BUILDDIR="$(readlink -f "$1" 2>/dev/null || realpath "$1")"
PORTFMT="${BUILDDIR}/_bin/portfmt"
SPLIT_TEST="${ROOT}/tests/split_test.awk"
: ${AWK:=awk}
: ${SH:=sh}
: ${UPDATE_AST_TESTS:=0}

export PORTFMT
export ROOT
export __PORTFMT_BUG_TRACKER_URL='$PORTFMT_BUG_TRACKER_URL'

t="$2"
mkdir -p "$(dirname "${BUILDDIR}/$t")"

fail() {
	echo "$t:1:1: FAIL" >&2
	exit 1
}

split_test() {
	cd "${BUILDDIR}"
	if ! ${AWK} -v testname="$t" -f "${SPLIT_TEST}" "${ROOT}/$t"; then
		echo "$t:1:1: split_test failed" >&2
		exit 1
	fi
}

case "$t" in
tests/parser/ast*.t|tests/parser/token*.t)
	split_test
	args=""
	end="false"
	case "$(basename "$t")" in
	ast_err*) args="ast"; end="true" ;;
	ast*) args="ast" ;;
	token*) args="ast" ;;
	esac
	if ${PORTFMT} ${args} "$t.in" >"$t.actual" 2>&1 || ${end}; then
		if ! diff -L "$t.expected" -L "$t.actual" -u "$t.expected" "$t.actual"; then
			if [ "${UPDATE_AST_TESTS}" = 1 ]; then
				cat "$t.in" >"${ROOT}/$t"
				echo "<<<<<<<<<" >>"${ROOT}/$t"
				${PORTFMT} ${args} <"$t.in" >>"${ROOT}/$t"
			fi
			fail
		fi
	else
		if [ "${UPDATE_AST_TESTS}" = 1 ]; then
			cat "$t.in" >"${ROOT}/$t"
			echo "<<<<<<<<<" >>"${ROOT}/$t"
			${PORTFMT} ${args} <"$t.in" >>"${ROOT}/$t"
		fi
		fail
	fi
	;;
tests/parser/*.sh)
	if ! ${SH} -o pipefail -eu "$t"; then
		fail
	fi
	;;
tests/format/*.t)
	split_test
	if ${PORTFMT} format -t "$t.in" >"$t.actual"; then
		if ! diff -L "$t.expected" -L "$t.actual" -u "$t.expected" "$t.actual"; then
			fail
		fi
	else
		fail
	fi
	if ${PORTFMT} format -t "$t.expected" >"$t.actual2"; then
		if ! diff -L "$t.expected" -L "$t.actual" -u "$t.expected" "$t.actual2"; then
			fail
		fi
	else
		fail
	fi
	;;
tests/edit/*.t)
	split_test
	if ! actual="$t.actual" input="$t.in" expected="$t.expected" ${SH} -o pipefail -eu "$t.sh"; then
		fail
	fi
	;;
tests/clippy/*.t)
	split_test
	${PORTFMT} lint "$t.in" >"$t.actual" || true
	if ! diff -L "$t.expected" -L "$t.actual" -u "$t.expected" "$t.actual"; then
		fail
	fi
	;;
tests/reject/*.in)
	if ${PORTFMT} format "$t" 2>/dev/null; then
		fail
	fi
	;;
tests/portscan/*.sh)
	export logdir="${BUILDDIR}/_tests/portscan/$(basename "$t")"
	rm -rf "${logdir}"
	mkdir -p "${logdir}"
	if ! (cd "${ROOT}/tests/portscan"; ${SH} -o pipefail -eu "$(basename "$t")"); then
		fail
	fi
	;;
esac
