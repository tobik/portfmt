// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/mempool.h>
#include <libias/str.h>
#include <libias/test.h>

#include "ast.h"
#include "ast/word.h"
#include "diagnostic.h"

libias_tests() {
	auto diagnostic = diagnostic_new();
	libias_mempool_take(pool, diagnostic);

	struct AST *variable = ast_new_variable();
	libias_mempool_take(pool, variable);
	libias_test(ast_type(variable) == AST_VARIABLE);
	libias_test(ast_variable_p(variable));

	struct AST *variable_name = ast_get_child(variable, AST_CHILD_VARIABLE_NAME);
	libias_test(variable_name);
	libias_test(ast_type(variable_name) == AST_VARIABLE_NAME);
	libias_test(ast_variable_name_p(variable_name));

	struct ASTWordsEdit *words_edit = ast_words_edit_new(
		variable_name,
		diagnostic,
		AST_WORDS_EDIT_DEFAULT);
	ast_words_edit_append_word(
		words_edit,
		ast_word_new_string("TEST"));
	ast_words_edit_apply(words_edit);

	struct ASTWord *word = ast_variable_name(variable_name);
	libias_test_streq("TEST", libias_mempool_take(pool, ast_word_flatten(word)));

	words_edit = ast_words_edit_new(
		variable_name,
		diagnostic,
		AST_WORDS_EDIT_DEFAULT);
	ast_words_edit_add_word(
		words_edit,
		ast_word_new_string("FOOBAR"),
		word);
	ast_words_edit_remove_word(words_edit, word);
	ast_words_edit_apply(words_edit);

	word = ast_variable_name(variable_name);
	libias_test_streq("FOOBAR", libias_mempool_take(pool, ast_word_flatten(word)));

	{
		words_edit = ast_words_edit_new(
			variable,
			diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		ast_words_edit_remove_all_words(words_edit);
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_comment("comment"));
		ast_words_edit_apply(words_edit);

		libias_scope_buffer(buf);
		ast_render(variable, buf);
		char *result = libias_buffer_data(buf);
		libias_test_streq("FOOBAR=#comment\n", result);
	}

	{
		words_edit = ast_words_edit_new(
			variable,
			diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		ast_words_edit_remove_all_words(words_edit);
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_string("FOOBAR"));
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_comment("comment"));
		ast_words_edit_apply(words_edit);

		libias_scope_buffer(buf);
		ast_render(variable, buf);
		char *result = libias_buffer_data(buf);
		libias_test_streq("FOOBAR=FOOBAR#comment\n", result);
	}

	{
		words_edit = ast_words_edit_new(
			variable,
			diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		ast_words_edit_remove_all_words(words_edit);
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_comment("comment"));
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_string("FOOBAR"));
		ast_words_edit_apply(words_edit);

		libias_scope_buffer(buf);
		ast_render(variable, buf);
		char *result = libias_buffer_data(buf);
		libias_test_streq("FOOBAR=FOOBAR#comment\n", result);
	}

	{
		words_edit = ast_words_edit_new(
			variable,
			diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		ast_words_edit_remove_all_words(words_edit);
		struct ASTWord *comment = ast_word_new_comment("comment");
		ast_words_edit_append_word(
			words_edit,
			comment);
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_string("FOOBAR"));
		ast_words_edit_append_word(
			words_edit,
			ast_word_new_whitespace("  "));
		ast_words_edit_remove_word(words_edit, comment);
		ast_words_edit_apply(words_edit);

		libias_scope_buffer(buf);
		ast_render(variable, buf);
		char *result = libias_buffer_data(buf);
		libias_test_streq("FOOBAR=FOOBAR  \n", result);
	}
}
