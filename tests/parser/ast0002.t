.if 1
PORTSCOUT=	foo
.if 4
.endif
.   elifdef 2
PORTSCOUT=	bar
.else
YEP=	contraband
.endif

#foo
foo:: meh meh
	asldfjalsdfj
.ifdef FOO
bar:
	slkdjfalsdkf
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1")))
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (value "\t"))
       (word
        (value "foo")))
      (children
       (variable-name
        (words
         (word
          (value "PORTSCOUT"))))))
     (if
      (end-indent "")
      (children
       (branch
        (type #:if)
        (indent "")
        (test
         (word-whitespace
          (value " "))
         (word
          (value "4"))))))))
   (branch
    (type #:ifdef)
    (indent "   ")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "2")))
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (value "\t"))
       (word
        (value "bar")))
      (children
       (variable-name
        (words
         (word
          (value "PORTSCOUT"))))))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (value "\t"))
       (word
        (value "contraband")))
      (children
       (variable-name
        (words
         (word
          (value "YEP"))))))))))
 (comment
  (words
   (word-whitespace
    (value ""))
   (word
    (value "#foo"))))
 (target-rule
  (operator "::")
  (children
   (target-rule-targets
    (words
     (word
      (value "foo"))))
   (target-rule-dependencies
    (words
     (word-whitespace
      (value " "))
     (word
      (value "meh"))
     (word-whitespace
      (value " "))
     (word
      (value "meh"))))
   (target-rule-body
    (children
     (target-command
      (words
       (word
        (value "asldfjalsdfj"))))
     (if
      (end-indent "")
      (children
       (branch
        (type #:ifdef)
        (indent "")
        (test
         (word-whitespace
          (value " "))
         (word
          (value "FOO")))
        (children
         (target-rule
          (operator ":")
          (children
           (target-rule-targets
            (words
             (word
              (value "bar"))))
           (target-rule-dependencies)
           (target-rule-body
            (children
             (target-command
              (words
               (word
                (value "slkdjfalsdkf")))))))))))))))))
