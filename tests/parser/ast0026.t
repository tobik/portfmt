.for i in 1 2 
.endfor
.for i in 3 4  # bla      
.endfor  # bla
<<<<<<<<<
(ast
 (for
  (indent "")
  (children
   (for-bindings
    (bindings
     (word-whitespace
      (value " "))
     (word
      (value "i"))
     (word-whitespace
      (value " "))))
   (for-words
    (words
     (word-whitespace
      (value " "))
     (word
      (value "1"))
     (word-whitespace
      (value " "))
     (word
      (value "2"))
     (word-whitespace
      (value " "))))
   (for-body)
   (for-end
    (indent ""))))
 (for
  (indent "")
  (children
   (for-bindings
    (bindings
     (word-whitespace
      (value " "))
     (word
      (value "i"))
     (word-whitespace
      (value " "))))
   (for-words
    (words
     (word-whitespace
      (value " "))
     (word
      (value "3"))
     (word-whitespace
      (value " "))
     (word
      (value "4"))
     (word-whitespace
      (value "  "))
     (word-comment
      (value " bla      "))))
   (for-body)
   (for-end
    (indent "")
    (words
     (word-whitespace
      (value "  "))
     (word-comment
      (value " bla")))))))
