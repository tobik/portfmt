.if 1
.else
FOO=fo
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1"))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word
        (value "fo")))
      (children
       (variable-name
        (words
         (word
          (value "FOO")))))))))))
