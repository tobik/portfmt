# You can force skipping these test by defining IGNORE_PATH_CHECKS
.if !defined(IGNORE_PATH_CHECKS)
.if ! ${PREFIX:M/*}
.BEGIN:
	@${ECHO_MSG} "PREFIX must be defined as an absolute path so that when 'make'"
	@${ECHO_MSG} "is invoked in the work area PREFIX points to the right place."
	@${FALSE}
.endif
.endif

DATADIR?=		${PREFIX}/share/${PORTNAME}
DOCSDIR?=		${PREFIX}/share/doc/${PORTNAME}
ETCDIR?=		${PREFIX}/etc/${PORTNAME}
EXAMPLESDIR?=	${PREFIX}/share/examples/${PORTNAME}
WWWDIR?=		${PREFIX}/www/${PORTNAME}
<<<<<<<<<
(ast
 (comment
  (words
   (word
    (value "# You can force skipping these test by defining IGNORE_PATH_CHECKS"))))
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word-bool
      (value "!"))
     (word
      (value "defined(IGNORE_PATH_CHECKS)")))
    (children
     (if
      (end-indent "")
      (children
       (branch
        (type #:if)
        (indent "")
        (test
         (word-whitespace
          (value " "))
         (word-bool
          (value "!"))
         (word-whitespace
          (value " "))
         (word-expand
          (word
           (value "PREFIX"))
          (modifiers
           (modifier
            (type #:M)
            (arg
             (word
              (value "/*")))))))
        (children
         (target-rule
          (operator ":")
          (children
           (target-rule-targets
            (words
             (word
              (value ".BEGIN"))))
           (target-rule-dependencies)
           (target-rule-body
            (children
             (target-command
              (words
               (word-expand
                (word
                 (value "ECHO_MSG")))
               (word-whitespace
                (value " "))
               (word
                (value "\"PREFIX must be defined as an absolute path so that when 'make'\"")))
              (flags "@"))
             (target-command
              (words
               (word-expand
                (word
                 (value "ECHO_MSG")))
               (word-whitespace
                (value " "))
               (word
                (value "\"is invoked in the work area PREFIX points to the right place.\"")))
              (flags "@"))
             (target-command
              (words
               (word-expand
                (word
                 (value "FALSE"))))
              (flags "@"))))))))))))))
 (comment
  (words
   (word-whitespace
    (value ""))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (value "\t\t"))
   (word-append
    (word-expand
     (word
      (value "PREFIX")))
    (word
     (value "/share/"))
    (word-expand
     (word
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (value "DATADIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (value "\t\t"))
   (word-append
    (word-expand
     (word
      (value "PREFIX")))
    (word
     (value "/share/doc/"))
    (word-expand
     (word
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (value "DOCSDIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (value "\t\t"))
   (word-append
    (word-expand
     (word
      (value "PREFIX")))
    (word
     (value "/etc/"))
    (word-expand
     (word
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (value "ETCDIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (value "\t"))
   (word-append
    (word-expand
     (word
      (value "PREFIX")))
    (word
     (value "/share/examples/"))
    (word-expand
     (word
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (value "EXAMPLESDIR"))))))
 (variable
  (modifier #:optional)
  (words
   (word-whitespace
    (value "\t\t"))
   (word-append
    (word-expand
     (word
      (value "PREFIX")))
    (word
     (value "/www/"))
    (word-expand
     (word
      (value "PORTNAME")))))
  (children
   (variable-name
    (words
     (word
      (value "WWWDIR")))))))
