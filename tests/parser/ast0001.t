PORTNAME=llvm90
.   for option a in CLANG COMPILER_RT EXTRAS LLD LLDB OPENMP
WRKSRC_${option:tl}_$a=	${WRKDIR}/${${option}_DISTFILES}
. endfor
FOO=meh

. include <bsd.port.mk>
<<<<<<<<<
(ast
 (variable
  (modifier #:assign)
  (words
   (word
    (value "llvm90")))
  (children
   (variable-name
    (words
     (word
      (value "PORTNAME"))))))
 (for
  (indent "   ")
  (children
   (for-bindings
    (bindings
     (word-whitespace
      (value " "))
     (word
      (value "option"))
     (word-whitespace
      (value " "))
     (word
      (value "a"))
     (word-whitespace
      (value " "))))
   (for-words
    (words
     (word-whitespace
      (value " "))
     (word
      (value "CLANG"))
     (word-whitespace
      (value " "))
     (word
      (value "COMPILER_RT"))
     (word-whitespace
      (value " "))
     (word
      (value "EXTRAS"))
     (word-whitespace
      (value " "))
     (word
      (value "LLD"))
     (word-whitespace
      (value " "))
     (word
      (value "LLDB"))
     (word-whitespace
      (value " "))
     (word
      (value "OPENMP"))))
   (for-body
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (value "\t"))
       (word-append
        (word-expand
         (word
          (value "WRKDIR")))
        (word
         (value "/"))
        (word-expand
         (word-expand
          (word
           (value "option")))
         (word
          (value "_DISTFILES")))))
      (children
       (variable-name
        (words
         (word-append
          (word
           (value "WRKSRC_"))
          (word-expand
           (word
            (value "option"))
           (modifiers
            (modifier
             (type #:tl))))
          (word
           (value "_"))
          (word-expand
           (word
            (value "a"))))))))))
   (for-end
    (indent " "))))
 (variable
  (modifier #:assign)
  (words
   (word
    (value "meh")))
  (children
   (variable-name
    (words
     (word
      (value "FOO"))))))
 (comment
  (words
   (word-whitespace
    (value ""))))
 (include
  (type #:bmake)
  (indent " ")
  (sys? #t)
  (loaded? #f)
  (words
   (word-whitespace
    (value " "))
   (word
    (value "<bsd.port.mk>")))))
