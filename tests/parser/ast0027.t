.if 1 
.elif 2  
.else   
.endif		
.if 3 # bla1
.elif 2 # bla2
.else # bla3
.endif		# bla4
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (words
   (word-whitespace
    (value "\t\t")))
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1"))
     (word-whitespace
      (value " "))))
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "2"))
     (word-whitespace
      (value "  "))))
   (branch
    (type #:else)
    (indent "")
    (test
     (word-whitespace
      (value "   "))))))
 (if
  (end-indent "")
  (words
   (word-whitespace
    (value "\t\t"))
   (word-comment
    (value " bla4")))
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "3"))
     (word-whitespace
      (value " "))
     (word-comment
      (value " bla1"))))
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "2"))
     (word-whitespace
      (value " "))
     (word-comment
      (value " bla2"))))
   (branch
    (type #:else)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word-comment
      (value " bla3")))))))
