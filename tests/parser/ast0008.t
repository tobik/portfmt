.if 1
.if 2
foo:
.if 3
	adflasdf
.endif
.endif
bar:
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1")))
    (children
     (if
      (end-indent "")
      (children
       (branch
        (type #:if)
        (indent "")
        (test
         (word-whitespace
          (value " "))
         (word
          (value "2")))
        (children
         (target-rule
          (operator ":")
          (children
           (target-rule-targets
            (words
             (word
              (value "foo"))))
           (target-rule-dependencies)
           (target-rule-body
            (children
             (if
              (end-indent "")
              (children
               (branch
                (type #:if)
                (indent "")
                (test
                 (word-whitespace
                  (value " "))
                 (word
                  (value "3")))
                (children
                 (target-command
                  (words
                   (word
                    (value "adflasdf"))))))))))))))))
     (target-rule
      (operator ":")
      (children
       (target-rule-targets
        (words
         (word
          (value "bar"))))
       (target-rule-dependencies)
       (target-rule-body))))))))
