.if 1
.for a in 1 2
FOO=bar
.endfor
.for a in 1 2
FOO=bar
.endfor
.for a in 1 2
FOO=bar
.endfor
.else
MUH=muh
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1")))
    (children
     (for
      (indent "")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (value " "))
         (word
          (value "a"))
         (word-whitespace
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (value " "))
         (word
          (value "1"))
         (word-whitespace
          (value " "))
         (word
          (value "2"))))
       (for-body
        (children
         (variable
          (modifier #:assign)
          (words
           (word
            (value "bar")))
          (children
           (variable-name
            (words
             (word
              (value "FOO"))))))))
       (for-end
        (indent ""))))
     (for
      (indent "")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (value " "))
         (word
          (value "a"))
         (word-whitespace
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (value " "))
         (word
          (value "1"))
         (word-whitespace
          (value " "))
         (word
          (value "2"))))
       (for-body
        (children
         (variable
          (modifier #:assign)
          (words
           (word
            (value "bar")))
          (children
           (variable-name
            (words
             (word
              (value "FOO"))))))))
       (for-end
        (indent ""))))
     (for
      (indent "")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (value " "))
         (word
          (value "a"))
         (word-whitespace
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (value " "))
         (word
          (value "1"))
         (word-whitespace
          (value " "))
         (word
          (value "2"))))
       (for-body
        (children
         (variable
          (modifier #:assign)
          (words
           (word
            (value "bar")))
          (children
           (variable-name
            (words
             (word
              (value "FOO"))))))))
       (for-end
        (indent ""))))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word
        (value "muh")))
      (children
       (variable-name
        (words
         (word
          (value "MUH")))))))))))
