quiet-flags:
	@a
	-b
	+c
	@-d
	@+i
	-@e
	+@f
	-+
	@@
	++i
	--j
	@+-
	@-+g
	-@+
	-+@i
	+-@
	+@-k
<<<<<<<<<
(ast
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word
      (value "quiet-flags"))))
   (target-rule-dependencies)
   (target-rule-body
    (children
     (target-command
      (words
       (word
        (value "a")))
      (flags "@"))
     (target-command
      (words
       (word
        (value "b")))
      (flags "-"))
     (target-command
      (words
       (word
        (value "c")))
      (flags "+"))
     (target-command
      (words
       (word
        (value "d")))
      (flags "@-"))
     (target-command
      (words
       (word
        (value "i")))
      (flags "@+"))
     (target-command
      (words
       (word
        (value "e")))
      (flags "-@"))
     (target-command
      (words
       (word
        (value "f")))
      (flags "+@"))
     (target-command
      (flags "-+"))
     (target-command
      (flags "@@"))
     (target-command
      (words
       (word
        (value "i")))
      (flags "++"))
     (target-command
      (words
       (word
        (value "j")))
      (flags "--"))
     (target-command
      (flags "@+-"))
     (target-command
      (words
       (word
        (value "g")))
      (flags "@-+"))
     (target-command
      (flags "-@+"))
     (target-command
      (words
       (word
        (value "i")))
      (flags "-+@"))
     (target-command
      (flags "+-@"))
     (target-command
      (words
       (word
        (value "k")))
      (flags "+@-")))))))
