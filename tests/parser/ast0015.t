do-install-${D:S/./_/g}:
<<<<<<<<<
(ast
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word-append
      (word
       (value "do-install-"))
      (word-expand
       (word
        (value "D"))
       (modifiers
        (modifier
         (type #:S)
         (delimiter "/")
         (pattern
          (word
           (value ".")))
         (replacement
          (word
           (value "_")))
         (chained? #f)
         (flags "g")))))))
   (target-rule-dependencies)
   (target-rule-body))))
