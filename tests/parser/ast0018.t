post-patch-CLANG$v-on:
<<<<<<<<<
(ast
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word-append
      (word
       (value "post-patch-CLANG"))
      (word-expand
       (word
        (value "v")))
      (word
       (value "-on")))))
   (target-rule-dependencies)
   (target-rule-body))))
