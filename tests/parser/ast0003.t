.if 1
fo=
.elifndef 2
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1")))
    (children
     (variable
      (modifier #:assign)
      (children
       (variable-name
        (words
         (word
          (value "fo"))))))))
   (branch
    (type #:ifndef)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "2")))))))
