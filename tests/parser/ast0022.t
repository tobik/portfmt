A=${${a} b:? :}
.MAKEFLAGS:  WITH=""
RUN_DEPENDS= foo:devel/foo
HELLO:=foo
<<<<<<<<<
(ast
 (variable
  (modifier #:assign)
  (words
   (word-expand
    (word-expand
     (word
      (value "a")))
    (word
     (value " b"))
    (modifiers
     (modifier
      (type #:?)
      (then
       (word
        (value " ")))
      (orelse
       (word-append))))))
  (children
   (variable-name
    (words
     (word
      (value "A"))))))
 (target-rule
  (operator ":")
  (children
   (target-rule-targets
    (words
     (word
      (value ".MAKEFLAGS"))))
   (target-rule-dependencies
    (words
     (word-whitespace
      (value "  "))
     (word
      (value "WITH=\"\""))))
   (target-rule-body
    (children
     (variable
      (modifier #:assign)
      (words
       (word-whitespace
        (value " "))
       (word
        (value "foo:devel/foo")))
      (children
       (variable-name
        (words
         (word
          (value "RUN_DEPENDS"))))))
     (variable
      (modifier #:expand)
      (words
       (word
        (value "foo")))
      (children
       (variable-name
        (words
         (word
          (value "HELLO")))))))))))
