.if 0
.    for deptype in ${_OPTIONS_DEPENDS}
.      if defined(${opt}_${deptype}_DEPENDS)
${deptype}_DEPENDS+=	${${opt}_${deptype}_DEPENDS}
.      endif
.endfor
.else
FOO=asd
.endif
<<<<<<<<<
(ast
 (if
  (end-indent "")
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "0")))
    (children
     (for
      (indent "    ")
      (children
       (for-bindings
        (bindings
         (word-whitespace
          (value " "))
         (word
          (value "deptype"))
         (word-whitespace
          (value " "))))
       (for-words
        (words
         (word-whitespace
          (value " "))
         (word-expand
          (word
           (value "_OPTIONS_DEPENDS")))))
       (for-body
        (children
         (if
          (end-indent "      ")
          (children
           (branch
            (type #:if)
            (indent "      ")
            (test
             (word-whitespace
              (value " "))
             (word-append
              (word
               (value "defined("))
              (word-expand
               (word
                (value "opt")))
              (word
               (value "_"))
              (word-expand
               (word
                (value "deptype")))
              (word
               (value "_DEPENDS)"))))
            (children
             (variable
              (modifier #:append)
              (words
               (word-whitespace
                (value "\t"))
               (word-expand
                (word-expand
                 (word
                  (value "opt")))
                (word
                 (value "_"))
                (word-expand
                 (word
                  (value "deptype")))
                (word
                 (value "_DEPENDS"))))
              (children
               (variable-name
                (words
                 (word-append
                  (word-expand
                   (word
                    (value "deptype")))
                  (word
                   (value "_DEPENDS")))))))))))))
       (for-end
        (indent ""))))))
   (branch
    (type #:else)
    (indent "")
    (children
     (variable
      (modifier #:assign)
      (words
       (word
        (value "asd")))
      (children
       (variable-name
        (words
         (word
          (value "FOO")))))))))))
