.if 1 # a
.elif 2 # b
. elif 3
.else # c
.  endif # d
<<<<<<<<<
(ast
 (if
  (end-indent "  ")
  (words
   (word-whitespace
    (value " "))
   (word-comment
    (value " d")))
  (children
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "1"))
     (word-whitespace
      (value " "))
     (word-comment
      (value " a"))))
   (branch
    (type #:if)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "2"))
     (word-whitespace
      (value " "))
     (word-comment
      (value " b"))))
   (branch
    (type #:if)
    (indent " ")
    (test
     (word-whitespace
      (value " "))
     (word
      (value "3"))))
   (branch
    (type #:else)
    (indent "")
    (test
     (word-whitespace
      (value " "))
     (word-comment
      (value " c")))))))
