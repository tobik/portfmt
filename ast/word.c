// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/iterator.h>
#include <libias/list.h>
#include <libias/mempool.h>
#include <libias/stack.h>
#include <libias/str.h>
#include <libias/trait/compare.h>
#include <libias/trait/hash.h>
#include <libias/util.h>

#include "ast/word.h"
#include "ast/word/expand/modifier.h"
#include "sexp.h"

struct ASTWord {
	const enum ASTWordType type;

	struct LibiasMempool *pool;

	bool edited;
	bool expand_single;
	bool expand_parens;

	union {
		enum ASTWordBoolType if_type;
		char *value;
		struct LibiasList *words; // [struct ASTWord *]
	};

	struct LibiasList *modifiers; // [struct ASTWordExpandModifier *]

	// Cache for ast_word_flatten(). It's called often, so it's worth it
	// to cache the return value. This must be reset to NULL if the word
	// is edited. Use ast_word_mark_edited() for that.
	char *_flat_cache;

	struct ASTWord *(*clone)(const struct ASTWord *);
	void (*free)(struct ASTWord *);
	void (*render)(struct ASTWord *, struct LibiasBuffer *);
	void (*sexp)(struct ASTWord *, struct SexpWriter *);
};

struct ASTWordChildrenIterator {
	struct LibiasListEntry *entry;
	size_t i;
	size_t len;
};

#define MAKE_AST_WORD(type, fun) \
	struct ASTWord *this = libias_alloc(struct ASTWord); \
	this->clone = ast_word_clone_##fun; \
	this->free = ast_word_free_##fun; \
	this->render = ast_word_render_##fun; \
	this->sexp = ast_word_sexp_##fun; \
	this->pool = libias_mempool_new(); \
	SET_AST_WORD_TYPE(this, type);

#define SET_AST_WORD_TYPE(word, ast_word_type) \
	do { \
		enum ASTWordType type = (ast_word_type); \
		memcpy((void *)&(word)->type, &type, sizeof(enum ASTWordType)); \
	} while (false);

// Prototypes
static libias_declare_compare(compare_ast_word);
static void ast_word_children_iterator_cleanup(void **this_ptr);
static bool ast_word_children_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);
static struct ASTWord *ast_word_clone_append(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_bool(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_comment(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_expand(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_string(const struct ASTWord *word);
static struct ASTWord *ast_word_clone_whitespace(const struct ASTWord *word);
static void ast_word_free_append(struct ASTWord *word);
static void ast_word_free_bool(struct ASTWord *word);
static void ast_word_free_comment(struct ASTWord *word);
static void ast_word_free_expand(struct ASTWord *word);
static void ast_word_free_string(struct ASTWord *word);
static void ast_word_free_whitespace(struct ASTWord *word);
static size_t ast_word_hash_datalen(const void *data_ptr, void *context);
static uint64_t ast_word_hash_impl(const void *data_ptr, const size_t datalen, void *context);
static void ast_word_mark_edited(struct ASTWord *word);
static void ast_word_render_append(struct ASTWord *word, struct LibiasBuffer *f);
static void ast_word_render_bool(struct ASTWord *word, struct LibiasBuffer *f);
static void ast_word_render_comment(struct ASTWord *word, struct LibiasBuffer *f);
static void ast_word_render_expand(struct ASTWord *word, struct LibiasBuffer *f);
static void ast_word_render_string(struct ASTWord *word, struct LibiasBuffer *f);
static void ast_word_render_whitespace(struct ASTWord *word, struct LibiasBuffer *f);
static void ast_word_sexp_append(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_bool(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_comment(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_expand(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_string(struct ASTWord *word, struct SexpWriter *w);
static void ast_word_sexp_whitespace(struct ASTWord *word, struct SexpWriter *w);

// Constants
struct LibiasCompareTrait *ast_word_compare = &(struct LibiasCompareTrait){
	.compare = compare_ast_word,
	.context = NULL,
};
struct LibiasHashTrait *ast_word_hash = &(struct LibiasHashTrait){
	.hash = ast_word_hash_impl,
	.datalen = ast_word_hash_datalen,
	.context = NULL,
};

struct ASTWord *
ast_word_new_append(void)
{
	MAKE_AST_WORD(AST_WORD_APPEND, append);
	this->words = libias_list_new();
	return this;
}

void
ast_word_free_append(struct ASTWord *word)
{
	libias_list_foreach(word->words, struct ASTWord *, word) {
		libias_cleanup(&word);
	}
	libias_cleanup(&word->words);
}

struct ASTWord *
ast_word_new_expand(void)
{
	MAKE_AST_WORD(AST_WORD_EXPAND, expand);
	this->words = libias_list_new();
	this->modifiers = libias_list_new();
	return this;
}

struct ASTWord *
ast_word_new_expand_parentheses(void)
{
	struct ASTWord *this = ast_word_new_expand();
	this->expand_parens = true;
	return this;
}

struct ASTWord *
ast_word_new_expand_single(void)
{
	struct ASTWord *this = ast_word_new_expand();
	this->expand_single = true;
	return this;
}

void
ast_word_free_expand(struct ASTWord *word)
{
	libias_list_foreach(word->words, struct ASTWord *, word) {
		libias_cleanup(&word);
	}
	libias_cleanup(&word->words);
	libias_list_foreach(word->modifiers, struct ASTWordExpandModifier *, modifier) {
		libias_cleanup(&modifier);
	}
	libias_cleanup(&word->modifiers);
}

struct ASTWord *
ast_word_new_bool(const enum ASTWordBoolType type)
{
	MAKE_AST_WORD(AST_WORD_BOOL, bool);
	this->if_type = type;
	return this;
}

void
ast_word_free_bool(struct ASTWord *word)
{
}

struct ASTWord *
ast_word_new_string(const char *value)
{
	MAKE_AST_WORD(AST_WORD_STRING, string);
	this->value = libias_str_dup(value);
	return this;
}

void
ast_word_free_string(struct ASTWord *word)
{
	libias_free(word->value);
}

struct ASTWord *
ast_word_new_whitespace(const char *value)
{
	// TODO: Validate that value only contains whitespace
	MAKE_AST_WORD(AST_WORD_WHITESPACE, whitespace);
	this->value = libias_str_dup(value);
	return this;
}

void
ast_word_free_whitespace(struct ASTWord *word)
{
	libias_free(word->value);
}

struct ASTWord *
ast_word_new_comment(const char *value)
{
	MAKE_AST_WORD(AST_WORD_COMMENT, comment);
	this->value = libias_str_dup(value);
	return this;
}

void
ast_word_free_comment(struct ASTWord *word)
{
	libias_free(word->value);
}

void
ast_word_cleanup(struct ASTWord **word_ptr)
{
	struct ASTWord *word = *word_ptr;
	if (word) {
		word->free(word);
		libias_free(word->_flat_cache);
		libias_cleanup(&word->pool);
		libias_free(word);
		*word_ptr = NULL;
	}
}

struct ASTWord *
ast_word_clone_append(const struct ASTWord *word)
{
	struct ASTWord *dup = ast_word_new_append();

	libias_list_foreach(word->words, struct ASTWord *, w) {
		ast_word_add_child(dup, ast_word_clone(w));
	}

	return dup;
}

struct ASTWord *
ast_word_clone_expand(const struct ASTWord *word)
{
	struct ASTWord *dup = ast_word_new_expand();

	libias_list_foreach(word->words, struct ASTWord *, w) {
		ast_word_add_child(dup, ast_word_clone(w));
	}

	libias_list_foreach(word->modifiers, struct ASTWordExpandModifier *, modifier) {
		libias_list_append(
			dup->modifiers,
			ast_word_expand_modifier_clone(modifier));
	}

	dup->expand_single = word->expand_single;
	dup->expand_parens = word->expand_parens;

	return dup;
}

struct ASTWord *
ast_word_clone_bool(const struct ASTWord *word)
{
	return ast_word_new_bool(word->if_type);
}

struct ASTWord *
ast_word_clone_string(const struct ASTWord *word)
{
	return ast_word_new_string(word->value);
}

struct ASTWord *
ast_word_clone_whitespace(const struct ASTWord *word)
{
	return ast_word_new_whitespace(word->value);
}

struct ASTWord *
ast_word_clone_comment(const struct ASTWord *word)
{
	return ast_word_new_comment(word->value);
}

struct ASTWord *
ast_word_clone(const struct ASTWord *word)
{
	return word->clone(word);
}

void
ast_word_mark_edited(struct ASTWord *word)
{
	if (word->_flat_cache) {
		libias_free(word->_flat_cache);
		word->_flat_cache = NULL;
	}
}

bool
ast_word_bool_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_BOOL;
}

bool
ast_word_string_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_STRING;
}

bool
ast_word_whitespace_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_WHITESPACE;
}

bool
ast_word_comment_p(const struct ASTWord *word)
{
	return word->type == AST_WORD_COMMENT;
}

// Return true if WORD is a meta word, i.e., whitespace or a comment.
bool
ast_word_meta_p(const struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_WHITESPACE:
	case AST_WORD_COMMENT:
		return true;
	default:
		return false;
	}
}

enum ASTWordType
ast_word_type(const struct ASTWord *word)
{
	return word->type;
}

void
ast_word_add_child(
	struct ASTWord *parent,
	struct ASTWord *child)
{
	libias_panic_unless(
		parent->type == AST_WORD_APPEND || parent->type == AST_WORD_EXPAND,
		"expected AST_WORD_APPEND or AST_WORD_EXPAND but got %s",
		ASTWordType_tostring(parent->type));
	libias_panic_if(
		parent == child,
		"cannot add parent as child");

	libias_list_append(parent->words, child);
	ast_word_mark_edited(parent);
}

struct LibiasArray *
ast_word_children(
	struct ASTWord *word,
	struct LibiasMempool *pool)
{
	struct LibiasArray *children = libias_mempool_array(pool);

	switch (word->type) {
	case AST_WORD_EXPAND:
	case AST_WORD_APPEND:
		libias_list_foreach(word->words, struct ASTWord *, word) {
			libias_array_append(children, word);
		}
		break;
	default:
		break;
	}

	return children;
}

// Returns true if WORD is empty, i.e., if it has no children, no value,
// or no modifiers.
bool
ast_word_empty_p(struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_EXPAND:
		return libias_list_len(word->words) == 0
			&& libias_list_len(word->modifiers) == 0;
	case AST_WORD_APPEND:
		return libias_list_len(word->words) == 0;
	case AST_WORD_BOOL:
		return false;
	default:
		return !word->value || *word->value == 0;
	}
}

struct LibiasArray *
ast_word_expand_modifiers(
	struct ASTWord *word,
	struct LibiasMempool *pool)
{
	struct LibiasArray *modifiers = libias_mempool_array(pool);

	if (word->type == AST_WORD_EXPAND) {
		libias_list_foreach(word->modifiers, struct ASTWordExpandModifier *, modifier) {
			libias_array_append(modifiers, modifier);
		}
	}

	return modifiers;
}

// Add MODIFIER to WORD. This function will panic if WORD is not an
// AST_WORD_EXPAND. WORD takes ownership of MODIFIER.
void
ast_word_add_expand_modifier(
	struct ASTWord *word,
	struct ASTWordExpandModifier *modifier)
{
	libias_panic_unless(
		word->type == AST_WORD_EXPAND,
		"expected AST_WORD_EXPAND but got %s",
		ASTWordType_tostring(word->type));

	libias_list_append(word->modifiers, modifier);
	ast_word_mark_edited(word);
}

// Returns true if WORD contains an AST_WORD_EXPAND, i.e., if its value
// can change depending on its environment when evaluated multiple
// times.
bool
ast_word_expand_p(struct ASTWord *word)
{
	switch (word->type) {
	case AST_WORD_EXPAND:
		return true;
	case AST_WORD_APPEND:
		libias_list_foreach(word->words, struct ASTWord *, child) {
			if (ast_word_expand_p(child)) {
				return true;
			}
		}
		return false;
	case AST_WORD_BOOL:
	case AST_WORD_WHITESPACE:
	case AST_WORD_STRING:
	case AST_WORD_COMMENT:
		return false;
	}

	return false;
}

// Updates the value of an AST_WORD_COMMENT, AST_WORD_STRING, or
// AST_WORD_WHITESPACE.  This functions will panic if WORD is not of
// these types.
void
ast_word_set_value(struct ASTWord *word, const char *new_value)
{
	libias_panic_unless(
		word->type == AST_WORD_WHITESPACE
		|| word->type == AST_WORD_STRING
		|| word->type == AST_WORD_COMMENT,
		"expected AST_WORD_COMMENT, AST_WORD_STRING, or AST_WORD_WHITESPACE but got %s",
		ASTWordType_tostring(word->type));

	ast_word_mark_edited(word);
	libias_free(word->value);
	word->value = libias_str_dup(new_value);
}

// Returns the read-only value of an AST_WORD_COMMENT, AST_WORD_STRING,
// or AST_WORD_WHITESPACE.  This functions will panic if WORD is not of
// these types.
//
// ast_word_flatten() will return a string representation of a word of
// any type.
//
// Use ast_word_set_value() to update it.
const char *
ast_word_value(const struct ASTWord *word)
{
	libias_panic_unless(
		word->type == AST_WORD_WHITESPACE
		|| word->type == AST_WORD_STRING
		|| word->type == AST_WORD_COMMENT,
		"expected AST_WORD_COMMENT, AST_WORD_STRING, or AST_WORD_WHITESPACE but got %s",
		ASTWordType_tostring(word->type));

	return word->value;
}

void
ast_word_set_bool_type(
	struct ASTWord *word,
	const enum ASTWordBoolType if_type)
{
	libias_panic_unless(
		word->type == AST_WORD_BOOL,
		"expected AST_WORD_BOOL but got %s",
		ASTWordType_tostring(word->type));

	word->if_type = if_type;
}

enum ASTWordBoolType
ast_word_bool_type(const struct ASTWord *word)
{
	libias_panic_unless(
		word->type == AST_WORD_BOOL,
		"expected AST_WORD_BOOL but got %s",
		ASTWordType_tostring(word->type));

	return word->if_type;
}

// If WORD is an AST_WORD_APPEND with one child word, then return the
// child word and free WORD recursively. This function will always
// return a value.
struct ASTWord *
ast_word_unwrap(struct ASTWord *word)
{
	if (word->type != AST_WORD_APPEND) {
		return word;
	} else if (libias_list_len(word->words) == 1) {
		struct LibiasListEntry *head = libias_list_head(word->words);
		struct ASTWord *retval = ast_word_unwrap(libias_list_entry_value(head));
		libias_list_entry_remove(head);
		libias_cleanup(&word);
		return retval;
	} else {
		return word;
	}
}

// Wrap WORD inside a new AST_WORD_APPEND. This function will always
// return a value.
struct ASTWord *
ast_word_wrap(struct ASTWord *word)
{
	struct ASTWord *wrapper = ast_word_new_append();
	ast_word_add_child(wrapper, word);
	return wrapper;
}

void
ast_word_render_append(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	libias_list_foreach(word->words, struct ASTWord *, w) {
		ast_word_render(w, f);
	}
}

void
ast_word_render_expand(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	if (word->expand_single) {
		libias_buffer_puts(f, "$");
	} else if (word->expand_parens) {
		libias_buffer_puts(f, "$(");
	} else {
		libias_buffer_puts(f, "${");
	}
	libias_list_foreach(word->words, struct ASTWord *, w) {
		ast_word_render(w, f);
	}
	libias_list_foreach(word->modifiers, struct ASTWordExpandModifier *, modifier) {
		ast_word_expand_modifier_render(modifier, f);
	}
	if (word->expand_single) {
	} else if (word->expand_parens) {
		libias_buffer_puts(f, ")");
	} else {
		libias_buffer_puts(f, "}");
	}
}

void
ast_word_render_bool(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ASTWordBoolType_bmake(word->if_type));
}

void
ast_word_render_string(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, word->value);
}

void
ast_word_render_whitespace(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	for (char *s = word->value; *s; s++) {
		if (*s == '\n') {
			libias_buffer_puts(f, "\\\n");
		} else {
			libias_buffer_write(f, s, 1, 1);
		}
	}
}

void
ast_word_render_comment(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, "#");
	libias_buffer_puts(f, word->value);
}

void
ast_word_render(
	struct ASTWord *word,
	struct LibiasBuffer *f)
{
	word->render(word, f);
}

void
ast_word_sexp_append(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-append");
	libias_list_foreach(word->words, struct ASTWord *, child) {
		ast_word_sexp(child, w);
	}
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_expand(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-expand");

	libias_list_foreach(word->words, struct ASTWord *, child) {
		ast_word_sexp(child, w);
	}
	if (libias_list_len(word->modifiers) > 0) {
		sexp_writer_open_tree(w, "modifiers");
		libias_list_foreach(word->modifiers, struct ASTWordExpandModifier *, modifier) {
			ast_word_expand_modifier_sexp(modifier, w);
		}
		sexp_writer_close_tree(w);
	}
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_bool(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-bool");
	const char *value = ASTWordBoolType_bmake(word->if_type);
	sexp_writer_string(w, "value", value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_string(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word");
	sexp_writer_string(w, "value", word->value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_whitespace(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-whitespace");
	sexp_writer_string(w, "value", word->value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp_comment(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "word-comment");
	sexp_writer_string(w, "value", word->value);
	sexp_writer_close_tree(w);
}

void
ast_word_sexp(
	struct ASTWord *word,
	struct SexpWriter *w)
{
	word->sexp(word, w);
}


// Returns a rendered version of WORD as a newly allocated string. The
// caller is responsible for freeing the returned value.
char *
ast_word_flatten(struct ASTWord *word)
{
	if (word->_flat_cache) {
		return libias_str_dup(word->_flat_cache);
	}

	libias_scope_buffer(f);
	ast_word_render(word, f);
	word->_flat_cache = libias_buffer_data_copy(f);
	return libias_buffer_data_disown(f);
}

int
ast_word_strcmp(struct ASTWord *word, const char *s)
{
	char *value = ast_word_flatten(word);
	int result = strcmp(value, s);
	libias_free(value);
	return result;
}

libias_define_compare(compare_ast_word, struct ASTWord, void)
{
	char *av = ast_word_flatten(a);
	char *bv = ast_word_flatten(b);
	int result =  strcmp(av, bv);
	libias_free(av);
	libias_free(bv);
	return result;
}

size_t
ast_word_hash_datalen(
	const void *data_ptr,
	void *context)
{
	struct ASTWord *word = *(struct ASTWord **)data_ptr;
	char *rendered_word = ast_word_flatten(word);
	size_t len = strlen(rendered_word);
	libias_free(rendered_word);
	return len;
}

uint64_t
ast_word_hash_impl(
	const void *data_ptr,
	const size_t datalen,
	void *context)
{
	struct ASTWord *word = *(struct ASTWord **)data_ptr;
	char *rendered_word = ast_word_flatten(word);
	uint64_t hashv = libias_hash_generic(rendered_word, datalen);
	libias_free(rendered_word);
	return hashv;
}

struct LibiasIterator *
ast_word_children_iterator(
	struct ASTWord *word,
	ssize_t a, ssize_t b)
{
	struct ASTWordChildrenIterator *this = libias_alloc(struct ASTWordChildrenIterator);
	size_t len = 0;
	bool word_has_children =
		word->words
		&& (word->type == AST_WORD_EXPAND || word->type == AST_WORD_APPEND);
	if (word_has_children) {
		this->entry = libias_list_head(word->words);
		len = libias_list_len(word->words);
	}
	libias_slice_to_range(len, a, b, &this->i, &this->len);

	for (size_t i = 0; this->entry && i < this->i; i++) {
		this->entry = libias_list_entry_next(this->entry);
	}

	return libias_iterator_new(
		ast_word_children_iterator_next,
		this,
		ast_word_children_iterator_cleanup);
}

void
ast_word_children_iterator_cleanup(void **this_ptr)
{
	struct ASTWordChildrenIterator *this = *this_ptr;
	if (this) {
		libias_free(this);
		*this_ptr = NULL;
	}
}

bool
ast_word_children_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct ASTWordChildrenIterator *this = libias_iterator_context(*it_ptr);
	if (this->entry && this->i < this->len) {
		*index = this->i++;
		*key = libias_list_entry_value(this->entry);
		*value = libias_list_entry_value(this->entry);
		this->entry = libias_list_entry_next(this->entry);
		return true;
	} else {
		return false;
	}
}
