// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/hashset.h>
#include <libias/iterator.h>
#include <libias/map.h>
#include <libias/mempool.h>
#include <libias/set.h>
#include <libias/str.h>
#include <libias/trait/compare.h>
#include <libias/trait/hash.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/word.h"
#include "diagnostic.h"
#include "rules.h"

struct ASTFormat {
	struct ASTFormatSettings settings;
	struct Rules *rules;

	struct LibiasMempool *pool;
	struct LibiasMap *goalcols;
};

struct ASTFormatPrepare {
	struct ASTFormat *ast_format;
	struct LibiasArray *nodes;
	int32_t moving_goalcol;
};

enum ASTFormatPrepareCmakeArgsState {
	AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_NONE,
	AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_CMAKE_ARGS,
	AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_D,
};

enum ASTFormatPrepareDedupAction {
	AST_FORMAT_PREPARE_DEDUP_ACTION_DEFAULT,
	AST_FORMAT_PREPARE_DEDUP_ACTION_USES,
};

// Prototypes
static libias_declare_compare(compare_category_makefile_subdir);
static libias_declare_compare(compare_sort_variable_words);
static void ast_format_category_makefile_p_visit_include(struct ASTVisit *visit, struct AST *node);
static void ast_format_comment(struct ASTVisit *visit, struct AST *node);
static void ast_format_expr(struct ASTVisit *visit, struct AST *node);
static int32_t ast_format_expr_helper(struct ASTWordsEdit *edit, int32_t wrapcol, const char *line_cont, int32_t linelen_after_wrap, const char **line_breaks_after, const size_t line_breaks_after_len, struct ASTWord *word, const int32_t linelen);
static void ast_format_for(struct ASTVisit *visit, struct AST *node);
static int32_t ast_format_goalcol(struct ASTFormat *this, struct AST *node);
static void ast_format_if_branch(struct ASTVisit *visit, struct AST *node);
static void ast_format_include(struct ASTVisit *visit, struct AST *node);
static void ast_format_prepare(struct ASTFormat *this, struct AST *node);
static void ast_format_prepare_collapse_adjacent_variables(struct AST *root);
static bool ast_format_prepare_collapse_adjacent_variables_candidate_p(struct ASTWord *group_name, struct AST *node, bool first);
static void ast_format_prepare_collapse_adjacent_variables_visit_variable(struct ASTVisit *visit, struct AST *first);
static void ast_format_prepare_dedup_words(struct AST *node, struct Rules *rules, struct Diagnostic *diagnostic);
static void ast_format_prepare_propagate_goalcol(struct ASTFormatPrepare *this);
static void ast_format_prepare_sanitize_cmake_args(struct AST *node, struct Rules *rules, struct Diagnostic *diagnostic);
static void ast_format_prepare_visit_comment(struct ASTVisit *visit, struct AST *node);
static void ast_format_prepare_visit_variable(struct ASTVisit *visit, struct AST *node);
static void ast_format_set_goalcol(struct ASTFormat *this, struct AST *node, int32_t goalcol);
static void ast_format_sort_category_makefile(struct AST *root, struct Diagnostic *diagnostic);
static void ast_format_target_command(struct ASTVisit *visit, struct AST *node);
static char *ast_format_target_command_join_words(struct LibiasMempool *extpool, struct LibiasArray *words, const char *sep);
static void ast_format_target_rule(struct ASTVisit *visit, struct AST *node);
static void ast_format_variable(struct ASTVisit *visit, struct AST *node);
static void ast_format_variable_as_list(struct ASTFormat *this, struct AST *node, struct ASTWordsEdit *edit);
static void ast_format_variable_in_category_makefile(struct ASTVisit *visit, struct AST *node);
static void ast_format_variable_sort_opt_use(struct ASTVisit *visit, struct AST *node, const char *varname_str);
static void ast_format_variable_with_line_breaks(struct ASTFormat *this, struct AST *node, struct ASTWordsEdit *edit);
static bool matches_opt_use_prefix(const char *s);
static bool matches_opt_use_prefix_helper(char c);

// Constants
static const int32_t TAB_WIDTH = 8;
static const char *EXPR_LINE_CONT = " \n\t";
static const char *TARGET_COMMAND_LINE_CONT = " \n\t\t";
static const char *LINE_CONT = " \n";
static const int32_t LINE_CONT_RENDERED_LEN = 3;

void
ast_format_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	// comments stay like they are
}

void
ast_format_expr(
	struct ASTVisit *visit,
	struct AST *node)
{
}

int32_t
ast_format_expr_helper(
	struct ASTWordsEdit *edit,
	int32_t wrapcol,
	const char *line_cont,
	int32_t linelen_after_wrap,
	const char **line_breaks_after,
	const size_t line_breaks_after_len,
	struct ASTWord *word,
	const int32_t linelen)
{
	if (ast_word_whitespace_p(word)) {
		return linelen;
	}

	wrapcol -= LINE_CONT_RENDERED_LEN - strlen("\n");

	char *word_str = ast_word_flatten(word);
	libias_scope_free(word_str);
	const int32_t word_str_len = strlen(word_str);

	if ((linelen + word_str_len) > wrapcol) {
		bool wrap_ok = true;
		for (size_t i = 0; i < line_breaks_after_len; i++) {
			if (strcmp(word_str, line_breaks_after[i]) == 0) {
				wrap_ok = false;
				break;
			}
		}
		if (wrap_ok) {
			struct ASTWord *ws = ast_word_new_whitespace(line_cont);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			return linelen_after_wrap + word_str_len;
		}
	}

	struct ASTWord *ws = ast_word_new_whitespace(" ");
	ast_words_edit_set_whitespace_before_word(edit, word, ws);

	return linelen + strlen(" ") + word_str_len;
}

void
ast_format_for(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);

	// TODO wrong if indent contains a \t
	int32_t linelen = strlen(ast_expr_indent(node)) + strlen(".for");

	struct AST *for_bindings = ast_get_child(node, AST_CHILD_FOR_BINDINGS);
	auto edit = ast_words_edit_new(
		for_bindings,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);

	ast_words_foreach(for_bindings, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.for_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);
	}

	ast_words_edit_apply(edit);

	linelen += strlen("in");

	struct AST *for_words = ast_get_child(node, AST_CHILD_FOR_WORDS);
	edit = ast_words_edit_new(
		for_words,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(for_words, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.for_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);

		if (ast_word_comment_p(word)) {
			// Add a space before the trailing comment
			struct ASTWord *ws = ast_word_new_whitespace(" ");
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
		}
	}
	ast_words_edit_remove_trailing_whitespace(edit);
	ast_words_edit_apply(edit);
}

void
ast_format_if_branch(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);

	static const char *line_breaks_after[] = {
		"&&",
		"||",
		"!=",
		"==",
		"<=",
		">=",
		"<",
		">",
	};

	auto edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);

	enum ASTIfType type = ast_if_branch_type(node);
	// TODO wrong if indent contains a \t
	int32_t linelen = strlen(ASTIfType_human(type)) +
		strlen(ast_expr_indent(node)) +
		strlen(".");
	bool else_branch =
		type == AST_IF_ELSE
		|| ast_get_child(ast_parent(node), 0) == node;
	if (!else_branch) {
		linelen += strlen("el");
	}

	ast_words_foreach(node, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.if_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			line_breaks_after, nitems(line_breaks_after),
			word,
			linelen);
	}

	ast_words_edit_remove_trailing_whitespace(edit);

	// Apply the edit we have so far. Afterwards we can fix the
	// whitespace of !, (, )
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(node, word) {
		if (ast_word_comment_p(word)) {
			// Add a space before the trailing comment
			struct ASTWord *ws = ast_word_new_whitespace(" ");
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			continue;
		} else if (!ast_word_bool_p(word)) {
			continue;
		}
		switch (ast_word_bool_type(word)) {
		case AST_WORD_BOOL_GROUP_START:
		case AST_WORD_BOOL_NOT:
		{
			struct ASTWord *next = ast_next_word(node, word);
			if (next && ast_word_whitespace_p(next)) {
				const char *next_str = ast_word_value(next);
				if (strcmp(next_str, EXPR_LINE_CONT) == 0) {
					ast_words_edit_set_whitespace_before_word(
						edit,
						word,
						ast_word_clone(next));
				}
				ast_words_edit_remove_whitespace_after_word(edit, word);
			}
			break;
		}
		case AST_WORD_BOOL_GROUP_END:
			ast_words_edit_remove_whitespace_before_word(edit, word);
			break;
		default:
			break;
		}
	}

	ast_words_edit_apply(edit);
}

void
ast_format_include(
	struct ASTVisit *visit,
	struct AST *node)
{
}

bool
matches_opt_use_prefix_helper(char c)
{
	return isupper((unsigned char)c) || islower((unsigned char)c) || isdigit((unsigned char)c) || c == '-' || c == '_';
}

bool
matches_opt_use_prefix(const char *s)
{
	// ^([-_[:upper:][:lower:][:digit:]]+)
	if (!matches_opt_use_prefix_helper(*s)) {
		return false;
	}
	size_t len = strlen(s);
	size_t i;
	for (i = 1; i < len && matches_opt_use_prefix_helper(s[i]); i++);

	// \+?
	if (s[i] == '+') {
		i++;
	}

	// =
	if (s[i] == '=') {
		return true;
	}

	return false;
}

void
ast_format_variable_sort_opt_use(
	struct ASTVisit *visit,
	struct AST *node,
	const char *varname_str)
{
	struct ASTFormat *this = ast_visit_context(visit);
	libias_scope_mempool(pool);

	bool opt_use = false;
	char *helper = NULL;
	if (rules_parse_variable_options_helper(
			this->rules,
			node,
			varname_str,
			NULL,
			&helper,
			NULL))
	{
		libias_scope_free(helper);
		if (strcmp(helper, "USE") == 0 || strcmp(helper, "USE_OFF") == 0)  {
			opt_use = true;
		} else if (strcmp(helper, "VARS") == 0 || strcmp(helper, "VARS_OFF") == 0) {
			opt_use = false;
		} else {
			goto sort;
		}
	} else {
		goto sort;
	}

	auto edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(node, word) {
		struct ASTWord *parent_word = word;
		// Ignore if the (once unwrapped) prefix isn't static
		if (ast_word_string_p(word)) {
			// ok
		} else {
			struct ASTWord *first = NULL;
			ast_word_children_foreach(word, child) {
				first = child;
				break;
			}
			if (first && ast_word_string_p(first)) {
				word = first;
			} else {
				continue;
			}
		}

		char *t = ast_word_flatten(word);
		libias_scope_free(t);
		if (!matches_opt_use_prefix(t)) {
			continue;
		}

		char *suffix = strchr(t, '=');
		if (suffix == NULL) {
			continue;
		}
		suffix++;

		char *prefix = str_map(t, suffix - t, toupper);
		libias_mempool_take(pool, prefix);
		enum ASTVariableModifier mod = AST_VARIABLE_MODIFIER_ASSIGN;
		if ((suffix - t) >= 1 && prefix[suffix - t - 1] == '=') {
			prefix[suffix - t - 1] = 0;
		}
		if ((suffix - t) >= 2 && prefix[suffix - t - 2] == '+') {
			mod = AST_VARIABLE_MODIFIER_APPEND;
			prefix[suffix - t - 2] = 0;
		}
		libias_scope_buffer(f);
		if (opt_use) {
			char *var = libias_str_printf("USE_%s", prefix);
			libias_mempool_take(pool, var);
			libias_buffer_puts(f, prefix);
			libias_buffer_puts(f, ASTVariableModifier_human(mod));
			struct RulesCompareTokens data = {
				.rules = this->rules,
				.root = node,
				.var = var,
			};
			struct LibiasArray *values = libias_str_split(pool, suffix, ",");
			libias_array_sort(values, &(struct LibiasCompareTrait){rules_compare_tokens, &data});
			libias_array_foreach(values, const char *, t2) {
				libias_buffer_puts(f, t2);
				if (t2_index < libias_array_len(values) - 1) {
					libias_buffer_puts(f, ",");
				}
			}
		} else {
			libias_buffer_puts(f, prefix);
			libias_buffer_puts(f, ASTVariableModifier_human(mod));
			libias_buffer_puts(f, suffix);
		}

		if (parent_word == word) {
			ast_words_edit_replace_word(edit, word, ast_word_new_string(libias_buffer_data(f)));
		} else {
			struct ASTWord *new_word = ast_word_new_append();
			ast_word_add_child(new_word, ast_word_new_string(libias_buffer_data(f)));
			ast_word_children_foreach_slice(parent_word, 1, -1, child) {
				ast_word_add_child(new_word, ast_word_clone(child));
			}
			ast_words_edit_replace_word(edit, parent_word, new_word);
		}
	}
	ast_words_edit_apply(edit);

sort: ;

	struct RulesCompareTokens data = {
		.rules = this->rules,
		.root = node,
		.var = varname_str,
	};
	struct LibiasCompareTrait compare = {
		.compare = compare_sort_variable_words,
		.context = &data,
	};
	ast_sort_words(node, &compare);
}

void
ast_format_variable_as_list(
	struct ASTFormat *this,
	struct AST *node,
	struct ASTWordsEdit *edit)
{
	char *varname = ast_word_flatten(ast_variable_name(node));
	int32_t wrapcol = this->settings.variable_wrapcol;
	int32_t goalcol = ast_format_goalcol(this, node);
	if (rules_ignore_wrap_col_p(this->rules, node, varname, ast_variable_modifier(node))) {
		wrapcol = 99999999;
	} else {
		wrapcol -= goalcol;
		wrapcol -= LINE_CONT_RENDERED_LEN - strlen("\n");
	}

	int32_t ntabs;
	int32_t rowlen = strlen(varname);
	if (libias_str_suffix_p(varname, "+")) {
		rowlen += strlen(" ");
	}
	rowlen += strlen(ASTVariableModifier_human(ast_variable_modifier(node)));
	if (rowlen > MAX(2 * TAB_WIDTH, goalcol)) {
		ntabs = ceil((rowlen - MAX(2 * TAB_WIDTH, goalcol)) / (1.0 * TAB_WIDTH));
	} else {
		ntabs = ceil((MAX(2 * TAB_WIDTH, goalcol) - rowlen) / (1.0 * TAB_WIDTH));
	}
	char *sep_first = libias_str_repeat("\t", ntabs);
	rowlen += ntabs * TAB_WIDTH;
	ntabs = ceil(MAX(2 * TAB_WIDTH, goalcol) / (1.0 * TAB_WIDTH));
	char *sep_next;
	{
		char *tabs = libias_str_repeat("\t", ntabs);
		sep_next = libias_str_printf("%s%s", LINE_CONT, tabs);
		libias_free(tabs);
	}

	rowlen = 0;
	bool first = true;
	ast_words_foreach(node, word) {
		if (ast_word_whitespace_p(word)) {
			continue;
		}
		int32_t word_str_len;
		{
			char *word_str = ast_word_flatten(word);
			word_str_len = strlen(word_str);
			libias_free(word_str);
		}
		if (first) {
			struct ASTWord *ws = ast_word_new_whitespace(sep_first);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			rowlen += word_str_len;
			first = false;
		} else if ((rowlen + word_str_len) > wrapcol) {
			struct ASTWord *ws = ast_word_new_whitespace(sep_next);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			rowlen = word_str_len;
		} else {
			rowlen += word_str_len + strlen(" ");
			struct ASTWord *ws = ast_word_new_whitespace(" ");
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
		}
	}

	ast_words_edit_remove_trailing_whitespace(edit);

	libias_free(varname);
	libias_free(sep_first);
	libias_free(sep_next);
}

void
ast_format_variable_with_line_breaks(
	struct ASTFormat *this,
	struct AST *node,
	struct ASTWordsEdit *edit)
{
	char *varname = ast_word_flatten(ast_variable_name(node));
	int32_t ntabs;
	int32_t startlen = strlen(varname);
	if (libias_str_suffix_p(varname, "+")) {
		startlen += strlen(" ");
	}
	startlen += strlen(ASTVariableModifier_human(ast_variable_modifier(node)));
	int32_t goalcol = ast_format_goalcol(this, node);
	if (startlen > MAX(2 * TAB_WIDTH, goalcol)) {
		ntabs = ceil((startlen - MAX(2 * TAB_WIDTH, goalcol)) / (1.0 * TAB_WIDTH));
	} else {
		ntabs = ceil((MAX(2 * TAB_WIDTH, goalcol) - startlen) / (1.0 * TAB_WIDTH));
	}
	char *sep_first = libias_str_repeat("\t", ntabs);
	startlen += ntabs * TAB_WIDTH;
	// For the next lines
	ntabs = ceil(MAX(2 * TAB_WIDTH, goalcol) / (1.0 * TAB_WIDTH));
	char *sep_next;
	{
		char *tabs = libias_str_repeat("\t", ntabs);
		sep_next = libias_str_printf(" \n%s", tabs);
		libias_free(tabs);
	}

	const char *sep = sep_first;
	int32_t non_whitespace_words = 0;
	ast_words_foreach(node, word) {
		if (ast_word_whitespace_p(word)) {
			continue;
		} else if (ast_word_comment_p(word)) {
			// Keep the comment on the same line as the last word
			const char *start = " ";
			if (non_whitespace_words == 0) {
				start = sep_first;
			}
			struct ASTWord *ws = ast_word_new_whitespace(start);
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
			continue;
		}

		struct ASTWord *ws = ast_word_new_whitespace(sep);
		ast_words_edit_set_whitespace_before_word(edit, word, ws);
		if (sep == sep_first) {
			sep = sep_next;
		}
		non_whitespace_words++;
	}

	ast_words_edit_remove_trailing_whitespace(edit);

	libias_free(varname);
	libias_free(sep_first);
	libias_free(sep_next);
}

libias_define_compare(
	compare_sort_variable_words,
	struct ASTWord,
	struct RulesCompareTokens)
{
	char *as = ast_word_flatten(a);
	char *bs = ast_word_flatten(b);
	int result = rules_compare_tokens(&as, &bs, this);
	libias_free(as);
	libias_free(bs);
	return result;
}

void
ast_format_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);

	struct ASTWord *varname = ast_variable_name(node);
	char *varname_str = ast_word_flatten(varname);
	libias_scope_free(varname_str);

	// Leave variables unformatted that have $\ in them
	ast_words_foreach(node, word) {
		if (!ast_word_whitespace_p(word)) {
			continue;
		}
		char *value = ast_word_flatten(word);
		libias_scope_free(value);
		if (libias_str_prefix_p(value, "$\\\n")) {
			return;
		}
	}
	if (rules_leave_unformatted_p(this->rules, node, varname_str)) {
		return;
	}

	bool should_sort = !(this->settings.flags & AST_FORMAT_UNSORTED_VARIABLES) &&
		((this->settings.flags & AST_FORMAT_ALWAYS_SORT_VARIABLES) ||
		 rules_should_sort_p(this->rules, node, varname_str, ast_variable_modifier(node)));
	if (should_sort) {
		ast_format_variable_sort_opt_use(visit, node, varname_str);
	}

	auto edit = ast_words_edit_new(
		ast_get_child(node, AST_CHILD_VARIABLE_NAME),
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_edit_remove_beginning_whitespace(edit);
	if (libias_str_suffix_p(varname_str, "+")) {
		struct ASTWord *ws = ast_word_new_whitespace(" ");
		ast_words_edit_set_whitespace_after_word(edit, varname, ws);
	} else {
		ast_words_edit_remove_trailing_whitespace(edit);
	}
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	if (rules_print_with_line_breaks_p(this->rules, node, varname_str)) {
		ast_format_variable_with_line_breaks(this, node, edit);
	} else {
		ast_format_variable_as_list(this, node, edit);
	}

	ast_words_edit_apply(edit);
}

void
ast_format_variable_in_category_makefile(
	struct ASTVisit *visit,
	struct AST *node)
{
	// Category Makefiles have a strict layout and their own formatting
	// rules. We apply those to COMMENT and SUBDIR and format any
	// additional variable/node normally. Checking that there are no
	// additional variables should be left to a lint.

	struct ASTWord *variable_name = ast_variable_name(node);

	if (ast_word_strcmp(variable_name, "COMMENT") == 0) {
		auto ast_edit = ast_edit_new(
			node,
			ast_visit_diagnostic(visit),
			AST_EDIT_DEFAULT);
		ast_edit_set_variable_modifier(
			ast_edit,
			AST_VARIABLE_MODIFIER_ASSIGN);
		ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);

		// Sanitize whitespace around COMMENT
		struct AST *variable_name_node = ast_get_child(node, AST_CHILD_VARIABLE_NAME);
		auto edit = ast_words_edit_new(
			variable_name_node,
			ast_visit_diagnostic(visit),
			AST_WORDS_EDIT_DEFAULT);
		ast_words_foreach(variable_name_node, word) {
			if (ast_word_whitespace_p(word)) {
				ast_words_edit_remove_word(edit, word);
			} else {
				struct ASTWord *ws = ast_word_new_whitespace("    ");
				ast_words_edit_set_whitespace_before_word(edit, word, ws);

				ws = ast_word_new_whitespace(" ");
				ast_words_edit_set_whitespace_after_word(edit, word, ws);
			}
		}
		ast_words_edit_apply(edit);

		edit = ast_words_edit_new(
			node,
			ast_visit_diagnostic(visit),
			AST_WORDS_EDIT_DEFAULT);
		ast_words_edit_remove_beginning_whitespace(edit);
		ast_words_edit_remove_trailing_whitespace(edit);
		ast_words_foreach(node, word) {
			if (!ast_word_whitespace_p(word)) {
				struct ASTWord *ws = ast_word_new_whitespace(" ");
				ast_words_edit_set_whitespace_before_word(edit, word, ws);
			}
		}
		ast_words_edit_apply(edit);
	} else if (ast_word_strcmp(variable_name, "SUBDIR") == 0) {
		// Move every word into their own SUBDIR+= node
		struct AST *sibling = node;
		ast_words_foreach(node, word) {
			if (ast_word_whitespace_p(word)) {
				continue;
			}

			struct AST *new_node = ast_new_variable();
			// Hook up the new node
			ast_edit_add_child(ast_visit_root_edit(visit), new_node, sibling);

			// Setup SUBDIR with the right formatting
			{
				auto ast_edit = ast_edit_new(
					new_node,
					ast_visit_diagnostic(visit),
					AST_EDIT_DEFAULT);
				ast_edit_set_variable_modifier(
					ast_edit,
					AST_VARIABLE_MODIFIER_APPEND);
				ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);

				auto edit = ast_words_edit_new(
					ast_get_child(new_node, AST_CHILD_VARIABLE_NAME),
					ast_visit_diagnostic(visit),
					AST_WORDS_EDIT_DEFAULT);

				struct ASTWord *subdir = ast_word_new_string("SUBDIR");
				ast_words_edit_append_word(edit, subdir);

				struct ASTWord *ws = ast_word_new_whitespace("    ");
				ast_words_edit_set_whitespace_before_word(edit, subdir, ws);

				ws = ast_word_new_whitespace(" ");
				ast_words_edit_set_whitespace_after_word(edit, subdir, ws);

				ast_words_edit_apply(edit);
			}

			// Add word
			{
				auto edit = ast_words_edit_new(
					new_node,
					ast_visit_diagnostic(visit),
					AST_WORDS_EDIT_DEFAULT);
				ast_words_edit_remove_all_words(edit);
				struct ASTWord *word_clone = ast_word_clone(word);
				ast_words_edit_append_word(edit, word_clone);
				struct ASTWord *ws = ast_word_new_whitespace(" ");
				ast_words_edit_set_whitespace_before_word(
					edit,
					word_clone,
					ws);
				ast_words_edit_apply(edit);
			}

			sibling = new_node;
		}

		// Remove the original node
		ast_edit_remove_child(ast_visit_root_edit(visit), node);
	} else {
		return ast_format_variable(visit, node);
	}
}

char *
ast_format_target_command_join_words(
	struct LibiasMempool *extpool,
	struct LibiasArray *words,
	const char *sep)
{
	libias_scope_buffer(f);
	bool after_first = false;
	libias_array_foreach(words, struct ASTWord *, word) {
		if (after_first) {
			libias_buffer_puts(f, sep);
		}
		char *value = ast_word_flatten(word);
		libias_buffer_puts(f, value);
		libias_free(value);
		after_first = true;
	}
	return libias_mempool_take(extpool, libias_buffer_data_disown(f));
}

void
ast_format_target_command(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);
	libias_scope_mempool(pool);

	if (!(this->settings.flags & AST_FORMAT_PRESERVE_TARGET_COMMAND_COMMENTS)) {
		// Normalize target command comments
		// \t# foo -> # foo
		auto node_comment = ast_node_comment(node);
		if (node_comment && ast_words_no_meta_len(node) == 0) {
			struct ASTEdit *ast_edit = ast_visit_root_edit(visit);
			struct AST *comment_node = ast_new_comment();
			struct ASTWordsEdit *edit = ast_words_edit_new(
				comment_node,
				ast_visit_diagnostic(visit),
				AST_WORDS_EDIT_DEFAULT);
			char *comment = libias_str_printf("#%s", ast_word_value(node_comment));
			struct ASTWord *comment_word = ast_word_new_string(comment);
			libias_free(comment);
			ast_words_edit_append_word(edit, comment_word);
			ast_words_edit_apply(edit);

			ast_edit_replace_child(ast_edit, node, comment_node);
			return;
		}
	}

	if (!(this->settings.flags & AST_FORMAT_TARGET_COMMANDS)) {
		return;
	}

	// TODO: Sanitize target command flags?

	// Normalize whitespace
	auto edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(node, word) {
		struct ASTWord *ws = ast_word_new_whitespace(" ");
		ast_words_edit_set_whitespace_after_word(edit, word, ws);
	}
	ast_words_edit_apply(edit);

	edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);

	struct LibiasArray *commands = libias_mempool_array(pool);
	struct LibiasArray *commands_words = libias_mempool_array(pool);
	struct LibiasArray *merge = libias_mempool_array(pool);
	const char *command_str = NULL;
	bool wrap_after = false;
	ast_words_foreach(node, word) {
		if (ast_word_whitespace_p(word)) {
			continue;
		}
		char *word_str = ast_word_flatten(word);
		libias_mempool_take(pool, word_str);

		if (command_str == NULL) {
			command_str = word_str;
		}
		if (rules_target_command_should_wrap_p(this->rules, word_str)) {
			command_str = NULL;
		}

		if (command_str &&
		    (strcmp(command_str, "${SED}") == 0 ||
		     strcmp(command_str, "${REINPLACE_CMD}") == 0)) {
			if (strcmp(word_str, "-e") == 0 || strcmp(word_str, "-i") == 0) {
				libias_array_append(merge, word);
				wrap_after = true;
				continue;
			}
		}

		libias_array_append(merge, word);
		libias_array_append(commands, ast_format_target_command_join_words(pool, merge, " "));
		libias_array_append(commands_words, merge);
		merge = libias_mempool_array(pool);
		if (wrap_after) {
			// An empty string is abused as a "wrap line here" marker
			libias_array_append(commands, libias_mempool_take(pool, libias_str_dup("")));
			libias_array_append(commands_words, libias_mempool_array(pool));
			wrap_after = false;
		}
		libias_array_truncate(merge);
	}
	if (libias_array_len(merge) > 0) {
		libias_array_append(commands, ast_format_target_command_join_words(pool, merge, " "));
		libias_array_append(commands_words, merge);
		if (wrap_after) {
			// An empty string is abused as a "wrap line here" marker
			libias_array_append(commands, libias_mempool_take(pool, libias_str_dup("")));
			libias_array_append(commands_words, libias_mempool_array(pool));
			wrap_after = false;
		}
	}
	merge = NULL;

	// Find the places we need to wrap to the next line.
	struct LibiasHashset *wraps = libias_mempool_hashset(pool, libias_compare_int64, libias_hash_int64);
	int32_t complexity = 0;
	size_t command_i = 0;
	int32_t linelen = TAB_WIDTH + strlen(ast_target_command_flags(node));
	libias_array_foreach(commands, const char *, word) {
		if (command_str == NULL) {
			command_str = word;
			command_i = word_index;
		}
		if (rules_target_command_should_wrap_p(this->rules, word)) {
			command_str = NULL;
			command_i = 0;
		}

		for (const char *c = word; *c != 0; c++) {
			switch (*c) {
			case '`':
			case '(':
			case ')':
			case '[':
			case ']':
			case ';':
				complexity++;
				break;
			}
		}

		linelen += TAB_WIDTH + strlen(word);
		if (linelen > this->settings.target_command_wrapcol ||
		    strcmp(word, "") == 0 ||
			rules_target_command_should_wrap_p(this->rules, word) ||
		    (command_str && word_index > command_i &&
			 rules_target_command_wrap_after_each_token_p(this->rules, command_str))) {
			if ((word_index + 1) < libias_array_len(commands)) {
				char *next = libias_array_get(commands, word_index + 1);
				if (strcmp(next, "") == 0 ||
					rules_target_command_should_wrap_p(this->rules, next)) {
					continue;
				}
			}
			linelen = 2 * TAB_WIDTH;
			int64_t *x = libias_mempool_alloc(pool, int64_t);
			*x = word_index;
			libias_hashset_add(wraps, x);
		}
	}

	if (!(this->settings.flags & AST_FORMAT_TARGET_COMMANDS) ||
	    complexity > this->settings.target_command_format_threshold) {
		return;
	}

	bool wrapped = false;
	libias_array_foreach(commands, const char *, command) {
		struct LibiasArray *command_words = libias_array_get(commands_words, command_index);
		if (wrapped) {
			struct ASTWord *first = libias_array_get(command_words, 0);
			struct ASTWord *ws = ast_word_new_whitespace(TARGET_COMMAND_LINE_CONT);
			ast_words_edit_set_whitespace_before_word(
				edit,
				first,
				ws);
		}
		wrapped = libias_hashset_contains_p(wraps, &command_index);

		if (wrapped) {
			libias_array_foreach(command_words, struct ASTWord *, word) {
				struct ASTWord *ws = ast_word_new_whitespace(" ");
				ast_words_edit_set_whitespace_after_word(edit, word, ws);
			}
			struct ASTWord *last = libias_array_get(
				command_words,
				libias_array_len(command_words) - 1);
			if (last) {
				struct ASTWord *ws = ast_word_new_whitespace(TARGET_COMMAND_LINE_CONT);
				ast_words_edit_set_whitespace_after_word(edit, last, ws);
			}
		}
	}

	ast_words_edit_remove_beginning_whitespace(edit);
	ast_words_edit_remove_trailing_whitespace(edit);

	// if (*ast_node_comment(node)) {
	// 	// Add a space before the trailing comment
	// 	struct ASTWord *ws = ast_word_new_whitespace(" ", 0);
	// 	ast_words_edit_add_word(edit, ws, NULL);
	// }

	ast_words_edit_apply(edit);
}

void
ast_format_target_rule(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormat *this = ast_visit_context(visit);

	int32_t linelen = 0;

	struct AST *targets = ast_get_child(node, AST_CHILD_TARGET_RULE_TARGETS);
	auto edit = ast_words_edit_new(
		targets,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(targets, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.target_rule_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);
	}
	ast_words_edit_remove_beginning_whitespace(edit);
	ast_words_edit_remove_trailing_whitespace(edit);
	ast_words_edit_apply(edit);

	// Separator
	linelen += strlen(":");

	struct AST *dependencies = ast_get_child(node, AST_CHILD_TARGET_RULE_DEPENDENCIES);
	edit = ast_words_edit_new(
		dependencies,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(dependencies, word) {
		linelen = ast_format_expr_helper(
			edit,
			this->settings.target_rule_wrapcol,
			EXPR_LINE_CONT,
			TAB_WIDTH,
			NULL, 0,
			word,
			linelen);

		if (ast_word_comment_p(word)) {
			// Add a space before the trailing comment
			struct ASTWord *ws = ast_word_new_whitespace(" ");
			ast_words_edit_set_whitespace_before_word(edit, word, ws);
		}
	}

	ast_words_edit_remove_trailing_whitespace(edit);

	ast_words_edit_apply(edit);
}

void
ast_format_init_settings(struct ASTFormatSettings *settings)
{
	settings->flags = AST_FORMAT_DEFAULT;
	settings->if_wrapcol = 80;
	settings->for_wrapcol = 80;
	settings->target_command_format_threshold = 8;
	settings->target_command_wrapcol = 80;
	settings->target_rule_wrapcol = 80;
	settings->variable_wrapcol = 80;
}

void
ast_format(
	struct AST *node,
	struct Rules *rules,
	struct ASTFormatSettings *settings,
	struct Diagnostic *diagnostic)
{
	libias_scope_mempool(pool);

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);

	visit_trait.visit_pre_comment = ast_format_comment;
	visit_trait.visit_pre_expr = ast_format_expr;
	visit_trait.visit_pre_for = ast_format_for;
	visit_trait.visit_pre_if_branch = ast_format_if_branch;
	visit_trait.visit_pre_include = ast_format_include;
	visit_trait.visit_pre_variable = ast_format_variable;
	visit_trait.visit_pre_target_command = ast_format_target_command;
	visit_trait.visit_pre_target_rule = ast_format_target_rule;

	struct ASTFormat *this = &(struct ASTFormat){
		.rules = rules,

		.pool = pool,
		.goalcols = libias_mempool_map(pool, libias_compare_ptr),
	};

	ast_format_init_settings(&this->settings);
	if (settings) {
		this->settings = *settings;
	}

	bool is_category_makefile =
		ast_root_p(node)
		&& ast_format_category_makefile_p(node, diagnostic);
	if (is_category_makefile) {
		visit_trait.visit_pre_variable = ast_format_variable_in_category_makefile;
	}

	ast_format_prepare(this, node);

	struct ASTVisit *visit = ast_visit_new(node, &visit_trait);
	ast_visit_set_context(visit, this);
	ast_visit_run(visit);

	if (is_category_makefile) {
		ast_format_sort_category_makefile(node, ast_visit_diagnostic(visit));
	}

	if (diagnostic) {
		diagnostic_move_reports(diagnostic, ast_visit_diagnostic(visit));
	}
	libias_cleanup(&visit);
}

void
ast_format_category_makefile_p_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	bool *is_category = ast_visit_context(visit);
	if (
		ast_include_type(node) == AST_INCLUDE_BMAKE
		&& ast_include_system_p(node)
		&& ast_include_path(node)
		)
	{
		char *path = ast_word_flatten(ast_include_path(node));
		libias_scope_free(path);
		if (strcmp(path, "<bsd.port.subdir.mk>") == 0) {
			*is_category = true;
			ast_visit_stop(visit);
			return;
		}
	}
}

bool
ast_format_category_makefile_p(
	struct AST *node,
	struct Diagnostic *diagnostic)
{
	bool is_category = false;

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_include = ast_format_category_makefile_p_visit_include;

	struct ASTVisit *visit = ast_visit_new(node, &visit_trait);
	ast_visit_set_context(visit, &is_category);
	ast_visit_run(visit);
	if (diagnostic) {
		diagnostic_move_reports(diagnostic, ast_visit_diagnostic(visit));
	}
	libias_cleanup(&visit);

	return is_category;
}

libias_define_compare(compare_category_makefile_subdir, struct AST, void)
{
	char *av = ast_words_flatten(a);
	char *bv = ast_words_flatten(b);
	int result = strcmp(av, bv);
	libias_free(av);
	libias_free(bv);
	return result;
}

void
ast_format_sort_category_makefile(
	struct AST *root,
	struct Diagnostic *diagnostic)
{
	libias_scope_mempool(pool);

	libias_panic_unless(ast_root_p(root), "expected AST_ROOT");

	ssize_t start_index = -1;
	struct LibiasArray *children = libias_mempool_array(pool);
	ast_children_foreach(root, node) {
		libias_array_append(children, node);
	}
	ssize_t end_index = -1;
	ast_children_foreach(root, node) {
		if (start_index > -1) {
			if (!ast_variable_p(node) || ast_word_strcmp(ast_variable_name(node), "SUBDIR") != 0) {
				end_index = node_index;
				break;
			}
		} else if (ast_variable_p(node) && ast_word_strcmp(ast_variable_name(node), "SUBDIR") == 0) {
			start_index = node_index;
		}
	}

	struct LibiasCompareTrait cmp = {
		.compare = compare_category_makefile_subdir,
		.context = NULL,
	};
	libias_array_sort_slice(children, start_index, end_index, &cmp);

	auto ast_edit = ast_edit_new(
		root,
		diagnostic,
		AST_EDIT_DEFAULT);
	struct LibiasArray *clones = libias_mempool_array(pool);
	libias_array_foreach(children, struct AST *, node) {
		libias_array_append(clones, ast_clone(node));
	}
	ast_edit_remove_all_children(ast_edit);
	libias_array_foreach(clones, struct AST *, node) {
		ast_edit_append_child(ast_edit, node);
	}
	ast_edit_apply(ast_edit);
}

void
ast_format_prepare_propagate_goalcol(struct ASTFormatPrepare *this)
{
	this->moving_goalcol = MAX(2 * TAB_WIDTH, this->moving_goalcol);
	libias_array_foreach(this->nodes, struct AST *, node) {
		ast_format_set_goalcol(
			this->ast_format,
			node,
			this->moving_goalcol);
	}

	this->moving_goalcol = 0;
	libias_array_truncate(this->nodes);
}

void
ast_format_prepare_visit_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormatPrepare *this = ast_visit_context(visit);

	// Ignore comments in between variables and treat variables
	// after them as part of the same block, i.e., indent them the
	// same way.
	ast_format_prepare_propagate_goalcol(this);
}

void
ast_format_prepare_dedup_words(
	struct AST *node,
	struct Rules *rules,
	struct Diagnostic *diagnostic)
{
	libias_scope_mempool(pool);

	char *varname = ast_word_flatten(ast_variable_name(node));
	libias_scope_free(varname);
	if (rules_skip_dedup_p(rules, node, varname, ast_variable_modifier(node))) {
		return;
	} else {
		libias_scope_set(seen, libias_str_compare);
		libias_scope_set(uses, libias_str_compare);
		enum ASTFormatPrepareDedupAction action = AST_FORMAT_PREPARE_DEDUP_ACTION_DEFAULT;
		char *helper = NULL;
		if (rules_parse_variable_options_helper(rules, node, varname, NULL, &helper, NULL)) {
			if (strcmp(helper, "USES") == 0 || strcmp(helper, "USES_OFF") == 0) {
				action = AST_FORMAT_PREPARE_DEDUP_ACTION_USES;
			}
			libias_free(helper);
		} else if (strcmp(varname, "USES") == 0) {
			action = AST_FORMAT_PREPARE_DEDUP_ACTION_USES;
		}
		auto edit = ast_words_edit_new(node, diagnostic, AST_WORDS_EDIT_DEFAULT);
		ast_words_foreach(node, word) {
			if (ast_word_comment_p(word)) {
				continue;
			}
			char *value = ast_word_flatten(word);
			libias_mempool_take(pool, value);
			// XXX: Handle *_DEPENDS (turn 'RUN_DEPENDS=foo>=1.5.6:misc/foo foo>0:misc/foo'
			// into 'RUN_DEPENDS=foo>=1.5.6:misc/foo')?
			switch (action) {
			case AST_FORMAT_PREPARE_DEDUP_ACTION_USES: {
				char *args = strchr(value, ':');
				if (args) {
					*args = 0;
				}
				// We follow the semantics of the ports framework.
				// 'USES=compiler:c++11-lang compiler:c++14-lang' is
				// semantically equivalent to just USES=compiler:c++11-lang
				// since compiler_ARGS has already been set once before.
				// As such compiler:c++14-lang can be dropped entirely.
				if (libias_set_contains_p(uses, value)) {
					ast_words_edit_remove_word(edit, word);
				} else {
					libias_set_add(uses, value);
					libias_set_add(seen, value);
				}
				break;
			} default:
				if (libias_set_contains_p(seen, value)) {
					ast_words_edit_remove_word(edit, word);
				} else {
					libias_set_add(seen, value);
				}
				break;
			}
		}
		ast_words_edit_apply(edit);
	}
}

bool
ast_format_prepare_collapse_adjacent_variables_candidate_p(
	struct ASTWord *group_name,
	struct AST *node,
	bool first)
{
	if (!ast_variable_p(node)) {
		return false;
	}

	if (ast_node_comment(node)) {
		return false;
	}

	struct ASTWord *node_name = ast_variable_name(node);
	if (ast_word_compare->compare(&group_name, &node_name, NULL) != 0) {
		return false;
	}

	switch (ast_variable_modifier(node)) {
	case AST_VARIABLE_MODIFIER_ASSIGN:
		return first;
	case AST_VARIABLE_MODIFIER_APPEND:
		return true;
	default:
		return false;
	}
}

void
ast_format_prepare_collapse_adjacent_variables_visit_variable(
	struct ASTVisit *visit,
	struct AST *first)
{
	struct ASTWord *group_name = ast_variable_name(first);

	if (!ast_format_prepare_collapse_adjacent_variables_candidate_p(group_name, first, true)) {
		return;
	}

	libias_scope_array(group);
	struct AST *sibling = ast_next_sibling(first);
	while (sibling) {
		if (ast_format_prepare_collapse_adjacent_variables_candidate_p(group_name, sibling, false)) {
			libias_array_append(group, sibling);
			ast_visit_skip(visit, sibling);
			sibling = ast_next_sibling(sibling);
		} else {
			break;
		}
	}

	// Merge the variables into FIRST

	if (libias_array_len(group) == 0) {
		return;
	}

	auto first_edit = ast_words_edit_new(
		first,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	libias_array_foreach(group, struct AST *, node) {
		auto node_edit = ast_words_edit_new(
			node,
			ast_visit_diagnostic(visit),
			AST_WORDS_EDIT_DEFAULT);
		ast_words_foreach(node, word) {
			ast_words_edit_append_word(first_edit, ast_word_clone(word));
			ast_words_edit_remove_word(node_edit, word);
		}
		ast_words_edit_apply(node_edit);
	}
	ast_words_edit_apply(first_edit);

	// The words of each variable in GROUP were added to the first
	// variable. Remove the superfluous variable nodes now.
	libias_array_foreach(group, struct AST *, node) {
		ast_edit_remove_child(ast_visit_root_edit(visit), node);
	}
}

void
ast_format_prepare_collapse_adjacent_variables(struct AST *root)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_variable = ast_format_prepare_collapse_adjacent_variables_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_run(visit);
	libias_cleanup(&visit);
}

// Sanitize CMAKE_ARGS and MESON_ARGS: -D FOO => -DFOO
void
ast_format_prepare_sanitize_cmake_args(
	struct AST *node,
	struct Rules *rules,
	struct Diagnostic *diagnostic)
{
	char *helper = NULL;
	enum ASTFormatPrepareCmakeArgsState state = AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_NONE;
	char *varname = ast_word_flatten(ast_variable_name(node));
	libias_scope_free(varname);
	if (rules_parse_variable_options_helper(rules, node, varname, NULL, &helper, NULL)) {
		if (strcmp(helper, "CMAKE_ON") == 0 || strcmp(helper, "CMAKE_OFF") == 0 ||
			strcmp(helper, "MESON_ON") == 0 || strcmp(helper, "MESON_OFF") == 0) {
			state = AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_CMAKE_ARGS;
		}
		libias_free(helper);
	} else if (strcmp(varname, "CMAKE_ARGS") == 0 || strcmp(varname, "MESON_ARGS") == 0) {
		state = AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_CMAKE_ARGS;
	}

	if (state == AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_NONE) {
		return;
	}

	auto edit = ast_words_edit_new(node, diagnostic, AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(node, word) {
		if (ast_word_meta_p(word)) {
			continue;
		}

		switch (state) {
		case AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_CMAKE_ARGS:
			if (ast_word_strcmp(word, "-D") == 0) {
				state = AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_D;
				ast_words_edit_remove_word(edit, word);
			}
			break;
		case AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_D:
		{
			auto wrapper = ast_word_wrap(ast_word_new_string("-D"));
			ast_word_add_child(wrapper, ast_word_clone(word));
			ast_words_edit_replace_word(edit, word, wrapper);

			state = AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_CMAKE_ARGS;
			break;
		}
		case AST_FORMAT_PREPARE_CMAKE_ARGS_STATE_NONE:
			break;
		}
	}
	ast_words_edit_apply(edit);
}

void
ast_format_prepare_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTFormatPrepare *this = ast_visit_context(visit);
	auto diagnostic = ast_visit_diagnostic(visit);

	ast_format_prepare_sanitize_cmake_args(node, this->ast_format->rules, diagnostic);
	ast_format_prepare_dedup_words(node, this->ast_format->rules, diagnostic);

	if (ast_words_no_meta_len(node) > 0) {
		char *varname = ast_word_flatten(ast_variable_name(node));
		if (rules_skip_goalcol_p(this->ast_format->rules, node, varname)) {
			ast_format_set_goalcol(
				this->ast_format,
				node,
				rules_indent_goalcol(varname, ast_variable_modifier(node)));
		} else {
			libias_array_append(this->nodes, node);
			this->moving_goalcol = MAX(
				rules_indent_goalcol(varname, ast_variable_modifier(node)),
				this->moving_goalcol);
		}
		libias_free(varname);
	}
}

void
ast_format_prepare(
	struct ASTFormat *this,
	struct AST *node)
{
	if (!(this->settings.flags & AST_FORMAT_PRESERVE_ADJACENT_VARIABLES)) {
		ast_format_prepare_collapse_adjacent_variables(node);
	}

	struct ASTFormatPrepare state = {
		.ast_format = this,
		.nodes = libias_array_new(),
		.moving_goalcol = 0,
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_comment = ast_format_prepare_visit_comment;
	visit_trait.visit_variable = ast_format_prepare_visit_variable;

	struct ASTVisit *visit = ast_visit_new(node, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	ast_format_prepare_propagate_goalcol(&state);
	libias_cleanup(&state.nodes);
}

int32_t
ast_format_goalcol(
	struct ASTFormat *this,
	struct AST *node)
{
	int32_t *goalcol = libias_map_get(this->goalcols, node);
	if (goalcol) {
		return *goalcol;
	} else {
		return 0;
	}
}

void
ast_format_set_goalcol(
	struct ASTFormat *this,
	struct AST *node,
	int32_t goalcol)
{
	int32_t *storage = libias_mempool_alloc(this->pool, int32_t);
	*storage = goalcol;
	libias_map_add(this->goalcols, node, storage);
}
