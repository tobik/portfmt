// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/hashset.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/format/normalize_append_modifier.h"
#include "ast/word.h"

struct NormalizeAppendModifierState {
	struct LibiasHashset *seen;
};

// Prototypes
static void normalize_append_modifier_visit_include(struct ASTVisit *visit, struct AST *node);
static void normalize_append_modifier_visit_variable(struct ASTVisit *visit, struct AST *node);

void
normalize_append_modifier_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (ast_ports_framework_include_p(node)) {
		ast_visit_stop(visit);
	}
}

void
normalize_append_modifier_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct NormalizeAppendModifierState *this = ast_visit_context(visit);
	struct ASTWord *name = ast_variable_name(node);
	if (libias_hashset_contains_p(this->seen, name)) {
		if (ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_APPEND) {
			ast_visit_format(visit, node);
		} else {
			libias_hashset_remove(this->seen, name);
		}
	} else {
		libias_hashset_add(this->seen, name);
		if (ast_word_strcmp(name, "CXXFLAGS") != 0 &&
			ast_word_strcmp(name, "CFLAGS") != 0 &&
			ast_word_strcmp(name, "LDFLAGS") != 0 &&
			ast_word_strcmp(name, "RUSTFLAGS") != 0 &&
			ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_APPEND) {
			const struct AST *parent = ast_parent(node);
			if (!ast_if_p(parent) && !ast_if_branch_p(parent) && !ast_for_p(parent)) {
				auto ast_edit = ast_edit_new(
					node,
					ast_visit_diagnostic(visit),
					AST_EDIT_DEFAULT);
				ast_edit_set_variable_modifier(ast_edit, AST_VARIABLE_MODIFIER_ASSIGN);
				ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);
			}
			ast_visit_format(visit, node);
		}
	}
}

// Normalize += before bsd.options.mk
void
ast_format_normalize_append_modifier(
	struct AST *root,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings,
	struct Diagnostic *diagnostic)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_include = normalize_append_modifier_visit_include;
	visit_trait.visit_pre_variable = normalize_append_modifier_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_format_settings(visit, rules, format_settings);

	libias_scope_hashset(seen, ast_word_compare, ast_word_hash);
	ast_visit_set_context(
		visit,
		&(struct NormalizeAppendModifierState){
			.seen = seen,
		});
	ast_visit_run(visit);
	libias_cleanup(&visit);
}
