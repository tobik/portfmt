// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTWordType {
	AST_WORD_STRING,
	AST_WORD_APPEND,
	AST_WORD_BOOL,
	AST_WORD_EXPAND,
	AST_WORD_WHITESPACE,
	AST_WORD_COMMENT,
};

const char *ASTWordType_tostring(enum ASTWordType);

enum ASTWordBoolType {
	AST_WORD_BOOL_AND,         // bmake:"&&"
	AST_WORD_BOOL_OR,          // bmake:"||"
	AST_WORD_BOOL_NOT,         // bmake:"!"
	AST_WORD_BOOL_EQ,          // bmake:"=="
	AST_WORD_BOOL_NEQ,         // bmake:"!="
	AST_WORD_BOOL_GE,          // bmake:">="
	AST_WORD_BOOL_GT,          // bmake:">"
	AST_WORD_BOOL_LE,          // bmake:"<="
	AST_WORD_BOOL_LT,          // bmake:"<"
	AST_WORD_BOOL_GROUP_START, // bmake:"("
	AST_WORD_BOOL_GROUP_END,   // bmake:")"
};

libias_attr_nonnull(1)
bool ASTWordBoolType_from_bmake(const char *, size_t, enum ASTWordBoolType *);

libias_attr_returns_nonnull
const char *ASTWordBoolType_bmake(enum ASTWordBoolType);

libias_attr_returns_nonnull
const char *ASTWordBoolType_tostring(enum ASTWordBoolType);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_append(void);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_expand(void);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_expand_parentheses(void);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_expand_single(void);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_bool(enum ASTWordBoolType);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_string(const char *);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_whitespace(const char *);

libias_attr_returns_nonnull
struct ASTWord *ast_word_new_comment(const char *);

libias_attr_nonnull(1)
void ast_word_cleanup(struct ASTWord **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWord *ast_word_clone(const struct ASTWord *);

libias_attr_nonnull(1)
bool ast_word_bool_p(const struct ASTWord *);

libias_attr_nonnull(1)
bool ast_word_string_p(const struct ASTWord *);

libias_attr_nonnull(1)
bool ast_word_whitespace_p(const struct ASTWord *);

libias_attr_nonnull(1)
bool ast_word_comment_p(const struct ASTWord *);

libias_attr_nonnull(1)
bool ast_word_meta_p(const struct ASTWord *);

libias_attr_nonnull(1)
enum ASTWordType ast_word_type(const struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_word_add_child(struct ASTWord *, struct ASTWord *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *ast_word_children(struct ASTWord *, struct LibiasMempool *);

libias_attr_nonnull(1)
bool ast_word_empty_p(struct ASTWord *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWord *ast_word_unwrap(struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_word_add_expand_modifier(struct ASTWord *, struct ASTWordExpandModifier *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *ast_word_expand_modifiers(struct ASTWord *, struct LibiasMempool *);

libias_attr_nonnull(1)
bool ast_word_expand_p(struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_word_set_value(struct ASTWord *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
const char *ast_word_value(const struct ASTWord *);

libias_attr_nonnull(1)
void ast_word_set_bool_type(struct ASTWord *, const enum ASTWordBoolType);

libias_attr_nonnull(1)
enum ASTWordBoolType ast_word_bool_type(const struct ASTWord *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWord *ast_word_unwrap(struct ASTWord *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWord *ast_word_wrap(struct ASTWord *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *ast_word_flatten(struct ASTWord *);

libias_attr_nonnull(1, 2)
void ast_word_render(struct ASTWord *, struct LibiasBuffer *);

libias_attr_nonnull(1, 2)
void ast_word_sexp(struct ASTWord *, struct SexpWriter *);

libias_attr_nonnull(1, 2)
int ast_word_strcmp(struct ASTWord *, const char *);

extern struct LibiasCompareTrait *ast_word_compare;
extern struct LibiasHashTrait *ast_word_hash;

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_word_children_iterator(struct ASTWord *, ssize_t, ssize_t);

#define ast_word_children_foreach_slice(NODE, A, B, VAR) \
	libias_iterator_foreach(ast_word_children_iterator(NODE, A, B), struct ASTWord *, VAR, struct ASTWord *, libias_macro_gensym(VAR))
#define ast_word_children_foreach(NODE, VAR) \
	ast_word_children_foreach_slice(NODE, 0, -1, VAR)
