// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/hashmap.h>
#include <libias/hashset.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/str.h>
#include <libias/trait/hash.h>

#include "ast.h"
#include "ast/query.h"
#include "ast/word.h"
#include "constants.h"

struct ASTQueryCache {
	struct LibiasMempool *pool;

	struct ASTWord *masterdir;
	struct ASTWord *portname;

	struct LibiasHashset *options;             // {char *}
	struct LibiasHashmap *option_descriptions; // {char * -> char *}
	struct LibiasHashset *option_groups;       // {char *}
	struct LibiasHashset *uses;                // {char *}
	struct LibiasHashset *flavors;             // {char *}
	struct LibiasHashset *licenses;            // {char *}
	struct LibiasHashset *subpackages;         // {char *}
	struct LibiasHashset *shebang_langs;       // {char *}
	struct LibiasHashset *post_plist_targets;  // {char *}
	struct LibiasHashset *cabal_executables;   // {char *}
};

struct ASTLookupTargetState {
	const char *name;
	struct AST *retval;
};

struct ASTLookupVariableState {
	const char *name;
	uint32_t behavior;
	struct LibiasArray *nodes;
};

// Prototypes
static struct ASTQueryCache *ast_get_or_create_query_cache(struct AST *node);
static struct ASTQueryCache *ast_lookup_cabal_executables(struct AST *node);
static struct ASTQueryCache *ast_lookup_flavors(struct AST *node);
static struct ASTQueryCache *ast_lookup_licenses(struct AST *node);
static void ast_lookup_options_add_from_group(struct AST *ast, const char *groupname);
static void ast_lookup_options_add_from_var(struct AST *root, const char *var);
static struct ASTQueryCache *ast_lookup_options_helper(struct AST *node);
static struct ASTQueryCache *ast_lookup_post_plist_targets(struct AST *node);
static struct ASTQueryCache *ast_lookup_shebang_langs(struct AST *node);
static struct ASTQueryCache *ast_lookup_subpackages(struct AST *node);
static void ast_lookup_target_visit_target_rule_targets(struct ASTVisit *visit, struct AST *node);
static struct ASTQueryCache *ast_lookup_uses(struct AST *node);
static void ast_lookup_variable_values_with_options(struct AST *root, struct LibiasMempool *pool, struct LibiasHashset *set, const char *var);
static void ast_lookup_variable_values_with_options_helper(struct LibiasMempool *pool, struct LibiasHashset *set, const char *var, char *value);
static bool ast_lookup_variable_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void ast_lookup_variable_visit_variable(struct ASTVisit *visit, struct AST *node);
static void ast_query_cache_cleanup(struct ASTQueryCache **this_ptr);

struct ASTQueryCache *
ast_get_or_create_query_cache(struct AST *node)
{
	auto query_cache = ast_get_query_cache(node);
	if (!query_cache) {
		query_cache = libias_alloc(struct ASTQueryCache);
		query_cache->pool = libias_mempool_new();
		ast_set_query_cache(node, query_cache, ast_query_cache_cleanup);
	}
	return query_cache;
}

void
ast_query_cache_cleanup(struct ASTQueryCache **this_ptr)
{
	auto this = *this_ptr;
	if (this) {
		libias_cleanup(&this->option_descriptions);
		libias_cleanup(&this->option_groups);
		libias_cleanup(&this->options);
		libias_cleanup(&this->uses);
		libias_cleanup(&this->flavors);
		libias_cleanup(&this->licenses);
		libias_cleanup(&this->subpackages);
		libias_cleanup(&this->shebang_langs);
		libias_cleanup(&this->post_plist_targets);
		libias_cleanup(&this->cabal_executables);
		libias_cleanup(&this->pool);
		libias_free(this);
		*this_ptr = NULL;
	}
}

void
ast_lookup_target_visit_target_rule_targets(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTLookupTargetState *this = ast_visit_context(visit);
	ast_words_foreach(node, src) {
		if (ast_word_meta_p(src)) {
			continue;
		}
		char *value = ast_word_flatten(src);
		libias_scope_free(value);
		if (strcmp(value, this->name) == 0) {
			this->retval = node;
			ast_visit_stop(visit);
			return;
		}
	}
}

struct AST *
ast_lookup_target(struct AST *root, const char *name)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_pre_target_rule_targets = ast_lookup_target_visit_target_rule_targets;

	struct ASTLookupTargetState state = {
		.name = name,
		.retval = NULL,
	};

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);

	return state.retval;
}

void
ast_lookup_variable_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTLookupVariableState *this = ast_visit_context(visit);
	if (ast_word_strcmp(ast_variable_name(node), this->name) == 0) {
		libias_array_append(this->nodes, node);
		if (this->behavior & AST_LOOKUP_VARIABLE_FIRST) {
			ast_visit_stop(visit);
			return;
		}
	}
}

bool
ast_lookup_variable_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTLookupVariableState *this = ast_visit_context(visit);
	switch (ast_type(node)) {
	case AST_FOR:
	case AST_IF:
	case AST_INCLUDE:
		if (this->behavior & AST_LOOKUP_VARIABLE_IGNORE_CONDITIONALS) {
			return false;
		}
		break;
	default:
		break;
	}

	return true;
}

struct AST *
ast_lookup_variable(
	struct AST *root,
	const char *name,
	uint32_t behavior)
{
	struct LibiasArray *nodes = ast_lookup_variables(root, name, behavior);
	if (nodes) {
		struct AST *node = libias_array_get(nodes, 0);
		libias_cleanup(&nodes);
		return node;
	} else {
		return NULL;
	}
}

// Returns an array of nodes that match NAME if there are any or NULL
// otherwise. The caller is responsible to free the returned
// array. There is no need to free its elements since it contains AST
// nodes that are owned by the parser's AST.
struct LibiasArray *
ast_lookup_variables(
	struct AST *root,
	const char *name,
	uint32_t behavior)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = ast_lookup_variable_visit_children_p;
	visit_trait.visit_variable = ast_lookup_variable_visit_variable;

	struct ASTLookupVariableState state = {
		.nodes = libias_array_new(),
		.behavior = behavior,
		.name = name,
	};

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	if (libias_array_len(state.nodes) > 0) {
		return state.nodes;
	} else {
		libias_cleanup(&state.nodes);
		return NULL;
	}
}

// Look up value of MASTERDIR if any in the root of NODE.
struct ASTWord *
ast_lookup_masterdir(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->masterdir) {
		return query_cache->masterdir;
	} else {
		auto var = ast_lookup_variable(
			root,
			"MASTERDIR",
			AST_LOOKUP_VARIABLE_FIRST | AST_LOOKUP_VARIABLE_IGNORE_CONDITIONALS);
		if (var) {
			ast_words_foreach(var, word) {
				if (!ast_word_meta_p(word)) {
					query_cache->masterdir = word;
					return query_cache->masterdir;
				}
			}
		}
	}

	return NULL;
}

// Look up value of PORTNAME if any in the root of NODE.
struct ASTWord *
ast_lookup_portname(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->portname) {
		return query_cache->portname;
	} else {
		auto var = ast_lookup_variable(
			root,
			"PORTNAME",
			AST_LOOKUP_VARIABLE_FIRST | AST_LOOKUP_VARIABLE_IGNORE_CONDITIONALS);
		if (var) {
			ast_words_foreach(var, word) {
				if (!ast_word_meta_p(word)) {
					query_cache->portname = word;
					return query_cache->portname;
				}
			}
		}
	}

	return NULL;
}

void
ast_lookup_options_add_from_group(
	struct AST *ast,
	const char *groupname)
{
	auto root = ast_root(ast);
	auto query_cache = ast_get_or_create_query_cache(root);
	auto nodes = ast_lookup_variables(
		root,
		groupname,
		AST_LOOKUP_VARIABLE_DEFAULT);
	if (!nodes) {
		return;
	}
	libias_scope_free(nodes);

	libias_array_foreach(nodes, struct AST *, node) {
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			char *optgroupname = ast_word_flatten(word);
			char *optgroupvar = libias_str_printf("%s_%s", groupname, optgroupname);
			libias_scope_free(optgroupvar);
			if (libias_hashset_contains_p(query_cache->option_groups, optgroupname)) {
				libias_free(optgroupname);
			} else {
				libias_mempool_take(query_cache->pool, optgroupname);
				libias_hashset_add(query_cache->option_groups, optgroupname);
			}
			auto nodes = ast_lookup_variables(
				root,
				optgroupvar,
				AST_LOOKUP_VARIABLE_DEFAULT);
			if (nodes) {
				libias_scope_free(nodes);
				libias_array_foreach(nodes, struct AST *, node) {
					ast_words_foreach(node, word) {
						if (ast_word_meta_p(word)) {
							continue;
						}
						char *option = ast_word_flatten(word);
						if (libias_hashset_contains_p(query_cache->options, option)) {
							libias_free(option);
						} else {
							libias_mempool_take(query_cache->pool, option);
							libias_hashset_add(
								query_cache->options,
								option);
						}
					}
				}
			}
		}
	}
}

void
ast_lookup_options_add_from_var(
	struct AST *root,
	const char *var)
{
	auto query_cache = ast_get_or_create_query_cache(root);
	auto nodes = ast_lookup_variables(
		root,
		var,
		AST_LOOKUP_VARIABLE_DEFAULT);
	if (!nodes) {
		return;
	}
	libias_scope_free(nodes);

	libias_array_foreach(nodes, struct AST *, node) {
		ast_words_foreach(node, word) {
			if (ast_word_meta_p(word)) {
				continue;
			}
			char *option = ast_word_flatten(word);
			if (libias_hashset_contains_p(query_cache->options, option)) {
				libias_free(option);
			} else {
				libias_mempool_take(query_cache->pool, option);
				libias_hashset_add(
					query_cache->options,
					option);
			}
		}
	}
}

struct ASTQueryCache *
ast_lookup_options_helper(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->options) {
		return query_cache;
	}

	query_cache->options = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);
	query_cache->option_groups = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);
	query_cache->option_descriptions = libias_hashmap_new(
		libias_str_compare,
		libias_hash_str);

#define FOR_EACH_ARCH(f, var) \
	for (size_t i = 0; i < known_architectures_len; i++) { \
		char *buf = libias_str_printf("%s_%s", var, known_architectures[i]); \
		f(root, buf); \
		libias_free(buf); \
	}

	ast_lookup_options_add_from_var(root, "OPTIONS_DEFINE");
	FOR_EACH_ARCH(ast_lookup_options_add_from_var, "OPTIONS_DEFINE");

	ast_lookup_options_add_from_group(root, "OPTIONS_GROUP");
	FOR_EACH_ARCH(ast_lookup_options_add_from_group, "OPTIONS_GROUP");

	ast_lookup_options_add_from_group(root, "OPTIONS_MULTI");
	FOR_EACH_ARCH(ast_lookup_options_add_from_group, "OPTIONS_MULTI");

	ast_lookup_options_add_from_group(root, "OPTIONS_RADIO");
	FOR_EACH_ARCH(ast_lookup_options_add_from_group, "OPTIONS_RADIO");

	ast_lookup_options_add_from_group(root, "OPTIONS_SINGLE");
	FOR_EACH_ARCH(ast_lookup_options_add_from_group, "OPTIONS_SINGLE");

#undef FOR_EACH_ARCH

	struct LibiasHashset *opts[] = {
		query_cache->options,
		query_cache->option_groups,
	};
	for (size_t i = 0; i < nitems(opts); i++) {
		if (!opts[i]) {
			continue;
		}
		libias_hashset_foreach(opts[i], const char *, opt) {
			char *var = libias_str_printf("%s_DESC", opt);
			libias_scope_free(var);
			if (!libias_hashmap_contains_p(query_cache->option_descriptions, var)) {
				auto node = ast_lookup_variable(
					root,
					var,
					AST_LOOKUP_VARIABLE_FIRST);
				if (node) {
					char *desc = ast_words_no_meta_flatten(node, " ");
					libias_mempool_take(query_cache->pool, desc);
					char *var_copy = libias_str_dup(var);
					libias_mempool_take(query_cache->pool, var_copy);
					libias_hashmap_add(
						query_cache->option_descriptions,
						var_copy,
						desc);
				}
			}
		}
	}

	return query_cache;
}

struct LibiasIterator *
ast_lookup_option_descriptions_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_options_helper(node);
	return libias_hashmap_iterator(query_cache->option_descriptions, a, b);
}


struct LibiasIterator *
ast_lookup_option_groups_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_options_helper(node);
	return libias_hashset_iterator(query_cache->option_groups, a, b);
}

struct LibiasIterator *
ast_lookup_options_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_options_helper(node);
	return libias_hashset_iterator(query_cache->options, a, b);
}

bool
ast_lookup_option_group_p(
	struct AST *node,
	const char *group)
{
	auto query_cache = ast_lookup_options_helper(node);
	return libias_hashset_contains_p(query_cache->option_groups, group);
}

bool
ast_lookup_option_p(
	struct AST *node,
	const char *option)
{
	auto query_cache = ast_lookup_options_helper(node);
	return libias_hashset_contains_p(query_cache->options, option);
}

void
ast_lookup_variable_values_with_options_helper(
	struct LibiasMempool *pool,
	struct LibiasHashset *set,
	const char *var,
	char *value)
{
	if (strcmp(var, "USES") == 0) {
		char *buf = strchr(value, ':');
		if (buf != NULL) {
			char *val = libias_str_ndup(value, buf - value);
			if (libias_hashset_contains_p(set, val)) {
				libias_free(val);
			} else {
				libias_mempool_take(pool, val);
				libias_hashset_add(set, val);
			}
			return;
		}
	}

	if (!libias_hashset_contains_p(set, value)) {
		char *value_copy = libias_str_dup(value);
		libias_mempool_take(pool, value_copy);
		libias_hashset_add(set, value_copy);
	}
}

void
ast_lookup_variable_values_with_options(
	struct AST *root,
	struct LibiasMempool *pool,
	struct LibiasHashset *set,
	const char *var)
{
	auto nodes = ast_lookup_variables(
		root,
		var,
		AST_LOOKUP_VARIABLE_DEFAULT);
	if (nodes) {
		libias_scope_free(nodes);
		libias_array_foreach(nodes, struct AST *, node) {
			ast_words_foreach(node, word) {
				if (ast_word_meta_p(word)) {
					continue;
				}
				char *value = ast_word_flatten(word);
				ast_lookup_variable_values_with_options_helper(pool, set, var, value);
				libias_free(value);
			}
		}
	}

	ast_lookup_options_foreach(root, opt) {
		char *buf = libias_str_printf("%s_VARS", opt);
		libias_scope_free(buf);
		nodes = ast_lookup_variables(
			root,
			buf,
			AST_LOOKUP_VARIABLE_DEFAULT);
		if (nodes) {
			libias_scope_free(nodes);
			libias_array_foreach(nodes, struct AST *, node) {
				ast_words_foreach(node, word) {
					if (ast_word_meta_p(word)) {
						continue;
					}
					char *value = ast_word_flatten(word);
					libias_scope_free(value);
					char *buf = libias_str_printf("%s+=", var);
					libias_scope_free(buf);
					if (libias_str_prefix_p(value, buf)) {
						value += strlen(buf);
					} else {
						buf = libias_str_printf("%s=", var);
						libias_scope_free(buf);
						if (libias_str_prefix_p(value, buf)) {
							value += strlen(buf);
						} else {
							continue;
						}
					}
					ast_lookup_variable_values_with_options_helper(pool, set, var, value);
				}
			}
		}

		buf = libias_str_printf("%s_VARS_OFF", opt);
		libias_scope_free(buf);
		nodes = ast_lookup_variables(
			root,
			buf,
			AST_LOOKUP_VARIABLE_DEFAULT);
		if (nodes) {
			libias_scope_free(nodes);
			libias_array_foreach(nodes, struct AST *, node) {
				ast_words_foreach(node, word) {
					if (ast_word_meta_p(word)) {
						continue;
					}
					char *value = ast_word_flatten(word);
					libias_scope_free(value);
					char *buf = libias_str_printf("%s+=", var);
					libias_scope_free(buf);
					if (libias_str_prefix_p(value, buf)) {
						value += strlen(buf);
					} else {
						buf = libias_str_printf("%s=", var);
						libias_scope_free(buf);
						if (libias_str_prefix_p(value, buf)) {
							value += strlen(buf);
						} else {
							continue;
						}
					}
					ast_lookup_variable_values_with_options_helper(pool, set, var, value);
				}
			}
		}

#if PORTFMT_SUBPACKAGES
		if (strcmp(var, "USES") == 0 || strcmp(var, "SUBPACKAGES") == 0) {
#else
		if (strcmp(var, "USES") == 0) {
#endif
			buf = libias_str_printf("%s_%s", opt, var);
			libias_scope_free(buf);
			nodes = ast_lookup_variables(
				root,
				buf,
				AST_LOOKUP_VARIABLE_DEFAULT);
			if (nodes) {
				libias_scope_free(nodes);
				libias_array_foreach(nodes, struct AST *, node) {
					ast_words_foreach(node, word) {
						if (ast_word_meta_p(word)) {
							continue;
						}
						char *value = ast_word_flatten(word);
						ast_lookup_variable_values_with_options_helper(pool, set, var, value);
						libias_free(value);
					}
				}
			}

			buf = libias_str_printf("%s_%s_OFF", opt, var);
			libias_scope_free(buf);
			nodes = ast_lookup_variables(
				root,
				buf,
				AST_LOOKUP_VARIABLE_DEFAULT);
			if (nodes) {
				libias_scope_free(nodes);
				libias_array_foreach(nodes, struct AST *, node) {
					ast_words_foreach(node, word) {
						if (ast_word_meta_p(word)) {
							continue;
						}
						char *value = ast_word_flatten(word);
						ast_lookup_variable_values_with_options_helper(pool, set, var, value);
						libias_free(value);
					}
				}
			}
		}
	}
}

struct ASTQueryCache *
ast_lookup_uses(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->uses) {
		return query_cache;
	}

	query_cache->uses = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);

	ast_lookup_variable_values_with_options(
		root,
		query_cache->pool,
		query_cache->uses,
		"USES");

	return query_cache;
}

struct LibiasIterator *
ast_lookup_uses_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_uses(node);
	return libias_hashset_iterator(query_cache->uses, a, b);
}

bool
ast_lookup_uses_p(
	struct AST *node,
	const char *uses)
{
	auto query_cache = ast_lookup_uses(node);
	return libias_hashset_contains_p(query_cache->uses, uses);

}

struct ASTQueryCache *
ast_lookup_flavors(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->flavors) {
		return query_cache;
	}

	query_cache->flavors = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);
	// XXX: Does not take into account USE_PYTHON=noflavors etc.
	for (size_t i = 0; i < static_flavors_len; i++) {
		if (ast_lookup_uses_p(root, (void*)static_flavors[i].uses) &&
			!libias_hashset_contains_p(query_cache->flavors, (void*)static_flavors[i].flavor)) {
			auto flavor = libias_str_dup(static_flavors[i].flavor);
			libias_mempool_take(query_cache->pool, flavor);
			libias_hashset_add(query_cache->flavors, flavor);
		}
	}

	ast_lookup_variable_values_with_options(
		root,
		query_cache->pool,
		query_cache->flavors,
		"FLAVORS");

	return query_cache;
}

struct LibiasIterator *
ast_lookup_flavors_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_flavors(node);
	return libias_hashset_iterator(query_cache->flavors, a, b);
}

bool
ast_lookup_flavor_p(
	struct AST *node,
	const char *flavor)
{
	auto query_cache = ast_lookup_flavors(node);
	return libias_hashset_contains_p(query_cache->flavors, flavor);
}

struct ASTQueryCache *
ast_lookup_licenses(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->licenses) {
		return query_cache;
	}

	query_cache->licenses = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);

	ast_lookup_variable_values_with_options(
		root,
		query_cache->pool,
		query_cache->licenses,
		"LICENSE");

	return query_cache;
}

struct LibiasIterator *
ast_lookup_licenses_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_licenses(node);
	return libias_hashset_iterator(query_cache->licenses, a, b);
}

bool
ast_lookup_license_p(
	struct AST *node,
	const char *license)
{
	auto query_cache = ast_lookup_licenses(node);
	return libias_hashset_contains_p(query_cache->licenses, license);
}

struct ASTQueryCache *
ast_lookup_subpackages(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->subpackages) {
		return query_cache;
	}

	query_cache->subpackages = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);

#if PORTFMT_SUBPACKAGES
	// There is always a main subpackage
	auto mainpkg = libias_str_dup("main");
	libias_mempool_take(query_cache->pool, mainpkg);
	libias_hashset_add(query_cache->subpackages, mainpkg);

	ast_lookup_variable_values_with_options(
		root,
		query_cache->pool,
		query_cache->subpackages,
		"SUBPACKAGES");
#endif

	return query_cache;
}

struct LibiasIterator *
ast_lookup_subpackages_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_subpackages(node);
	return libias_hashset_iterator(query_cache->subpackages, a, b);
}

bool
ast_lookup_subpackage_p(
	struct AST *node,
	const char *subpackage)
{
	auto query_cache = ast_lookup_subpackages(node);
	return libias_hashset_contains_p(query_cache->subpackages, subpackage);
}

struct ASTQueryCache *
ast_lookup_shebang_langs(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->shebang_langs) {
		return query_cache;
	}

	query_cache->shebang_langs = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);

	ast_lookup_variable_values_with_options(
		root,
		query_cache->pool,
		query_cache->shebang_langs,
		"SHEBANG_LANG");

	return query_cache;
}

struct LibiasIterator *
ast_lookup_shebang_langs_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_shebang_langs(node);
	return libias_hashset_iterator(query_cache->shebang_langs, a, b);
}

bool
ast_lookup_shebang_lang_p(
	struct AST *node,
	const char *shebang_lang)
{
	auto query_cache = ast_lookup_shebang_langs(node);
	return libias_hashset_contains_p(query_cache->shebang_langs, shebang_lang);
}

struct ASTQueryCache *
ast_lookup_post_plist_targets(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->post_plist_targets) {
		return query_cache;
	}

	query_cache->post_plist_targets = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);

	ast_lookup_variable_values_with_options(
		root,
		query_cache->pool,
		query_cache->post_plist_targets,
		"POST_PLIST");

	return query_cache;
}

struct LibiasIterator *
ast_lookup_post_plist_targets_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_post_plist_targets(node);
	return libias_hashset_iterator(query_cache->post_plist_targets, a, b);
}

bool
ast_lookup_post_plist_target_p(
	struct AST *node,
	const char *post_plist_target)
{
	auto query_cache = ast_lookup_post_plist_targets(node);
	return libias_hashset_contains_p(query_cache->post_plist_targets, post_plist_target);
}

struct ASTQueryCache *
ast_lookup_cabal_executables(struct AST *node)
{
	auto root = ast_root(node);
	auto query_cache = ast_get_or_create_query_cache(root);

	if (query_cache->cabal_executables) {
		return query_cache;
	}

	query_cache->cabal_executables = libias_hashset_new(
		libias_str_compare,
		libias_hash_str);

	if (ast_lookup_uses_p(root, "cabal")) {
		ast_lookup_variable_values_with_options(
			root,
			query_cache->pool,
			query_cache->cabal_executables,
			"CABAL_EXECUTABLES");
		if (libias_hashset_len(query_cache->cabal_executables) == 0) {
			auto portname = ast_lookup_portname(root);
			if (portname) {
				char *portname_value = ast_word_flatten(portname);
				if (!libias_hashset_contains_p(query_cache->cabal_executables, portname_value)) {
					libias_mempool_take(query_cache->pool, portname_value);
					libias_hashset_add(query_cache->cabal_executables, portname_value);
				}
			}
		}
	}

	return query_cache;
}

struct LibiasIterator *
ast_lookup_cabal_executables_iterator(
	struct AST *node,
	ssize_t a,
	ssize_t b)
{
	auto query_cache = ast_lookup_cabal_executables(node);
	return libias_hashset_iterator(query_cache->cabal_executables, a, b);
}

bool
ast_lookup_cabal_executable_p(
	struct AST *node,
	const char *cabal_executable)
{
	auto query_cache = ast_lookup_cabal_executables(node);
	return libias_hashset_contains_p(query_cache->cabal_executables, cabal_executable);
}
