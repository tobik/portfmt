// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTWordExpandModifierType {
	AST_WORD_EXPAND_MODIFIER_APPEND,              // bmake:":+="       sexp:":+="
	AST_WORD_EXPAND_MODIFIER_BANG,                // bmake:"!"         sexp:"!"
	AST_WORD_EXPAND_MODIFIER_CMDSET,              // bmake:":!="       sexp:":!="
	AST_WORD_EXPAND_MODIFIER_D,                   // bmake:"D"         sexp:"D"
	AST_WORD_EXPAND_MODIFIER_E,                   // bmake:"E"         sexp:"E"
	AST_WORD_EXPAND_MODIFIER_FOR,                 // bmake:"@"         sexp:"@"
	AST_WORD_EXPAND_MODIFIER_H,                   // bmake:"H"         sexp:"H"
	AST_WORD_EXPAND_MODIFIER_IF,                  // bmake:"?"         sexp:"?"
	AST_WORD_EXPAND_MODIFIER_L,                   // bmake:"L"         sexp:"L"
	AST_WORD_EXPAND_MODIFIER_M,                   // bmake:"M"         sexp:"M"
	AST_WORD_EXPAND_MODIFIER_N,                   // bmake:"N"         sexp:"N"
	AST_WORD_EXPAND_MODIFIER_O,                   // bmake:"O"         sexp:"O"
	AST_WORD_EXPAND_MODIFIER_OPTSET,              // bmake:":?="       sexp:":?="
	AST_WORD_EXPAND_MODIFIER_On,                  // bmake:"On"        sexp:"On"
	AST_WORD_EXPAND_MODIFIER_Or,                  // bmake:"Or"        sexp:"Or"
	AST_WORD_EXPAND_MODIFIER_Orn,                 // bmake:"Orn"       sexp:"Orn"
	AST_WORD_EXPAND_MODIFIER_Ox,                  // bmake:"Ox"        sexp:"Ox"
	AST_WORD_EXPAND_MODIFIER_P,                   // bmake:"P"         sexp:"P"
	AST_WORD_EXPAND_MODIFIER_Q,                   // bmake:"Q"         sexp:"Q"
	AST_WORD_EXPAND_MODIFIER_R,                   // bmake:"R"         sexp:"R"
	AST_WORD_EXPAND_MODIFIER_SET,                 // bmake:":="        sexp:":="
	AST_WORD_EXPAND_MODIFIER_SLICE_EXPAND,        // bmake:"["         sexp:"slice-expand"
	AST_WORD_EXPAND_MODIFIER_SLICE_INDEX,         // bmake:"["         sexp:"slice-index"
	AST_WORD_EXPAND_MODIFIER_SLICE_LENGTH,        // bmake:"[#"        sexp:"slice-length"
	AST_WORD_EXPAND_MODIFIER_SLICE_RANGE,         // bmake:"["         sexp:"slice-range"
	AST_WORD_EXPAND_MODIFIER_SLICE_SINGLE_WORD,   // bmake:"[*"        sexp:"slice-single-word"
	AST_WORD_EXPAND_MODIFIER_SLICE_SINGLE_WORD0,  // bmake:"[0"        sexp:"slice-single-word0"
	AST_WORD_EXPAND_MODIFIER_SLICE_WORD_SEQUENCE, // bmake:"[@"        sexp:"slice-word-sequence"
	AST_WORD_EXPAND_MODIFIER_T,                   // bmake:"T"         sexp:"T"
	AST_WORD_EXPAND_MODIFIER_U,                   // bmake:"U"         sexp:"U"
	AST_WORD_EXPAND_MODIFIER_UNDERSCORE,          // bmake:"_"         sexp:"_"
	AST_WORD_EXPAND_MODIFIER_gmtime,              // bmake:"gmtime"    sexp:"gmtime"
	AST_WORD_EXPAND_MODIFIER_hash,                // bmake:"hash"      sexp:"hash"
	AST_WORD_EXPAND_MODIFIER_localtime,           // bmake:"localtime" sexp:"localtime"
	AST_WORD_EXPAND_MODIFIER_q,                   // bmake:"q"         sexp:"q"
	AST_WORD_EXPAND_MODIFIER_range,               // bmake:"range"     sexp:"range"
	AST_WORD_EXPAND_MODIFIER_sh,                  // bmake:"sh"        sexp:"sh"
	AST_WORD_EXPAND_MODIFIER_tA,                  // bmake:"tA"        sexp:"tA"
	AST_WORD_EXPAND_MODIFIER_tW,                  // bmake:"tW"        sexp:"tW"
	AST_WORD_EXPAND_MODIFIER_tl,                  // bmake:"tl"        sexp:"tl"
	AST_WORD_EXPAND_MODIFIER_ts,                  // bmake:"ts"        sexp:"ts"
	AST_WORD_EXPAND_MODIFIER_tu,                  // bmake:"tu"        sexp:"tu"
	AST_WORD_EXPAND_MODIFIER_tw,                  // bmake:"tw"        sexp:"tw"
	AST_WORD_EXPAND_MODIFIER_u,                   // bmake:"u"         sexp:"u"
	AST_WORD_EXPAND_MODIFIER_SUBST,               // bmake:"S"         sexp:"S"
	AST_WORD_EXPAND_MODIFIER_SUBST_EXT,           // bmake:"C"         sexp:"C"
	AST_WORD_EXPAND_MODIFIER_SUBST_SYSV,          // bmake:"="         sexp:"system-v-subst"
	AST_WORD_EXPAND_MODIFIER_LIST,                // bmake:"$"         sexp:"$"
	AST_WORD_EXPAND_MODIFIER_EMPTY,               // bmake:""          sexp:"<empty>"
};

libias_attr_returns_nonnull
const char *ASTWordExpandModifierType_bmake(enum ASTWordExpandModifierType);

libias_attr_returns_nonnull
const char *ASTWordExpandModifierType_sexp(enum ASTWordExpandModifierType);

libias_attr_returns_nonnull
const char *ASTWordExpandModifierType_tostring(enum ASTWordExpandModifierType);

libias_attr_nonnull(2, 4)
libias_attr_returns_nonnull
struct ASTWordExpandModifierBuilder *ast_word_expand_modifier_builder(enum ASTWordExpandModifierType, struct ASTBuilder *, size_t, struct Diagnostic *);

libias_attr_nonnull(1)
void ast_word_expand_modifier_builder_cleanup(struct ASTWordExpandModifierBuilder **);

libias_attr_nonnull(1, 2)
void ast_word_expand_modifier_builder_add_argument(struct ASTWordExpandModifierBuilder *, struct ASTWord *);

libias_attr_nonnull(1)
void ast_word_expand_modifier_builder_set_chained_substitution(struct ASTWordExpandModifierBuilder *, bool);

libias_attr_nonnull(1)
struct ASTWordExpandModifier *ast_word_expand_modifier_builder_finish(struct ASTWordExpandModifierBuilder *);

libias_attr_nonnull(1)
void ast_word_expand_modifier_cleanup(struct ASTWordExpandModifier **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWordExpandModifier *ast_word_expand_modifier_clone(struct ASTWordExpandModifier *);

libias_attr_nonnull(1, 2)
void ast_word_expand_modifier_render(struct ASTWordExpandModifier *, struct LibiasBuffer *);

libias_attr_nonnull(1, 2)
void ast_word_expand_modifier_sexp(struct ASTWordExpandModifier *, struct SexpWriter *);

libias_attr_nonnull(1)
enum ASTWordExpandModifierType ast_word_expand_modifier_type(struct ASTWordExpandModifier *);
