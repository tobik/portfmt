// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast/word.h"
#include "ast/word/expand/modifier.h"
#include "diagnostic.h"
#include "sexp.h"

#define EXPAND_MODIFIER_PARSER_ARGS \
	struct ASTWordExpandModifierBuilder *this, struct ASTWordExpandModifier *modifier, struct LibiasArray *arguments
typedef bool (*ASTWordExpandModifierParser)(EXPAND_MODIFIER_PARSER_ARGS);
typedef void (*ASTWordExpandModifierRender)(struct ASTWordExpandModifier *, struct LibiasBuffer *);
typedef void (*ASTWordExpandModifierSexp)(struct ASTWordExpandModifier *, struct SexpWriter *);

struct ASTWordExpandModifier {
	const enum ASTWordExpandModifierType type;

	struct ASTWord *arg1;
	struct ASTWord *arg2;

	bool subst_chained;
	char *flags;
	char *separator;
	int64_t count;
	int64_t utc;

	struct {
		int64_t start;
		int64_t end;
	} slice;

	void (*free)(struct ASTWordExpandModifier *);
	ASTWordExpandModifierRender render;
	ASTWordExpandModifierSexp sexp;
};

struct ASTWordExpandModifierBuilder {
	// Input buffer and start position offset needed for better diagnostics
	struct ASTBuilder *ast_builder;
	size_t offset;

	struct Diagnostic *diagnostic;
	struct ASTWordExpandModifier *modifier;
	struct LibiasArray *arguments; // [struct ASTWord *]

	ASTWordExpandModifierParser parser;
};

// Prototypes
static void ast_word_expand_modifier_builder_methods(enum ASTWordExpandModifierType type, ASTWordExpandModifierParser *parser, ASTWordExpandModifierRender *render, ASTWordExpandModifierSexp *sexp);
static bool ast_word_expand_modifier_builder_parse_for(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_gmtime(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_if(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_list(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_no_argument(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_one_argument(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_range(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_expand(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_index(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_length(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_range(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_single_word(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_single_word0(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_slice_word_sequence(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_subst(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_subst_sysv(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_ts(EXPAND_MODIFIER_PARSER_ARGS);
static bool ast_word_expand_modifier_builder_parse_underscore(EXPAND_MODIFIER_PARSER_ARGS);
static void ast_word_expand_modifier_render_bang(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_for(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_gmtime(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_if(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_list(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_no_argument(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_one_argument(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_range(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_expand(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_index(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_length(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_range(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_single_word(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_single_word0(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_slice_word_sequence(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_subst(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_subst_sysv(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_ts(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_render_underscore(struct ASTWordExpandModifier *modifier, struct LibiasBuffer *f);
static void ast_word_expand_modifier_sexp_for(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_gmtime(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_if(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_list(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_no_argument(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_one_argument(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_range(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_slice(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_subst(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_subst_sysv(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_ts(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);
static void ast_word_expand_modifier_sexp_underscore(struct ASTWordExpandModifier *modifier, struct SexpWriter *w);

void
ast_word_expand_modifier_builder_methods(
	enum ASTWordExpandModifierType type,
	ASTWordExpandModifierParser *parser,
	ASTWordExpandModifierRender *render,
	ASTWordExpandModifierSexp *sexp)
{
	switch (type) {
	case AST_WORD_EXPAND_MODIFIER_APPEND:
	case AST_WORD_EXPAND_MODIFIER_CMDSET:
	case AST_WORD_EXPAND_MODIFIER_D:
	case AST_WORD_EXPAND_MODIFIER_M:
	case AST_WORD_EXPAND_MODIFIER_N:
	case AST_WORD_EXPAND_MODIFIER_OPTSET:
	case AST_WORD_EXPAND_MODIFIER_SET:
	case AST_WORD_EXPAND_MODIFIER_U:
		*parser = ast_word_expand_modifier_builder_parse_one_argument;
		*render = ast_word_expand_modifier_render_one_argument;
		*sexp = ast_word_expand_modifier_sexp_one_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_E:
	case AST_WORD_EXPAND_MODIFIER_O:
	case AST_WORD_EXPAND_MODIFIER_On:
	case AST_WORD_EXPAND_MODIFIER_Or:
	case AST_WORD_EXPAND_MODIFIER_Orn:
	case AST_WORD_EXPAND_MODIFIER_Ox:
	case AST_WORD_EXPAND_MODIFIER_H:
	case AST_WORD_EXPAND_MODIFIER_L:
	case AST_WORD_EXPAND_MODIFIER_P:
	case AST_WORD_EXPAND_MODIFIER_Q:
	case AST_WORD_EXPAND_MODIFIER_R:
	case AST_WORD_EXPAND_MODIFIER_T:
	case AST_WORD_EXPAND_MODIFIER_hash:
	case AST_WORD_EXPAND_MODIFIER_q:
	case AST_WORD_EXPAND_MODIFIER_sh:
	case AST_WORD_EXPAND_MODIFIER_tA:
	case AST_WORD_EXPAND_MODIFIER_tW:
	case AST_WORD_EXPAND_MODIFIER_tl:
	case AST_WORD_EXPAND_MODIFIER_tu:
	case AST_WORD_EXPAND_MODIFIER_tw:
	case AST_WORD_EXPAND_MODIFIER_u:
		*parser = ast_word_expand_modifier_builder_parse_no_argument;
		*render = ast_word_expand_modifier_render_no_argument;
		*sexp = ast_word_expand_modifier_sexp_no_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_BANG:
		*parser = ast_word_expand_modifier_builder_parse_one_argument;
		*render = ast_word_expand_modifier_render_bang;
		*sexp = ast_word_expand_modifier_sexp_one_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_FOR:
		*parser = ast_word_expand_modifier_builder_parse_for;
		*render = ast_word_expand_modifier_render_for;
		*sexp = ast_word_expand_modifier_sexp_for;
		break;
	case AST_WORD_EXPAND_MODIFIER_IF:
		*parser = ast_word_expand_modifier_builder_parse_if;
		*render = ast_word_expand_modifier_render_if;
		*sexp = ast_word_expand_modifier_sexp_if;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_EXPAND:
		*parser = ast_word_expand_modifier_builder_parse_slice_expand;
		*render = ast_word_expand_modifier_render_slice_expand;
		*sexp = ast_word_expand_modifier_sexp_slice;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_RANGE:
		*parser = ast_word_expand_modifier_builder_parse_slice_range;
		*render = ast_word_expand_modifier_render_slice_range;
		*sexp = ast_word_expand_modifier_sexp_slice;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_INDEX:
		*parser = ast_word_expand_modifier_builder_parse_slice_index;
		*render = ast_word_expand_modifier_render_slice_index;
		*sexp = ast_word_expand_modifier_sexp_slice;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_LENGTH:
		*parser = ast_word_expand_modifier_builder_parse_slice_length;
		*render = ast_word_expand_modifier_render_slice_length;
		*sexp = ast_word_expand_modifier_sexp_no_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_SINGLE_WORD:
		*parser = ast_word_expand_modifier_builder_parse_slice_single_word;
		*render = ast_word_expand_modifier_render_slice_single_word;
		*sexp = ast_word_expand_modifier_sexp_no_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_SINGLE_WORD0:
		*parser = ast_word_expand_modifier_builder_parse_slice_single_word0;
		*render = ast_word_expand_modifier_render_slice_single_word0;
		*sexp = ast_word_expand_modifier_sexp_no_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_WORD_SEQUENCE:
		*parser = ast_word_expand_modifier_builder_parse_slice_word_sequence;
		*render = ast_word_expand_modifier_render_slice_word_sequence;
		*sexp = ast_word_expand_modifier_sexp_no_argument;
		break;
	case AST_WORD_EXPAND_MODIFIER_UNDERSCORE:
		*parser = ast_word_expand_modifier_builder_parse_underscore;
		*render = ast_word_expand_modifier_render_underscore;
		*sexp = ast_word_expand_modifier_sexp_underscore;
		break;
	case AST_WORD_EXPAND_MODIFIER_gmtime:
	case AST_WORD_EXPAND_MODIFIER_localtime:
		*parser = ast_word_expand_modifier_builder_parse_gmtime;
		*render = ast_word_expand_modifier_render_gmtime;
		*sexp = ast_word_expand_modifier_sexp_gmtime;
		break;
	case AST_WORD_EXPAND_MODIFIER_range:
		*parser = ast_word_expand_modifier_builder_parse_range;
		*render = ast_word_expand_modifier_render_range;
		*sexp = ast_word_expand_modifier_sexp_range;
		break;
	case AST_WORD_EXPAND_MODIFIER_ts:
		*parser = ast_word_expand_modifier_builder_parse_ts;
		*render = ast_word_expand_modifier_render_ts;
		*sexp = ast_word_expand_modifier_sexp_ts;
		break;
	case AST_WORD_EXPAND_MODIFIER_SUBST:
	case AST_WORD_EXPAND_MODIFIER_SUBST_EXT:
		*parser = ast_word_expand_modifier_builder_parse_subst;
		*render = ast_word_expand_modifier_render_subst;
		*sexp = ast_word_expand_modifier_sexp_subst;
		break;
	case AST_WORD_EXPAND_MODIFIER_SUBST_SYSV:
		*parser = ast_word_expand_modifier_builder_parse_subst_sysv;
		*render = ast_word_expand_modifier_render_subst_sysv;
		*sexp = ast_word_expand_modifier_sexp_subst_sysv;
		break;
	case AST_WORD_EXPAND_MODIFIER_LIST:
		*parser = ast_word_expand_modifier_builder_parse_list;
		*render = ast_word_expand_modifier_render_list;
		*sexp = ast_word_expand_modifier_sexp_list;
		break;
	case AST_WORD_EXPAND_MODIFIER_EMPTY:
		*parser = ast_word_expand_modifier_builder_parse_no_argument;
		*render = ast_word_expand_modifier_render_no_argument;
		*sexp = ast_word_expand_modifier_sexp_no_argument;
		break;
	}
}

struct ASTWordExpandModifierBuilder *
ast_word_expand_modifier_builder(
	enum ASTWordExpandModifierType type,
	struct ASTBuilder *ast_builder,
	size_t offset,
	struct Diagnostic *diagnostic)
{
	struct ASTWordExpandModifier *modifier = libias_alloc(
		struct ASTWordExpandModifier);

	struct ASTWordExpandModifierBuilder *builder = libias_alloc(
		struct ASTWordExpandModifierBuilder);

	builder->ast_builder = ast_builder;
	builder->offset = offset;
	builder->diagnostic = diagnostic;
	builder->modifier = modifier;
	builder->arguments = libias_array_new();

	ast_word_expand_modifier_builder_methods(
		type,
		&builder->parser,
		&modifier->render,
		&modifier->sexp);

	memcpy(
		(void *)&modifier->type,
		&type,
		sizeof(enum ASTWordExpandModifierType));

	return builder;
}

void
ast_word_expand_modifier_builder_cleanup(
	struct ASTWordExpandModifierBuilder **builder_ptr)
{
	struct ASTWordExpandModifierBuilder *builder = *builder_ptr;
	if (builder) {
		libias_array_foreach(builder->arguments, struct ASTWord *, word) {
			libias_cleanup(&word);
		}
		libias_cleanup(&builder->arguments);
		libias_cleanup(&builder->modifier);
		libias_free(builder);
		*builder_ptr = NULL;
	}
}

// Adds ARGUMENT to the list of arguments for the new expand
// modifier. If ARGUMENT is NULL it is ignored. The builder takes
// ownership of ARGUMENT.
void
ast_word_expand_modifier_builder_add_argument(
	struct ASTWordExpandModifierBuilder *builder,
	struct ASTWord *argument)
{
	libias_array_append(builder->arguments, argument);
}

void
ast_word_expand_modifier_builder_set_chained_substitution(
	struct ASTWordExpandModifierBuilder *builder,
	bool chained)
{
	builder->modifier->subst_chained = chained;
}

// Creates an expand modifier based on the given arguments. Returns
// NULL if that was not possible. BUILDER is freed in any case.
struct ASTWordExpandModifier *
ast_word_expand_modifier_builder_finish(
	struct ASTWordExpandModifierBuilder *builder)
{
	struct ASTWordExpandModifier *modifier = builder->modifier;
	struct LibiasArray *arguments = builder->arguments;
#if PARSER_ASTBUILDER_DEBUG
	fprintf(stderr, "ARGS: %zu\n", libias_array_len(arguments));
#endif

	if (builder->parser(builder, modifier, arguments)) {
		builder->modifier = NULL;
		libias_array_truncate(builder->arguments);
		libias_cleanup(&builder);
		return modifier;
	} else {
		libias_cleanup(&builder);
		return NULL;
	}
}

bool
ast_word_expand_modifier_builder_parse_no_argument(EXPAND_MODIFIER_PARSER_ARGS)
{
	// no arguments
	if (libias_array_len(arguments) == 0) {
		return true;
	} else {
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"expected no arguments for '%s' modifier",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_one_argument(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 1) {
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_for(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 2) {
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		modifier->arg2 = ast_word_unwrap(libias_array_get(arguments, 1));
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 2 arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_if(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 2) {
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		modifier->arg2 = ast_word_unwrap(libias_array_get(arguments, 1));
		return true;
	} else {
		// We should never get into this state since it should have
		// been caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 2 arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_index(EXPAND_MODIFIER_PARSER_ARGS)
{
	libias_scope_mempool(pool);

	if (libias_array_len(arguments) == 1) {
		struct ASTWord *word = ast_word_unwrap(libias_array_get(arguments, 0));
		libias_mempool_take(pool, word);
		if (ast_word_expand_p(word)) {
			return false;
		} else {
			const char *error = NULL;
			modifier->slice.start = modifier->slice.end = strtonum(
				libias_mempool_take(pool, ast_word_flatten(word)),
				INT64_MIN, INT64_MAX,
				&error);
			if (error) {
				diagnostic_parse_error(
					this->diagnostic,
					this->ast_builder,
					this->offset, this->offset,
					"%s modifier index parse error: %s",
					ASTWordExpandModifierType_bmake(
						ast_word_expand_modifier_type(modifier)),
					error);
				return false;
			} else {
				return true;
			}
		}
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_length(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects no arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_single_word(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects no arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_single_word0(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects no arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_word_sequence(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects no arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_expand(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 1) {
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		if (ast_word_expand_p(modifier->arg1)) {
			return true;
		} else {
			diagnostic_parse_error(
				this->diagnostic,
				this->ast_builder,
				this->offset, this->offset,
				"%s modifier expects an expandable word",
				ASTWordExpandModifierType_bmake(
					ast_word_expand_modifier_type(modifier)));
			return false;
		}
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_slice_range(EXPAND_MODIFIER_PARSER_ARGS)
{
	libias_scope_mempool(pool);

	if (libias_array_len(arguments) == 2) {
		struct ASTWord *arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		libias_mempool_take(pool, arg1);
		struct ASTWord *arg2 = ast_word_unwrap(libias_array_get(arguments, 1));
		libias_mempool_take(pool, arg2);

		if (ast_word_expand_p(arg1) || ast_word_expand_p(arg2)) {
			return false;
		} else {
			const char *error = NULL;
			char *arg1_value = ast_word_flatten(arg1);
			modifier->slice.start = strtonum(
				arg1_value,
				INT64_MIN, INT64_MAX,
				&error);
			libias_free(arg1_value);
			if (error) {
				diagnostic_parse_error(
					this->diagnostic,
					this->ast_builder,
					this->offset, this->offset,
					"%s modifier range parse error: %s",
					ASTWordExpandModifierType_bmake(
						ast_word_expand_modifier_type(modifier)),
					error);
				return false;
			} else {
				char *arg2_value = ast_word_flatten(arg2);
				modifier->slice.end = strtonum(
					arg2_value,
					INT64_MIN, INT64_MAX,
					&error);
				libias_free(arg2_value);
				return true;
			}
		}

		// TODO: Wrong :-)
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_underscore(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		return true;
	} else {
		return ast_word_expand_modifier_builder_parse_one_argument(
			this,
			modifier,
			arguments);
	}
}

bool
ast_word_expand_modifier_builder_parse_gmtime(EXPAND_MODIFIER_PARSER_ARGS)
{
	libias_scope_mempool(pool);

	if (libias_array_len(arguments) == 0) {
		modifier->utc = 0;
		return true;
	} else if (libias_array_len(arguments) == 1) {
		struct ASTWord *word = libias_array_get(arguments, 0);
		libias_mempool_take(pool, word);

		const char *error = NULL;
		char *value = ast_word_flatten(word);
		modifier->utc = strtonum(
			value,
			0, INT64_MAX,
			&error);
		libias_free(value);
		if (error) {
			return false;
		} else {
			return true;
		}
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_range(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		modifier->count = 0;
		return true;
	} else if (libias_array_len(arguments) == 1) {
		struct ASTWord *word = libias_array_get(arguments, 0);
		const char *error = NULL;
		char *value = ast_word_flatten(word);
		modifier->count = strtonum(
			value,
			0, INT64_MAX,
			&error);
		libias_free(value);
		libias_cleanup(&word);
		if (error) {
			return false;
		} else {
			return true;
		}
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

bool
ast_word_expand_modifier_builder_parse_ts(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 0) {
		modifier->separator = libias_str_dup("");
	} else if (libias_array_len(arguments) == 1) {
		struct ASTWord *separator = ast_word_unwrap(libias_array_get(arguments, 0));
		modifier->separator = ast_word_flatten(separator);
		libias_cleanup(&separator);
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 0 or 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}

	return true;
}

bool
ast_word_expand_modifier_builder_parse_subst(EXPAND_MODIFIER_PARSER_ARGS)
{
	libias_scope_mempool(pool);

	if (libias_array_len(arguments) >= 3) {
		struct ASTWord *delimiter = ast_word_unwrap(libias_array_get(arguments, 0));
		if (delimiter && ast_word_string_p(delimiter)) {
			modifier->separator = ast_word_flatten(delimiter);
			libias_cleanup(&delimiter);
		} else {
			const char *value = "(null)";
			if (delimiter) {
				value = libias_mempool_take(pool, ast_word_flatten(delimiter));
			}
			diagnostic_parse_error(
				this->diagnostic,
				this->ast_builder,
				this->offset, this->offset,
				"invalid substitution delimiter: %s",
				value);
			return false;
		}

		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 1));
		if (!modifier->arg1) {
			diagnostic_parse_error(
				this->diagnostic,
				this->ast_builder,
				this->offset, this->offset,
				"invalid substitution pattern: %s",
				ast_word_flatten(modifier->arg1));
			return false;
		}

		modifier->arg2 = ast_word_unwrap(libias_array_get(arguments, 2));
		if (!modifier->arg2) {
			diagnostic_parse_error(
				this->diagnostic,
				this->ast_builder,
				this->offset, this->offset,
				"invalid substitution replacement: %s",
				ast_word_flatten(modifier->arg2));
			return false;
		}

		struct ASTWord *flags = libias_array_get(arguments, 3);
		if (flags) {
			flags = ast_word_unwrap(flags);
		}
		if (flags) {
			if (!ast_word_string_p(flags)) {
				diagnostic_parse_error(
					this->diagnostic,
					this->ast_builder,
					this->offset, this->offset,
					"invalid substitution modifier: %s",
					ast_word_flatten(flags));
				return false;
			}
			modifier->flags = ast_word_flatten(flags);
			libias_cleanup(&flags);
		}
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects at least 3 arguments but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}

	return true;
}

bool
ast_word_expand_modifier_builder_parse_subst_sysv(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 2) {
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		if (!modifier->arg1) {
			diagnostic_parse_error(
				this->diagnostic,
				this->ast_builder,
				this->offset, this->offset,
				"invalid substitution pattern: %s",
				ast_word_flatten(modifier->arg1));
			return false;
		}

		modifier->arg2 = ast_word_unwrap(libias_array_get(arguments, 1));
		if (!modifier->arg2) {
			diagnostic_parse_error(
				this->diagnostic,
				this->ast_builder,
				this->offset, this->offset,
				"invalid substitution replacement: %s",
				ast_word_flatten(modifier->arg2));
			return false;
		}
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"Sysem V substitution modifier expects 2 arguments but got %zu",
			libias_array_len(arguments));
		return false;
	}

	return true;
}

bool
ast_word_expand_modifier_builder_parse_list(EXPAND_MODIFIER_PARSER_ARGS)
{
	if (libias_array_len(arguments) == 1) {
		modifier->arg1 = ast_word_unwrap(libias_array_get(arguments, 0));
		return true;
	} else {
		// We should never get into this state since it should have been
		// caught earlier
		diagnostic_parse_error(
			this->diagnostic,
			this->ast_builder,
			this->offset, this->offset,
			"%s modifier expects 1 argument but got %zu",
			ASTWordExpandModifierType_bmake(
				ast_word_expand_modifier_type(modifier)),
			libias_array_len(arguments));
		return false;
	}
}

void
ast_word_expand_modifier_cleanup(struct ASTWordExpandModifier **modifier_ptr)
{
	struct ASTWordExpandModifier *modifier = *modifier_ptr;
	if (modifier) {
		libias_cleanup(&modifier->arg1);
		libias_cleanup(&modifier->arg2);
		libias_free(modifier->separator);
		libias_free(modifier->flags);
		libias_free(modifier);
		*modifier_ptr = NULL;
	}
}

struct ASTWordExpandModifier *
ast_word_expand_modifier_clone(struct ASTWordExpandModifier *template)
{
	struct ASTWordExpandModifier *modifier =
		libias_alloc(struct ASTWordExpandModifier);
	memcpy(modifier, template, sizeof(struct ASTWordExpandModifier));

	if (template->arg1) {
		modifier->arg1 = ast_word_clone(template->arg1);
	}

	if (template->arg2) {
		modifier->arg2 = ast_word_clone(template->arg2);
	}

	if (template->separator) {
		modifier->separator = libias_str_dup(template->separator);
	}

	if (template->flags) {
		modifier->flags = libias_str_dup(template->flags);
	}

	return modifier;
}

void
ast_word_expand_modifier_render(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	modifier->render(modifier, f);
}

void
ast_word_expand_modifier_render_no_argument(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
}

void
ast_word_expand_modifier_render_one_argument(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	ast_word_render(modifier->arg1, f);
}

void
ast_word_expand_modifier_render_bang(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	ast_word_render(modifier->arg1, f);
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
}

void
ast_word_expand_modifier_render_for(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	ast_word_render(modifier->arg1, f);
	libias_buffer_puts(f, "@");
	ast_word_render(modifier->arg2, f);
	libias_buffer_puts(f, "@");
}

void
ast_word_expand_modifier_render_if(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	ast_word_render(modifier->arg1, f);
	libias_buffer_puts(f, ":");
	ast_word_render(modifier->arg2, f);
}

void
ast_word_expand_modifier_render_slice_index(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, modifier->slice.start);
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_slice_length(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_slice_single_word(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_slice_single_word0(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_slice_word_sequence(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_slice_expand(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	ast_word_render(modifier->arg1, f);
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_slice_range(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, modifier->slice.start);
	libias_buffer_puts(f, "..");
	libias_buffer_puts(f, modifier->slice.end);
	libias_buffer_puts(f, "]");
}

void
ast_word_expand_modifier_render_underscore(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	if (modifier->arg1) {
		libias_buffer_puts(f, "=");
		ast_word_render(modifier->arg1, f);
	}
}

void
ast_word_expand_modifier_render_gmtime(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	if (modifier->utc > 0) {
		libias_buffer_puts(f, "=");
		libias_buffer_puts(f, modifier->utc);
	}
}

void
ast_word_expand_modifier_render_range(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	if (modifier->count > 0) {
		libias_buffer_puts(f, "=");
		libias_buffer_puts(f, modifier->count);
	}
}

void
ast_word_expand_modifier_render_ts(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, modifier->separator);
}

void
ast_word_expand_modifier_render_subst(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	if (!modifier->subst_chained) {
		libias_buffer_puts(f, ":");
	}
	libias_buffer_puts(f, ASTWordExpandModifierType_bmake(ast_word_expand_modifier_type(modifier)));
	libias_buffer_puts(f, modifier->separator);
	ast_word_render(modifier->arg1, f);
	libias_buffer_puts(f, modifier->separator);
	ast_word_render(modifier->arg2, f);
	libias_buffer_puts(f, modifier->separator);
	if (modifier->flags) {
		libias_buffer_puts(f, modifier->flags);
	}
}

void
ast_word_expand_modifier_render_subst_sysv(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	ast_word_render(modifier->arg1, f);
	libias_buffer_puts(f, "=");
	ast_word_render(modifier->arg2, f);
}

void
ast_word_expand_modifier_render_list(
	struct ASTWordExpandModifier *modifier,
	struct LibiasBuffer *f)
{
	libias_buffer_puts(f, ":");
	ast_word_render(modifier->arg1, f);
}

void
ast_word_expand_modifier_sexp(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "modifier");
	sexp_writer_keyword(
		w,
		"type",
		ASTWordExpandModifierType_sexp(ast_word_expand_modifier_type(modifier)));
	modifier->sexp(modifier, w);
	sexp_writer_close_tree(w);
}

void
ast_word_expand_modifier_sexp_no_argument(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
}

void
ast_word_expand_modifier_sexp_one_argument(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "arg");
	ast_word_sexp(modifier->arg1, w);
	sexp_writer_close_tree(w);
}

void
ast_word_expand_modifier_sexp_for(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "identifier");
	ast_word_sexp(modifier->arg1, w);
	sexp_writer_close_tree(w);
	sexp_writer_open_tree(w, "expr");
	ast_word_sexp(modifier->arg2, w);
	sexp_writer_close_tree(w);
}

void
ast_word_expand_modifier_sexp_if(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "then");
	ast_word_sexp(modifier->arg1, w);
	sexp_writer_close_tree(w);
	sexp_writer_open_tree(w, "orelse");
	ast_word_sexp(modifier->arg2, w);
	sexp_writer_close_tree(w);
}

void
ast_word_expand_modifier_sexp_slice(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	switch (modifier->type) {
	case AST_WORD_EXPAND_MODIFIER_SLICE_EXPAND:
		sexp_writer_open_tree(w, "expand");
		ast_word_sexp(modifier->arg1, w);
		sexp_writer_close_tree(w);
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_INDEX:
		sexp_writer_int64(w, "index", modifier->slice.start);
		break;
	case AST_WORD_EXPAND_MODIFIER_SLICE_RANGE:
		sexp_writer_int64(w, "range", modifier->slice.start, modifier->slice.end);
		break;
	default:
		libias_panic("unreachable");
	}
}

void
ast_word_expand_modifier_sexp_underscore(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	if (modifier->arg1) {
		sexp_writer_open_tree(w, "var");
		ast_word_sexp(modifier->arg1, w);
		sexp_writer_close_tree(w);
	}
}

void
ast_word_expand_modifier_sexp_gmtime(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_int64(w, "utc", modifier->utc);
}

void
ast_word_expand_modifier_sexp_range(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_int64(w, "count", modifier->count);
}

void
ast_word_expand_modifier_sexp_ts(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_string(w, "separator", modifier->separator);
}

void
ast_word_expand_modifier_sexp_subst(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	if (modifier->separator) {
		sexp_writer_string(w, "delimiter", modifier->separator);
	}
	sexp_writer_open_tree(w, "pattern");
	ast_word_sexp(modifier->arg1, w);
	sexp_writer_close_tree(w);
	sexp_writer_open_tree(w, "replacement");
	ast_word_sexp(modifier->arg2, w);
	sexp_writer_close_tree(w);
	sexp_writer_bool(w, "chained?", modifier->subst_chained);
	if (modifier->flags && *modifier->flags) {
		sexp_writer_string(w, "flags", modifier->flags);
	}
}

void
ast_word_expand_modifier_sexp_subst_sysv(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "pattern");
	ast_word_sexp(modifier->arg1, w);
	sexp_writer_close_tree(w);
	sexp_writer_open_tree(w, "replacement");
	ast_word_sexp(modifier->arg2, w);
	sexp_writer_close_tree(w);
}

void
ast_word_expand_modifier_sexp_list(
	struct ASTWordExpandModifier *modifier,
	struct SexpWriter *w)
{
	sexp_writer_open_tree(w, "list");
	ast_word_sexp(modifier->arg1, w);
	sexp_writer_close_tree(w);
}

enum ASTWordExpandModifierType
ast_word_expand_modifier_type(struct ASTWordExpandModifier *modifier)
{
	return modifier->type;
}
