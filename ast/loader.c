// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <fcntl.h>
#include <string.h>

#include <libias/io.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/path.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/loader.h"
#include "ast/query.h"
#include "ast/word.h"
#include "diagnostic.h"

struct ASTLoadIncludesState {
	struct AST *root;
	struct LibiasDirectory *portsdir;
	const char *filename;
};

// Prototypes
static bool ast_load_includes_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void ast_load_includes_visit_include(struct ASTVisit *visit, struct AST *node);
static struct LibiasPath *process_include(struct AST *node, struct LibiasMempool *extpool, struct LibiasPath *curdir_path, char *filename);

struct LibiasPath *
process_include(
	struct AST *node,
	struct LibiasMempool *extpool,
	struct LibiasPath *curdir_path,
	char *filename)
{
	libias_scope_mempool(pool);

	if (libias_str_prefix_p(filename, "${MASTERDIR}/")) {
		filename += strlen("${MASTERDIR}/");
		auto masterdir = ast_lookup_masterdir(node);
		if (masterdir) {
			char *masterdir_value = ast_word_flatten(masterdir);
			filename = libias_str_printf("%s/%s", masterdir_value, filename);
			libias_cleanup(&masterdir_value);
		} else {
			filename = libias_str_printf("./%s", filename);
		}
		libias_mempool_take(pool, filename);
	}

	if (strstr(filename, "${PORTNAME}")) {
		auto portname = ast_lookup_portname(node);
		if (portname) {
			// XXX: implement a str_replace()
			auto portname_value = ast_word_flatten(portname);
			filename = libias_str_join(
				libias_str_split(pool, filename, "${PORTNAME}"),
				portname_value);
			libias_cleanup(&portname_value);
			libias_mempool_take(pool, filename);
		}
	}

	const char *curdir = libias_path_data(curdir_path);
	struct LibiasPath *path = NULL;
	if (libias_str_prefix_p(filename, "${.PARSEDIR}/")) {
		path = libias_path_new(curdir, filename + strlen("${.PARSEDIR}/"));
	} else if (libias_str_prefix_p(filename, "${.CURDIR}/")) {
		path = libias_path_new(curdir, filename + strlen("${.CURDIR}/"));
	} else if (libias_str_prefix_p(filename, "${.CURDIR:H}/")) {
		path = libias_path_new(curdir, "..", filename + strlen("${.CURDIR:H}/"));
	} else if (libias_str_prefix_p(filename, "${.CURDIR:H:H}/")) {
		path = libias_path_new(curdir, "..", "..", filename + strlen("${.CURDIR:H:H}/"));
	} else if (libias_str_prefix_p(filename, "${PORTSDIR}/")) {
		const char *relative_path = filename + strlen("${PORTSDIR}/");
		path = libias_path_new(relative_path);
	} else if (libias_str_prefix_p(filename, "${FILESDIR}/")) {
		path = libias_path_new(curdir, "/files/", filename + strlen("${FILESDIR}/"));
	} else {
		path = libias_path_new(curdir, "/", filename);
	}

	libias_mempool_take(extpool, path);
	return path;
}

bool
ast_load_includes_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	switch (ast_type(node)) {
	case AST_FOR:
	case AST_FOR_BINDINGS:
	case AST_FOR_WORDS:
	case AST_FOR_BODY:
	case AST_FOR_END:
	case AST_IF:
	case AST_IF_BRANCH:
		// Shorten the walk; we only care about top level includes
		return false;
	default:
		return true;
	}
}

void
ast_load_includes_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct ASTLoadIncludesState *this = ast_visit_context(visit);;
	auto diagnostic = ast_visit_diagnostic(visit);
	libias_scope_mempool(pool);

	bool should_process_include = ast_include_type(node) == AST_INCLUDE_BMAKE
		&& !ast_include_loaded_p(node)
		&& !ast_include_system_p(node);
	if (!should_process_include) {
		return;
	}

	auto curdir = libias_path_new(this->filename, "..");
	libias_mempool_take(pool, curdir);
	char *filename = ast_word_flatten(ast_include_path(node));
	libias_scope_free(filename);
	if (*filename == '"') {
		filename++;
	}
	if (libias_str_suffix_p(filename, "\"")) {
		filename[strlen(filename) - 1] = 0;
	}

	struct LibiasPath *path = process_include(
		ast_visit_root(visit),
		pool,
		curdir,
		filename);
	if (!path) {
		diagnostic_ast_error(
			diagnostic,
			node,
			NULL, NULL, // TODO maybe pass .include word?
			"cannot open include: %s", filename);
		ast_visit_stop(visit);
		return;
	}
	auto f = libias_file_openat(
		this->portsdir,
		libias_path_data(path),
		O_RDONLY,
		0);
	if (f == NULL) {
		diagnostic_ast_error(
			diagnostic,
			node,
			NULL, NULL, // TODO maybe pass .include word?
			"cannot open include: %s", filename);
		ast_visit_stop(visit);
		return;
	}
#if HAVE_CAPSICUM
	libias_panic_if(caph_limit_stream(libias_file_fd(f), CAPH_READ) < 0, "caph_limit_stream");
#endif

	auto incroot = ast_from_file(f, diagnostic, libias_path_data(path));
	libias_cleanup(&f);
	if (!incroot) {
		diagnostic_ast_error(
			diagnostic,
			node,
			NULL, NULL, // TODO maybe pass .include word?
			"cannot parse include");
		ast_visit_abort(visit);
		return;
	}

	auto ast_edit = ast_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_EDIT_DEFAULT);
	ast_children_foreach(incroot, child) {
		ast_edit_append_child(ast_edit, ast_clone(child));
	}
	ast_edit_chain_apply(ast_visit_root_edit(visit), ast_edit);
	ast_set_include_loaded(node, true);
	libias_cleanup(&incroot);
}

bool
ast_load_includes(
	struct AST *root,
	struct LibiasDirectory *portsdir,
	const char *filename,
	struct Diagnostic *diagnostic)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = ast_load_includes_visit_children_p;
	visit_trait.visit_pre_include = ast_load_includes_visit_include;

	struct ASTLoadIncludesState state = {
		.root = root,
		.portsdir = portsdir,
		.filename = filename,
	};

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &state);
	ast_visit_run(visit);

	bool retval = true;
	auto visit_diagnostic = ast_visit_diagnostic(visit);
	if (diagnostic_error_p(visit_diagnostic)) {
		retval = false;
		if (diagnostic) {
			diagnostic_move_reports(diagnostic, visit_diagnostic);
		}
	}
	libias_cleanup(&visit);
	return retval;
}
