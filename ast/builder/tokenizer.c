// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <limits.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/iterator.h>
#if PARSER_TOKENIZER_UTF8
#include <libias/libgrapheme/grapheme.h>
#endif
#include <libias/mempool.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/builder.h"
#include "diagnostic.h"
#include "enum.h"
#include "tokenizer.h"

#if PARSER_TOKENIZER_UTF8
typedef uint_least32_t ParserTokenizerChar;
#else
typedef char ParserTokenizerChar;
#endif

struct ParserTokenizerData {
	struct LibiasMempool *pool;
	struct Diagnostic *diagnostic;
	struct ASTBuilder *builder;
	struct LibiasArray *accepted_tokens;

	size_t offset;

	struct LibiasBuffer *buffer;
	size_t i;

	size_t last_match_start;
	size_t last_match_len;
};

#undef DEBUG
#undef MACHINE_DEFAULT_ARGS
#undef LOOKAHEAD
#undef MACHINE
#undef _CHAIN
#undef CHAIN
#undef LOOKAHEAD
#undef MATCH
#undef LOOKAHEAD_ANY
#undef MATCH_ANY
#undef MATCH_COMMENT
#undef LOOKAHEAD_EOL
#undef MATCH_EOL
#undef MATCH_ESCAPE
#undef LOOKAHEAD_LINECONT
#undef MATCH_LINECONT
#undef LOOKAHEAD_WHITESPACE
#undef MATCH_WHITESPACE
#undef EOS
#undef ACCEPT
#undef REJECT
#undef PUSH_TOKEN
#undef PUSH_TOKEN_MARKER
#undef PUSH_TOKEN_INCLUDING_MATCH

#if PARSER_ASTBUILDER_DEBUG
#define DEBUG(this, format, ...) \
	debug((this), __func__, __LINE__, (format), ##__VA_ARGS__)
#else
#define DEBUG(this, format, ...) \
	(this)
#endif

#define MACHINE_DEFAULT_ARGS \
	struct ParserTokenizerData *this, \
	size_t initial_token_count, \
	uint32_t state, \
	const size_t __start_pos, \
	libias_attr_unused size_t start_pos
#define MACHINE(x, ...) \
	bool x(MACHINE_DEFAULT_ARGS, ##__VA_ARGS__)
#define _CHAIN(x, ...) \
	(x)(DEBUG(this, " CHAIN " #x), libias_array_len(this->accepted_tokens), 0, this->i, this->i, ##__VA_ARGS__)
#define CHAIN(...) \
	if (!_CHAIN(__VA_ARGS__)) { REJECT; }

#define LOOKAHEAD(x) \
	(this->i < libias_buffer_len(this->buffer) && \
	 (x) && \
	 libias_str_prefix_p(libias_buffer_data(this->buffer) + this->i, (x)))
#define MATCH(x) \
	(LOOKAHEAD(x) && \
	 ((this->last_match_start = this->i) || true) && \
	 ((this->last_match_len = strlen(x)) || true) && \
	 ((this->i += strlen(x)) || true))
#define LOOKAHEAD_ANY() \
	(read_char(this, true) != INVALID_CODEPOINT)
#define MATCH_ANY() \
	(read_char(this, false) != INVALID_CODEPOINT)
#define MATCH_COMMENT() \
	MATCH("#")
#define LOOKAHEAD_EOL() \
	LOOKAHEAD("\n")
#define MATCH_EOL() \
	MATCH("\n")
#define MATCH_ESCAPE() \
	MATCH("\\")
#define LOOKAHEAD_LINECONT() \
	(LOOKAHEAD("$\\\n") || LOOKAHEAD("\\\n"))
#define MATCH_LINECONT() \
	(MATCH("$\\\n") || MATCH("\\\n"))
#define LOOKAHEAD_WHITESPACE() \
	(LOOKAHEAD(" ") || LOOKAHEAD("\t") || LOOKAHEAD_LINECONT())
#define MATCH_WHITESPACE() \
	(MATCH(" ") || MATCH("\t") || MATCH_LINECONT())
#define EOS() \
	(!(this->i < libias_buffer_len(this->buffer)) && \
	 ((this->last_match_len = 0) || true))

#define ACCEPT \
	do { \
		(void)DEBUG(this, "ACCEPT"); \
		return true; \
	} while (false);
#define REJECT \
	do { \
		(void)DEBUG(this, "REJECT"); \
		libias_array_truncate_at(this->accepted_tokens, initial_token_count); \
		this->i = __start_pos; \
		return false; \
	} while (false);
#define PUSH_TOKEN(type) \
	do { \
		size_t len = this->i - start_pos - this->last_match_len; \
		(void)DEBUG(this, "  PUSH %s[%zu-%zu]", ASTBuilderTokenType_human(type), start_pos, len); \
		push_token( \
			this, \
			(type), \
			start_pos, \
			len); \
		start_pos = this->i; \
	} while (false);
#define PUSH_TOKEN_MARKER(token_type) \
	do { \
		struct ASTBuilderToken *token = libias_mempool_alloc( \
			this->pool, \
			struct ASTBuilderToken); \
		token->type = (token_type); \
		token->i = this->i; \
		token->len = 0; \
		libias_array_append(this->accepted_tokens, token); \
	} while (false);
#define PUSH_TOKEN_INCLUDING_MATCH(type) \
	do { \
		push_token( \
			this, \
			(type), \
			start_pos, this->i - start_pos); \
		start_pos = this->i; \
	} while (false);

// Prototypes
static MACHINE(comment);
static MACHINE(conditional);
static MACHINE(line_type);
static MACHINE(target_command);
static MACHINE(target_rule);
static MACHINE(token, enum ASTBuilderTokenType type, enum ASTBuilderTokenType sep_type, const char **sep_tokens, const size_t sep_tokens_len);
static MACHINE(token_comment, enum ASTBuilderTokenType type);
static MACHINE(token_quoted, enum ASTBuilderTokenType type, bool in_recursion);
static MACHINE(variable);
static void push_token(struct ParserTokenizerData *this, enum ASTBuilderTokenType type, size_t i, size_t len);
static ParserTokenizerChar read_char(struct ParserTokenizerData *this, bool lookahead);

#if PARSER_ASTBUILDER_DEBUG
static struct ParserTokenizerData *debug(struct ParserTokenizerData *this, const char *func, int line, const char *format, ...);
#endif

// Constants
#if PARSER_TOKENIZER_UTF8
static const ParserTokenizerChar INVALID_CODEPOINT = GRAPHEME_INVALID_CODEPOINT;
#else
static const ParserTokenizerChar INVALID_CODEPOINT = 0;
#endif

ParserTokenizerChar
read_char(struct ParserTokenizerData *this, bool lookahead)
{
	if (this->i >= libias_buffer_len(this->buffer)) {
		return INVALID_CODEPOINT;
	}

#if PARSER_TOKENIZER_UTF8
	ParserTokenizerChar c = GRAPHEME_INVALID_CODEPOINT;
	size_t clen = grapheme_decode_utf8(
		libias_buffer_data(this->buffer) + this->i,
		libias_buffer_len(this->buffer) - this->i,
		&c);
	// This shouldn't be possible since we already went through the tokenizer
	libias_panic_if(
		c == GRAPHEME_INVALID_CODEPOINT,
		"invalid UTF-8 codepoint in at position %zu: %s",
		this->i, this->buf);

	if (!lookahead) {
		this->last_match_start = this->i;
		this->last_match_len = clen;
		this->i += clen;
	}

	return c;
#else
	if (lookahead) {
		return libias_buffer_data(this->buffer)[this->i];
	} else {
		this->last_match_start = this->i;
		this->last_match_len = 1;
		return libias_buffer_data(this->buffer)[this->i++];
	}
#endif
}

void
push_token(
	struct ParserTokenizerData *this,
	enum ASTBuilderTokenType type,
	size_t i,
	size_t len)
{
	if (len > 0) {
		struct ASTBuilderToken *token = libias_mempool_alloc(
			this->pool,
			struct ASTBuilderToken);
		token->type = type;
		token->i = i;
		token->len = len;
		libias_array_append(this->accepted_tokens, token);
	}
}

#if PARSER_ASTBUILDER_DEBUG
struct ParserTokenizerData *
debug(
	struct ParserTokenizerData *this,
	const char *func,
	int line,
	const char *format,
	...)
{
	va_list ap;
	va_start(ap, format);
	char *function = libias_str_printf("%s@%d", func, line);
	fprintf(stderr, "%-5zu %-30s ", this->i, function);
	libias_free(function);
	vfprintf(stderr, format, ap);
	fprintf(stderr, "\n");
	va_end(ap);
	return this;
}
#endif

MACHINE(
	token_comment,
	enum ASTBuilderTokenType type)
{
	if (!MATCH_COMMENT()) {
		REJECT;
	}
	while (!EOS()) {
		if (MATCH_LINECONT()) {
			// nada
		} else if (LOOKAHEAD_EOL()) {
			PUSH_TOKEN_INCLUDING_MATCH(type);
			ACCEPT;
		} else if (MATCH_ANY()) {
			// nada
		} else {
			REJECT;
		}
	}

	if (EOS()) {
		PUSH_TOKEN_INCLUDING_MATCH(type);
		ACCEPT;
	} else {
		REJECT;
	}
}

MACHINE(
	token_quoted,
	enum ASTBuilderTokenType type,
	bool in_recursion)
{
	struct Pair {
		const char *start;
		const char *end;
		bool on_recursion;
	};

	static struct Pair normal_pairs[] = {
		{ "{", "}", true },
		{ "(", ")", true },
		{ "${", "}", false },
		{ "$(", ")", false },
		{ "\"", "\"", false },
		{ "'", "'", false },
		{ "`", "`", false },
	};
	static struct Pair cond_pairs[] = {
		{ "{", "}", true },
		{ "(", ")", true },
		{ "${", "}", false },
		{ "$(", ")", false },
		{ "\"", "\"", false },
		{ "'", "'", false },
		{ "`", "`", false },
		{ "defined(", ")", false },
		{ "empty(", ")", false },
		{ "exists(", ")", false },
		{ "make(", ")", false },
		{ "target(", ")", false },
		{ "commands(", ")", false },
	};
	struct Pair *pairs = normal_pairs;
	size_t pairs_len = nitems(normal_pairs);
	if (type == AST_BUILDER_TOKEN_CONDITIONAL_TOKEN) {
		pairs = cond_pairs;
		pairs_len = nitems(cond_pairs);
	}

	for (size_t i = 0; i < pairs_len; i++) {
		if (!in_recursion && pairs[i].on_recursion) {
			continue;
		}
		if (MATCH(pairs[i].start)) {
			while (!EOS()) {
				if (MATCH(pairs[i].end) || LOOKAHEAD_EOL()) {
					ACCEPT;
				} else if (MATCH_ESCAPE()) {
					if (MATCH_ANY()) {}
				} else if (_CHAIN(token_quoted, type, true)) {
					// nada
				} else if (MATCH_ANY()) {
					// nada
				} else {
					REJECT;
				}
			}
			REJECT;
		}
	}
	REJECT;
}

MACHINE(
	token,
	enum ASTBuilderTokenType type,
	enum ASTBuilderTokenType sep_type,
	const char **sep_tokens,
	const size_t sep_tokens_len)
{
	while (MATCH_WHITESPACE());
	PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);

	while (!EOS()) {
		if (LOOKAHEAD_WHITESPACE()) {
			PUSH_TOKEN_INCLUDING_MATCH(type);
			if (MATCH_WHITESPACE()) {
				while (MATCH_WHITESPACE());
				PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);
			} else {
				REJECT;
			}
		}
		for (size_t i = 0; i < sep_tokens_len; i++) {
			if (LOOKAHEAD(sep_tokens[i])) {
				PUSH_TOKEN_INCLUDING_MATCH(type);
				if (MATCH(sep_tokens[i])) {
					PUSH_TOKEN_INCLUDING_MATCH(sep_type);
					ACCEPT;
				}
			}
		}

		if (MATCH_ESCAPE()) {
			if (MATCH_ANY()) {}
		} else if (LOOKAHEAD_EOL()) {
			PUSH_TOKEN_INCLUDING_MATCH(type);
			ACCEPT;
		} else if (MATCH_WHITESPACE()) {
			while (MATCH_WHITESPACE());
			PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);
		} else if (LOOKAHEAD("#")) {
			PUSH_TOKEN_INCLUDING_MATCH(type);
			CHAIN(token_comment, type);
			ACCEPT;
		} else if (_CHAIN(token_quoted, type, false)) {
			// nada
		} else if (MATCH_ANY()) {
			// nada
		}
	}

	if (EOS()) {
		PUSH_TOKEN(type);
		ACCEPT;
	} else {
		REJECT;
	}
}

MACHINE(comment)
{
	if (MATCH_EOL()) {
		PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_COMMENT);
		ACCEPT;
	}

	while (MATCH_WHITESPACE());

	if (MATCH_COMMENT()) {
		while (!EOS()) {
			if (MATCH_ESCAPE()) {
				if (MATCH_ANY()) {}
			} else if (MATCH_EOL()) {
				PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_COMMENT);
				ACCEPT;
			} else if (MATCH_ANY()) {
				// nada
			} else {
				REJECT;
			}
		}
	} else {
		while (!EOS()) {
			if (MATCH_WHITESPACE()) {
				// nada
			} else if (MATCH_EOL()) {
				PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_COMMENT);
				ACCEPT;
			} else {
				REJECT;
			}
		}
	}

	if (EOS()) {
		PUSH_TOKEN(AST_BUILDER_TOKEN_COMMENT);
		ACCEPT;
	} else {
		REJECT;
	}
}

MACHINE(conditional)
{
	static const char *if_ops[] = {
		"&&",
		"||",
		"!=",
		"==",
		"<=",
		">=",
		"<",
		">",
		"!",
		"(",
		")",
	};
	static struct {
		const char *token;
		const char **ops;
		size_t ops_len;
	} conditionals[] = {
		{ "dinclude", NULL, 0 },
		{ "error", NULL, 0 },
		{ "export-env", NULL, 0 },
		{ "export.env", NULL, 0 },
		{ "export-literal", NULL, 0 },
		{ "export", NULL, 0 },
		{ "unexport-env", NULL, 0 },
		{ "unexport", NULL, 0 },
		{ "undef", NULL, 0 },
		{ "info", NULL, 0 },
		{ "for", NULL, 0 },
		{ "endfor", NULL, 0 },
		{ "warning", NULL, 0 },
		{ "ifdef", if_ops, nitems(if_ops) },
		{ "ifndef", if_ops, nitems(if_ops) },
		{ "-include", NULL, 0 },
		{ "include", NULL, 0 },
		{ "ifmake", if_ops, nitems(if_ops) },
		{ "ifnmake", if_ops, nitems(if_ops) },
		{ "if", if_ops, nitems(if_ops) },
		{ "else", NULL, 0 },
		{ "elifdef", if_ops, nitems(if_ops) },
		{ "elifndef", if_ops, nitems(if_ops) },
		{ "elifmake", if_ops, nitems(if_ops) },
		{ "elifnmake", if_ops, nitems(if_ops) },
		{ "elif", if_ops, nitems(if_ops) },
		{ "endif", NULL, 0 },
		{ "sinclude", NULL, 0 },
	};

	const char **ops = NULL;
	size_t ops_len = 0;

	if (MATCH(".")) {
		start_pos = this->i;
		PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_CONDITIONAL_START);

		while (MATCH_WHITESPACE());
		PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);
		if (EOS()) {
			REJECT;
		}

		bool matched = false;
		for (size_t i = 0; i < nitems(conditionals); i++) {
			if (MATCH(conditionals[i].token)) {
				matched = true;
				PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_CONDITIONAL_EXPR);
				ops = conditionals[i].ops;
				ops_len = conditionals[i].ops_len;
				break;
			}
		}
		if (!matched) {
			REJECT;
		}
	} else if (MATCH("include") || MATCH("sinclude") || MATCH("-include")) {
		PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_CONDITIONAL_START);
		PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_CONDITIONAL_POSIX);
	} else {
		REJECT;
	}

	if (MATCH_EOL() || EOS()) {
		PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_CONDITIONAL_END);
		ACCEPT;
	} else if (MATCH_WHITESPACE()) {
		while (MATCH_WHITESPACE());
		PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);
	} else if (
		ops == if_ops &&
		(LOOKAHEAD("(") || LOOKAHEAD("!("))) { // e.g., .ifdef(WITH_BLA)
		// nada
	} else {
		REJECT;
	}

	while (!EOS()) {
		if (_CHAIN(
				token,
				AST_BUILDER_TOKEN_CONDITIONAL_TOKEN,
				AST_BUILDER_TOKEN_CONDITIONAL_TOKEN,
				ops,
				ops_len)) {
			if (MATCH_EOL() || EOS()) {
				PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_CONDITIONAL_END);
				ACCEPT;
			}
		} else {
			REJECT;
		}
	}

	REJECT;
}

MACHINE(target_command)
{
	if (!MATCH("\t")) {
		REJECT;
	}
	start_pos = this->i;

	PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_TARGET_COMMAND_START);
	bool flags = false;
	while (!EOS()) {
		if (MATCH("@") || MATCH("-") || MATCH("+")) {
			flags = true;
		} else if (LOOKAHEAD_ANY()) {
			if (flags) {
				PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS);
			}
			break;
		} else {
			REJECT;
		}
	}
	while (!EOS()) {
		CHAIN(
			token,
			AST_BUILDER_TOKEN_TARGET_COMMAND_TOKEN,
			AST_BUILDER_TOKEN_TARGET_COMMAND_TOKEN,
			NULL,
			0);
		if (MATCH_EOL() || EOS()) {
			PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_TARGET_COMMAND_END);
			ACCEPT;
		}
	}

	REJECT;
}

MACHINE(target_rule)
{
	static const char *sep_tokens[] = {
		"::",
		":",
		"!",
	};

	PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_TARGET_RULE_START);

	while (MATCH_WHITESPACE());
	PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);

	CHAIN(
		token,
		AST_BUILDER_TOKEN_TARGET_RULE_TOKEN,
		AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR,
		sep_tokens,
		nitems(sep_tokens));

	bool target_rule = false;
	libias_array_foreach_slice(
		this->accepted_tokens,
		initial_token_count, -1,
		struct ASTBuilderToken *, token)
	{
		if (token->type == AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR) {
			target_rule =
				(token->len == 2
				 && strncmp(
					 libias_buffer_data(this->buffer) + token->i,
					 "::",
					 2) == 0)
				|| (token->len == 1
					&& strncmp(
						libias_buffer_data(this->buffer) + token->i,
						"!",
						1) == 0)
				|| (token->len == 1
					&& strncmp(
						libias_buffer_data(this->buffer) + token->i,
						":",
						1) == 0);
		}
	}
	if (!target_rule) {
		REJECT;
	}

	while (!(MATCH_EOL() || EOS())) {
		CHAIN(
			token,
			AST_BUILDER_TOKEN_TARGET_RULE_TOKEN,
			AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR,
			NULL,
			0);
	}

	PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_TARGET_RULE_END);
	ACCEPT;
}

MACHINE(variable)
{
	static const char *sep_tokens[] = {
		"?=",
		"!=",
		"+=",
		":=",

		// We need to make sure that this line is not a target rule
		"::",
		":",

		"=",
	};

	PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_VARIABLE_START);

	while (MATCH_WHITESPACE());
	PUSH_TOKEN_INCLUDING_MATCH(AST_BUILDER_TOKEN_WS);

	CHAIN(
		token,
		AST_BUILDER_TOKEN_VARIABLE_TOKEN,
		AST_BUILDER_TOKEN_VARIABLE_MODIFIER,
		sep_tokens,
		nitems(sep_tokens));

	bool target_rule = true;
	libias_array_foreach_slice(
		this->accepted_tokens,
		initial_token_count, -1,
		struct ASTBuilderToken *, token)
	{
		if (token->type == AST_BUILDER_TOKEN_VARIABLE_MODIFIER) {
			target_rule =
				(token->len == 2
				 && strncmp(
					 libias_buffer_data(this->buffer) + token->i,
					 "::",
					 2) == 0)
				|| (token->len == 1
					&& strncmp(
						libias_buffer_data(this->buffer) + token->i,
						":",
						1) == 0);
			break;
		}
	}
	if (target_rule) {
		REJECT;
	}

	while (!(MATCH_EOL() || EOS())) {
		CHAIN(
			token,
			AST_BUILDER_TOKEN_VARIABLE_TOKEN,
			AST_BUILDER_TOKEN_VARIABLE_MODIFIER,
			NULL,
			0);
	}

	PUSH_TOKEN_MARKER(AST_BUILDER_TOKEN_VARIABLE_END);
	ACCEPT;
}

MACHINE(line_type)
{
	while (!EOS()) {
		if (_CHAIN(conditional)) {
		} else if (_CHAIN(target_command)) {
		} else if (_CHAIN(comment)) {
		} else if (_CHAIN(variable)) {
		} else if (_CHAIN(target_rule)) {
		} else {
			diagnostic_parse_error(
				this->diagnostic,
				this->builder,
				this->i, this->i,
				"unknown line type");
			REJECT;
		}
	}
	ACCEPT;
}

bool
ast_builder_tokenize_buffer(
	struct ASTBuilder *builder,
	struct LibiasBuffer *buffer,
	struct Diagnostic *diagnostic)
{
	if (libias_buffer_len(buffer) == 0) {
		return true;
	}

	libias_scope_array(accepted_tokens);
	libias_scope_mempool(pool);
	struct ParserTokenizerData *this = &(struct ParserTokenizerData){
		.pool = pool,
		.diagnostic = diagnostic,
		.builder = builder,
		.accepted_tokens = accepted_tokens,
		.buffer = buffer,
		.i = 0,
	};

	if (!_CHAIN(line_type)) {
		return false;
	}

	libias_array_foreach(this->accepted_tokens, struct ASTBuilderToken *, token) {
		if (!ast_builder_process_token(builder, token)) {
			return false;
		}
	}

	return ast_builder_process_token(builder, NULL);
}
