// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <libias/array.h>
#include <libias/buffer.h>
#if PARSER_TOKENIZER_UTF8
#include <libias/libgrapheme/grapheme.h>
#endif
#include <libias/io.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/stack.h>
#include <libias/str.h>

#include "ast/builder.h"
#include "ast/word.h"
#include "ast/word/expand/modifier.h"
#include "diagnostic.h"
#include "sexp.h"
#include "word.h"

#if PARSER_TOKENIZER_UTF8
typedef uint_least32_t ParserTokenizerChar;
#else
typedef char ParserTokenizerChar;
#endif

struct ParseWordData {
	struct Diagnostic *diagnostic;
	struct LibiasArray *words;

	size_t offset;

	struct ASTBuilder *builder;
	const char *buf;
	size_t i;
	size_t len;

	size_t last_match_start;
	size_t last_match_len;
};

struct ASTBuilderWord {
	enum ASTBuilderWordType type;

	enum ASTWordExpandModifierType modifier_type;
	uint32_t modifier_flags;

	bool bool_expr;

	size_t i;
	size_t len;

#if PARSER_ASTBUILDER_DEBUG
	const char *func;
	int line;
#endif
};

struct ParseWordExpandModifier {
	const char *modifier;
	size_t (*state_fn)(struct ParseWordData *, size_t);
};

#undef MACHINE_DEFAULT_ARGS
#undef MACHINE
#undef _CHAIN
#undef CHAIN
#undef LOOKAHEAD
#undef MATCH
#undef MATCH_ANY
#undef EOS
#undef ACCEPT
#undef REJECT
#undef EXPAND_MODIFIER
#undef EXPAND_MODIFIER_WITH_FLAGS
#undef PUSH_MARKER
#undef __PUSH_WORD
#undef PUSH_WORD_ALLOW_ZERO_LEN
#undef PUSH_WORD
#undef PUSH_WORD_INCLUDING_MATCH

#define MACHINE_DEFAULT_ARGS \
	struct ParseWordData *this, \
	size_t initial_words_count, \
	uint32_t state, \
	bool bool_expr, \
	const char *expand_terminal, \
	const size_t __start_pos, \
	size_t start_pos
#define MACHINE(x, ...) \
	bool x(MACHINE_DEFAULT_ARGS, ##__VA_ARGS__)
#define _CHAIN(x, ...) \
	(x)(this, libias_array_len(this->words), 0, bool_expr, expand_terminal, this->i, this->i, ##__VA_ARGS__)
#define CHAIN(...) \
	if (!_CHAIN(__VA_ARGS__)) { REJECT; }

#define LOOKAHEAD(x) \
	(this->i < this->len && \
	 (x) && \
	 libias_str_prefix_p(this->buf + this->i, (x)))
#define MATCH(x) \
	(LOOKAHEAD(x) && \
	 ((this->last_match_start = this->i) || true) && \
	 ((this->last_match_len = strlen(x)) || true) && \
	 ((this->i += strlen(x)) || true))
#define MATCH_ANY() \
	(read_char(this) != INVALID_CODEPOINT)
#define EOS() \
	(!(this->i < this->len) && \
	 ((this->last_match_len = 0) || true))

#define ACCEPT \
	return true
#define REJECT \
	do { \
		libias_array_foreach_slice(this->words, initial_words_count, -1, struct ASTBuilderWord *, word) { \
			libias_free(word); \
		} \
		libias_array_truncate_at(this->words, initial_words_count); \
		this->i = __start_pos; \
		return false; \
	} while (false)
#define EXPAND_MODIFIER(modtype) \
	EXPAND_MODIFIER_WITH_FLAGS(modtype, 0)
#define EXPAND_MODIFIER_WITH_FLAGS(modtype, flags) \
	do { \
		struct ASTBuilderWord *builder_word = push_word( \
			this, \
			AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER, \
			bool_expr, \
			true, \
			__func__, \
			__LINE__, \
			start_pos, \
			this->i - start_pos); \
		builder_word->modifier_type = (modtype); \
		builder_word->modifier_flags = (flags); \
	} while (false)
#define PUSH_MARKER(marker) \
	do { \
		push_word( \
			this, \
			(marker), \
			bool_expr, \
			true, \
			__func__, \
			__LINE__, \
			start_pos - this->last_match_len, \
			this->i - start_pos); \
	} while (false)
#define __PUSH_WORD(allow_zero_len, length) \
	do { \
		push_word( \
			this, \
			AST_BUILDER_WORD_WORD, \
			bool_expr, \
			(allow_zero_len), \
			__func__, \
			__LINE__, \
			start_pos, \
			(length)); \
		start_pos = this->i; \
	} while (false);
#define PUSH_WORD_ALLOW_ZERO_LEN \
	__PUSH_WORD(true, this->i - start_pos - this->last_match_len)
#define PUSH_WORD \
	__PUSH_WORD(false, this->i - start_pos - this->last_match_len)
#define PUSH_WORD_INCLUDING_MATCH \
	__PUSH_WORD(false, this->i - start_pos)

// Prototypes
static MACHINE(append);
static MACHINE(expand);
static MACHINE(expand_modifier_C);
static MACHINE(expand_modifier_D);
static MACHINE(expand_modifier_E);
static MACHINE(expand_modifier_H);
static MACHINE(expand_modifier_L);
static MACHINE(expand_modifier_M);
static MACHINE(expand_modifier_N);
static MACHINE(expand_modifier_O);
static MACHINE(expand_modifier_On);
static MACHINE(expand_modifier_Or);
static MACHINE(expand_modifier_Orn);
static MACHINE(expand_modifier_Ox);
static MACHINE(expand_modifier_P);
static MACHINE(expand_modifier_Q);
static MACHINE(expand_modifier_R);
static MACHINE(expand_modifier_S);
static MACHINE(expand_modifier_T);
static MACHINE(expand_modifier_U);
static MACHINE(expand_modifier_append);
static MACHINE(expand_modifier_bang);
static MACHINE(expand_modifier_cmdset);
static MACHINE(expand_modifier_for);
static MACHINE(expand_modifier_gmtime);
static MACHINE(expand_modifier_hash);
static MACHINE(expand_modifier_if);
static MACHINE(expand_modifier_list);
static MACHINE(expand_modifier_localtime);
static MACHINE(expand_modifier_number);
static MACHINE(expand_modifier_optset);
static MACHINE(expand_modifier_q);
static MACHINE(expand_modifier_range);
static MACHINE(expand_modifier_set);
static MACHINE(expand_modifier_sh);
static MACHINE(expand_modifier_slice);
static MACHINE(expand_modifier_slice_index);
static MACHINE(expand_modifier_slice_range);
static MACHINE(expand_modifier_subst, enum ASTWordExpandModifierType type, bool chained);
static MACHINE(expand_modifier_subst_systemv);
static MACHINE(expand_modifier_tA);
static MACHINE(expand_modifier_tW);
static MACHINE(expand_modifier_tl);
static MACHINE(expand_modifier_ts);
static MACHINE(expand_modifier_tu);
static MACHINE(expand_modifier_tw);
static MACHINE(expand_modifier_u);
static MACHINE(expand_modifier_underscore);
static MACHINE(expand_modifiers);
static MACHINE(expand_modifiers_arg);
static MACHINE(expand_modifiers_arg_with_terminals, bool eat_terminal, const char *terminals[], size_t terminals_len);
static MACHINE(expand_modifiers_arg_with_terminals_and_delimiter, bool eat_terminal, const char *terminals[], size_t terminals_len, const char *delimiter);
static struct ASTBuilderWord *push_word(struct ParseWordData *this, enum ASTBuilderWordType type, bool bool_expr, bool allow_zero_len, const char *func, int line, size_t i, size_t len);
static ParserTokenizerChar read_char(struct ParseWordData *this);
static ParserTokenizerChar read_digit(struct ParseWordData *this);

#if PARSER_ASTBUILDER_DEBUG
static void debug_print_words(struct LibiasArray *words, const char *buf);
#endif

// Constants
#if PARSER_TOKENIZER_UTF8
static const ParserTokenizerChar INVALID_CODEPOINT = GRAPHEME_INVALID_CODEPOINT;
#else
static const ParserTokenizerChar INVALID_CODEPOINT = 0;
#endif
#if 0
static ParserTokenizerChar default_escape_chars[] = {
	'#', '"', '\'', '\\', ' ', '\t', '\n',
};
#endif

struct ASTBuilderWord *
push_word(
	struct ParseWordData *this,
	enum ASTBuilderWordType type,
	bool bool_expr,
	bool allow_zero_len,
	const char *func,
	int line,
	size_t i,
	size_t len)
{
	if (allow_zero_len || len > 0) {
		struct ASTBuilderWord *word = libias_alloc(struct ASTBuilderWord);
		word->type = type;
		word->bool_expr = bool_expr;
		word->i = i;
		word->len = len;
#if PARSER_ASTBUILDER_DEBUG
		word->func = func;
		word->line = line;
#endif
		libias_array_append(this->words, word);
		return word;
	} else {
		return NULL;
	}
}

static ParserTokenizerChar
read_char(struct ParseWordData *this)
{
	if (this->i >= this->len) {
		return INVALID_CODEPOINT;
	}

#if PARSER_TOKENIZER_UTF8
	ParserTokenizerChar c = GRAPHEME_INVALID_CODEPOINT;
	size_t clen = grapheme_decode_utf8(this->buf + this->i, this->len - this->i, &c);
	// This shouldn't be possible since we already went through the tokenizer
	panic_if(
		c == GRAPHEME_INVALID_CODEPOINT,
		"invalid UTF-8 codepoint in at position %zu: %s",
		this->i, this->buf);

	this->last_match_start = this->i;
	this->last_match_len = clen;
	this->i += clen;

	return c;
#else
	this->last_match_start = this->i;
	this->last_match_len = 1;
	return this->buf[this->i++];
#endif
}

static ParserTokenizerChar
read_digit(struct ParseWordData *this)
{
	if (this->i >= this->len) {
		return INVALID_CODEPOINT;
	}

#if PARSER_TOKENIZER_UTF8
	ParserTokenizerChar c = GRAPHEME_INVALID_CODEPOINT;
	size_t clen = grapheme_decode_utf8(this->buf + this->i, this->len - this->i, &c);
	// This shouldn't be possible since we already went through the tokenizer
	panic_if(
		c == GRAPHEME_INVALID_CODEPOINT,
		"invalid UTF-8 codepoint in at position %zu: %s",
		this->i, this->buf);

	if (clen == 1 && isdigit((unsigned char)c)) {
		this->last_match_start = this->i;
		this->last_match_len = clen;
		this->i += clen;
		return c;
	} else {
		return INVALID_CODEPOINT;
	}

	this->last_match_start = this->i;
	this->last_match_len = clen;
	this->i += clen;

	return c;
#else
	ParserTokenizerChar c = this->buf[this->i];
	if (isdigit((unsigned char)c)) {
		this->last_match_start = this->i;
		this->last_match_len = 1;
		this->i++;
		return c;
	} else {
		return INVALID_CODEPOINT;
	}
#endif
}

MACHINE(expand_modifiers)
{
	static struct {
		const char *token;
		bool (*state_machine)(MACHINE_DEFAULT_ARGS);
	} expand_modifiers_[] = {
		{ "E", expand_modifier_E },
		{ "H", expand_modifier_H },
		{ "M", expand_modifier_M },
		{ "N", expand_modifier_N },
		{ "Orn", expand_modifier_Orn },
		{ "On", expand_modifier_On },
		{ "Or", expand_modifier_Or },
		{ "Ox", expand_modifier_Ox },
		{ "O", expand_modifier_O },
		{ "Q", expand_modifier_Q },
		{ "q", expand_modifier_q },
		{ "R", expand_modifier_R },
		{ "range", expand_modifier_range },
		{ "gmtime", expand_modifier_gmtime },
		{ "hash", expand_modifier_hash },
		{ "localtime", expand_modifier_localtime },
		{ "tA", expand_modifier_tA },
		{ "tl", expand_modifier_tl },
		{ "ts", expand_modifier_ts },
		{ "tu", expand_modifier_tu },
		{ "tW", expand_modifier_tW },
		{ "tw", expand_modifier_tw },
		{ "S", expand_modifier_S },
		{ "C", expand_modifier_C },
		{ "T", expand_modifier_T },
		{ "u", expand_modifier_u },
		{ "?", expand_modifier_if },
		{ "@", expand_modifier_for },
		{ "_", expand_modifier_underscore },
		{ "U", expand_modifier_U },
		{ "D", expand_modifier_D },
		{ "L", expand_modifier_L },
		{ "P", expand_modifier_P },
		{ "!", expand_modifier_bang },
		{ "sh", expand_modifier_sh },
		{ ":=", expand_modifier_set },
		{ ":?=", expand_modifier_optset },
		{ ":+=", expand_modifier_append },
		{ ":!=", expand_modifier_cmdset },
		{ "[", expand_modifier_slice },
		{ "$", expand_modifier_list },
	};

	for (size_t i = 0; i < nitems(expand_modifiers_); i++) {
		if (MATCH(expand_modifiers_[i].token)) {
			CHAIN(expand_modifiers_[i].state_machine);
			if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
				ACCEPT;
			} else {
				REJECT;
			}
		}
	}

	if (_CHAIN(expand_modifier_subst_systemv)) {
		ACCEPT;
	} else if (LOOKAHEAD(expand_terminal)) {
		// Record something like ${FOO:}
		EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_EMPTY);
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	}

	REJECT;
}

MACHINE(expand)
{
	while (!EOS()) {
		switch (state) {
		case 0:
			if (MATCH("{")) {
				PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND);
				expand_terminal = "}";
				start_pos = this->i;
				state = 1;
			} else if (MATCH("(")) {
				PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_PARENTHESES);
				expand_terminal = ")";
				start_pos = this->i;
				state = 1;
			} else if (MATCH_ANY()) {
				PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_SINGLE);
				// It's an expansion like $i with only a single char
				PUSH_WORD_INCLUDING_MATCH;
				PUSH_MARKER(AST_BUILDER_WORD_POP);
				ACCEPT;
			} else {
				REJECT;
			}
			break;
		case 1:
			if (MATCH(":")) {
				// For something like ${:Ufoo}
				PUSH_WORD_ALLOW_ZERO_LEN;
				CHAIN(expand_modifiers);
				start_pos = this->i;
			} else if (MATCH(expand_terminal)) {
				PUSH_WORD;
				PUSH_MARKER(AST_BUILDER_WORD_POP);
				ACCEPT;
			} else if (MATCH("$")) {
				if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
					continue;
				}
				if (EOS()) {
					PUSH_WORD;
					PUSH_MARKER(AST_BUILDER_WORD_POP);
					ACCEPT;
				}
				PUSH_WORD;
				CHAIN(expand);
				start_pos = this->i;
			} else if (MATCH("\\")) {
				if (!MATCH("$$") && !MATCH_ANY()) {
					REJECT;
				}
			} else if (MATCH_ANY()) {
				// nada
			} else {
				REJECT;
			}
			break;
		default:
			REJECT;
		}
	}

	// TODO: push error diagnostic
	REJECT;
}

MACHINE(append)
{
	PUSH_MARKER(AST_BUILDER_WORD_PUSH_APPEND);

	while (true) {
		if (EOS()) {
			PUSH_WORD;
			PUSH_MARKER(AST_BUILDER_WORD_POP);
			ACCEPT;
		} else if (MATCH("$")) {
			if (EOS()) {
				PUSH_WORD;
				PUSH_MARKER(AST_BUILDER_WORD_POP);
				ACCEPT;
			}
			PUSH_WORD;
			bool orig_bool_expr = bool_expr;
			bool_expr = false;
			CHAIN(expand);
			bool_expr = orig_bool_expr;
			start_pos = this->i;
		} else if (MATCH("\\")) {
			if (!MATCH("$$") && !MATCH_ANY()) {
				REJECT;
			}
		} else if (MATCH_ANY()) {
			// nothing to do
		}
	}

	REJECT;
}

MACHINE(
	expand_modifiers_arg_with_terminals_and_delimiter,
	bool eat_terminal,
	const char *terminals[],
	size_t terminals_len,
	const char *delimiter)
{
	PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
	PUSH_MARKER(AST_BUILDER_WORD_PUSH_APPEND);
	while (true) {
		for (size_t i = 0; i < terminals_len; i++) {
			if (MATCH(terminals[i])) {
				PUSH_WORD;
				PUSH_MARKER(AST_BUILDER_WORD_POP);
				PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
				if (!eat_terminal) {
					// Unwind to before the terminal
					this->i -= this->last_match_len;
				}
				ACCEPT;
			}
		}
		if (MATCH("$")) {
			if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
				continue;
			}
			if (delimiter && LOOKAHEAD(delimiter)) {
				// HACK: This is only here to support :S/$//
				continue;
			}
			if (EOS()) {
				PUSH_WORD;
				PUSH_MARKER(AST_BUILDER_WORD_POP);
				PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
				ACCEPT;
			}
			PUSH_WORD;
			CHAIN(expand);
			start_pos = this->i;
		} else if (MATCH("\\")) {
			if (!MATCH("$$") && !MATCH_ANY()) {
				REJECT;
			}
		} else if (MATCH_ANY()) {
			// nothing to do
		} else {
			REJECT;
		}
	}

	REJECT;
}

MACHINE(
	expand_modifiers_arg_with_terminals,
	bool eat_terminal,
	const char *terminals[],
	size_t terminals_len)
{
	CHAIN(
		expand_modifiers_arg_with_terminals_and_delimiter,
		eat_terminal,
		terminals,
		terminals_len,
		NULL);
	ACCEPT;
}

MACHINE(expand_modifiers_arg)
{
	const char *terminals[] = { ":", expand_terminal };
	CHAIN(
		expand_modifiers_arg_with_terminals,
		false,
		terminals,
		nitems(terminals));
	ACCEPT;
}

// :E (no arguments)
MACHINE(expand_modifier_E)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_E);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :H (no arguments)
MACHINE(expand_modifier_H)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_H);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :M<pattern>
// additional escape chars: * ? [ ]
MACHINE(expand_modifier_M)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_M);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :N<pattern>
// additional escape chars: * ? [ ]
MACHINE(expand_modifier_N)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_N);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :Orn (no arguments)
MACHINE(expand_modifier_Orn)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_Orn);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :On (no arguments)
MACHINE(expand_modifier_On)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_On);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :Or (no arguments)
MACHINE(expand_modifier_Or)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_Or);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :Ox (no arguments)
MACHINE(expand_modifier_Ox)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_Ox);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :O (no arguments)
MACHINE(expand_modifier_O)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_O);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :Q (no arguments)
MACHINE(expand_modifier_Q)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_Q);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :q (no arguments)
MACHINE(expand_modifier_q)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_q);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :R (no arguments)
MACHINE(expand_modifier_R)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_R);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :range[=<count>]
MACHINE(expand_modifier_range)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_range);
	CHAIN(expand_modifier_number);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :gmtime[=<utc>]
MACHINE(expand_modifier_gmtime)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_gmtime);
	CHAIN(expand_modifier_number);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :localtime[=<utc>]
MACHINE(expand_modifier_localtime)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_localtime);
	CHAIN(expand_modifier_number);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

MACHINE(expand_modifier_number)
{
	while (true) {
		switch (state) {
		case 0:
			if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
				ACCEPT;
			} else if (MATCH("=")) {
				state = 1;
				start_pos = this->i; // skip the =
			} else {
				REJECT;
			}
			break;
		case 1:
			if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
				size_t len = this->i - start_pos;
				if (len == 0) {
					ACCEPT;
				}
				const char *error = NULL;
				char *number = libias_str_ndup(this->buf + start_pos, len);
				strtonum(number, 0, INT64_MAX, &error);
				libias_free(number);
				if (error) {
					// TODO diagnostics
					REJECT;
				} else {
					PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
					PUSH_WORD_INCLUDING_MATCH;
					PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
					ACCEPT;
				}
			} else if (read_digit(this) != INVALID_CODEPOINT) {
				// nothing to do
			} else {
				REJECT;
			}
		}
	}

	REJECT;
}

// :_[=<var>]
MACHINE(expand_modifier_underscore)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_UNDERSCORE);
	if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else if (MATCH("=")) {
		CHAIN(expand_modifiers_arg);
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	}

	// TODO: diagnostics
	REJECT;
}

// :hash (no arguments)
MACHINE(expand_modifier_hash)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_hash);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :tA (no arguments)
MACHINE(expand_modifier_tA)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_tA);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :tl (no arguments)
MACHINE(expand_modifier_tl)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_tl);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :ts<c>
MACHINE(expand_modifier_ts)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_ts);

	if (LOOKAHEAD("::") || LOOKAHEAD(":}")) {
		// separator is probably :
		// for example ${FOO:ts:} or ${FOO:ts::M*}
	} else if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
		// empty separator
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	}

	if (MATCH_ANY()) {
		if (!LOOKAHEAD(":") && !LOOKAHEAD(expand_terminal)) {
			// TODO: push diagnostic
			REJECT;
		}
		PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
		PUSH_WORD_INCLUDING_MATCH;
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
	}

	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :tu (no arguments)
MACHINE(expand_modifier_tu)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_tu);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :tW (no arguments)
MACHINE(expand_modifier_tW)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_tW);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :tw (no arguments)
MACHINE(expand_modifier_tw)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_tw);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :S<delimiter><old_string><delimiter><new_string><delimiter>[1gW]
// additional escape characters: ^ & <delimiter>
MACHINE(expand_modifier_S)
{
	CHAIN(
		expand_modifier_subst,
		AST_WORD_EXPAND_MODIFIER_SUBST,
		false);
	ACCEPT;
}

// :C<delimiter><pattern><delimiter><replacement><delimiter>[1gW]
// additional escape characters: <delimiter>, and see regex(3)
MACHINE(expand_modifier_C)
{
	CHAIN(
		expand_modifier_subst,
		AST_WORD_EXPAND_MODIFIER_SUBST_EXT,
		false);
	ACCEPT;
}

MACHINE(
	expand_modifier_subst,
	enum ASTWordExpandModifierType type,
	bool chained)
{
	EXPAND_MODIFIER_WITH_FLAGS(type, chained);

	if (!MATCH_ANY()) {
		REJECT;
	}
	char *delimiter = libias_str_ndup(
		this->buf + start_pos,
		this->i - start_pos);
	libias_scope_free(delimiter);

	const char *terminals[] =  { delimiter };
	PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
	PUSH_WORD_INCLUDING_MATCH;
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
	CHAIN(
		expand_modifiers_arg_with_terminals_and_delimiter,
		true,
		terminals,
		nitems(terminals),
		delimiter);

	// Set start to after the arg
	start_pos = this->i;

	CHAIN(
		expand_modifiers_arg_with_terminals_and_delimiter,
		true,
		terminals,
		nitems(terminals),
		delimiter);

	// Set start to after the arg
	start_pos = this->i;

	// Flags
	while (MATCH("1") || MATCH("g") || MATCH("W"));
	if (this->i > start_pos) {
		PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
		PUSH_WORD_INCLUDING_MATCH;
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
	}

	// Chained substitutions
	if (MATCH("C")) {
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		CHAIN(
			expand_modifier_subst,
			AST_WORD_EXPAND_MODIFIER_SUBST_EXT,
			true);
		ACCEPT;
	} else if (MATCH("S")) {
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		CHAIN(
			expand_modifier_subst,
			AST_WORD_EXPAND_MODIFIER_SUBST,
			true);
		ACCEPT;
	} else if (LOOKAHEAD(":") || LOOKAHEAD(expand_terminal)) {
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else {
		REJECT;
	}
}

MACHINE(expand_modifier_subst_systemv)
{
	// include the =
	this->i = this->last_match_start;
	if (MATCH(":")) {
		// nada
	}

	// TODO: bmake(1) says: a backslash is used to prevent the expansion
	// of a dollar sign (‘$’), not a preceding dollar sign as is usual.

	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SUBST_SYSV);

	const char *terminals[] =  { "=" };
	CHAIN(
		expand_modifiers_arg_with_terminals_and_delimiter,
		true,
		terminals,
		nitems(terminals),
		NULL);
	CHAIN(expand_modifiers_arg);

	// must be the last modifier
	if (LOOKAHEAD(expand_terminal)) {
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else {
		diagnostic_parse_error(
			this->diagnostic,
			this->builder,
			this->i, this->i,
			"System V substitution must be the last modifier");
		REJECT;
	}
}

// :T (no arguments)
MACHINE(expand_modifier_T)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_T);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :u (no arguments)
MACHINE(expand_modifier_u)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_u);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :?<true_string>:<false_string>
MACHINE(expand_modifier_if)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_IF);
	CHAIN(expand_modifiers_arg);
	if (MATCH(":")) {
		CHAIN(expand_modifiers_arg);
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	}
	REJECT;
}

// :@<temp>@<string>@
// additional escape characters: @ (maybe?)
MACHINE(expand_modifier_for)
{
	static const char *terminals[] = { "@" };
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_FOR);
	CHAIN(
		expand_modifiers_arg_with_terminals,
		true,
		terminals,
		nitems(terminals));

	CHAIN(
		expand_modifiers_arg_with_terminals,
		true,
		terminals,
		nitems(terminals));

	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :U<newval>
MACHINE(expand_modifier_U)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_U);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :D<newval>
MACHINE(expand_modifier_D)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_D);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :L (no arguments)
MACHINE(expand_modifier_L)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_L);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :P (no arguments)
MACHINE(expand_modifier_P)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_P);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :!<cmd>!
// addtional escape characters: !
MACHINE(expand_modifier_bang)
{
	static const char *terminals[] = { "!" };
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_BANG);
	CHAIN(
		expand_modifiers_arg_with_terminals,
		true,
		terminals,
		nitems(terminals));
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :sh (no arguments)
MACHINE(expand_modifier_sh)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_sh);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// ::=<str>
MACHINE(expand_modifier_set)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SET);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// ::?=<str>
MACHINE(expand_modifier_optset)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_OPTSET);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// ::+=<str>
MACHINE(expand_modifier_append)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_APPEND);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// ::!=<cmd>
MACHINE(expand_modifier_cmdset)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_CMDSET);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

// :[<index>]
// OR
// :[*]
// OR
// :[0]
// OR
// :[@]
// OR
// :[#]
MACHINE(expand_modifier_slice_index)
{
	if (LOOKAHEAD("@]")) {
		EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_WORD_SEQUENCE);
		MATCH("@]");
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else if (LOOKAHEAD("#]")) {
		EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_LENGTH);
		MATCH("#]");
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else if (LOOKAHEAD("*]")) {
		EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_SINGLE_WORD);
		MATCH("*]");
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else if (LOOKAHEAD("0]")) {
		EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_SINGLE_WORD0);
		MATCH("0]");
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else {
		EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_INDEX);
		MATCH("-");
		if (read_digit(this) == INVALID_CODEPOINT) {
			REJECT;
		}
		while (read_digit(this) != INVALID_CODEPOINT);
		if (MATCH("]")) {
			PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
			PUSH_WORD;
			PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);
			PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
			ACCEPT;
		} else {
			REJECT;
		}
	}

	REJECT;
}

// :[<start>..<end>]
MACHINE(expand_modifier_slice_range)
{
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_RANGE);

	if (MATCH("-")) {
		if (read_digit(this) == INVALID_CODEPOINT) {
			REJECT;
		}
		while (read_digit(this) != INVALID_CODEPOINT);
	} else if (read_digit(this) != INVALID_CODEPOINT) {
		while (read_digit(this) != INVALID_CODEPOINT);
	} else {
		REJECT;
	}

	PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
	PUSH_WORD_INCLUDING_MATCH;
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);

	if (!MATCH("..")) {
		REJECT;
	}
	start_pos = this->i;

	if (MATCH("-")) {
		if (read_digit(this) == INVALID_CODEPOINT) {
			REJECT;
		}
		while (read_digit(this) != INVALID_CODEPOINT);
	} else if (read_digit(this) != INVALID_CODEPOINT) {
		while (read_digit(this) != INVALID_CODEPOINT);
	} else {
		REJECT;
	}

	PUSH_MARKER(AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG);
	PUSH_WORD_INCLUDING_MATCH;
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG);

	if (MATCH("]")) {
		PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
		ACCEPT;
	} else {
		REJECT;
	}
}

MACHINE(expand_modifier_slice)
{
	if (_CHAIN(expand_modifier_slice_range)) {
		ACCEPT;
	}

	if (_CHAIN(expand_modifier_slice_index)) {
		ACCEPT;
	}

	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_SLICE_EXPAND);
	static const char *terminals[] = { "]" };
	CHAIN(
		expand_modifiers_arg_with_terminals,
		true,
		terminals,
		nitems(terminals));
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

MACHINE(expand_modifier_list)
{
	// include the $ so we can reuse expand_modifiers_arg below
	this->i = this->last_match_start;
	EXPAND_MODIFIER(AST_WORD_EXPAND_MODIFIER_LIST);
	CHAIN(expand_modifiers_arg);
	PUSH_MARKER(AST_BUILDER_WORD_POP_EXPAND_MODIFIER);
	ACCEPT;
}

#if PARSER_ASTBUILDER_DEBUG
void
debug_print_words(
	struct LibiasArray *words,
	const char *buf)
{
	struct SexpWriter *w = sexp_writer_new(true);
	sexp_writer_open_tree(w, "ast-builder-words");
	libias_array_foreach(words, struct ASTBuilderWord *, word) {
		const char *type =
			ASTBuilderWordType_tostring(word->type)
			+ strlen("AST_BUILDER_WORD_");
		sexp_writer_open_tree(w, type);
		sexp_writer_int64(w, "i", word->i);
		sexp_writer_int64(w, "len", word->len);
		if (word->type == AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER) {
			sexp_writer_keyword(
				w,
				"modifier-type",
				ASTWordExpandModifierType_sexp(word->modifier_type));
		}
		if (word->len > 0) {
			char *value = libias_str_ndup(buf + word->i, word->len);
			sexp_writer_string(w, "value", value);
			libias_free(value);
		}
		char *function = libias_str_printf("%s@%d", word->func, word->line);
		sexp_writer_string(w, "function", function);
		libias_free(function);
		sexp_writer_close_tree(w);
	}
	sexp_writer_close_tree(w);
	{
		libias_scope_buffer(f);
		sexp_writer_finish(w, f);
		fputs(libias_buffer_data(f), stderr);
	}
	libias_cleanup(&w);
}
#endif

struct ASTWord *
ast_builder_parse_word(
	struct ASTBuilder *builder,
	const size_t len,
	size_t offset,
	bool bool_expr,
	struct Diagnostic *diagnostic)
{
	libias_scope_array(words);
	struct ParseWordData *this = &(struct ParseWordData){
		.diagnostic = diagnostic,
		.words = words,
		.offset = offset,
		.builder = builder,
		.buf = libias_buffer_data(ast_builder_buffer(builder)) + offset,
		.i = 0,
		.len = len,
	};

	if (!append(this, 0, 0, bool_expr, NULL, this->i, this->i)) {
		return NULL;
	}

#if PARSER_ASTBUILDER_DEBUG
	debug_print_words(this->words, this->buf);
#endif

	libias_scope_stack(modifier_stack);
	libias_scope_stack(word_stack);
	struct ASTWord *root = ast_word_new_append();
	libias_stack_push(word_stack, root);
	size_t builder_words_len = libias_array_len(this->words);
	for (
		size_t builder_word_index = 0;
		builder_word_index < builder_words_len;
		builder_word_index++)
	{
		struct ASTBuilderWord *builder_word = libias_array_get(this->words, builder_word_index);
		switch (builder_word->type) {
		case AST_BUILDER_WORD_WORD:
		{
			struct ASTWord *parent = libias_stack_peek(word_stack);
			enum ASTWordBoolType if_type;
			if (builder_word->len == 0) {
				// nothing to do
			} else if (
				builder_word->bool_expr
				&& ASTWordBoolType_from_bmake(
					this->buf + builder_word->i,
					builder_word->len,
					&if_type)
				)
			{
				struct ASTWord *child = ast_word_new_bool(if_type);
				ast_word_add_child(parent, child);
			} else {
				struct ASTWord *child = ast_builder_word_string_unescape(
					this->buf + builder_word->i,
					builder_word->len,
					builder_word->i + this->offset);
				ast_word_add_child(parent, child);
			}
			break;
		}
		case AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER:
		{
			struct ASTWordExpandModifierBuilder *modifier_builder =
				ast_word_expand_modifier_builder(
					builder_word->modifier_type,
					this->builder,
					builder_word->i,
					diagnostic);
			switch (builder_word->modifier_type) {
			case AST_WORD_EXPAND_MODIFIER_SUBST:
			case AST_WORD_EXPAND_MODIFIER_SUBST_EXT:
				ast_word_expand_modifier_builder_set_chained_substitution(
					modifier_builder, builder_word->modifier_flags);
				break;
			default:
				break;
			}

			libias_stack_push(modifier_stack, modifier_builder);
			break;
		}
		case AST_BUILDER_WORD_POP_EXPAND_MODIFIER:
		{
			struct ASTWord *parent = libias_stack_peek(word_stack);
			struct ASTWordExpandModifierBuilder *modifier_builder =
				libias_stack_pop(modifier_stack);

			if (!parent) {
				libias_panic("parse error");
			}
			if (!modifier_builder) {
				libias_panic("parse error");
			}

			struct ASTWordExpandModifier *modifier =
				ast_word_expand_modifier_builder_finish(modifier_builder);
			if (modifier) {
				ast_word_add_expand_modifier(parent, modifier);
			} else {
				return NULL;
			}

			break;
		}
		case AST_BUILDER_WORD_PUSH_APPEND:
		{
			struct ASTWord *append = ast_word_new_append();
			libias_stack_push(word_stack, append);
			break;
		}
		case AST_BUILDER_WORD_PUSH_EXPAND:
		{
			struct ASTWord *expand = ast_word_new_expand();
			libias_stack_push(word_stack, expand);
			break;
		}
		case AST_BUILDER_WORD_PUSH_EXPAND_PARENTHESES:
		{
			struct ASTWord *expand = ast_word_new_expand_parentheses();
			libias_stack_push(word_stack, expand);
			break;
		}
		case AST_BUILDER_WORD_PUSH_EXPAND_SINGLE:
		{
			struct ASTWord *expand = ast_word_new_expand_single();
			libias_stack_push(word_stack, expand);
			break;
		}
		case AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG:
		{
			struct ASTWord *append = ast_word_new_append();
			libias_stack_push(word_stack, append);
			break;
		}
		case AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG:
		{
			struct ASTWordExpandModifierBuilder *modifier_builder =
				libias_stack_peek(modifier_stack);
			if (modifier_builder) {
				ast_word_expand_modifier_builder_add_argument(
					modifier_builder,
					ast_word_unwrap(libias_stack_pop(word_stack)));
			} else {
				libias_panic("modifier_args == NULL");
			}
			break;
		}
		case AST_BUILDER_WORD_POP:
		{
			struct ASTWord *word = libias_stack_pop(word_stack);
			word = ast_word_unwrap(word);
			if (ast_word_empty_p(word)) {
				libias_cleanup(&word);
			} else {
				struct ASTWord *parent = libias_stack_peek(word_stack);
				ast_word_add_child(parent, word);
			}
			break;
		}
		}
	}

	libias_array_foreach(this->words, struct ASTBuilderWord *, word) {
		libias_free(word);
	}

	root = ast_word_unwrap(libias_stack_pop(word_stack));

#if PARSER_ASTBUILDER_DEBUG
	libias_panic_unless(
		libias_stack_len(word_stack) == 0,
		"word stack not empty");
	libias_panic_unless(
		libias_stack_len(modifier_stack) == 0,
		"modifier stack not empty");
#endif

	// Cleanup
	libias_stack_foreach(
		modifier_stack,
		struct ASTWordExpandModifierBuilder *, modifier_builder)
	{
		libias_cleanup(&modifier_builder);
	}
	libias_stack_foreach(word_stack, struct ASTWord *, word) {
		libias_cleanup(&word);
	}

	return root;
}

struct ASTWord *
ast_builder_word_string_unescape(
	const char *buf,
	size_t len,
	size_t offset)
{
	char *s = libias_str_ndup(buf, len);
	struct ASTWord *word = ast_word_new_string(s);
	libias_free(s);
	return word;
}

struct ASTWord *
ast_builder_word_whitespace_unescape(
	const char *word,
	size_t len,
	size_t offset)
{
	libias_scope_buffer(buf);
	struct ParseWordData this = {};
	this.buf = word;
	this.len = len;

	// Don't do any validation here since word already went through the
	// tokenizer, so it consists only of '\t', ' ', '\\\n'. If there's a
	// problem it's up a layer. We want to unescape the line
	// continuation '\\\n' to just '\n'. Additionally
	// ast_word_new_whitespace() will validate it's input since it's a
	// user facing function.
	while (1) {
		auto c = read_char(&this);
		if (c == INVALID_CODEPOINT) {
			break;
		}
		if (c == '\\') {
			auto next = read_char(&this);
			if (next == INVALID_CODEPOINT) {
				break;
			}
		}
		libias_buffer_write(buf, word + this.last_match_start, 1, this.last_match_len);
	}

	return ast_word_new_whitespace(libias_buffer_data(buf));
}
