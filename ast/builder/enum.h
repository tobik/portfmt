// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTBuilderTokenType {
	AST_BUILDER_TOKEN_COMMENT,              // human:"comment"
    AST_BUILDER_TOKEN_CONDITIONAL_END,      // human:"conditional-end"
    AST_BUILDER_TOKEN_CONDITIONAL_EXPR,     // human:"conditional-expr"
    AST_BUILDER_TOKEN_CONDITIONAL_POSIX,    // human:"conditional-posix"
    AST_BUILDER_TOKEN_CONDITIONAL_TOKEN,    // human:"conditional-token"
    AST_BUILDER_TOKEN_CONDITIONAL_START,    // human:"conditional-start"
    AST_BUILDER_TOKEN_TARGET_COMMAND_END,   // human:"target-command-end"
    AST_BUILDER_TOKEN_TARGET_COMMAND_START, // human:"target-command-start"
    AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS, // human:"target-command-flags"
    AST_BUILDER_TOKEN_TARGET_COMMAND_TOKEN, // human:"target-command-token"
    AST_BUILDER_TOKEN_TARGET_RULE_END,      // human:"target-rule-end"
    AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR, // human:"target-rule-operator"
    AST_BUILDER_TOKEN_TARGET_RULE_START,    // human:"target-rule-start"
    AST_BUILDER_TOKEN_TARGET_RULE_TOKEN,    // human:"target-rule-token"
    AST_BUILDER_TOKEN_VARIABLE_END,         // human:"variable-end"
    AST_BUILDER_TOKEN_VARIABLE_START,       // human:"variable-start"
    AST_BUILDER_TOKEN_VARIABLE_MODIFIER,    // human:"variable-modifier"
    AST_BUILDER_TOKEN_VARIABLE_TOKEN,       // human:"variable-token"
    AST_BUILDER_TOKEN_WS,                   // human:"whitespace"
};

libias_attr_returns_nonnull
const char *ASTBuilderTokenType_human(enum ASTBuilderTokenType);

libias_attr_returns_nonnull
const char *ASTBuilderTokenType_tostring(enum ASTBuilderTokenType);

enum ASTBuilderConditionalType {
	AST_BUILDER_CONDITIONAL_INVALID,                  // human:"<invalid>"       bmake:"<invalid>"
    AST_BUILDER_CONDITIONAL_ELIF,                     // human:".elif"           bmake:"elif"
    AST_BUILDER_CONDITIONAL_ELIFDEF,                  // human:".elifdef"        bmake:"elifdef"
    AST_BUILDER_CONDITIONAL_ELIFNDEF,                 // human:".elifndef"       bmake:"elifndef"
    AST_BUILDER_CONDITIONAL_ELIFMAKE,                 // human:".elifmake"       bmake:"elifmake"
    AST_BUILDER_CONDITIONAL_ELIFNMAKE,                // human:".elifnmake"      bmake:"elifnmake"
    AST_BUILDER_CONDITIONAL_ELSE,                     // human:".else"           bmake:"else"
    AST_BUILDER_CONDITIONAL_ENDFOR,                   // human:".endfor"         bmake:"endfor"
    AST_BUILDER_CONDITIONAL_ENDIF,                    // human:".endif"          bmake:"endif"
    AST_BUILDER_CONDITIONAL_ERROR,                    // human:".error"          bmake:"error"
    AST_BUILDER_CONDITIONAL_EXPORT_ENV,               // human:".export-env"     bmake:"export-env"
    AST_BUILDER_CONDITIONAL_EXPORT_ENV_DOT,           // human:".export.env"     bmake:"export.env"
    AST_BUILDER_CONDITIONAL_EXPORT_LITERAL,           // human:".export-literal" bmake:"export-literal"
    AST_BUILDER_CONDITIONAL_EXPORT,                   // human:".export"         bmake:"export"
    AST_BUILDER_CONDITIONAL_FOR,                      // human:".for"            bmake:"for"
    AST_BUILDER_CONDITIONAL_IF,                       // human:".if"             bmake:"if"
    AST_BUILDER_CONDITIONAL_IFDEF,                    // human:".ifdef"          bmake:"ifdef"
    AST_BUILDER_CONDITIONAL_IFMAKE,                   // human:".ifmake"         bmake:"ifmake"
    AST_BUILDER_CONDITIONAL_IFNDEF,                   // human:".ifndef"         bmake:"ifndef"
    AST_BUILDER_CONDITIONAL_IFNMAKE,                  // human:".ifnmake"        bmake:"ifnmake"
    AST_BUILDER_CONDITIONAL_INCLUDE,                  // human:".include"        bmake:"include"
    AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL,         // human:".-include"       bmake:"-include"
    AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_D,       // human:".dinclude"       bmake:"dinclude"
    AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S,       // human:".sinclude"       bmake:"sinclude"
    AST_BUILDER_CONDITIONAL_INCLUDE_POSIX,            // human:"include"         bmake:"include"
    AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL,   // human:"-include"        bmake:"-include"
    AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S, // human:"sinclude"        bmake:"sinclude"
    AST_BUILDER_CONDITIONAL_INFO,                     // human:".info"           bmake:"info"
    AST_BUILDER_CONDITIONAL_UNDEF,                    // human:".undef"          bmake:"undef"
    AST_BUILDER_CONDITIONAL_UNEXPORT_ENV,             // human:".unexport-env"   bmake:"unexport-env"
    AST_BUILDER_CONDITIONAL_UNEXPORT,                 // human:".unexport"       bmake:"unexport"
    AST_BUILDER_CONDITIONAL_WARNING,                  // human:".warning"        bmake:"warning"
};

libias_attr_nonnull(1)
bool ASTBuilderConditionalType_from_bmake(const char *, const size_t, enum ASTBuilderConditionalType *);

libias_attr_returns_nonnull
const char *ASTBuilderConditionalType_human(enum ASTBuilderConditionalType);

libias_attr_returns_nonnull
const char *ASTBuilderConditionalType_tostring(enum ASTBuilderConditionalType);
