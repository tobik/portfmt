// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTBuilderWordType {
	AST_BUILDER_WORD_WORD,
	AST_BUILDER_WORD_PUSH_APPEND,
	AST_BUILDER_WORD_PUSH_EXPAND,
	AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER,
	AST_BUILDER_WORD_PUSH_EXPAND_MODIFIER_ARG,
	AST_BUILDER_WORD_PUSH_EXPAND_PARENTHESES,
	AST_BUILDER_WORD_PUSH_EXPAND_SINGLE,
	AST_BUILDER_WORD_POP,
	AST_BUILDER_WORD_POP_EXPAND_MODIFIER,
	AST_BUILDER_WORD_POP_EXPAND_MODIFIER_ARG,
};

libias_attr_returns_nonnull
const char *ASTBuilderWordType_tostring(enum ASTBuilderWordType);

libias_attr_nonnull(1, 5)
struct ASTWord *ast_builder_parse_word(struct ASTBuilder *, const size_t, size_t, bool, struct Diagnostic *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWord *ast_builder_word_string_unescape(const char *, size_t, size_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct ASTWord *ast_builder_word_whitespace_unescape(const char *, size_t, size_t);
