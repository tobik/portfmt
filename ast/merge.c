// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libias/array.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/str.h>
#include <libias/trait/compare.h>

#include "ast.h"
#include "ast/format.h"
#include "ast/merge.h"
#include "ast/word.h"
#include "rules.h"

enum InsertVariableState {
	INSERT_VARIABLE_NO_POINT_FOUND = -1,
	INSERT_VARIABLE_PREPEND = -2,
};

struct EditMergeState {
	struct LibiasMempool *pool;
	struct Rules *rules;
	struct LibiasArray *format_nodes;
	struct AST *root;
	struct Diagnostic *diagnostic;
	uint32_t flags; // see enum ASTMergeFlags
};

struct FindVariableState {
	struct AST *var;
	struct AST *retval;
};

struct RemoveConsecutiveEmptyLinesState {
	size_t counter;
};

struct VariableMergeParameter {
	struct Variable *var;
	struct LibiasArray *nonvars;
	struct LibiasArray *values;
};

// Prototypes
static void delete_variable(struct AST *root, struct AST *var);
static void delete_variable_visit_include(struct ASTVisit *visit, struct AST *node);
static void delete_variable_visit_variable(struct ASTVisit *visit, struct AST *node);
static bool edit_merge_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void edit_merge_visit_variable(struct ASTVisit *visit, struct AST *node);
static struct AST *empty_line(struct EditMergeState *this);
static ssize_t find_insert_point_generic(struct Rules *rules, struct AST *root, const char *var, enum BlockType *block_before_var);
static ssize_t find_insert_point_same_block(struct Rules *rules, struct AST *root, const char *var, enum BlockType *block_before_var);
static struct AST *find_variable(struct AST *root, struct AST *var);
static bool find_variable_visit_children_p(struct ASTVisit *visit, struct AST *node);
static void find_variable_visit_variable(struct ASTVisit *visit, struct AST *node);
static bool insert_empty_line_before_block(enum BlockType before, enum BlockType block);
static void insert_variable(struct EditMergeState *this, struct ASTEdit *ast_edit, struct AST *root, struct AST *template);
static void prepend_variable(struct EditMergeState *this, struct ASTEdit *ast_edit, struct AST *root, struct AST *node, enum BlockType block_var);
static void refactor_remove_consecutive_empty_lines(struct AST *root);
static void refactor_remove_consecutive_empty_lines_visit_comment(struct ASTVisit *visit, struct AST *node);
static void refactor_remove_consecutive_empty_lines_visit_other(struct ASTVisit *visit, struct AST *node);
static void refactor_remove_consecutive_empty_lines_visit_root(struct ASTVisit *visit, struct AST *node);

void
refactor_remove_consecutive_empty_lines_visit_root(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RemoveConsecutiveEmptyLinesState *this = ast_visit_context(visit);
	// Clear initial empty lines
	this->counter = 1;
}

void
refactor_remove_consecutive_empty_lines_visit_comment(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RemoveConsecutiveEmptyLinesState *this = ast_visit_context(visit);
	struct ASTWordsEdit *edit = ast_words_edit_new(
		node,
		ast_visit_diagnostic(visit),
		AST_WORDS_EDIT_DEFAULT);
	ast_words_foreach(node, line) {
		if (ast_word_whitespace_p(line)) {
			if (this->counter++ > 0) {
				ast_words_edit_remove_word(edit, line);
			}
		} else {
			this->counter = 0;
		}
	}
	ast_words_edit_apply(edit);

	// There's no need to reformat the edited node. Formatting a
	// comment does nothing at the moment.
}

void
refactor_remove_consecutive_empty_lines_visit_other(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct RemoveConsecutiveEmptyLinesState *this = ast_visit_context(visit);
	this->counter = 0;
}

void
refactor_remove_consecutive_empty_lines(struct AST *root)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.default_visit_fn = refactor_remove_consecutive_empty_lines_visit_other;
	visit_trait.visit_pre_root = refactor_remove_consecutive_empty_lines_visit_root;
	visit_trait.visit_comment = refactor_remove_consecutive_empty_lines_visit_comment;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(
		visit,
		&(struct RemoveConsecutiveEmptyLinesState){
			.counter = 0,
		});
	ast_visit_run(visit);
	libias_cleanup(&visit);
}

struct AST *
empty_line(struct EditMergeState *this)
{
	struct AST *node = ast_new_comment();
	auto edit = ast_words_edit_new(
		node,
		this->diagnostic,
		AST_WORDS_EDIT_DEFAULT);
	struct ASTWord *comment_word = ast_word_new_whitespace("");
	ast_words_edit_append_word(edit, comment_word);
	ast_words_edit_apply(edit);
	return node;
}

bool
insert_empty_line_before_block(enum BlockType before, enum BlockType block)
{
	return before < block && (before < BLOCK_USES || block > BLOCK_PLIST);
}

void
prepend_variable(
	struct EditMergeState *this,
	struct ASTEdit *ast_edit,
	struct AST *root,
	struct AST *node,
	enum BlockType block_var)
{
	// Append only after initial comments
	size_t start_index = 0;
	ast_children_foreach(ast_parent(root), sibling) {
		if (ast_comment_p(sibling)) {
			start_index = sibling_index + 1;
		} else {
			break;
		}
	}

	bool added = false;
	ast_children_foreach_slice(ast_parent(root), start_index, -1, sibling) {
		if (sibling_index == start_index) {
			ast_edit_add_child_before(ast_edit, node, sibling);
			added = true;
		}
		// Insert new empty line
		switch (ast_type(sibling)) {
		case AST_COMMENT:
		case AST_EXPR:
		case AST_FOR:
		case AST_IF:
		case AST_INCLUDE:
		case AST_TARGET_RULE:
			if (added) {
				ast_edit_add_child_before(ast_edit, empty_line(this), sibling);
				goto reformat;
			}
			break;
		case AST_FOR_BINDINGS:
		case AST_FOR_WORDS:
		case AST_FOR_BODY:
		case AST_FOR_END:
		case AST_IF_BRANCH:
		case AST_TARGET_RULE_BODY:
		case AST_TARGET_RULE_TARGETS:
		case AST_TARGET_RULE_DEPENDENCIES:
		case AST_VARIABLE_NAME:
			break;
		case AST_VARIABLE: {
			char *name = ast_word_flatten(ast_variable_name(sibling));
			enum BlockType block = rules_variable_order_block(
				this->rules,
				root,
				name,
				NULL);
			libias_free(name);
			if (block != block_var && insert_empty_line_before_block(block, block_var)) {
				ast_edit_add_child_before(ast_edit, empty_line(this), sibling);
				goto reformat;
			}
			break;
		} case AST_ROOT:
		case AST_TARGET_COMMAND:
			break;
		}
	}

	if (!added) {
		// If all else fails just append it
		ast_edit_append_child(ast_edit, node);
	}

 reformat:
	libias_array_append(this->format_nodes, node);
}

void
delete_variable_visit_include(
	struct ASTVisit *visit,
	struct AST *node)
{
	if (ast_ports_framework_include_p(node)) {
		ast_visit_stop(visit);
	}
}

void
delete_variable_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct AST *var = ast_visit_context(visit);
	struct ASTWord *node_name = ast_variable_name(node);
	struct ASTWord *var_name = ast_variable_name(var);
	if (ast_word_compare->compare(&node_name, &var_name, NULL) == 0) {
		ast_edit_remove_child(ast_visit_root_edit(visit), node);
	}
}

void
delete_variable(
	struct AST *root,
	struct AST *var)
{
	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_include = delete_variable_visit_include;
	visit_trait.visit_variable = delete_variable_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, var);
	ast_visit_run(visit);
	libias_cleanup(&visit);
}

ssize_t
find_insert_point_generic(
	struct Rules *rules,
	struct AST *root,
	const char *var,
	enum BlockType *block_before_var)
{
	ssize_t insert_after = INSERT_VARIABLE_NO_POINT_FOUND;
	*block_before_var = BLOCK_UNKNOWN;
	bool always_greater = true;

	ast_children_foreach(ast_parent(root), sibling) {
		switch (ast_type(sibling)) {
		case AST_ROOT:
		case AST_IF:
		case AST_IF_BRANCH:
		case AST_FOR:
		case AST_FOR_BINDINGS:
		case AST_FOR_WORDS:
		case AST_FOR_BODY:
		case AST_FOR_END:
		case AST_COMMENT:
		case AST_EXPR:
		case AST_TARGET_COMMAND:
		case AST_TARGET_RULE:
		case AST_TARGET_RULE_TARGETS:
		case AST_TARGET_RULE_DEPENDENCIES:
		case AST_TARGET_RULE_BODY:
		case AST_VARIABLE_NAME:
			break;
		case AST_VARIABLE: {
			char *name = ast_word_flatten(ast_variable_name(sibling));
			struct RulesCompareOrder compare_order_data = { rules, root };
			if (rules_compare_order(&name, &var, &compare_order_data) < 0) {
				*block_before_var = rules_variable_order_block(rules, root, name, NULL);
				insert_after = sibling_index;
				always_greater = false;
			}
			libias_free(name);
			break;
		}
		case AST_INCLUDE:
			if (insert_after >=0 && ast_ports_framework_include_p(sibling)) {
				goto loop_end;
			}
			break;
		}
	}

loop_end:
	if (always_greater) {
		insert_after = INSERT_VARIABLE_PREPEND;
	}

	return insert_after;
}

ssize_t
find_insert_point_same_block(
	struct Rules *rules,
	struct AST *root,
	const char *var,
	enum BlockType *block_before_var)
{
	ssize_t insert_after = INSERT_VARIABLE_NO_POINT_FOUND;
	enum BlockType block_var = rules_variable_order_block(rules, root, var, NULL);
	*block_before_var = BLOCK_UNKNOWN;

	ast_children_foreach(ast_parent(root), sibling) {
		switch (ast_type(sibling)) {
		case AST_ROOT:
		case AST_IF:
		case AST_IF_BRANCH:
		case AST_FOR:
		case AST_FOR_BINDINGS:
		case AST_FOR_WORDS:
		case AST_FOR_BODY:
		case AST_FOR_END:
		case AST_COMMENT:
		case AST_EXPR:
		case AST_TARGET_COMMAND:
		case AST_TARGET_RULE:
		case AST_TARGET_RULE_TARGETS:
		case AST_TARGET_RULE_DEPENDENCIES:
		case AST_TARGET_RULE_BODY:
		case AST_VARIABLE_NAME:
			break;
		case AST_VARIABLE: {
			char *name = ast_word_flatten(ast_variable_name(sibling));
			libias_scope_free(name);
			enum BlockType block = rules_variable_order_block(rules, root, name, NULL);
			if (block != block_var) {
				continue;
			}
			struct RulesCompareOrder compare_order_data = { rules, root };
			int cmp = rules_compare_order(&name, &var, &compare_order_data);
			if (cmp < 0) {
				*block_before_var = block;
				insert_after = sibling_index;
			} else if (cmp == 0) {
				insert_after = sibling_index;
			}
			break;
		} case AST_INCLUDE:
			if (ast_ports_framework_include_p(sibling)) {
				return insert_after;
			}
			break;
		}
	}

	return insert_after;
}

void
insert_variable(
	struct EditMergeState *this,
	struct ASTEdit *ast_edit,
	struct AST *root,
	struct AST *template)
{
	struct AST *node = ast_clone(template);

	char *name = ast_word_flatten(ast_variable_name(node));
	libias_scope_free(name);

	enum BlockType block_var = rules_variable_order_block(
		this->rules,
		root,
		name,
		NULL);
	enum BlockType block_before_var = BLOCK_UNKNOWN;
	ssize_t insert_after = find_insert_point_same_block(
		this->rules,
		root,
		name,
		&block_before_var);
	if (insert_after < 0) {
		insert_after = find_insert_point_generic(
			this->rules,
			root,
			name,
			&block_before_var);
	}

	switch (insert_after) {
	case INSERT_VARIABLE_PREPEND:
		prepend_variable(this, ast_edit, root, node, block_var);
		return;
	case INSERT_VARIABLE_NO_POINT_FOUND:
		// No variable found where we could insert our new
		// var.  Insert it before any conditional or target
		// if there are any.
		ast_children_foreach(ast_parent(root), sibling) {
			switch (ast_type(sibling)) {
			case AST_ROOT:
			case AST_IF:
			case AST_IF_BRANCH:
			case AST_FOR:
			case AST_FOR_BINDINGS:
			case AST_FOR_WORDS:
			case AST_FOR_BODY:
			case AST_FOR_END:
			case AST_COMMENT:
			case AST_EXPR:
			case AST_TARGET_COMMAND:
			case AST_TARGET_RULE:
			case AST_TARGET_RULE_TARGETS:
			case AST_TARGET_RULE_DEPENDENCIES:
			case AST_VARIABLE:
			case AST_VARIABLE_NAME:
				break;
			case AST_INCLUDE:
			case AST_TARGET_RULE_BODY:
				ast_edit_add_child_before(ast_edit, node, sibling);
				ast_edit_add_child_before(
					ast_edit,
					empty_line(this),
					sibling);
				return;
			}
		}
		// Prepend it instead if there are no conditionals or targets
		prepend_variable(this, ast_edit, root, node, block_var);
		return;
	default:
		libias_panic_if(insert_after < 0, "negative insertion point");
	}

	size_t insert_point = insert_after;
	ast_children_foreach(ast_parent(root), sibling) {
		if (sibling_index > insert_point) {
			ast_edit_add_child_before(ast_edit, node, sibling);
			libias_array_append(this->format_nodes, node);
			if (block_before_var != BLOCK_UNKNOWN && block_before_var != block_var &&
			    insert_empty_line_before_block(block_before_var, block_var)) {
				ast_edit_add_child_before(ast_edit, empty_line(this), node);
			}
			return;
		}
	}

	// If all else fails just append it
	if (block_before_var != BLOCK_UNKNOWN && block_before_var != block_var &&
	    insert_empty_line_before_block(block_before_var, block_var)) {
		ast_edit_append_child(ast_edit, empty_line(this));
	}
	ast_edit_append_child(ast_edit, node);
	libias_array_append(this->format_nodes, node);
}

bool
find_variable_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	switch (ast_type(node)) {
	case AST_FOR:
	case AST_IF:
	case AST_INCLUDE:
	case AST_TARGET_RULE:
		return false;
	default:
		return true;
	}
}

void
find_variable_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct FindVariableState *this = ast_visit_context(visit);

	struct ASTWord *node_name = ast_variable_name(node);
	struct ASTWord *var_name = ast_variable_name(this->var);
	if (ast_word_compare->compare(&node_name, &var_name, NULL) == 0) {
		this->retval = node;
		ast_visit_stop(visit);
	}
}

struct AST *
find_variable(struct AST *root, struct AST *var)
{
	struct FindVariableState this = {
		.var = var,
		.retval = NULL,
	};

	struct ASTVisitTrait visit_trait;
	ast_visit_trait_init(&visit_trait);
	visit_trait.visit_children_p = find_variable_visit_children_p; // XXX
	visit_trait.visit_variable = find_variable_visit_variable;

	struct ASTVisit *visit = ast_visit_new(root, &visit_trait);
	ast_visit_set_context(visit, &this);
	ast_visit_run(visit);
	libias_cleanup(&visit);

	return this.retval;
}

bool
edit_merge_visit_children_p(
	struct ASTVisit *visit,
	struct AST *node)
{
	switch (ast_type(node)) {
	case AST_FOR:
	case AST_IF:
	case AST_INCLUDE:
	case AST_TARGET_RULE:
		return false;
	default:
		return true;
	}
}

void
edit_merge_visit_variable(
	struct ASTVisit *visit,
	struct AST *node)
{
	struct EditMergeState *this = ast_visit_context(visit);

	struct AST *mergenode = NULL;
	if (ast_variable_modifier(node) == AST_VARIABLE_MODIFIER_SHELL &&
		(this->flags & AST_MERGE_SHELL_IS_DELETE)) {
		auto ast_edit = ast_edit_new(
			this->root,
			ast_visit_diagnostic(visit),
			AST_EDIT_DEFAULT);
		delete_variable(this->root, node);
		ast_edit_apply(ast_edit);
		return;
	} else if ((mergenode = find_variable(this->root, node))) {
		switch (ast_variable_modifier(node)) {
		case AST_VARIABLE_MODIFIER_ASSIGN:
		{
			auto edit = ast_words_edit_new(
				mergenode,
				ast_visit_diagnostic(visit),
				AST_WORDS_EDIT_DEFAULT);
			struct ASTWord *comment = ast_node_comment(mergenode);
			ast_words_edit_remove_all_words(edit);
			if (comment) {
				ast_words_edit_append_word(
					edit,
					ast_word_clone(comment));
			}
			ast_words_foreach(node, word) {
				if (ast_word_whitespace_p(word)) {
					continue;
				}
				struct ASTWord *clone = ast_word_clone(word);
				ast_words_edit_append_word(edit, clone);
			}
			ast_words_edit_apply(edit);
			libias_array_append(this->format_nodes, mergenode);
			return;
		}
		case AST_VARIABLE_MODIFIER_APPEND:
			if (!ast_node_comment(mergenode)) {
				auto edit = ast_words_edit_new(
					mergenode,
					ast_visit_diagnostic(visit),
					AST_WORDS_EDIT_DEFAULT);
				ast_words_foreach(node, word) {
					if (ast_word_whitespace_p(word)) {
						continue;
					}
					struct ASTWord *clone = ast_word_clone(word);
					ast_words_edit_append_word(edit, clone);
				}
				ast_words_edit_apply(edit);
				libias_array_append(this->format_nodes, mergenode);
				return;
			}
			break;
		case AST_VARIABLE_MODIFIER_EXPAND: // TODO
		{
			auto edit = ast_words_edit_new(
				mergenode,
				ast_visit_diagnostic(visit),
				AST_WORDS_EDIT_DEFAULT);
			ast_words_edit_remove_all_words(edit);
			ast_words_foreach(node, word) {
				if (ast_word_whitespace_p(word)) {
					continue;
				}
				struct ASTWord *clone = ast_word_clone(word);
				ast_words_edit_append_word(edit, clone);
			}
			ast_words_edit_apply(edit);
			libias_array_append(this->format_nodes, mergenode);
			auto ast_edit = ast_edit_new(
				mergenode,
				ast_visit_diagnostic(visit),
				AST_EDIT_DEFAULT);
			ast_edit_set_variable_modifier(
				ast_edit,
				AST_VARIABLE_MODIFIER_EXPAND);
			ast_edit_apply(ast_edit);
			return;
		}
		case AST_VARIABLE_MODIFIER_OPTIONAL:
		{
			auto edit = ast_words_edit_new(
				mergenode,
				ast_visit_diagnostic(visit),
				AST_WORDS_EDIT_DEFAULT);
			ast_words_edit_remove_all_words(edit);
			ast_words_foreach(node, word) {
				if (ast_word_whitespace_p(word)) {
					continue;
				}
				struct ASTWord *clone = ast_word_clone(word);
				ast_words_edit_append_word(edit, clone);
			}
			ast_words_edit_apply(edit);
			libias_array_append(this->format_nodes, mergenode);
			auto ast_edit = ast_edit_new(mergenode, this->diagnostic, AST_EDIT_DEFAULT);
			ast_edit_set_variable_modifier(
				ast_edit,
				AST_VARIABLE_MODIFIER_OPTIONAL);
			ast_edit_apply(ast_edit);
			return;
		}
		case AST_VARIABLE_MODIFIER_SHELL:
		{
			auto edit = ast_words_edit_new(
				mergenode,
				ast_visit_diagnostic(visit),
				AST_WORDS_EDIT_DEFAULT);
			ast_words_edit_remove_all_words(edit);
			ast_words_foreach(node, word) {
				if (ast_word_whitespace_p(word)) {
					continue;
				}
				struct ASTWord *clone = ast_word_clone(word);
				ast_words_edit_append_word(edit, clone);
			}
			ast_words_edit_apply(edit);
			libias_array_append(this->format_nodes, mergenode);
			auto ast_edit = ast_edit_new(mergenode, this->diagnostic, AST_EDIT_DEFAULT);
			ast_edit_set_variable_modifier(
				ast_edit,
				AST_VARIABLE_MODIFIER_SHELL);
			ast_edit_apply(ast_edit);
			return;
		}
		}
	}
	auto ast_edit = ast_edit_new(
		this->root,
		ast_visit_diagnostic(visit),
		AST_EDIT_DEFAULT);
	insert_variable(
		this,
		ast_edit,
		this->root,
		node);
	ast_edit_apply(ast_edit);
}

// Merge SOURCE tree into TARGET tree according to RULES and FLAGS. If
// FORMAT_SETTINGS is non-NULL then the merged nodes are reformatted.
void
ast_merge(
	struct AST *target,
	struct AST *source,
	struct Diagnostic *diagnostic,
	struct Rules *rules,
	struct ASTFormatSettings *format_settings,
	uint32_t flags)
{
	libias_scope_mempool(pool);

	// fputs("to merge:\n", stderr);
	// ast_print(mergetree, stderr);
	// fputs("\nbefore:\n", stderr);
	// ast_print(root, stderr);
	struct EditMergeState this = {
		.pool = pool,
		.rules = rules,
		.format_nodes = libias_mempool_array(pool),
		.root = target,
		.flags = flags,
	};

	struct ASTVisitTrait source_visit_trait;
	ast_visit_trait_init(&source_visit_trait);
	source_visit_trait.visit_children_p = edit_merge_visit_children_p;
	source_visit_trait.visit_variable = edit_merge_visit_variable;

	struct ASTVisit *source_visit = ast_visit_new(source, &source_visit_trait);
	this.diagnostic = ast_visit_diagnostic(source_visit);
	ast_visit_set_context(source_visit, &this);
	ast_visit_run(source_visit);
	libias_cleanup(&source_visit);

	// XXX: If the code above would be good this should be needed at all
	refactor_remove_consecutive_empty_lines(target);

	if (format_settings) {
		libias_array_foreach(this.format_nodes, struct AST *, node) {
			ast_format(node, rules, format_settings, diagnostic);
		}
	}

	// fputs("\nafter:\n", stderr);
	// ast_print(root, stderr);
}
