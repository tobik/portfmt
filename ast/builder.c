// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libias/array.h>
#include <libias/buffer.h>
#include <libias/iterator.h>
#include <libias/mempool.h>
#include <libias/stack.h>
#include <libias/str.h>

#include "ast.h"
#include "ast/builder.h"
#include "ast/builder/enum.h"
#include "ast/builder/tokenizer.h"
#include "ast/builder/word.h"
#include "ast/word.h"
#include "diagnostic.h"
#include "rules.h"

struct ASTBuilder {
	bool finished;
	struct LibiasMempool *pool;
	char *filename;
	struct AST *root;
	struct LibiasArray *comments;
	struct LibiasArray *tokens;
	struct LibiasStack *forstack;
	struct LibiasStack *ifstack;
	struct LibiasStack *nodestack;
	struct LibiasBuffer *only_whitespace;
	struct Diagnostic *diagnostic;

	struct LibiasBuffer *buffer;
};

// Prototypes
static bool ASTBuilderConditionalType_to_ASTExprType(enum ASTBuilderConditionalType value, enum ASTExprType *retval);
static bool ASTBuilderConditionalType_to_ASTIfType(enum ASTBuilderConditionalType value, enum ASTIfType *retval);
static bool ASTBuilderConditionalType_to_ASTIncludeType(enum ASTBuilderConditionalType value, enum ASTIncludeType *retval);
static struct ASTWord *create_ast_words(struct LibiasMempool *pool, struct ASTBuilder *this, ssize_t a, ssize_t b, struct ASTWordsEdit *edit, size_t word_start_pos, bool if_expr);
static bool flush_comments(struct ASTBuilder *this, struct AST *parent);
static void flush_trailing_whitespace(struct ASTBuilder *this, struct ASTWordsEdit *edit, const size_t i);
static struct AST *unwind_until_node(struct LibiasStack *nodestack, struct AST *parent);

bool
ASTBuilderConditionalType_to_ASTExprType(enum ASTBuilderConditionalType value, enum ASTExprType *retval)
{
	switch (value) {
	case AST_BUILDER_CONDITIONAL_ERROR:
		*retval = AST_EXPR_ERROR;
		return true;
	case AST_BUILDER_CONDITIONAL_EXPORT_ENV:
		*retval = AST_EXPR_EXPORT_ENV;
		return true;
	case AST_BUILDER_CONDITIONAL_EXPORT_ENV_DOT:
		*retval = AST_EXPR_EXPORT_ENV_DOT;
		return true;
	case AST_BUILDER_CONDITIONAL_EXPORT_LITERAL:
		*retval = AST_EXPR_EXPORT_LITERAL;
		return true;
	case AST_BUILDER_CONDITIONAL_EXPORT:
		*retval = AST_EXPR_EXPORT;
		return true;
	case AST_BUILDER_CONDITIONAL_INFO:
		*retval = AST_EXPR_INFO;
		return true;
	case AST_BUILDER_CONDITIONAL_UNDEF:
		*retval = AST_EXPR_UNDEF;
		return true;
	case AST_BUILDER_CONDITIONAL_UNEXPORT_ENV:
		*retval = AST_EXPR_UNEXPORT_ENV;
		return true;
	case AST_BUILDER_CONDITIONAL_UNEXPORT:
		*retval = AST_EXPR_UNEXPORT;
		return true;
	case AST_BUILDER_CONDITIONAL_WARNING:
		*retval = AST_EXPR_WARNING;
		return true;
	default:
		return false;
	}
}

bool
ASTBuilderConditionalType_to_ASTIncludeType(enum ASTBuilderConditionalType value, enum ASTIncludeType *retval)
{
	switch (value) {
	case AST_BUILDER_CONDITIONAL_INCLUDE:
		*retval = AST_INCLUDE_BMAKE;
		return true;
	case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL:
		*retval = AST_INCLUDE_OPTIONAL;
		return true;
	case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_D:
		*retval = AST_INCLUDE_OPTIONAL_D;
		return true;
	case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S:
		*retval = AST_INCLUDE_OPTIONAL_S;
		return true;
	case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX:
		*retval = AST_INCLUDE_POSIX;
		return true;
	case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL:
		*retval = AST_INCLUDE_POSIX_OPTIONAL;
		return true;
	case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S:
		*retval = AST_INCLUDE_POSIX_OPTIONAL_S;
		return true;
	default:
		return false;
	}
}

bool
ASTBuilderConditionalType_to_ASTIfType(enum ASTBuilderConditionalType value, enum ASTIfType *retval)
{
	switch (value) {
	case AST_BUILDER_CONDITIONAL_IF:
		*retval = AST_IF_IF;
		return true;
	case AST_BUILDER_CONDITIONAL_IFDEF:
		*retval = AST_IF_DEF;
		return true;
	case AST_BUILDER_CONDITIONAL_IFMAKE:
		*retval = AST_IF_MAKE;
		return true;
	case AST_BUILDER_CONDITIONAL_IFNDEF:
		*retval = AST_IF_NDEF;
		return true;
	case AST_BUILDER_CONDITIONAL_IFNMAKE:
		*retval = AST_IF_NMAKE;
		return true;
	case AST_BUILDER_CONDITIONAL_ELIF:
		*retval = AST_IF_IF;
		return true;
	case AST_BUILDER_CONDITIONAL_ELIFDEF:
		*retval = AST_IF_DEF;
		return true;
	case AST_BUILDER_CONDITIONAL_ELIFNDEF:
		*retval = AST_IF_NDEF;
		return true;
	case AST_BUILDER_CONDITIONAL_ELIFMAKE:
		*retval = AST_IF_MAKE;
		return true;
	case AST_BUILDER_CONDITIONAL_ELIFNMAKE:
		*retval = AST_IF_NMAKE;
		return true;
	case AST_BUILDER_CONDITIONAL_ELSE:
		*retval = AST_IF_ELSE;
		return true;
	default:
		return false;
	}
}

struct ASTWord *
create_ast_words(
	struct LibiasMempool *pool,
	struct ASTBuilder *this,
	ssize_t a,
	ssize_t b,
	struct ASTWordsEdit *edit,
	size_t word_start_pos,
	bool if_expr)
{
	struct ASTBuilderToken *comment = NULL;
	libias_array_foreach_slice(this->tokens, a, b, struct ASTBuilderToken *, t) {
		libias_panic_if(
			t->type == AST_BUILDER_TOKEN_WS,
			"whitespace tokens are not allowed here");
		if (t->len == 0) {
			continue;
		}

		if (t->whitespace_before_len > 0) {
			struct ASTWord *word = ast_builder_word_whitespace_unescape(
				t->whitespace_before,
				t->whitespace_before_len,
				t->i - t->whitespace_before_len);
			ast_words_edit_append_word(edit, word);
		}

		if (libias_str_prefix_p(libias_buffer_data(this->buffer) + t->i, "#")) {
			libias_panic_unless(
				(t_index + 1) == libias_array_len(this->tokens),
				"comment should be the last token");
			comment = t;
			break;
		} else {
			struct ASTWord *word = ast_builder_parse_word(
				this,
				t->len,
				t->i,
				if_expr,
				this->diagnostic);
			if (!word) {
				return NULL;
			}
			ast_words_edit_append_word(edit, word);
		}
	}

	if (comment) {
		libias_panic_unless(
			comment->len > 0,
			"zero length comment");
		// We remove the # since it's implied for AST_WORD_COMMENT
		struct ASTWord *word = ast_word_new_comment(
			libias_mempool_take(
				pool,
				libias_str_ndup(
					libias_buffer_data(this->buffer) + comment->i + 1,
					comment->len - 1)));
		ast_words_edit_append_word(edit, word);
		return word;
	} else {
		return NULL;
	}
}

struct ASTBuilder *
ast_builder_new(
	struct LibiasBuffer *buffer,
	struct Diagnostic *diagnostic,
	const char *filename)
{
	struct ASTBuilder *builder = libias_alloc(struct ASTBuilder);
	builder->filename = libias_str_dup(filename);
	builder->buffer = buffer;
	builder->diagnostic = diagnostic;
	builder->pool = libias_mempool_new();
	builder->comments = libias_mempool_array(builder->pool);
	builder->only_whitespace = libias_buffer_new();
	libias_mempool_take(builder->pool, builder->only_whitespace);
	builder->tokens = libias_mempool_array(builder->pool);
	builder->forstack = libias_mempool_stack(builder->pool);
	builder->ifstack = libias_mempool_stack(builder->pool);
	builder->nodestack = libias_mempool_stack(builder->pool);
	builder->root = ast_new_root();
	libias_stack_push(builder->nodestack, builder->root);

	return builder;
}

void
ast_builder_cleanup(struct ASTBuilder **builder_ptr)
{
	struct ASTBuilder *builder = *builder_ptr;
	if (builder) {
		libias_cleanup(&builder->filename);
		libias_cleanup(&builder->root);
		libias_cleanup(&builder->pool);
		libias_free(builder);
		*builder_ptr = NULL;
	}
}

struct LibiasBuffer *
ast_builder_buffer(struct ASTBuilder *this)
{
	return this->buffer;
}

const char *
ast_builder_filename(struct ASTBuilder *this)
{
	return this->filename;
}

bool
flush_comments(struct ASTBuilder *this, struct AST *parent)
{
	libias_scope_mempool(pool);

	struct LibiasArray *comments = this->comments;

	if (libias_array_len(comments) == 0) {
		return true;
	}

	struct AST *node = ast_new_comment();
	struct ASTEdit *ast_edit = ast_edit_new(
		parent,
		this->diagnostic,
		AST_EDIT_DEFAULT);
	ast_edit_append_child(ast_edit, node);
	if (!ast_edit_apply(ast_edit)) {
		return false;
	}

	{
		struct LibiasArray *tokens = this->tokens;
		this->tokens = comments;
		this->tokens = tokens;
	}

	auto edit = ast_words_edit_new(
		node,
		this->diagnostic,
		AST_WORDS_EDIT_DEFAULT);
	libias_array_foreach(comments, struct ASTBuilderToken *, t) {
		size_t len = t->len;
		if (len > 0 && libias_buffer_data(this->buffer)[t->i + len - 1] == '\n') {
			len--;
		}
		char *comment = libias_str_ndup(libias_buffer_data(this->buffer) + t->i, len);
		libias_mempool_take(pool, comment);
		bool whitespace = true;
		for (size_t i = 0; i < len; i++) {
			if (!isspace((unsigned char)comment[i])) {
				whitespace = false;
				break;
			}
		}
		struct ASTWord *word;
		if (whitespace) {
			word = ast_builder_word_whitespace_unescape(
				comment,
				strlen(comment),
				t->i);
		} else {
			word = ast_builder_word_string_unescape(
				comment,
				strlen(comment),
				t->i);
		}
		ast_words_edit_append_word(edit, word);
	}

	if (!ast_words_edit_apply(edit)) {
		return false;
	}

	libias_array_truncate(comments);

	return true;
}

void
flush_trailing_whitespace(
	struct ASTBuilder *this,
	struct ASTWordsEdit *edit,
	const size_t i)
{
	// Record trailing whitespace if any
	if (libias_buffer_len(this->only_whitespace) > 0) {
		size_t len = libias_buffer_len(this->only_whitespace);
		char *ws = libias_buffer_data(this->only_whitespace);
		struct ASTWord *word = ast_builder_word_whitespace_unescape(
			ws,
			len,
			i);
		ast_words_edit_append_word(edit, word);
		libias_buffer_truncate(this->only_whitespace);
	}
}

struct AST *
unwind_until_node(struct LibiasStack *nodestack, struct AST *parent)
{
	if (!parent) {
		return NULL;
	}

	// Unwind until we hit parent. Child nodes must now go to the parent's parent.
	struct AST *lastnode = libias_stack_peek(nodestack);
	while (1) {
		struct AST *node = libias_stack_peek(nodestack);
		if (node == NULL) {
			// the stack cannot be left empty; the last node is the
			// root node, so put it back on the stack.
			libias_stack_push(nodestack, lastnode);
			return NULL;
		} else if (node == parent) {
			libias_stack_pop(nodestack);
			return node;
		} else {
			lastnode = libias_stack_pop(nodestack);
		}
	}

	return NULL;
}

#define ERROR(loc, ...) \
	do { \
		diagnostic_parse_error(this->diagnostic, this, loc, loc, __VA_ARGS__); \
		goto error; \
	} while (0);

bool
ast_builder_process_token(
	struct ASTBuilder *this,
	struct ASTBuilderToken *t)
{
	if (!t) {
		// We are done
		return flush_comments(this, libias_stack_peek(this->nodestack));
	}

	libias_panic_if(this->finished, "AST was already built");
	libias_scope_mempool(pool);
	if (libias_stack_len(this->nodestack) == 0) {
		ERROR(t->i, "node stack exhausted");
	}

#if PARSER_ASTBUILDER_DEBUG
	char *msg = libias_str_printf("[%zu-%zu]", t->i, t->i + t->len);
	char *token = libias_str_ndup(libias_buffer_data(this->buffer) + t->i, t->len);
	fprintf(
		stderr,
		"%9s %-21s '%s'\n",
		msg,
		ASTBuilderTokenType_human(t->type),
		token);
	libias_cleanup(&msg);
	libias_cleanup(&token);
#endif

	if (t->type != AST_BUILDER_TOKEN_COMMENT) {
		if (!flush_comments(this, libias_stack_peek(this->nodestack))) {
			return false;
		}
	}

	switch (t->type) {
	case AST_BUILDER_TOKEN_CONDITIONAL_START:
	case AST_BUILDER_TOKEN_TARGET_COMMAND_START:
	case AST_BUILDER_TOKEN_TARGET_RULE_START:
	case AST_BUILDER_TOKEN_VARIABLE_START:
		libias_buffer_truncate(this->only_whitespace);
		libias_array_truncate(this->tokens);
		break;
	case AST_BUILDER_TOKEN_CONDITIONAL_EXPR:
	case AST_BUILDER_TOKEN_CONDITIONAL_POSIX:
	case AST_BUILDER_TOKEN_CONDITIONAL_TOKEN:
	case AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS:
	case AST_BUILDER_TOKEN_TARGET_COMMAND_TOKEN:
	case AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR:
	case AST_BUILDER_TOKEN_TARGET_RULE_TOKEN:
	case AST_BUILDER_TOKEN_VARIABLE_MODIFIER:
	case AST_BUILDER_TOKEN_VARIABLE_TOKEN:
		t->whitespace_before_len = libias_buffer_len(this->only_whitespace);
		t->whitespace_before = libias_mempool_take(
			this->pool,
			libias_buffer_data_disown(this->only_whitespace));
		libias_array_append(this->tokens, t);
		break;
	case AST_BUILDER_TOKEN_CONDITIONAL_END:
	{
		struct ASTBuilderToken *start = libias_array_get(this->tokens, 0);
		if (!start) {
			ERROR(t->i, "conditional has no tokens");
		}
		bool valid_token = start->type == AST_BUILDER_TOKEN_CONDITIONAL_EXPR
			|| start->type == AST_BUILDER_TOKEN_CONDITIONAL_POSIX;
		if (!valid_token) {
			ERROR(t->i, "invalid token of type '%s'", ASTBuilderTokenType_human(start->type));
		}

		const char *indent = "";
		if (start->whitespace_before) {
			indent = start->whitespace_before;
		}
		enum ASTBuilderConditionalType condtype =
			AST_BUILDER_CONDITIONAL_INVALID;
		if (!ASTBuilderConditionalType_from_bmake(
				libias_buffer_data(this->buffer) + start->i,
				start->len,
				&condtype))
		{
			condtype = AST_BUILDER_CONDITIONAL_INVALID;
		}
		if (start->type == AST_BUILDER_TOKEN_CONDITIONAL_POSIX) {
			switch (condtype) {
			case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL:
			case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL:
				condtype = AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL;
				break;
			case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S:
			case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S:
				condtype = AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S;
				break;
			case AST_BUILDER_CONDITIONAL_INCLUDE:
			case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX:
				condtype = AST_BUILDER_CONDITIONAL_INCLUDE_POSIX;
				break;
			default:
				condtype = AST_BUILDER_CONDITIONAL_INVALID;
				break;
			}
		}
		switch (condtype) {
		case AST_BUILDER_CONDITIONAL_INVALID:
			ERROR(t->i, "invalid conditional");
		case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX:
		case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL:
		case AST_BUILDER_CONDITIONAL_INCLUDE_POSIX_OPTIONAL_S:
		case AST_BUILDER_CONDITIONAL_INCLUDE:
		case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL:
		case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_D:
		case AST_BUILDER_CONDITIONAL_INCLUDE_OPTIONAL_S:
		{
			enum ASTIncludeType type;
			if (!ASTBuilderConditionalType_to_ASTIncludeType(
					condtype,
					&type))
			{
				ERROR(t->i, "cannot map %s to ASTIncludeType", ASTBuilderConditionalType_tostring(condtype));
			}
			struct AST *node = ast_new_include();
			ast_set_include_type(node, type);
			ast_set_expr_indent(node, indent);
			auto ast_edit = ast_edit_new(
				libias_stack_peek(this->nodestack),
				this->diagnostic,
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			if (!ast_edit_apply(ast_edit)) {
				return false;
			}

			struct ASTWordsEdit *edit = ast_words_edit_new(
				node,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				false);
			flush_trailing_whitespace(
				this,
				edit,
				t->i);
			if (!ast_words_edit_apply(edit)) {
				return false;
			}

			// An .include terminates the previous target per bmake behavior
			node = libias_stack_peek(this->nodestack);
			if (node && ast_target_rule_p(node)) {
				libias_stack_pop(this->nodestack);
			}

			break;
		} case AST_BUILDER_CONDITIONAL_ERROR:
		case AST_BUILDER_CONDITIONAL_EXPORT_ENV:
		case AST_BUILDER_CONDITIONAL_EXPORT_ENV_DOT:
		case AST_BUILDER_CONDITIONAL_EXPORT_LITERAL:
		case AST_BUILDER_CONDITIONAL_EXPORT:
		case AST_BUILDER_CONDITIONAL_INFO:
		case AST_BUILDER_CONDITIONAL_UNDEF:
		case AST_BUILDER_CONDITIONAL_UNEXPORT_ENV:
		case AST_BUILDER_CONDITIONAL_UNEXPORT:
		case AST_BUILDER_CONDITIONAL_WARNING:
		{
			enum ASTExprType type;
			if (!ASTBuilderConditionalType_to_ASTExprType(
					condtype,
					&type))
			{
				ERROR(t->i, "cannot map %s to ASTExprType", ASTBuilderConditionalType_tostring(condtype));
			}
			struct AST *node = ast_new_expr();
			ast_set_expr_type(node, type);
			ast_set_expr_indent(node, indent);
			auto ast_edit = ast_edit_new(
				libias_stack_peek(this->nodestack),
				this->diagnostic,
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			if (!ast_edit_apply(ast_edit)) {
				return false;
			}
			auto edit = ast_words_edit_new(
				node,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				false);
			flush_trailing_whitespace(
				this,
				edit,
				t->i);
			if (!ast_words_edit_apply(edit)) {
				return false;
			}
			break;
		}
		case AST_BUILDER_CONDITIONAL_FOR:
		{
			struct AST *node = libias_mempool_take(this->pool, ast_new_for());
			struct AST *for_bindings = ast_get_child(node, AST_CHILD_FOR_BINDINGS);
			struct AST *for_words = ast_get_child(node, AST_CHILD_FOR_WORDS);
			struct AST *for_body = ast_get_child(node, AST_CHILD_FOR_BODY);
			ast_set_expr_indent(node, indent);
			libias_stack_push(this->forstack, for_body);
			size_t word_start = 1;
			auto bindings_edit = ast_words_edit_new(
				for_bindings,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			libias_array_foreach_slice(this->tokens, 1, -1, struct ASTBuilderToken *, t) {
				if (t->len == 2 && libias_str_prefix_p(libias_buffer_data(this->buffer) + t->i, "in")) {
					word_start = t_index + 1;
					if (t->whitespace_before_len > 0) {
						struct ASTWord *ws = ast_builder_word_whitespace_unescape(
							t->whitespace_before,
							t->whitespace_before_len,
							t->i - t->whitespace_before_len);
						ast_words_edit_append_word(bindings_edit, ws);
					}
					break;
				} else {
					struct ASTWord *word = ast_builder_parse_word(
						this,
						t->len,
						t->i,
						false,
						this->diagnostic);
					if (!word) {
						return false;
					}
					if (t->whitespace_before_len > 0) {
						struct ASTWord *ws = ast_builder_word_whitespace_unescape(
							t->whitespace_before,
							t->whitespace_before_len,
							t->i - t->whitespace_before_len);
						ast_words_edit_append_word(bindings_edit, ws);
					}
					ast_words_edit_append_word(bindings_edit, word);
				}
			}
			if (!ast_words_edit_apply(bindings_edit)) {
				return false;
			}
			auto edit = ast_words_edit_new(
				for_words,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				word_start, -1,
				edit,
				0,
				false);
			flush_trailing_whitespace(
				this,
				edit,
				t->i);
			if (!ast_words_edit_apply(edit)) {
				return false;
			}
			libias_stack_push(this->nodestack, for_body);
			break;
		}
		case AST_BUILDER_CONDITIONAL_ENDFOR:
		{
			struct AST *node = unwind_until_node(this->nodestack, libias_stack_pop(this->forstack));
			if (!node || !ast_for_body_p(node)) {
				ERROR(start->i, "could not find matching .for for .endfor");
			}
			struct AST *for_node = ast_parent(node);
			if (!for_node || !ast_for_p(for_node)) {
				ERROR(start->i, "could not find matching .for for .endfor");
			}
			struct AST *for_end = ast_get_child(for_node, AST_CHILD_FOR_END);
			struct ASTWordsEdit *edit = ast_words_edit_new(
				for_end,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				false);
			flush_trailing_whitespace(
				this,
				edit,
				t->i);
			if (!ast_words_edit_apply(edit)) {
				return false;
			}
			ast_set_expr_indent(for_end, indent);
			auto ast_edit = ast_edit_new(
				libias_stack_peek(this->nodestack),
				this->diagnostic,
				AST_EDIT_DEFAULT);
			libias_mempool_forget(this->pool, for_node);
			ast_edit_append_child(ast_edit, for_node);
			if (!ast_edit_apply(ast_edit)) {
				return false;
			}
			break;
		}
		case AST_BUILDER_CONDITIONAL_IF:
		case AST_BUILDER_CONDITIONAL_IFDEF:
		case AST_BUILDER_CONDITIONAL_IFMAKE:
		case AST_BUILDER_CONDITIONAL_IFNDEF:
		case AST_BUILDER_CONDITIONAL_IFNMAKE:
		{
			enum ASTIfType type;
			if (!ASTBuilderConditionalType_to_ASTIfType(
					condtype,
					&type))
			{
				ERROR(t->i, "cannot map %s to ASTIfType", ASTBuilderConditionalType_tostring(condtype));
			}

			struct AST *ifnode = libias_mempool_take(this->pool, ast_new_if());
			libias_stack_push(this->ifstack, ifnode);
			libias_stack_push(this->nodestack, ifnode);

			struct AST *node = ast_new_if_branch();
			ast_set_if_branch_type(node, type);
			ast_set_expr_indent(node, indent);
			auto ast_edit = ast_edit_new(
				ifnode,
				this->diagnostic,
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			if (!ast_edit_apply(ast_edit)) {
				return false;
			}
			libias_stack_push(this->nodestack, node);

			auto edit = ast_words_edit_new(
				node,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				true);
			flush_trailing_whitespace(
				this,
				edit,
				t->i);
			if (!ast_words_edit_apply(edit)) {
				return false;
			}
			break;
		}
		case AST_BUILDER_CONDITIONAL_ELIF:
		case AST_BUILDER_CONDITIONAL_ELIFDEF:
		case AST_BUILDER_CONDITIONAL_ELIFNDEF:
		case AST_BUILDER_CONDITIONAL_ELIFMAKE:
		case AST_BUILDER_CONDITIONAL_ELIFNMAKE:
		case AST_BUILDER_CONDITIONAL_ELSE:
		{
			struct AST *parent = libias_stack_peek(this->ifstack);
			if (!parent) {
				ERROR(t->i, "%s without parent", ASTBuilderConditionalType_tostring(condtype));
			}
			enum ASTIfType type;
			if (!ASTBuilderConditionalType_to_ASTIfType(
					condtype,
					&type))
			{
				ERROR(t->i, "cannot map %s to ASTIfType", ASTBuilderConditionalType_tostring(condtype));
			}

			struct AST *node = ast_new_if_branch();
			ast_set_if_branch_type(node, type);
			ast_set_expr_indent(node, indent);
			auto edit = ast_words_edit_new(
				node,
				this->diagnostic,
				AST_WORDS_EDIT_DEFAULT);
			create_ast_words(
				pool,
				this,
				1, -1,
				edit,
				0,
				true);
			flush_trailing_whitespace(
				this,
				edit,
				t->i);
			if (!ast_words_edit_apply(edit)) {
				return false;
			}
			libias_stack_push(this->nodestack, node);
			auto ast_edit = ast_edit_new(
				parent,
				this->diagnostic,
				AST_EDIT_DEFAULT);
			ast_edit_append_child(ast_edit, node);
			if (!ast_edit_apply(ast_edit)) {
				return false;
			}

			switch (type) {
			case AST_IF_IF:
			case AST_IF_DEF:
			case AST_IF_MAKE:
			case AST_IF_NDEF:
			case AST_IF_NMAKE:
				if (ast_words_no_meta_len(node) == 0) {
					ERROR(t->i, ".%s with no words in test expression", ASTIfType_human(type));
				}
				break;
			case AST_IF_ELSE:
			{
				if (ast_words_no_meta_len(node) != 0) {
					ERROR(
						t->i,
						".%s with non-whitespace words in test expression: %s",
						ASTIfType_human(type),
						ast_words_no_meta_flatten(node, " "));
				}
				break;
			}
			}
			break;
		}
		case AST_BUILDER_CONDITIONAL_ENDIF:
		{
				struct AST *parentif = libias_stack_pop(this->ifstack);
				if (!parentif || !ast_if_p(parentif)) {
					ERROR(start->i, "could not find matching .if for .endif");
				}
				ast_set_expr_indent(parentif, indent);
				struct ASTWordsEdit *edit = ast_words_edit_new(
					parentif,
					this->diagnostic,
					AST_WORDS_EDIT_DEFAULT);
				create_ast_words(
					pool,
					this,
					1, -1,
					edit,
					t->i,
					false);
				flush_trailing_whitespace(
					this,
					edit,
					t->i);
				if (!ast_words_edit_apply(edit)) {
					return false;
				}
				unwind_until_node(this->nodestack, parentif);
				auto ast_edit = ast_edit_new(
					libias_stack_peek(this->nodestack),
					this->diagnostic,
					AST_EDIT_DEFAULT);
				libias_mempool_forget(this->pool, parentif);
				ast_edit_append_child(ast_edit, parentif);
				if (!ast_edit_apply(ast_edit)) {
					return false;
				}
		}
		}
		break;
	}
	case AST_BUILDER_TOKEN_TARGET_RULE_END:
	{
		// A new target terminates the previous target
		struct AST *node = libias_stack_peek(this->nodestack);
		if (node && ast_target_rule_p(node)) {
			libias_stack_pop(this->nodestack);
		}

		node = ast_new_target_rule();
		size_t dependencies_start = 0;
		struct ASTBuilderToken *operator_token = NULL;
		libias_array_foreach(this->tokens, struct ASTBuilderToken *, t) {
			if (t->type == AST_BUILDER_TOKEN_TARGET_RULE_OPERATOR) {
				dependencies_start = t_index + 1;
				operator_token = t;
				enum ASTTargetRuleOperator operator;
				if (!ASTTargetRuleOperator_from_bmake(
						libias_buffer_data(this->buffer) + t->i,
						t->len,
						&operator))
				{
					ERROR(
						t->i,
						"invalid target rule operator");
				}
				auto ast_edit = ast_edit_new(
					node,
					this->diagnostic,
					AST_EDIT_DEFAULT);
				ast_edit_set_target_rule_operator(ast_edit, operator);
				if (!ast_edit_apply(ast_edit)) {
					return false;
				}
				break;
			}
		}

		if (dependencies_start == 0) {
			ERROR(
				t->i,
				"unable to parse target rule");
		}

		struct ASTWordsEdit *edit = ast_words_edit_new(
			ast_get_child(node, AST_CHILD_TARGET_RULE_TARGETS),
			this->diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		// We shouldn't get an eol comment here yet…
		if (create_ast_words(
				pool,
				this,
				0, dependencies_start - 1,
				edit,
				0,
				false))
		{
			ERROR(
				t->i,
				"unable to parse target rule");
		}

		if (operator_token->whitespace_before_len > 0) {
			struct ASTWord *word = ast_builder_word_whitespace_unescape(
				operator_token->whitespace_before,
				operator_token->whitespace_before_len,
				operator_token->i - operator_token->whitespace_before_len);
			ast_words_edit_append_word(edit, word);
		}

		if (!ast_words_edit_apply(edit)) {
			return false;
		}

		edit = ast_words_edit_new(
			ast_get_child(node, AST_CHILD_TARGET_RULE_DEPENDENCIES),
			this->diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		create_ast_words(
			pool,
			this,
			dependencies_start, -1,
			edit,
			0,
			false);
		flush_trailing_whitespace(
			this,
			edit,
			t->i);
		if (!ast_words_edit_apply(edit)) {
			return false;
		}

		// Attach the target
		struct AST *parent = libias_stack_peek(this->nodestack);
		auto ast_edit = ast_edit_new(
			parent,
			this->diagnostic,
			AST_EDIT_DEFAULT);
		libias_mempool_forget(this->pool, node);
		ast_edit_append_child(ast_edit, node);
		if (!ast_edit_apply(ast_edit)) {
			return false;
		}
		libias_stack_push(this->nodestack, ast_get_child(node, AST_CHILD_TARGET_RULE_BODY));
		break;
	}
	case AST_BUILDER_TOKEN_TARGET_COMMAND_END:
	{
		struct AST *node = ast_new_target_command();
		auto ast_edit = ast_edit_new(
			libias_stack_peek(this->nodestack),
			this->diagnostic,
			AST_EDIT_DEFAULT);
		ast_edit_append_child(ast_edit, node);
		if (!ast_edit_apply(ast_edit)) {
			return false;
		}
		struct ASTBuilderToken *flags = libias_array_get(this->tokens, 0);
		size_t start = 0;
		if (flags && flags->type == AST_BUILDER_TOKEN_TARGET_COMMAND_FLAGS) {
			start = 1;
			char *target_command_flags = libias_str_ndup(
				libias_buffer_data(this->buffer) + flags->i,
				flags->len);
			ast_set_target_command_flags(
				node,
				target_command_flags);
			libias_free(target_command_flags);
		}
		auto edit = ast_words_edit_new(
			node,
			this->diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		create_ast_words(
			pool,
			this,
			start, -1,
			edit,
			0,
			false);
		flush_trailing_whitespace(
			this,
			edit,
			t->i);
		if (!ast_words_edit_apply(edit)) {
			return false;
		}
		break;
	}
	case AST_BUILDER_TOKEN_VARIABLE_END: {
		if (libias_array_len(this->tokens) == 0) {
			ERROR(t->i, "variable has no tokens");
		}
		struct ASTBuilderToken *varstart = libias_array_get(this->tokens, 0);
		struct ASTBuilderToken *modifier_token = libias_array_get(this->tokens, 1);
		if (!varstart ||
			!modifier_token ||
			modifier_token->type != AST_BUILDER_TOKEN_VARIABLE_MODIFIER) {
			ERROR(t->i, "unable to parse variable");
		}

		enum ASTVariableModifier modifier;
		if (!ASTVariableModifier_from_human(
				libias_buffer_data(this->buffer) + modifier_token->i,
				modifier_token->len,
				&modifier))
		{
			ERROR(modifier_token->i, "unknown variable modifier");
		}

		struct AST *node = ast_new_variable();
		{
			auto ast_edit = ast_edit_new(
				node,
				this->diagnostic,
				AST_EDIT_DEFAULT);
			ast_edit_set_variable_modifier(ast_edit, modifier);
			if (!ast_edit_apply(ast_edit)) {
				return false;
			}
		}

		auto name_edit = ast_words_edit_new(
			ast_get_child(node, AST_CHILD_VARIABLE_NAME),
			this->diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		if (varstart->whitespace_before_len > 0) {
			struct ASTWord *word = ast_builder_word_whitespace_unescape(
				varstart->whitespace_before,
				varstart->whitespace_before_len,
				varstart->i - varstart->whitespace_before_len);
			ast_words_edit_append_word(name_edit, word);
		}
		{
			struct ASTWord *word = ast_builder_parse_word(
				this,
				varstart->len,
				varstart->i,
				false,
				this->diagnostic);
			if (!word) {
				return false;
			}
			ast_words_edit_append_word(name_edit, word);
		}
		if (modifier_token->whitespace_before_len > 0) {
			struct ASTWord *word = ast_builder_word_whitespace_unescape(
				modifier_token->whitespace_before,
				modifier_token->whitespace_before_len,
				modifier_token->i - modifier_token->whitespace_before_len);
			ast_words_edit_append_word(name_edit, word);
		}
		if (!ast_words_edit_apply(name_edit)) {
			return false;
		}

		auto ast_edit = ast_edit_new(
			libias_stack_peek(this->nodestack),
			this->diagnostic,
			AST_EDIT_DEFAULT);
		ast_edit_append_child(
			ast_edit,
			node);
		if (!ast_edit_apply(ast_edit)) {
			return false;
		}
		auto edit = ast_words_edit_new(
			node,
			this->diagnostic,
			AST_WORDS_EDIT_DEFAULT);
		create_ast_words(
			pool,
			this,
			2, -1,
			edit,
			varstart->i + varstart->len,
			false);
		flush_trailing_whitespace(
			this,
			edit,
			t->i);
		if (!ast_words_edit_apply(edit)) {
			return false;
		}

		// A variable terminates the previous target per bmake behavior
		node = libias_stack_peek(this->nodestack);
		if (node && ast_target_rule_p(node)) {
			libias_stack_pop(this->nodestack);
		}

		break;
	} case AST_BUILDER_TOKEN_COMMENT:
		libias_array_append(this->comments, t);
		break;
	case AST_BUILDER_TOKEN_WS:
		libias_buffer_write(
			this->only_whitespace,
			libias_buffer_data(this->buffer) + t->i,
			1,
			t->len);
		break;
	}

	return true;

error:
	libias_cleanup(&this->root);
	return false;
}

struct AST *
ast_builder_finish(struct ASTBuilder *this)
{
	libias_panic_if(this->finished, "AST was already built");

	if (diagnostic_error_p(this->diagnostic)) {
		return NULL;
	} else if (ast_builder_tokenize_buffer(this, this->buffer, this->diagnostic)) {
		if (libias_stack_len(this->ifstack) > 0) {
			diagnostic_parse_error(
				this->diagnostic,
				this,
				libias_buffer_len(this->buffer),
				libias_buffer_len(this->buffer),
				"node stack not exhausted: missing .endif?");
			return NULL;
		} else if (libias_stack_len(this->forstack) > 0) {
			diagnostic_parse_error(
				this->diagnostic,
				this,
				libias_buffer_len(this->buffer),
				libias_buffer_len(this->buffer),
				"node stack not exhausted: missing .endfor?");
			return NULL;
		} else {
			this->finished = true;
			struct AST *root = this->root;
			this->root = NULL;
			return root;
		}
	} else if (diagnostic_error_p(this->diagnostic)) {
		return NULL;
	} else {
		diagnostic_parse_error(
			this->diagnostic,
			this,
			libias_buffer_len(this->buffer),
			libias_buffer_len(this->buffer),
			"unspecified error");
		return NULL;
	}
}

#undef ERROR
