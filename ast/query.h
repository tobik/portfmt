// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTLookupVariableBehavior {
	AST_LOOKUP_VARIABLE_DEFAULT             = 0,
	AST_LOOKUP_VARIABLE_FIRST               = 1 << 0,
	AST_LOOKUP_VARIABLE_IGNORE_CONDITIONALS = 1 << 1,
};

libias_attr_nonnull(1, 2)
struct AST *ast_lookup_target(struct AST *, const char *);

libias_attr_nonnull(1, 2)
struct AST *ast_lookup_variable(struct AST *, const char *, uint32_t);

libias_attr_nonnull(1, 2)
struct LibiasArray *ast_lookup_variables(struct AST *, const char *, uint32_t);

libias_attr_nonnull(1)
struct ASTWord *ast_lookup_masterdir(struct AST *);

libias_attr_nonnull(1)
struct ASTWord *ast_lookup_portname(struct AST *);

libias_attr_nonnull(1, 2)
bool ast_lookup_option_group_p(struct AST *, const char *);

libias_attr_nonnull(1, 2)
bool ast_lookup_option_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_option_descriptions_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_option_groups_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_options_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_uses_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_uses_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_flavor_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_flavors_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_license_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_licenses_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_subpackage_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_subpackages_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_shebang_lang_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_shebang_langs_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_post_plist_target_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_post_plist_targets_iterator(struct AST *, ssize_t, ssize_t);

libias_attr_nonnull(1, 2)
bool ast_lookup_cabal_executable_p(struct AST *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *ast_lookup_cabal_executables_iterator(struct AST *, ssize_t, ssize_t);

#define ast_lookup_option_groups_foreach(NODE, GROUP) \
	libias_iterator_foreach(ast_lookup_option_groups_iterator(NODE, 0, -1), const char *, GROUP, const char *, libias_macro_gensym(GROUP))

#define ast_lookup_option_descriptions_foreach(NODE, OPTION, DESC) \
	libias_iterator_foreach(ast_lookup_options_iterator(NODE, 0, -1), const char *, OPTION, const char *, DESC)

#define ast_lookup_options_foreach(NODE, OPTION) \
	libias_iterator_foreach(ast_lookup_options_iterator(NODE, 0, -1), const char *, OPTION, const char *, libias_macro_gensym(OPTION))

#define ast_lookup_uses_foreach(NODE, USES) \
	libias_iterator_foreach(ast_lookup_uses_iterator(NODE, 0, -1), const char *, USES, const char *, libias_macro_gensym(USES))

#define ast_lookup_flavors_foreach(NODE, FLAVOR) \
	libias_iterator_foreach(ast_lookup_flavors_iterator(NODE, 0, -1), const char *, FLAVOR, const char *, libias_macro_gensym(FLAVOR))

#define ast_lookup_licenses_foreach(NODE, LICENSE) \
	libias_iterator_foreach(ast_lookup_licenses_iterator(NODE, 0, -1), const char *, LICENSE, const char *, libias_macro_gensym(LICENSE))

#define ast_lookup_subpackages_foreach(NODE, SUBPACKAGE) \
	libias_iterator_foreach(ast_lookup_subpackages_iterator(NODE, 0, -1), const char *, SUBPACKAGE, const char *, libias_macro_gensym(SUBPACKAGE))

#define ast_lookup_shebang_langs_foreach(NODE, SHEBANG_LANG) \
	libias_iterator_foreach(ast_lookup_shebang_langs_iterator(NODE, 0, -1), const char *, SHEBANG_LANG, const char *, libias_macro_gensym(SHEBANG_LANG))

#define ast_lookup_post_plist_targets_foreach(NODE, POST_PLIST_TARGET) \
	libias_iterator_foreach(ast_lookup_post_plist_targets_iterator(NODE, 0, -1), const char *, POST_PLIST_TARGET, const char *, libias_macro_gensym(POST_PLIST_TARGET))

#define ast_lookup_cabal_executables_foreach(NODE, CABAL_EXECUTABLE) \
	libias_iterator_foreach(ast_lookup_cabal_executables_iterator(NODE, 0, -1), const char *, CABAL_EXECUTABLE, const char *, libias_macro_gensym(CABAL_EXECUTABLE))
