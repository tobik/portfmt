// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

enum ASTFormatFlags {
	AST_FORMAT_DEFAULT = 0,
	AST_FORMAT_TARGET_COMMANDS = 1 << 0,
	AST_FORMAT_UNSORTED_VARIABLES = 1 << 1,
	AST_FORMAT_PRESERVE_ADJACENT_VARIABLES = 1 << 2,
	AST_FORMAT_PRESERVE_TARGET_COMMAND_COMMENTS = 1 << 3,
	AST_FORMAT_ALWAYS_SORT_VARIABLES = 1 << 4,
};

struct ASTFormatSettings {
	uint32_t flags; // see enum ASTFormatFlags
	int32_t for_wrapcol;
	int32_t if_wrapcol;
	int32_t target_command_format_threshold;
	int32_t target_command_wrapcol;
	int32_t target_rule_wrapcol;
	int32_t variable_wrapcol;
};

libias_attr_nonnull(1)
void ast_format_init_settings(struct ASTFormatSettings *);

libias_attr_nonnull(1, 2)
void ast_format(struct AST *, struct Rules *, struct ASTFormatSettings *, struct Diagnostic *);

libias_attr_nonnull(1)
bool ast_format_category_makefile_p(struct AST *, struct Diagnostic *);
