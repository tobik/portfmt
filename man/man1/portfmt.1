.\" © Tobias Kortkamp <tobik@FreeBSD.org>
.\" SPDX-License-Identifier: BSD-2-Clause
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\"
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above
.\"    copyright notice, this list of conditions and the following
.\"    disclaimer in the documentation and/or other materials provided
.\"    with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
.\" A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
.\" HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
.\" OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
.\" WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
.\" POSSIBILITY OF SUCH DAMAGE.
.Dd December 27, 2023
.Dt PORTFMT 1
.Os
.Sh NAME
.Nm portfmt
.Nd "edit, format, and scan FreeBSD Ports Collection Makefiles"
.Sh SYNOPSIS
.Nm
.Cm bump-revision
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Nm
.Cm bump-epoch
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Nm
.Cm format
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Fl e Ar expr
.Op Ar Makefile
.Nm
.Cm get
.Ar variable-regexp
.Op Ar Makefile
.Nm
.Cm lint
.Op Fl -strict
.Op Ar Makefile
.Nm
.Cm merge
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Fl e Ar expr
.Op Ar Makefile
.Nm
.Cm sanitize-append
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Nm
.Cm scan
.Op Fl l Ar logdir
.Op Fl p Ar portsdir
.Op Fl q Ar regexp
.Op Fl -categories
.Op Fl -clones
.Op Fl -comments
.Op Fl -option-default-descriptions Ns Op Ns = Ns Ar editdist
.Op Fl -options
.Op Fl -progress Ns Op Ns = Ns Ar interval
.Op Fl -strict
.Op Fl -unknown-targets
.Op Fl -unknown-variables
.Op Fl -variable-values Ns Op Ns = Ns Ar regex
.Op Ar origin ...
.Nm
.Cm set-version
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Ar version
.Op Ar Makefile
.Sh DESCRIPTION
.Nm
is a tool for editing, formatting, and scanning
.Fx
Ports Collection Makefiles.
.Pp
If the optional
.Ar Makefile
argument is not given, the Makefile will be read from stdin.
This can be useful for editor integration where you might want to
only format portions of your Makefile.
.Pp
The following options are shared between the
.Cm bump-epoch ,
.Cm bump-revision ,
.Cm format ,
.Cm merge ,
.Cm sanitize-append ,
.Cm set-version
commands.
.Bl -tag -width indent
.It Fl D Ns Op Ar context
Output a unified diff from the original to the formatted version.
This can optionally be followed by the number of context lines.
It defaults to 3 lines of context.
For full context use 0.
.It Fl d
Dump the AST to stdout.
This is only useful for debugging purposes.
If this flag is specified
.Fl i
is ignored.
.It Fl i
Format
.Ar Makefile
in-place instead of writing the result to stdout.
.It Fl u
Leave variables unsorted.
.It Fl U
Always sort variables.
.It Fl w Ar wrapcol
Sets the wrapping column to
.Ar wrapcol
(default: 80).
This is a goal not a hard value.
It will be ignored for several variables like COMMENT, DISTFILES,
MASTER_SITES, etc.
.Pp
With a value of -1 it is ignored for all variables.
Variables with wrapped tokens over multiple lines will be concatenated
onto a single line.
.El
.Sh COMMANDS
.Bl -tag -width 2n
.It Xo
.Nm
.Cm bump-epoch
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Xc
.Pp
Same as
.Cm bump-revision
but for PORTEPOCH.
.It Xo
.Nm
.Cm bump-revision
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Xc
.Pp
Increments PORTREVISION by one.
It will be inserted if not already present in the Makefile.
If PORTREVISION is not currently in the right place it will be moved
as part of the operation.
.It Xo
.Nm
.Cm format
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Xc
.Pp
Formats
.Ar Makefile .
.It Xo
.Nm
.Cm get
.Ar variable-regexp
.Op Ar Makefile
.Xc
.Pp
Returns the raw tokens of a variable matching the extended regular
expression
.Po see
.Xr re_format 7 Pc
.Ar variable-regexp .
.It Xo
.Nm
.Cm lint
.Op Fl -strict
.Op Ar Makefile
.Xc
.Pp
Lint
. Ar Makefile .
See
.Xr portfmt-lint 1 .
.It Xo
.Nm
.Cm merge
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Fl e Ar expr
.Op Ar Makefile
.Xc
.Pp
Merges files in
.Xr make 1
syntax into
.Ar Makefile .
The input is read from stdin in the normal case, but
one or more
.Fl e
can be used to specify the input instead.
Each
.Fl e
is treated as if it would be an input line.
.Pp
This is currently limited to variables only.
Conditionals and targets are ignored.
.Pp
The
.Sy !=
modifier is overloaded to delete variables.
.Sy +=
will append to the specified variable.
No deduplication of tokens happens yet.
.Pp
Variables in the target Makefile should be in the order as suggested
by
.Xr portfmt-lint 1
for best results when inserting new variables.
.It Xo
.Nm
.Cm sanitize-append
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Op Ar Makefile
.Xc
.Pp
Sanitize
.Sy +=
in Makefiles before
.Sy bsd.port.options.mk
or
.Sy bsd.port.pre.mk .
This prevents leaking variable values from the build environment.
CXXFLAGS, CFLAGS, LDFLAGS, RUSTFLAGS will not be sanitized since
users might want to set optimization flags that affect port builds
in their environment.
.It Xo
.Nm
.Cm scan
.Op Fl l Ar logdir
.Op Fl p Ar portsdir
.Op Fl q Ar regexp
.Op Fl -categories
.Op Fl -clones
.Op Fl -comments
.Op Fl -option-default-descriptions Ns Op Ns = Ns Ar editdist
.Op Fl -options
.Op Fl -progress Ns Op Ns = Ns Ar interval
.Op Fl -strict
.Op Fl -unknown-targets
.Op Fl -unknown-variables
.Op Fl -variable-values Ns Op Ns = Ns Ar regex
.Op Ar origin ...
.Xc
.Pp
Scan the
.Fx
Ports Collection.
See
.Xr portfmt-scan 1 .
.It Xo
.Nm
.Cm set-version
.Op Fl D Ns Op Ar context
.Op Fl diuU
.Op Fl w Ar wrapcol
.Ar version
.Op Ar Makefile
.Xc
.Pp
Sets the port version.
If the port currently has PORTVERSION it will be used again for the
new version.
Otherwise it defaults to DISTVERSION.
.Pp
PORTREVISION will be removed (reset to 0).
However special care is taken for slave ports where PORTREVISION
will not be removed and will be set to 0 explicitly to prevent
inheriting the revision from the master port.
.Pp
It tries to be smart based on the format of
.Ar version .
.Pp
If
.Ar version
starts with the current value of DISTVERSIONPREFIX or ends with the
current value of DISTVERSIONSUFFIX, it will be trimmed accordingly
to set DISTVERSION or PORTVERSION correctly.
.Pp
If
.Ar version
is in the format as returned by
.Cm git describe --tags ,
DISTVERSION{PREFIX,SUFFIX} will be set accordingly.
.El
.Sh ENVIRONMENT
The following environment variables affect the execution of
.Nm :
.Bl -tag -width ".Ev CLICOLOR_FORCE"
.It Ev CLICOLOR_FORCE
If defined
.Nm
will output text with ANSI colors even when stdout is not a tty or
.Ev NO_COLOR
is set.
.It Ev NO_COLOR
If defined colors will be disabled.
.El
.Sh EXIT STATUS
Unless otherwise specified in the respective command manuals
.Nm
commands will exit with one of the following values:
.Bl -tag -width indent
.It 0
Success.
.It 1
An error occurred.
.It 2
There were changes when compared to the original file.
Only possible with
.Fl D .
.El
.Sh EDITOR INTEGRATION
You can integrate Portfmt into your editor to conveniently run it
only on parts of the port, e.g., to reformat USES after adding a
new item to it.
.Ss Emacs
Add this to
.Pa ~/.emacs.d/init.el
to format the current region with
.Sy C-c p :
.Bd -literal -offset indent
(defun portfmt (&optional b e)
  "PORTFMT(1) on region"
  (interactive "r")
  (shell-command-on-region b e "portfmt " (current-buffer) t
                           "*portfmt errors*" t))
.Ed
.Bd -literal -offset indent
(with-eval-after-load 'make-mode
  (define-key makefile-bsdmake-mode-map (kbd "C-c p") 'portfmt))
.Ed
.Ss Kakoune
Add this to
.Pa ~/.config/kak/kakrc
for filtering the current selection through
.Nm
with
.Sy ,1 :
.Bd -literal -offset indent
map global user 1 '|portfmt<ret>;' \\
	-docstring "portfmt on selection"
.Ed
.Ss Vim
Add this to
.Pa ~/.vimrc
for filtering the current selection through
.Nm
with
.Sy \e1 :
.Bd -literal -offset indent
xnoremap <leader>1 <esc>:'<,'>!portfmt<CR>
.Ed
.Sh EXAMPLES
In-place format
.Pa /usr/ports/audio/sndio/Makefile :
.Bd -literal -offset indent
portfmt -i /usr/ports/audio/sndio/Makefile
.Ed
.Pp
Append
.Sy pkgconfig
to USES and delete MASTER_SITES:
.Bd -literal -offset indent
$ printf 'USES+=pkgconfig\\nMASTER_SITES!=' | portfmt merge -i Makefile
.Ed
.Pp
Or alternatively using
.Fl e :
.Bd -literal -offset indent
$ portfmt merge -i -e 'USES+=pkgconfig' -e 'MASTER_SITES!=' Makefile
.Ed
.Pp
During maintainance of USES=cargo ports we have to regenerate
CARGO_CRATES and related variables based on output of
.Cm make cargo-crates .
We can merge the output of this command into our Makefile with
.Nm Cm merge :
.Bd -literal -offset indent
$ make cargo-crates
CARGO_CRATES=	# ...
$ make cargo-crates | portfmt merge -i Makefile
.Ed
.Pp
We have to run
.Cm make cargo-crates
twice since the first time the output might not be clean and might
contain framework messages.
.Pp
A port that is using a versioning scheme that is derived from
.Cm git describe --tags
might have
.Bd -literal -offset indent
DISTVERSIONPREFIX=	v
DISTVERSION=	0.9.2-259
DISTVERSIONSUFFIX=	-gc07c115
.Ed
.Pp
To update it we can run
.Bd -literal -offset indent
$ portfmt set-version -i v0.9.2-279-gc23f123
.Ed
.Pp
The triple will have been updated accordingly:
.Bd -literal -offset indent
DISTVERSIONPREFIX=	v
DISTVERSION=	0.9.2-279
DISTVERSIONSUFFIX=	-gc23f123
.Ed
.Sh SEE ALSO
.Xr portfmt-lint 1 ,
.Xr portfmt-scan 1
.Sh AUTHORS
.An Tobias Kortkamp Aq Mt tobik@FreeBSD.org
