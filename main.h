// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

struct PortfmtArgs;

struct Portfmt {
	struct {
		int32_t dump_ast;
		int32_t edit_in_place;
		int32_t keep_stdin_open;
		int32_t no_color;
		int32_t show_diff;
		int32_t show_help;

		int32_t always_sort_variables;
		int32_t fuzzy_variable_matching;
		int32_t keep_variables_unsorted;
		int32_t format_target_commands;
	} flags;

	struct {
		struct Regexp *regexp;
	} get_variable;

	struct {
		struct PortfmtCommand *cmd;
	} help;

	struct {
		struct LibiasBuffer *script_buffer;
	} merge;

	struct {
		struct {
			int32_t check_variable_references;
			int32_t strict_variable_references;
		} flags;
	} lint;

	struct {
		struct {
			int32_t categories;
			int32_t clones;
			int32_t comments;
			int32_t option_default_descriptions;
			int32_t options;
			int32_t progress;
			int32_t strict_variables;
			int32_t unknown_targets;
			int32_t unknown_variables;
			int32_t variable_values;
		} flags;

		struct Regexp *keyquery;
		struct Regexp *query;

		const char *portsdir;
		const char *logdir;

		uint32_t progressinterval;
		ssize_t option_default_description_editdist;

		struct LibiasArray *origins;
	} scan;

	struct {
		const char *version;
	} set_version;

	int32_t diff_context;

	const char *input_makefile_arg;

	// Available after read_arguments()
	struct LibiasMempool *pool;
	struct Diagnostic *diagnostic;
	struct Rules *rules;
	struct ASTFormatSettings *format;
	struct LibiasFile *fp_out;
	struct LibiasPath *filename;
};

enum PortfmtExitStatus {
	PORTFMT_EXIT_OK,
	PORTFMT_EXIT_HELP,
	PORTFMT_EXIT_DIFFERENCES_FOUND,
	PORTFMT_EXIT_ERROR,
	PORTFMT_EXIT_SCAN_NO_CHANGES,
};

struct PortfmtOption {
	const char short_option;
	const char *long_option;
	int32_t *flag;
	const char *help;

	bool required_arg;
	enum PortfmtExitStatus (*read_argument)(struct Portfmt *, const char *);
};

struct PortfmtCommand {
	const char *name;
	const char *help;
	const char *manual;
	enum PortfmtExitStatus (*filter_callback)(struct Portfmt *, struct AST *);
	enum PortfmtExitStatus (*main_callback)(struct Portfmt *);
	enum PortfmtExitStatus (*query_callback)(struct Portfmt *, struct AST *);
	enum PortfmtExitStatus (*read_arguments)(struct Portfmt *, struct PortfmtArgs *);
};

libias_attr_nonnull(1)
const char *args_peek(struct PortfmtArgs *);

libias_attr_nonnull(1)
const char *args_pop(struct PortfmtArgs *);

libias_attr_nonnull(1)
void args_unwind(struct PortfmtArgs *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_merge_expression(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_diff_context(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_format_variable_wrapcol(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_get_variable_regexp(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_help_topic(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_makefile_arg(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_logdir_path(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_option_default_descriptions_editdist(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_origin_arg(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_progress_interval(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_portsdir(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_variable_values(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_scan_query(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus read_set_version_arg(struct Portfmt *, const char *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_ast(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_bump_epoch(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_bump_revision(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_cat(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_format(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_get_variable(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_lint(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_merge(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_sanitize_append(struct Portfmt *, struct AST *);

libias_attr_nonnull(1, 2)
enum PortfmtExitStatus do_set_version(struct Portfmt *, struct AST *);

libias_attr_nonnull(1)
enum PortfmtExitStatus do_help(struct Portfmt *);

libias_attr_nonnull(1)
enum PortfmtExitStatus do_scan(struct Portfmt *);

libias_attr_nonnull(1)
enum PortfmtExitStatus do_scan(struct Portfmt *);

libias_attr_nonnull(1, 2, 3)
enum PortfmtExitStatus read_arguments(struct Portfmt *, const char *, struct PortfmtArgs *);

extern struct PortfmtCommand subcommands[];
extern const size_t subcommands_len;
